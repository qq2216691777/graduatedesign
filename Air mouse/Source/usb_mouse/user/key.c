/**
  ***************************************
  *@Project  Air mouse
  *@FileName key.c
  *@Auther   YNXF  
  *@date     2015-11-6
  *@Version  v1.0
  *@Action   for stm32f103  
  ***************************************
**/
#include "key.h"
#include "stm32f10x.h"                  // Device header
#include "sys.h"
#include "delay.h"

unsigned char RKD = 0;
unsigned char LKD = 0;
unsigned char CKD = 0;

void Key_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE );
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU ;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15 | GPIO_Pin_2;
	GPIO_Init( GPIOA, &GPIO_InitStructure );	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_Init( GPIOB, &GPIO_InitStructure );	
}

unsigned char Right_Key_Down( void )
{		
	if( (R_Key==0) && (RKD==0) )
	{
		delay_ms( 10 );
		if( R_Key==0 )
		{
			RKD = 1;
			return 1;
		}
	}
	return 0;
}

unsigned char Left_Key_Down( void )
{	
	if( (L_Key==0) && (LKD==0) )
	{
		delay_ms( 10 );
		if( L_Key==0 )
		{
			LKD = 1;
			return 1;
		}
	}
	return 0;
}

unsigned char Con_Key_Down( void )
{
	if( (C_Key==0) && (CKD==0) )
	{
		delay_ms( 10 );
		if( C_Key==0 )
		{
			CKD = 1;
			return 1;
		}
	}
	return 0;
}

unsigned char Right_Key_Up( void )
{
	if( (RKD==1) && (R_Key!=0) )
	{	
		RKD=0;
		return 1;	
	}
	return 0;
}

unsigned char Left_Key_Up( void )
{
	if( (LKD==1) && (L_Key!=0) )
	{	
		LKD=0;
		return 1;	
	}
	return 0;
}

unsigned char Con_Key_Up( void )
{
	if( (CKD==1) && (C_Key!=0) )
	{	
		CKD=0;
		return 1;	
	}
	return 0;
}

unsigned char Key_Value( void )
{
	
	if( Left_Key_Up() )
	{
		return 1;	
	}
	
	if( Con_Key_Up() )
	{
		return 2;
	}
	
	if( Right_Key_Up() )
	{
		return 3;
	}
	
	if( Right_Key_Down() || Con_Key_Down() || Left_Key_Down() )
	{
		return 4;
	}


	return 0;
}




