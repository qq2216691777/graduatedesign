/**
  ***************************************
  *@Project  Air mouse
  *@FileName debug.h
  *@Auther   YNXF  
  *@date     2015-11-5
  *@Version  v1.0
  *@Action   for stm32f103  
  ***************************************
**/
#ifndef _DEBUG_H__
#define _DEBUG_H__

#include "mpu6050.h"


void Printf_Serial_Data( Angle* o_d );
void LED_Init( void );


#endif


