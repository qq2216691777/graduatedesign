/**
  ***************************************
  *@Project  Air mouse
  *@FileName key.h
  *@Auther   YNXF  
  *@date     2015-11-6
  *@Version  v1.0
  *@Action   for stm32f103  
  ***************************************
**/
#ifndef _KEY_H__
#define _KEY_H__

void Key_Init( void );

unsigned char Right_Key_Down( void );

unsigned char Left_Key_Down( void );

unsigned char Con_Key_Down( void );

unsigned char Left_Key_Up( void );

unsigned char Right_Key_Up( void );

unsigned char Con_Key_Up( void );

unsigned char Key_Value( void );

#define R_Key   GPIO_ReadInputDataBit( GPIOA,GPIO_Pin_2 )
#define C_Key   GPIO_ReadInputDataBit( GPIOA,GPIO_Pin_15 )
#define L_Key   GPIO_ReadInputDataBit( GPIOB,GPIO_Pin_4 )


#endif
