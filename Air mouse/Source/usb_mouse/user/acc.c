/**
  ***************************************
  *@FileName main.c
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@Action   Air mouse usb   
  ***************************************
**/
#include "acc.h"
#include "kalman.h"

float F_Abs( float dd )
{
	if( dd > 0.001)
		return dd;
	else
		return -dd;
}

float YAnggg[3];
float YAccgg[3];
float Get_Y_Speed( float Acc, float Ang )
{
	static float YAcc_speed=0;
//	static float Last_Ang;
	
	YAnggg[0] = YAnggg[1];
	YAnggg[1] = YAnggg[2];
	YAnggg[2] = Ang;
	Ang = ( YAnggg[0] + YAnggg[1] + YAnggg[2] )/3;
	
	YAccgg[0] = YAccgg[1];
	YAccgg[1] = YAccgg[2];
	YAccgg[2] = Acc;
	Acc = ( YAccgg[0] + YAccgg[1] + YAccgg[2] )/3;
	
	
//	if( (F_Abs( Acc ) < 0.05) && ( F_Abs(Ang)<0.05 ) )
//	{
//		YAcc_speed = 0.0f;
//		return YAcc_speed;
//	}
	
	
//	Acc_speed = Acc_speed + (Acc*0.01) + ( Ang - Last_Ang );
	YAcc_speed = YAcc_speed + ( Ang*0.01 );
	

	
	return YAcc_speed;
}


float XAnggg[3];

float Get_X_Speed( float Acc, float Ang )
{
	static float XAcc_speed;
//	static float Last_Ang;
	
	XAnggg[0] = XAnggg[1];
	XAnggg[1] = XAnggg[2];
	XAnggg[2] = Ang;
	Ang = ( XAnggg[0] + XAnggg[1] + XAnggg[2] )/3;
	
	
	if( (F_Abs( Acc ) < 0.05) && ( F_Abs(Ang)<0.05 ) )
	{
		XAcc_speed = 0.0f;
		return XAcc_speed;
	}
	
	
//	Acc_speed = Acc_speed + (Acc*0.01) + ( Ang - Last_Ang );
	XAcc_speed = XAcc_speed + (Ang*0.01);
	

	
	return XAcc_speed;
}



