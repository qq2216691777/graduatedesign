/**
  ***************************************
  *@FileName main.c
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@Action   Air mouse usb   
  ***************************************
**/
#ifndef _ACC_H__
#define _ACC_H__

float Get_Y_Speed( float Acc, float Ang );

float Get_X_Speed( float Acc, float Ang );



#endif
