/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
  ***************************************
  *@FileName main.c
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@Action   Air mouse usb   
  ***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "stm32f10x_conf.h"
#include "sys.h"
#include "key.h"
#include "mpu6050.h"
#include "timer.h"

#include "usb_lib.h"
#include "hw_config.h"
#include "usb_pwr.h"

float Roll;
float Pitch;

float Last_Roll;
float Last_Pitch;

void usb_port_set(u8 enable)
{  	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	
	if(enable)_SetCNTR(_GetCNTR()&(~(1<<1)));//退出断电模式
	else
	{	  
		_SetCNTR(_GetCNTR()|(1<<1));  // 断电模式
		GPIOA->CRH&=0XFFF00FFF;
		GPIOA->CRH|=0X00033000;
		PAout(12)=0;    		  
	}
}


int main( void )
{
	u8 keysta;
	
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	Key_Init();
 	
	usb_port_set(0); 	//USB先断开
	delay_ms(300);
   	usb_port_set(1);	//USB再次连接
	//USB配置
 	USB_Interrupts_Config();    
 	Set_USBClock();   
 	USB_Init();	 
	
	MPU6050_Init();
	Handler_TIM2_Init( 10000, 72 );
	
	while(1)
	{		
		
		switch( Key_Value() )
		{
			case 0:
				
				break;
			
			case 1: 
				keysta|=0X01;					//发送鼠标左键    
				Joystick_Send(keysta,0,0,0);
				break;
			
			case 2: 
				Last_Roll = Roll;
				Last_Pitch = Pitch;
				while( Key_Value() != 4 )
				{
					Joystick_Send(keysta, (int)(( Last_Pitch - Pitch)*700), (int)(( Last_Roll - Roll)*13), 0); //发送数据到电脑  (int)(( Last_Roll - Roll)*13)
					Last_Roll = Roll;
					Last_Pitch = Pitch;
					delay_ms( 40 );
					
				}
				break;
			
			case 3: 
				keysta|=0X02;						//发送鼠标右键
				Joystick_Send(keysta,0,0,0);
				break;
			
			case 4: 
				keysta=0;
				Joystick_Send(0,0,0,0); //发送松开命令给电脑
				break;
				
			default : break;
			
		}
		

		

	}
}

