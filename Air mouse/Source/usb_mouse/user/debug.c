/**
  ***************************************
  *@Project  Air mouse
  *@FileName debug.c
  *@Auther   YNXF  
  *@date     2015-11-5
  *@Version  v1.0
  *@Action   for stm32f103  
  ***************************************
**/
#include "stm32f10x.h"                  // Device header

#include "debug.h"

void Printf_Serial_Data( Angle* o_d )
{
	u8 Buf[33];
	u32 sum;
	u8 i;

	Buf[0]=0x88;
	Buf[1]=0xaf;
	Buf[2]=0x1c;
	Buf[3]=0;			
	Buf[4]=0;//Serial 1原始数据
	Buf[5]=0;			
	Buf[6]=0;//Serial 2
	Buf[7]=0;//(short)(Angle_Z_Temp)>>8;
	Buf[8]=0;//(short)(Angle_Z_Temp)%256;//Serial 3
	
	Buf[9]=0;
	Buf[10]=0;
	Buf[11]=0;			//Serial 5	H		
	Buf[12]=0;
	Buf[13]=0;			//Serial 6	H	
	Buf[14]=0;
	
	Buf[15]=0;		//Serial 7	H	
	Buf[16]=0x0;
	Buf[17]=0x0;	//Serial 8	H	
	Buf[18]=0x0;
	Buf[19]=0x0;		//Serial 9	H	
	Buf[20]=0x0;
	Buf[21]=((short)( o_d->Roll*100 ))>>8;		//Serial 10	H		
	Buf[22]=((short)( o_d->Roll*100 ))%256;
	Buf[23]=((short)( o_d->Pitch*100 ))>>8;		//Serial 11	H	
	Buf[24]=((short)( o_d->Pitch*100 ))%256;
	Buf[25]=((short)( o_d->Yaw*10 ))>>8;
	Buf[26]=((short)( o_d->Yaw*10 ))%256;
	Buf[27]=0x0;
	Buf[28]=0x0;
	Buf[29]=0x0;
	Buf[30]=0x0;	
	sum = 0; 
	for(i=0;i<31;i++)
			sum += Buf[i];
	Buf[31]=sum;	
	for(i=0;i<32;i++)
	{

		USART_SendData(USART1,Buf[i]);//向串口1发送数据
		 while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);
	}

}

void LED_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB, ENABLE );
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOB, &GPIO_InitStructure );
	
	
}



