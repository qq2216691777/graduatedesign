/**
  ***************************************
  *@FileName Kalman.h
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@Action   Air mouse usb   
  ***************************************
**/
#ifndef _KALMAN_H__
#define _KALMAN_H__

//float Kalman_Ang_X( float Acc );
//float Kalman_Ang_Y( float Acc );
//float Kalman_Acc_X( float Acc );
//float Kalman_Acc_Y( float Acc );
float Kalman_Filter_roll(float angle_m, float gyro_m);



#endif

