/**
  ***************************************
  *@Project  Air mouse
  *@FileName imu.c
  *@Auther   YNXF  
  *@date     2015-11-5
  *@Version  v1.0
  *@Action   for stm32f103  
  ***************************************
**/
#include "imu.h"
#include "math.h"

float Q_rsqrt( float Number )
{
	long i;
	float x2, y;
	const float threehalfs = 1.5F;
 
	x2 = Number * 0.5F;
	y  = Number;
	i  = * ( long * ) &y;                      
	i  = 0x5f3759df - ( i >> 1 );               
	y  = * ( float * ) &i;
	y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration （第一次牛顿迭代）
	return y;
}

float Fl_Abs( float Number )
{
	if( Number < 0.00001 )
		return -Number;
	else
		return Number;
}

float q0 = 1, q1 = 0, q2 = 0, q3 = 0;     // quaternion elements representing the estimated orientation
float exInt = 0, eyInt = 0, ezInt = 0;    // scaled integral error

#define RtA 		57.324841	

#define Kp 1.6f                        //1.5f proportional gain governs rate of convergence to accelerometer/magnetometer 
#define Ki 0.0001f                          // 0.002f  integral gain governs rate of convergence of gyroscope biases  
#define halfT 0.005f                  // half the sample period采样周期的一半

void IMUuodate( Angle* Acc, Angle* Ang, Angle* OUT_Ang )
{
	float norm;
	float vx, vy, vz;// wx, wy, wz;
	float ex, ey, ez;
	float ax = Acc->Roll;
	float ay = Acc->Pitch;
	float az = Acc->Yaw;
	
	float gx = Ang->Roll;
	float gy = Ang->Pitch;
	float gz = Ang->Yaw;

	// 先把这些用得到的值算好
	float q0q0 = q0*q0;
	float q0q1 = q0*q1;
	float q0q2 = q0*q2;
	//  float q0q3 = q0*q3;//
	float q1q1 = q1*q1;
	//  float q1q2 = q1*q2;//
	float q1q3 = q1*q3;
	float q2q2 = q2*q2;
	float q2q3 = q2*q3;
	float q3q3 = q3*q3;
			
	norm = Q_rsqrt(ax*ax + ay*ay + az*az);       //acc数据归一化
	ax = ax *norm;
	ay = ay * norm;
	az = az * norm;

	// estimated direction of gravity and flux (v and w)              估计重力方向和流量/变迁
	vx = 2*(q1q3 - q0q2);												//四元素中xyz的表示
	vy = 2*(q0q1 + q2q3);
	vz = q0q0 - q1q1 - q2q2 + q3q3 ;

	// error is sum of cross product between reference direction of fields and direction measured by sensors
	ex = (ay*vz - az*vy) ;                           					 //向量外积在相减得到差分就是误差
	ey = (az*vx - ax*vz) ;
	ez = (ax*vy - ay*vx) ;

	exInt = exInt + ex * Ki;								  //对误差进行积分
	eyInt = eyInt + ey * Ki;
	ezInt = ezInt + ez * Ki;
	// adjusted gyroscope measurements
	if( Fl_Abs(ay) < 0.8 && Fl_Abs(az) < 0.8 )
	   gx = gx + Kp*ex + exInt;					   							//将误差PI后补偿到陀螺仪，即补偿零点漂移
	else
	   gx = gx; 
	if( Fl_Abs(ax) < 0.8 && Fl_Abs(az) < 0.8 )
	   gy = gy + Kp*ey + eyInt;				   							
	else
	   gy = gy; 
	if( Fl_Abs(ax)<0.8 && Fl_Abs(ay) < 0.8 )
	   gz = gz + Kp*ez + ezInt;					   					//这里的gz由于没有观测者进行矫正会产生漂移，表现出来的就是积分自增或自减		
	else
	   gz = gz;   							

	// integrate quaternion rate and normalise						   //四元素的微分方程
	q0 = q0 + (-q1*gx - q2*gy - q3*gz)*halfT;
	q1 = q1 + (q0*gx + q2*gz - q3*gy)*halfT;
	q2 = q2 + (q0*gy - q1*gz + q3*gx)*halfT;
	q3 = q3 + (q0*gz + q1*gy - q2*gx)*halfT;

	// normalise quaternion
	norm = Q_rsqrt(q0q0 + q1q1 + q2q2 + q3q3);
	q0 = q0 * norm;
	q1 = q1 * norm;
	q2 = q2 * norm;
	q3 = q3 * norm;

	OUT_Ang->Roll = atan2(2*q2q3 + 2*q0q1, -2*q1q1 - 2*q2q2 + 1) *RtA; // roll
	OUT_Ang->Pitch = asin(-2*q1q3 + 2*q0q2) * RtA; // pitch	
	OUT_Ang->Yaw = 0;//atan2(2 * q1 * q2 + 2 * q0 * q3, -2 * q2*q2 - 2 * q3 * q3 + 1)* RtA; // yaw
	
	//if(last_mpu_yaw-mpu_yaw>350)mpu_yaw=360+mpu_yaw;
	
}



