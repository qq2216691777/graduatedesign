/**
  ***************************************
  *@Project  Air mouse
  *@FileName imu.h
  *@Auther   YNXF  
  *@date     2015-11-5
  *@Version  v1.0
  *@Action   for stm32f103  
  ***************************************
**/
#ifndef _IMU_H__
#define _IMU_H__

#include "mpu6050.h"

float Q_rsqrt( float Number );
float Fl_Abs( float Number );
void IMUuodate( Angle* Acc, Angle* Ang, Angle* OUT_Ang );


#endif

