/**
  ***************************************
  *@FileName Kalman.c
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@Action   Air mouse usb   
  ***************************************
**/
#include "kalman.h"

//#define Q_X 0.007
//#define R_X 0.16

//float Kalman_Ang_X( float Acc )
//{
//   static float x_last;
//   float x_mid ;
//   float x_now;
//   static float p_last;
//   float p_mid ;
//   float p_now;
//   float kg;        

//   x_mid = x_last; 					//x_last=x(k-1|k-1),x_mid=x(k|k-1)
//   p_mid = p_last + Q_X; 					//p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=??
//   kg = p_mid/( p_mid+R_X); 				//kg?kalman filter,R???
//   x_now = x_mid + kg * (Acc-x_mid);		
//                
//   p_now = (1-kg) * p_mid;			
//   p_last = p_now; 
//   x_last = x_now; 
//   return x_now;         
//	
//}


//#define Q_Y 0.007
//#define R_Y 0.16

//float Kalman_Ang_Y( float Acc )
//{
//   static float y_last;
//   float x_mid ;
//   float x_now;
//   static float py_last;
//   float p_mid ;
//   float p_now;
//   float kg;        

//   x_mid = y_last; 					//x_last=x(k-1|k-1),x_mid=x(k|k-1)
//   p_mid = py_last + Q_Y; 					//p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=??
//   kg = p_mid/( p_mid+R_Y); 				//kg?kalman filter,R???
//   x_now = x_mid + kg * (Acc-x_mid);		
//                
//   p_now = (1-kg) * p_mid;			
//   py_last = p_now; 
//   y_last = x_now; 
//   return x_now;         
//	
//}

//#define QG_X 0.0055
//#define RG_X 0.18

//float Kalman_Acc_X( float Acc )
//{
//   static float xg_last;
//   float x_mid ;
//   float x_now;
//   static float pg_last;
//   float p_mid ;
//   float p_now;
//   float kg;        

//   x_mid = xg_last; 					//x_last=x(k-1|k-1),x_mid=x(k|k-1)
//   p_mid = pg_last + QG_X; 					//p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=??
//   kg = p_mid/( p_mid+RG_X); 				//kg?kalman filter,R???
//   x_now = x_mid + kg * (Acc-x_mid);		
//                
//   p_now = (1-kg) * p_mid;			
//   pg_last = p_now; 
//   xg_last = x_now; 
//   return x_now;         
//	
//}


//#define QG_Y 0.007
//#define RG_Y 0.16

//float Kalman_Acc_Y( float Acc )
//{
//   static float yg_last;
//   float x_mid ;
//   float x_now;
//   static float pgy_last;
//   float p_mid ;
//   float p_now;
//   float kg;        

//   x_mid = yg_last; 					//x_last=x(k-1|k-1),x_mid=x(k|k-1)
//   p_mid = pgy_last + QG_Y; 					//p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=??
//   kg = p_mid/( p_mid+RG_Y); 				//kg?kalman filter,R???
//   x_now = x_mid + kg * (Acc-x_mid);		
//                
//   p_now = (1-kg) * p_mid;			
//   pgy_last = p_now; 
//   yg_last = x_now; 
//	
//   return x_now;         
//	
//}



//卡尔曼滤波参数与函数
float dt_roll=0.010;//注意：dt的取值为kalman滤波器采样时间
float angle_kalmen_roll, angle_dot_roll;//角度和角速度
float P_roll[2][2] = {{ 1, 0 },
              { 0, 1 }};
float Pdot_roll[4] ={ 0,0,0,0};
float Q_angle_roll=0.001, Q_gyro_roll=0.005; //角度数据置信度,角速度数据置信度
float R_angle_roll=0.5 ,C_0_roll = 1; 

float q_bias_roll, angle_err_roll, PCt_0_roll, PCt_1_roll, E_roll, K_0_roll, K_1_roll, t_0_roll, t_1_roll;
float Kalman_Filter_roll(float angle_m, float gyro_m)//angleAx 和 gyroGy
{
		angle_kalmen_roll += (gyro_m-q_bias_roll) * dt_roll;
		angle_err_roll = angle_m - angle_kalmen_roll;
		Pdot_roll[0]=Q_angle_roll - P_roll[0][1] - P_roll[1][0];
		Pdot_roll[1]=-P_roll[1][1];
		Pdot_roll[2]=-P_roll[1][1];
		Pdot_roll[3]=Q_gyro_roll;
		P_roll[0][0] += Pdot_roll[0] * dt_roll;
		P_roll[0][1] += Pdot_roll[1] * dt_roll;
		P_roll[1][0] += Pdot_roll[2] * dt_roll;
		P_roll[1][1] += Pdot_roll[3] * dt_roll;
		PCt_0_roll = C_0_roll * P_roll[0][0];
		PCt_1_roll = C_0_roll * P_roll[1][0];
		E_roll = R_angle_roll + C_0_roll * PCt_0_roll;
		K_0_roll = PCt_0_roll / E_roll;
		K_1_roll = PCt_1_roll / E_roll;
		t_0_roll = PCt_0_roll;
		t_1_roll = C_0_roll * P_roll[0][1];
		P_roll[0][0] -= K_0_roll * t_0_roll;
		P_roll[0][1] -= K_0_roll * t_1_roll;
		P_roll[1][0] -= K_1_roll * t_0_roll;
		P_roll[1][1] -= K_1_roll * t_1_roll;
		angle_kalmen_roll += K_0_roll * angle_err_roll; //最优角度
		q_bias_roll += K_1_roll * angle_err_roll;
		angle_dot_roll = gyro_m-q_bias_roll;//最优角速度
		return angle_kalmen_roll;
}


