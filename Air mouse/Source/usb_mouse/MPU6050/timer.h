/**
  ***************************************
  *@Project  Air mouse
  *@FileName timer.h
  *@Auther   YNXF  
  *@date     2015-11-5
  *@Version  v1.0
  *@Action   for stm32f103  
  ***************************************
**/
#ifndef _TIMER_H__
#define _TIMER_H__

void Handler_TIM2_Init( unsigned short arr, unsigned short psc );
	

#endif
