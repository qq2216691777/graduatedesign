/**
  ***************************************
  *@Project  Air mouse
  *@FileName iic_sim.h
  *@Auther   YNXF  
  *@date     2015-11-4
  *@Version  v1.0
  *@Action   for stm32f103  
  ***************************************
**/

#ifndef _IIC_SIM_H__
#define _IIC_SIM_H__

#define SDA_IN()  	{GPIOC->CRH&=0XF0FFFF0F;GPIOC->CRH|=0x08000000;}
#define SDA_OUT()	{GPIOC->CRH&=0XF0FFFF0F;GPIOC->CRH|=0x03000000;}

#define IIC_SCL 	PCout(13)
#define IIC_SDA 	PCout(14)
#define READ_SDA 	PCin(14)

void IIC_SIM_Init( void );
void IIC_SIM_Start( void );
void IIC_SIM_Write_Byte( unsigned char txd );
unsigned char IIC_SIM_Read_Byte( void );
void IIC_SIM_Send_Ack( unsigned char Ack );
unsigned char IIC_SIM_Wait_Ack( void );
void IIC_SIM_Stop( void );
void IIC_SIM_Delay( unsigned char d );


#endif

