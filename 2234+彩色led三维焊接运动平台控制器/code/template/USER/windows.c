/**
  ***************************************
  *@File     windows.c
  *@Auther   YNXF  
  *@date     2017-05-10
  *@Version  v1.0
  *@brief    管理LCD显示，创建窗口、绘制波形等     
  *@HAUST - RobotLab
  ***************************************
**/
#include "windows.h"
#include "button.h"
#include "slider.h"
#include "text.h"
#include "ucos_ii.h"
#include <stdio.h>
#include "motor.h"

unsigned char menuUpdate = 0;

GUI_HWIN WndLineChart;  //波形图窗口句柄
GUI_HWIN WndMenu;  //菜单窗口句柄
GUI_HWIN WndSetZone; 	//设置安全区间
GUI_HWIN WndSetSpeed;		//设置速度句柄
GUI_HWIN WndHandle;		//手动句柄

BUTTON_Handle BtSetSafeZone;		//设置安全区间按钮
BUTTON_Handle BtSetSpeed;	//设置速度按钮
BUTTON_Handle BtSetZero;	//设置零点
BUTTON_Handle BtEmerStop;		//急停按钮
BUTTON_Handle BtPause;		//暂停按钮
BUTTON_Handle BtHandle;		//手动按钮
BUTTON_Handle BtAuto;		//自动按钮
BUTTON_Handle BtHome;			//回领按钮


//画一条波形函数
void DrawLine( short *X )
{
	unsigned char i;
	unsigned char pos=0;
	int x0,x1;
	GUI_DrawHLine(X[pos]*2/5+75,25,45);		//画起始位置
	pos++;
	for(i=45;i<210;i+=20)
	{
		  x0 = X[pos-1]*2/5+75;
			x1 = X[pos]*2/5+75;
			if(x1>x0)
				GUI_DrawVLine(i,x0,x1);
			else	
				GUI_DrawVLine(i,x1,x0);	
			GUI_DrawHLine(X[pos]*2/5+75,i,i+20);
			pos++;
		
	}
	

}
/*
	波形图窗口回调函数
*/
int Wavetime=0;
short  WaveLineX[10]={0,0,0,0,0,0,0,0,0,0}; 
short  WaveLineY[10]={0,0,0,0,0,0,0,0,0,0}; 
short  WaveLineZ[10]={0,0,0,0,0,0,0,0,0,0}; 
	
static void cbLineChartWin( WM_MESSAGE *pMsg )
{
	static TEXT_Handle xPos1;
	static TEXT_Handle xPos2;
	static TEXT_Handle xPos3;
	static TEXT_Handle xPos4;
	static TEXT_Handle xPos5;
	static TEXT_Handle xPos6;
	static TEXT_Handle xPos7;
	static TEXT_Handle xPos8;
	static TEXT_Handle xPos9;
	static TEXT_Handle xPos10;
	static TEXT_Handle xPos11;

	char DispData[5] = {0};
	switch(pMsg->MsgId)
	{
		int myPos;
			case WM_PAINT:
				GUI_SetBkColor(GUI_LIGHTGRAY);		//设置背景为浅灰色
			  GUI_Clear();
			
				
				GUI_SetColor( GUI_BLACK );	
				GUI_DrawLine(25,125,240,125);		//画X轴
				GUI_DrawLine(25,20,25,125);		//Y轴
			  GUI_DrawLine(22,25,25,20);
			  GUI_DrawLine(28,25,25,20);
			
				GUI_DrawLine(65,124,65,122);		 //x坐标
				GUI_DrawLine(105,124,105,122);
				GUI_DrawLine(145,124,145,122);
				GUI_DrawLine(185,124,185,122);
				GUI_DrawLine(225,124,225,122);

				GUI_DrawLine(26,115,27,115);		//y轴
				GUI_DrawLine(26,95,27,95);
				GUI_DrawLine(26,75,27,75);
				GUI_DrawLine(26,55,27,55);
				GUI_DrawLine(26,35,27,35);

				GUI_SetTextMode(GUI_TEXTMODE_TRANS);
				GUI_SetFont(&GUI_Font6x8);
				GUI_DispStringAt("-100",2,113);		//Y轴数值
				GUI_DispStringAt("-50",2,93);
				GUI_DispCharAt('0',6,73);
				GUI_DispStringAt("50",6,53);
				GUI_DispStringAt("100",6,33);

				
				//画红  蓝  黄三个指示线
				GUI_SetFont(&GUI_Font8x9);
				GUI_SetColor( GUI_RED );
				GUI_DispCharAt('X',30,5);
				GUI_DrawLine(45,7,80,7);
				DrawLine(WaveLineX);
				
				GUI_SetColor( GUI_BLUE );
				GUI_DispCharAt('Y',90,5);
				GUI_DrawLine(105,7,140,7);
				DrawLine(WaveLineY);
				
				GUI_SetColor( GUI_YELLOW );
				GUI_DispCharAt('Z',150,5);
				GUI_DrawLine(165,7,200,7);
				DrawLine(WaveLineZ);
				
				
				break;
				
			case WM_CREATE:
				//创建x轴上的文本框
				sprintf(DispData,"%d",Wavetime);
				xPos1 = TEXT_Create(23,305,60,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				sprintf(DispData,"%d",Wavetime+1);
				xPos2 = TEXT_Create(43,305,60,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				sprintf(DispData,"%d",Wavetime+2);
				xPos3 = TEXT_Create(63,305,60,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				sprintf(DispData,"%d",Wavetime+3);
				xPos4 = TEXT_Create(83,305,60,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				sprintf(DispData,"%d",Wavetime+4);
				xPos5 = TEXT_Create(103,305,60,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				sprintf(DispData,"%d",Wavetime+5);
				xPos6 = TEXT_Create(123,305,60,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				sprintf(DispData,"%d",Wavetime+6);
				xPos7 = TEXT_Create(143,305,60,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				sprintf(DispData,"%d",Wavetime+7);
				xPos8 = TEXT_Create(163,305,60,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				sprintf(DispData,"%d",Wavetime+8);
				xPos9 = TEXT_Create(183,305,60,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				sprintf(DispData,"%d",Wavetime+9);
				xPos10 = TEXT_Create(203,305,60,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				sprintf(DispData,"%d",Wavetime+10);
				xPos11 = TEXT_Create(223,305,60,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				break;
				
			case WM_UPDATE:
				//更新x轴上的坐标
				sprintf(DispData,"%d",Wavetime);
				TEXT_SetText(xPos1,DispData);
				sprintf(DispData,"%d",Wavetime+1);
				TEXT_SetText(xPos2,DispData);
				sprintf(DispData,"%d",Wavetime+2);
				TEXT_SetText(xPos3,DispData);
				sprintf(DispData,"%d",Wavetime+3);
				TEXT_SetText(xPos4,DispData);				sprintf(DispData,"%d",Wavetime+4);
				TEXT_SetText(xPos5,DispData);				sprintf(DispData,"%d",Wavetime+5);
				TEXT_SetText(xPos6,DispData);				sprintf(DispData,"%d",Wavetime+6);
				TEXT_SetText(xPos7,DispData);				sprintf(DispData,"%d",Wavetime+7);
				TEXT_SetText(xPos8,DispData);				sprintf(DispData,"%d",Wavetime+8);
				TEXT_SetText(xPos9,DispData);				sprintf(DispData,"%d",Wavetime+9);
				TEXT_SetText(xPos10,DispData);				sprintf(DispData,"%d",Wavetime+10);		
				TEXT_SetText(xPos11,DispData);		
				//清除上次画的3条波形
				GUI_SetColor(GUI_LIGHTGRAY);
				DrawLine(WaveLineX);
				DrawLine(WaveLineY);
				DrawLine(WaveLineZ);
				//追加新的图形
				for(myPos=0;myPos<9;myPos++)
				{
					WaveLineX[myPos] = WaveLineX[myPos+1];
					WaveLineY[myPos] = WaveLineY[myPos+1];
					WaveLineZ[myPos] = WaveLineZ[myPos+1];
				}
				WaveLineX[myPos] = 0-m.nowSpeed.x;
				WaveLineY[myPos] = 0-m.nowSpeed.y;
				WaveLineZ[myPos] = 0-m.nowSpeed.z;
				//更新波形
				GUI_SetColor( GUI_RED );
				DrawLine(WaveLineX);

				GUI_SetColor( GUI_BLUE );
				DrawLine(WaveLineY);
				GUI_SetColor( GUI_YELLOW );
			  DrawLine(WaveLineZ);	
				

				break;
			default: WM_DefaultProc(pMsg);
	}
}
int xM=0;
int yM=0;
int zM=0;


/*
	菜单窗口回调函数
*/
static TEXT_Handle xRelDis;
static TEXT_Handle yRelDis;
static TEXT_Handle zRelDis;
static void cbMenu( WM_MESSAGE *pMsg )
{
	static TEXT_Handle xDis;
	static TEXT_Handle yDis;
	static TEXT_Handle zDis;
	
	static TEXT_Handle ZoneMax;
	static TEXT_Handle ZoneMix;
	

	
	char DispData[10]={0};
	switch(pMsg->MsgId)
	{
			
			case WM_PAINT:
				GUI_SetBkColor(GUI_WHITE);
			  GUI_Clear();
				GUI_SetColor(GUI_BLACK);
			  GUI_DispStringAt("Safe Zone",10,10);
			
				GUI_DispStringAt("Speed",20,95);
			
			//	GUI_DispStringAt("SET Zero",120,150);
			  GUI_DispStringAt("System Running",110,20);
			
	
			  GUI_SetTextMode(GUI_TEXTMODE_TRANS);
				GUI_SetFont(&GUI_Font6x8);
			
				GUI_DispStringAt("X       Y       Z",105,40);
			

				sprintf(DispData,"Max:%d",m.SafeZoneMax);
				TEXT_SetText(ZoneMax,DispData);
				sprintf(DispData,"Mix:%d",m.SafeZoneMin);
				TEXT_SetText(ZoneMix,DispData);

				sprintf(DispData,"X:%d",xM);
				TEXT_SetText(xDis,DispData);
				sprintf(DispData,"Y:%d",yM);
				TEXT_SetText(yDis,DispData);
				sprintf(DispData,"Z:%d",zM);
				TEXT_SetText(zDis,DispData);
				
				
				menuUpdate = 1;				
				break;
			
			case WM_CREATE:
				//创建窗口上的按钮
				BtSetSafeZone = BUTTON_Create(12,60,60,25,GUI_ID_SETZONE,WM_CF_SHOW);
				BUTTON_SetText(BtSetSafeZone,"SET ZONE");
				
				BtSetSpeed = BUTTON_Create(12,150,60,25,GUI_ID_SETSPEED,WM_CF_SHOW);
				BUTTON_SetText(BtSetSpeed,"SET SPEED");
				
				
			
				BtEmerStop = BUTTON_Create(100,70,60,25,GUI_ID_EMERSTOP,WM_CF_SHOW);
				BUTTON_SetText(BtEmerStop,"emer stop");
			
				BtPause = BUTTON_Create(170,70,60,25,GUI_ID_PAUSE,WM_CF_SHOW);
				BUTTON_SetText(BtPause,"pause");
			
				BtHandle = BUTTON_Create(100,110,60,25,GUI_ID_HANDLE,WM_CF_SHOW);
				BUTTON_SetText(BtHandle,"handle");
			
				BtAuto = BUTTON_Create(170,110,60,25,GUI_ID_AUTO,WM_CF_SHOW);
				BUTTON_SetText(BtAuto,"Auto");
			
				BtSetZero = BUTTON_Create(100,150,60,25,GUI_ID_SETZERO,WM_CF_SHOW);
				BUTTON_SetText(BtSetZero,"SET ZERO");
				BtHome = BUTTON_Create(170,150,60,25,GUI_ID_HOME,WM_CF_SHOW);
				BUTTON_SetText(BtHome,"Home");
			
				sprintf(DispData,"X:%d",xM);
				xDis = TEXT_Create(20,115,30,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				sprintf(DispData,"Y:%d",yM);
				yDis = TEXT_Create(20,125,30,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				sprintf(DispData,"Z:%d",zM);
				zDis = TEXT_Create(20,135,30,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				
				sprintf(DispData,"Mix:%d",m.SafeZoneMin);
				ZoneMix = TEXT_Create(20,25,60,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				sprintf(DispData,"Max:%d",m.SafeZoneMax);
				ZoneMax = TEXT_Create(20,40,60,30,0,WM_CF_SHOW,DispData,TEXT_CF_LEFT);
				
				sprintf(DispData,"%d",(int)m.rel.x);
				xRelDis = TEXT_Create(80,50,60,20,0,WM_CF_SHOW,DispData,TEXT_CF_HCENTER);
				sprintf(DispData,"%d",(int)m.rel.y);
				yRelDis = TEXT_Create(127,50,60,20,0,WM_CF_SHOW,DispData,TEXT_CF_HCENTER);
				sprintf(DispData,"%d",(int)m.rel.z);
				zRelDis = TEXT_Create(175,50,60,20,0,WM_CF_SHOW,DispData,TEXT_CF_HCENTER);
				menuUpdate = 1;
				
		/*		yDis = TEXT_Create(20,125,30,30,0,WM_CF_SHOW,"Y:000",TEXT_CF_LEFT);
				zDis = TEXT_Create(20,135,30,30,0,WM_CF_SHOW,"Z:000",TEXT_CF_LEFT);*/
				break;
				
			case WM_HIDE:
				menuUpdate = 0;
				break;
			default: WM_DefaultProc(pMsg);
	}
}

/*
	//设置安全区间回调函数
*/
unsigned char lightValue = 80;
static SLIDER_Handle BackLight;		//背光
static void cbSetZone( WM_MESSAGE *pMsg )
{
	static BUTTON_Handle Btquit;
	static BUTTON_Handle BtAddMin;
	static BUTTON_Handle BtAddMax;
	static BUTTON_Handle BtSubMin;
	static BUTTON_Handle BtSubMax;
	static TEXT_Handle myZoneMax;
	static TEXT_Handle myZoneMix;
	
	
	
	char  DispData[18] = {0};
		
	switch(pMsg->MsgId)
	{
			case WM_PAINT:
				GUI_SetBkColor(GUI_GRAY);
				GUI_Clear();
				GUI_SetColor(GUI_BLACK);
				GUI_DispStringAt("Safe Zone",10,10);
			
				
				WM_ShowWindow(Btquit);
				WM_ShowWindow(BtAddMin);
				WM_ShowWindow(BtSubMin);
				WM_ShowWindow(BtAddMax);
				WM_ShowWindow(BtSubMax);
			
				WM_ShowWindow(myZoneMax);
				WM_ShowWindow(myZoneMix);
				WM_ShowWindow(BackLight);
				GUI_SetTextMode(GUI_TEXTMODE_TRANS);
				GUI_SetFont(&GUI_Font8x9);
			
/*  			GUI_DispStringAt("Min: 00.00",20,45);
				GUI_DispStringAt("Max: 00.00",20,105);		*/	
			GUI_DispStringAt("Light",105,100);
			
				break;
			
			case WM_CREATE:
				//创建按钮
				GUI_SetColor(GUI_BLACK);
				Btquit = BUTTON_Create(200,70,15,15,GUI_ID_HIDEZONE,WM_CF_HIDE);
				BUTTON_SetText(Btquit,"X");
			
				BtAddMin = BUTTON_Create(25,120,15,15,GUI_ID_ADDMIN,WM_CF_HIDE);
				BUTTON_SetText(BtAddMin,"+");
			
				BtSubMin = BUTTON_Create(50,120,15,15,GUI_ID_SUBMIN,WM_CF_HIDE);
				BUTTON_SetText(BtSubMin,"-");
			
			  BtAddMax = BUTTON_Create(25,180,15,15,GUI_ID_ADDMAX,WM_CF_HIDE);
				BUTTON_SetText(BtAddMax,"+");
			
				BtSubMax = BUTTON_Create(50,180,15,15,GUI_ID_SUBMAX,WM_CF_HIDE);
				BUTTON_SetText(BtSubMax,"-");
			
				sprintf(DispData,"Max:%d",(int)m.SafeZoneMax);
				myZoneMax = TEXT_Create(20,165,60,20,0,WM_CF_HIDE,DispData,TEXT_CF_HCENTER);
				sprintf(DispData,"Min:%d",(int)m.SafeZoneMin);
				myZoneMix = TEXT_Create(20,105,60,20,0,WM_CF_HIDE,DispData,TEXT_CF_HCENTER);
				
				BackLight = SLIDER_Create(105,175,130,20,WndSetZone,5,WM_CF_HIDE,0);
				SLIDER_SetRange(BackLight,5,100);
				SLIDER_SetValue(BackLight,lightValue);
				break;
			
			case WM_HIDE:
				WM_HideWindow(Btquit);
				WM_HideWindow(BtAddMin);
				WM_HideWindow(BtSubMin);
				WM_HideWindow(BtAddMax);
				WM_HideWindow(BtSubMax);
				WM_HideWindow(myZoneMax);
				WM_HideWindow(myZoneMix);
			
				WM_HideWindow(BackLight);
				
				break;
			
			case WM_UPDATE:
				sprintf(DispData,"Max:%d",(int)m.SafeZoneMax);
				TEXT_SetText(myZoneMax,DispData);
				sprintf(DispData,"Min:%d",(int)m.SafeZoneMin);
				TEXT_SetText(myZoneMix,DispData);
				break;
		
			default: 
				WM_DefaultProc(pMsg);
			
	}
}

/*设置速度*/
static SLIDER_Handle xValue;
static SLIDER_Handle yValue;
static SLIDER_Handle zValue;


static void cbSetSpeed( WM_MESSAGE *pMsg )
{
	static BUTTON_Handle Btquit;
	static TEXT_Handle xDis;
	static TEXT_Handle yDis;
	static TEXT_Handle zDis;
	char ddata[6]={0};
	
	switch(pMsg->MsgId)
	{
			case WM_PAINT:
				GUI_SetBkColor(GUI_GRAY);
				GUI_Clear();
				GUI_SetColor(GUI_BLACK);
				GUI_DispStringAt("Safe Speed",10,10);
			
				WM_ShowWindow(Btquit);
			
				WM_ShowWindow(xValue);
				WM_ShowWindow(yValue);
				WM_ShowWindow(zValue);
			
				WM_ShowWindow(xDis);
				WM_ShowWindow(yDis);
				WM_ShowWindow(zDis);
			
				GUI_SetTextMode(GUI_TEXTMODE_TRANS);
				GUI_SetFont(&GUI_Font6x8);
			
  			//GUI_DispStringAt("X:",40,65);
				//GUI_DispDec(xM,3);
				//GUI_DispStringAt("Y:",40,95);	
				//GUI_DispDec(yM,3);
				//GUI_DispStringAt("Z:",40,125);	
				//GUI_DispDec(zM,3);
			
				break;
			
			case WM_CREATE:
				GUI_SetColor(GUI_BLACK);
				Btquit = BUTTON_Create(200,70,15,15,GUI_ID_HIDEZONE,WM_CF_HIDE);
				BUTTON_SetText(Btquit,"X");
				xValue = SLIDER_Create(75,115,130,20,WndSetSpeed,5,WM_CF_HIDE,0);
				yValue = SLIDER_Create(75,145,130,20,WndSetSpeed,5,WM_CF_HIDE,0);
				zValue = SLIDER_Create(75,175,130,20,WndSetSpeed,5,WM_CF_HIDE,0);
			
				xM = 111-m.MotorSpeed.x;
				yM = 111-m.MotorSpeed.y;
				zM = 111-m.MotorSpeed.z;
				sprintf(ddata,"X:%d",xM);
				xDis = TEXT_Create(43,120,30,30,0,WM_CF_HIDE,ddata,TEXT_CF_LEFT);
				sprintf(ddata,"Y:%d",zM);
				yDis = TEXT_Create(43,150,30,30,0,WM_CF_HIDE,ddata,TEXT_CF_LEFT);
				sprintf(ddata,"Z:%d",yM);
				zDis = TEXT_Create(43,180,30,30,0,WM_CF_HIDE,ddata,TEXT_CF_LEFT);
				/*
				xDis = TEXT_Create(43,120,30,30,0,WM_CF_HIDE,"X:000",TEXT_CF_LEFT);
				yDis = TEXT_Create(43,150,30,30,0,WM_CF_HIDE,"Y:000",TEXT_CF_LEFT);
				zDis = TEXT_Create(43,180,30,30,0,WM_CF_HIDE,"Z:000",TEXT_CF_LEFT);*/
				SLIDER_SetRange(xValue,1,100);
				SLIDER_SetRange(yValue,1,100);
				SLIDER_SetRange(zValue,1,100);
				SLIDER_SetValue(xValue,xM);
				SLIDER_SetValue(yValue,yM);
				SLIDER_SetValue(zValue,zM);
				break;
			
			case WM_UPDATE:
				sprintf(ddata,"X:%d",xM);
				TEXT_SetText(xDis,ddata);
				sprintf(ddata,"Y:%d",yM);
				TEXT_SetText(yDis,ddata);
				sprintf(ddata,"Z:%d",zM);
				TEXT_SetText(zDis,ddata);
				
				m.MotorSpeed.x = 110-xM;
				m.MotorSpeed.y = 110-yM;
				m.MotorSpeed.z = 110-zM;
		
				break;
			
			case WM_HIDE:
				WM_HideWindow(Btquit);
				WM_HideWindow(xValue);
				WM_HideWindow(yValue);
				WM_HideWindow(zValue);
			
				WM_HideWindow(xDis);
				WM_HideWindow(yDis);
				WM_HideWindow(zDis);
				
				break;
			
			default: 
				WM_DefaultProc(pMsg);
			
	}
}
/* 点动 */
static void cbHandle( WM_MESSAGE *pMsg )
{
	static BUTTON_Handle Btquit;
	static BUTTON_Handle BtAddX;
	static BUTTON_Handle BtSubX;
	
	static BUTTON_Handle BtAddY;
	static BUTTON_Handle BtSubY;
	
	static BUTTON_Handle BtAddZ;
	static BUTTON_Handle BtSubZ;
	
	switch(pMsg->MsgId)
	{
			case WM_PAINT:
				GUI_SetBkColor(GUI_GRAY);
				GUI_Clear();
				GUI_SetColor(GUI_BLACK);
			
				GUI_SetColor(GUI_BLACK);
			  GUI_DispStringAt("Handle Control",10,10);
				GUI_DispCharAt('X',123,50);
				GUI_DispCharAt('Y',123,90);
				GUI_DispCharAt('Z',123,130);
			
				WM_ShowWindow(BtAddX);
				WM_ShowWindow(BtSubX);
				WM_ShowWindow(BtAddY);
				WM_ShowWindow(BtSubY);
				WM_ShowWindow(BtAddZ);
				WM_ShowWindow(BtSubZ);
				WM_ShowWindow(Btquit);
				break;
			
			case WM_CREATE:
				Btquit = BUTTON_Create(200,70,15,15,GUI_ID_HIDEZONE,WM_CF_HIDE);
				BUTTON_SetText(Btquit,"X");
			
			
				BtAddX = BUTTON_Create(80,110,15,15,GUI_ID_ADDX,WM_CF_HIDE);
				BUTTON_SetText(BtAddX,"+");
				BtSubX = BUTTON_Create(160,110,15,15,GUI_ID_SUBX,WM_CF_HIDE);
				BUTTON_SetText(BtSubX,"-");
			
				BtAddY = BUTTON_Create(80,150,15,15,GUI_ID_ADDY,WM_CF_HIDE);
				BUTTON_SetText(BtAddY,"+");
				BtSubY = BUTTON_Create(160,150,15,15,GUI_ID_SUBY,WM_CF_HIDE);
				BUTTON_SetText(BtSubY,"-");
			
				BtAddZ = BUTTON_Create(80,190,15,15,GUI_ID_ADDZ,WM_CF_HIDE);
				BUTTON_SetText(BtAddZ,"+");
				BtSubZ = BUTTON_Create(160,190,15,15,GUI_ID_SUBZ,WM_CF_HIDE);
				BUTTON_SetText(BtSubZ,"-");
			
			
				break;
				
			
			case WM_HIDE:
				WM_HideWindow(Btquit);
				WM_HideWindow(BtAddX);
				WM_HideWindow(BtSubX);
				WM_HideWindow(BtAddY);
				WM_HideWindow(BtSubY);
				WM_HideWindow(BtAddZ);
				WM_HideWindow(BtSubZ);
				break;
			
			default: 
				WM_DefaultProc(pMsg);
			
	}
}

/*
* 窗口初始化函数  在创建窗口后，gui会自动调用该窗口的回调函数 
* 并传入WM_CREATE参数  如果属性为显示  则还会传入WM_PAINT
*/
void WindowsInit( void )
{
	//创建波形图窗口  属性设置为显示 WM_CF_SHOW
	WndLineChart = WM_CreateWindow(0,180,240,320,WM_CF_SHOW,cbLineChartWin,0);
	/* 创建菜单窗口 属性设置为显示*/
	WndMenu = WM_CreateWindow(0,0,240,180,WM_CF_SHOW,cbMenu,0);
	/* 创建设置零点窗口 属性设置为隐藏 WM_CF_HIDE */
	WndSetZone = WM_CreateWindow(0,60,240,170,WM_CF_HIDE,cbSetZone,0);
	/* 创建设置速度窗口  属性设置为隐藏 */
	WndSetSpeed = WM_CreateWindow(0,60,240,170,WM_CF_HIDE,cbSetSpeed,0);
	/* 创建手动窗口  属性设置为隐藏 */
	WndHandle = WM_CreateWindow(0,60,240,170,WM_CF_HIDE,cbHandle,0);
}

/*
*	 gui函数，在gui中最先执行
*/
void GUI_Work()
{
	int getIDvalue=0X11;
	int timerflag = 0;
	unsigned char Mystatus = 0;
	int myPos;
	char AutoFlag=0;
	
	static char pausetemp=0;
	
	static char handleax=0;
	static char handlesx=0;
	static char handleay=0;
	static char handlesy=0;
	static char handleaz=0;
	static char handlesz=0;
	int autoTime = 0;
	char DispData[12];
	
	/*
	*  画面初始化
	*/
	WindowsInit();
	
	while(1)
	{
		getIDvalue = GUI_GetKey();		//判断哪个按钮被按下
		if( getIDvalue == GUI_ID_EMERSTOP)			
		{
				m.emergyPause = 0x80;			//如果是急停按钮被按下  则将m.emergyPause最高位或运算  
																	//	m.emergyPause将会在电机控制函数中被判断  最高位是1 则电机停转
		}
		else if(getIDvalue == GUI_ID_PAUSE)			//判断暂停按钮是否被按下
		{
				pausetemp =!pausetemp;
				if(pausetemp)
				{
					m.emergyPause |= 0x01;
				}
				else
				{
					m.emergyPause &= (~0x01);
				}
		}
		else	if( getIDvalue == GUI_ID_AUTO)			//判断自动按钮是否被按下
		{	
				AutoFlag = !AutoFlag;					//对自动标志位取反  可以实现按下自动运行 再按下则取消自动运行
			  if( !AutoFlag )
				{
						m.dest.x = m.rel.x;
						m.dest.y = m.rel.y;
						m.dest.z = m.rel.z;
				}
		}
		if(!AutoFlag)
		{
			//如果不是自动运行  则判断其他按钮的状态
			if(getIDvalue == GUI_ID_SETZONE )			//判断设置安全区间的按钮是否被按下
			{
				WM_HideWindow(WndMenu);		//隐藏菜单窗口
				WM_ShowWindow(WndSetZone);		//显示设置零点窗口
				Mystatus |= 0X02;							//标记设置零点按钮已经被按下
			}
			else if(getIDvalue == GUI_ID_SETSPEED )
			{
				WM_HideWindow(WndMenu);
				WM_ShowWindow(WndSetSpeed);
				Mystatus |= 1;			//标记设置速度按钮已经被按下
			}
			else if(getIDvalue == GUI_ID_HANDLE )
			{
				WM_HideWindow(WndMenu);
				WM_ShowWindow(WndHandle);
			}
			else if(getIDvalue == GUI_ID_HIDEZONE )
			{
				WM_HideWindow(WndSetZone);		//如果按下了X退出按钮  则代表返回主菜单 隐藏所有子窗口 并显示菜单
				WM_HideWindow(WndSetSpeed);
				WM_HideWindow(WndHandle);
				
				
				WM_ShowWindow(WndMenu);
				
				Mystatus &= ~0X01;
				Mystatus &= ~0X02;
				handleax = 0;
				m.handle.x = handleax;
				m.dest.x = m.rel.x;
				handleay = 0;
				m.handle.y = handleay;
				m.dest.y = m.rel.y;
				handleaz = 0;
				m.handle.z = handleaz;
				m.dest.z = m.rel.z;
			}
			switch( getIDvalue )
			{
					case GUI_ID_ADDX:			//如果按下了手动的X轴+按钮
						handleax = !handleax;
						m.handle.x = handleax;	//	将 m.handle.x设置为1或0  在电机中会判断  来移动x轴
						if( !m.handle.x )
						{
								m.dest.x = m.rel.x;			//按一下是x轴再按一下是取消，再按取消的时候，将当前的坐标设置为目标坐标 否则电机会自动返回
						}
						//printf("%d\r\n",(int)m.rel.x);
						break;
					case GUI_ID_ADDY:
						handleay = !handleay;
						m.handle.y = handleay;
						if( !m.handle.y )
						{
								m.dest.y = m.rel.y;
						}
						
						break;
					case GUI_ID_ADDZ:
						handleaz = !handleaz;
						m.handle.z = handleaz;
						if( !m.handle.z )
						{
								m.dest.z = m.rel.z;
						}
						break;
					
					case GUI_ID_SUBX:
						handlesx = !handlesx;
						m.handle.x = handlesx<<1;
						if( !m.handle.x )
						{
								m.dest.x = m.rel.x;
						}
						break;
					case GUI_ID_SUBY:
						handlesy = !handlesy;
						m.handle.y = handlesy<<1;
						if( !m.handle.y )
						{
								m.dest.y = m.rel.y;
						}
						break;
					case GUI_ID_SUBZ:
						handlesz = !handlesz;
						m.handle.z = handlesz<<1;
						if( !m.handle.z )
						{
								m.dest.z = m.rel.z;
						}
						break;
					
					case GUI_ID_SETZERO:			//在设置0点时，将真实坐标设置为0  并将目标坐标也设置为0
						m.dest.x = 0;
						m.dest.y = 0;
						m.dest.z = 0;
						m.step.x = 0;
						m.step.y = 0;
						m.step.z = 0;
						break;
					
					
					
						
					case GUI_ID_HOME:		//按下回零点按钮，将目标坐标设置为0  则电机会自动回到零点
						m.dest.x = 0;
						m.dest.y = 0;
						m.dest.z = 0;
						break;
					
					case GUI_ID_ADDMIN: WM__SendMsgNoData(WndSetZone, WM_UPDATE);
						m.SafeZoneMin++;
						break;
					
					case GUI_ID_SUBMIN: WM__SendMsgNoData(WndSetZone, WM_UPDATE);
						m.SafeZoneMin--;		
						break;
					
					case GUI_ID_ADDMAX: WM__SendMsgNoData(WndSetZone, WM_UPDATE);
						m.SafeZoneMax++;
						break;
					
					case GUI_ID_SUBMAX: WM__SendMsgNoData(WndSetZone, WM_UPDATE);
						m.SafeZoneMax--;
						break;
					
					
					
					default: break;
			}
		}
		else
		{							//处于自动模式下时
				if(!pausetemp)			//在自动模式下，xyz轴会依次转到给定的位置 然后再反向，不断循环
				{
						autoTime++;
						if(autoTime<-200)
								m.dest.x = -500;
						else if(autoTime<-100)
								m.dest.y = -500;
						else if(autoTime<0)
								m.dest.z = -500;
						else if(autoTime<100)
								m.dest.x = 500;	
						else if(autoTime<200)		
								m.dest.y = 500;		
						else if( autoTime<300 )
							m.dest.z = 500;
						else
							autoTime = -300;
				}						
		}
		
		if(Mystatus&0x01)			//如果设置速度的按钮被按下  则不断获取滑条的数值
		{
			xM = SLIDER_GetValue(xValue);
			yM = SLIDER_GetValue(yValue);
			zM = SLIDER_GetValue(zValue);
			WM__SendMsgNoData(WndSetSpeed, WM_UPDATE);  //发信号给回调函数 更新显示
			
		}
		else if(Mystatus&0x02)		//如果设置安全区间被按下  可以修改背光
		{
			TIM_SetCompare2( TIM2,SLIDER_GetValue(BackLight));
		}
		if(menuUpdate)
		{
			if(!(timerflag--))
			{
				timerflag = 125;
				sprintf(DispData,"%d",(int)m.rel.x);		//更新实时的坐标显示
				TEXT_SetText(xRelDis,DispData);  
				sprintf(DispData,"%d",(int)m.rel.y);
				TEXT_SetText(yRelDis,DispData);
				sprintf(DispData,"%d",(int)m.rel.z);
				TEXT_SetText(zRelDis,DispData);
				if(Wavetime++>88)
					Wavetime = 0;
				WM_SelectWindow(WndLineChart);
		
				WM__SendMsgNoData(WndLineChart, WM_UPDATE);		//发信号给波形窗口 更新波形
				
			}
		}
		OSTimeDlyHMSM(0,0,0,1);
		GUI_Delay(10);
		GUI_Exec();
		
	}
}
