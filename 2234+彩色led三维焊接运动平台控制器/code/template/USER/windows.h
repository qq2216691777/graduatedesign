#ifndef _MY_WINDOWS_H__
#define _MY_WINDOWS_H__
#include "gui.h"
#include "wm.h"
#include "FrameWin.h"

void WindowsInit( void );
void GUI_Work();

#define GUI_ID_SETZERO		1
#define GUI_ID_SETSPEED			2
#define	GUI_ID_SETZONE		3
#define	GUI_ID_EMERSTOP		5
#define	GUI_ID_PAUSE			6
#define	GUI_ID_HANDLE			7
#define	GUI_ID_AUTO				8

#define	GUI_ID_HIDEZONE		4

#define	GUI_ID_ADDMIN		9
#define	GUI_ID_ADDMAX		10
#define	GUI_ID_SUBMIN		11
#define	GUI_ID_SUBMAX		12

#define	GUI_ID_ADDX		13
#define	GUI_ID_SUBX		14
#define	GUI_ID_ADDY		15
#define	GUI_ID_SUBY		16
#define	GUI_ID_ADDZ		17
#define	GUI_ID_SUBZ		18

#define GUI_ID_HOME   19

#endif
