#include "motor.h"

Motor m= {0};
//电机控制引脚初始化
// B0 C4 A0 A2 A4 A6
void MotorGpioInit( void )
{
	
 	GPIO_InitTypeDef GPIO_InitStructure;
	
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOC,ENABLE);//??PORTE??

	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
 	GPIO_Init(GPIOB, &GPIO_InitStructure);//???GPIO
	
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_4|GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; //????  ??
 	GPIO_Init(GPIOC, &GPIO_InitStructure);//???GPIO
	
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_0|GPIO_Pin_2|GPIO_Pin_4|GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; //????  ??
 	GPIO_Init(GPIOA, &GPIO_InitStructure);//???GPIO
	
	PBout(0) = 0;
	PCout(4) = 0;
	PAout(0) = 0;
	PAout(2) = 0;
	PAout(4) = 0;
	PAout(6) = 0;
	PCout(3) = 0;
	

}


/*电机控制函数  10ms循环执行依次*/
void MotorLoop( void )  //10ms
{
	static int i;
	unsigned char beepW=0;

	
	//根据输出脉冲个数计算电机的实际位置
	m.rel.x = m.step.x*1;
	m.rel.y = m.step.y*1;
	m.rel.z = m.step.z*1;
	
	//判断实际位置有没有超过安全位置  如果超过对	beepW 加1 。 beepW将会在后面判断  
	if( (m.SafeZoneMin>m.rel.x)||(m.SafeZoneMax<m.rel.x) )
			beepW++;
	else if( (m.SafeZoneMin>m.rel.y)||(m.SafeZoneMax<m.rel.y) )
			beepW++;

	else if( (m.SafeZoneMin>m.rel.z)||(m.SafeZoneMax<m.rel.z) )
			beepW++;
		
	if(beepW)			//如果beepW不为0 说明有+1操作 则报警
		BEEP = 1;
	else 
		BEEP = 0;
	
	if(m.emergyPause)		//
	{
			m.MotorCount.x = 0;		//如果按下暂停按钮 将输出脉冲个数设置为0 然后退出函数
			m.MotorCount.y = 0;
			m.MotorCount.z = 0;
			return ;
	}
	
	if( m.handle.x==1)			//如果手动x按钮被按下  则给电机一个方向和脉冲输出
	{
			m.MotorCount.x = 10;
			XMOTORDIR = 1;		//正向
			m.dir.x = 1;
			return ;
	}
	else if( m.handle.x==2)
	{
			m.MotorCount.x = 10;
			XMOTORDIR = 0;		//反向
			m.dir.x = 2;
			return ;
	}
	
	if( m.handle.y==1)
	{
			m.MotorCount.y = 10;
			YMOTORDIR = 1;
			m.dir.y = 1;
			return ;
	}
	else if( m.handle.y==2)
	{
			m.MotorCount.y = 50;
			YMOTORDIR = 0;
			m.dir.y = 2;
			return ;
	}
	
	if( m.handle.z==1)
	{
			m.MotorCount.z = 50;
			ZMOTORDIR = 1;
			m.dir.z = 1;
			return ;
	}
	else if( m.handle.z==2)
	{
			m.MotorCount.z = 50;
			ZMOTORDIR = 0;
			m.dir.z = 2;
			return ;
	}

	if(beepW)			//如果 beepW不为0  上面已经报警  这里还是将输出脉冲个数设置为0  防止电机旋转
	{
		m.MotorCount.x = 0;
		m.MotorCount.y = 0;
		m.MotorCount.z = 0;
		return ;
	}
	
	
	
	m.err.x = m.dest.x - m.rel.x; //电机目标值 - (脉冲数) == 电机当前的绝对位置)
	m.err.y = m.dest.y - m.rel.y;
	m.err.z = m.dest.z - m.rel.z;
//	printf("%f %f %f\r\n",m.rel.x,m.rel.y,m.rel.z);
	
	//x轴判断电机旋转方向
	if( m.err.x>0 )
		m.dir.x = 1;
	else if( m.err.x<0 )
		m.dir.x = 2;
	else
		m.dir.x = 0;	//停转
	
	if( m.err.y>0 )
		m.dir.y = 1;
	else if( m.err.y<0 )
		m.dir.y = 2;
	else
		m.dir.y = 0;
	
	if( m.err.z>0 )
		m.dir.z = 1;
	else if( m.err.z<0 )
		m.dir.z = 2;
	else
		m.dir.z = 0;
	
//	自动

	if( m.dir.x ==1 )
	{
			XMOTORDIR = 1;//x电机正转
		
		  m.MotorCount.x = 100;  //10ms  10
	} else if( m.dir.x ==2 )
	{
			XMOTORDIR = 0;//x电机反转
		  m.MotorCount.x = 100;
	} else
	{
			m.MotorCount.x=0;//x轴电机停止
	}
	
	if( m.dir.y ==1 )
	{
			YMOTORDIR=1;//y电机正转
		  m.MotorCount.y = 100;
	//	  m.MotorSpeed.y = 100;
	} else if( m.dir.y ==2 )
	{
			YMOTORDIR=0;//y电机反转
		  m.MotorCount.y = 100;
		//  m.MotorSpeed.y = 100;
	} else
	{
			m.MotorCount.y=0;//y轴电机停止
	}
	
	if( m.dir.z ==1 )
	{
			ZMOTORDIR=1;//z电机正转
		  m.MotorCount.z = 100;
	//	  m.MotorSpeed.z = 100;
	} else if( m.dir.z ==2 )
	{
			ZMOTORDIR=0;//z电机反转
		  m.MotorCount.z = 100;
	//	  m.MotorSpeed.z = 100;
	} else
	{
			m.MotorCount.z=0;//z轴电机停止
	}
	

}


