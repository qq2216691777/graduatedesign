#ifndef _MOTOR_H__
#define _MOTOR_H__
#include "includes.h"

struct pos{
	float x;
	float y;
	float z;
};

struct gra{
	long x;
	long y;
	long z;
};

struct uchar{
	unsigned char x;
	unsigned char y;
	unsigned char z;
};



typedef struct {
	struct pos dest;	//电机的目标位置
	struct pos rel;	//电机的目标位置
	struct pos err;		//电机的实际位置与目标位置的差
	struct gra dir;		//xyz前进方向  0停止   1前进  2后退
	struct gra step;	//xyz前进方向  走的步数
	struct gra MotorSpeed;  //  频率 = 1000/(MotorSpeed.x+1) * 0.1ms 最小是 1
	struct gra MotorCount;  //需要输出脉冲数量
	int SafeZoneMin;		//安全区间 最小
	int SafeZoneMax;		//安全区间	最大
	struct uchar handle;	//点动
	struct gra nowSpeed;
	unsigned char emergyPause;		//紧急停车 1:有效
} Motor;

#define  XMOTOROUT  PBout(0)
#define  XMOTORDIR  PCout(4)

#define  YMOTOROUT  PAout(0)
#define  YMOTORDIR  PAout(6)

#define  ZMOTOROUT  PAout(2)
#define  ZMOTORDIR  PAout(4)

#define  BEEP PCout(3)

void MotorGpioInit( void );
void MotorLoop( void );


extern Motor m;


#endif
