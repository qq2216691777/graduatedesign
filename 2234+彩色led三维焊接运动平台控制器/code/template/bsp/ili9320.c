#include "ili9320.h"
#include "ili9320_font.h"
#include "string.h"
#include "timer.h"

void Ili9320_Init()
{
    u16 i;
		u16 DeviceCode;
    LCDx_Init();
		FSMC_Init();
		TIM2_PWM_Init(100,71);
	//	TIM_SetCompare2(TIM2,100);
	  
	  OSTimeDly(50);
    DeviceCode = LCD_ReadReg(0x0000);
    DeviceCode = 0x1505;		  //??LCD????   9325?9328?9320?9300?9331?9919?RU1505?8989?
	if(DeviceCode==0x9325||DeviceCode==0x9328)
	{
  		LCD_WriteReg(0x00e7,0x0010);      
        LCD_WriteReg(0x0000,0x0001);  			     //start internal osc
        LCD_WriteReg(0x0001,0x0100);     
        LCD_WriteReg(0x0002,0x0700); 				 //power on sequence                     
        LCD_WriteReg(0x0003,(1<<12)|(1<<5)|(1<<4) ); //65K 
        LCD_WriteReg(0x0004,0x0000);                                   
        LCD_WriteReg(0x0008,0x0207);	           
        LCD_WriteReg(0x0009,0x0000);         
        LCD_WriteReg(0x000a,0x0000); 				 //display setting         
        LCD_WriteReg(0x000c,0x0001);				 //display setting          
        LCD_WriteReg(0x000d,0x0000); 				 //0f3c          
        LCD_WriteReg(0x000f,0x0000);
        //Power On sequence //
        LCD_WriteReg(0x0010,0x0000);   
        LCD_WriteReg(0x0011,0x0007);
        LCD_WriteReg(0x0012,0x0000);                                                                 
        LCD_WriteReg(0x0013,0x0000);                 
        for(i=50000;i>0;i--);
		for(i=50000;i>0;i--);
        LCD_WriteReg(0x0010,0x1590);   
        LCD_WriteReg(0x0011,0x0227);
        for(i=50000;i>0;i--);
		for(i=50000;i>0;i--);
        LCD_WriteReg(0x0012,0x009c);                  
        for(i=50000;i>0;i--);
		for(i=50000;i>0;i--);
        LCD_WriteReg(0x0013,0x1900);   
        LCD_WriteReg(0x0029,0x0023);
        LCD_WriteReg(0x002b,0x000e);
        for(i=50000;i>0;i--);
		for(i=50000;i>0;i--);
        LCD_WriteReg(0x0020,0x0000);                                                            
        LCD_WriteReg(0x0021,0x0000);           
        ///////////////////////////////////////////////////////      
        for(i=50000;i>0;i--);
		for(i=50000;i>0;i--);
        LCD_WriteReg(0x0030,0x0007); 
        LCD_WriteReg(0x0031,0x0707);   
        LCD_WriteReg(0x0032,0x0006);
        LCD_WriteReg(0x0035,0x0704);
        LCD_WriteReg(0x0036,0x1f04); 
        LCD_WriteReg(0x0037,0x0004);
        LCD_WriteReg(0x0038,0x0000);        
        LCD_WriteReg(0x0039,0x0706);     
        LCD_WriteReg(0x003c,0x0701);
        LCD_WriteReg(0x003d,0x000f);
        for(i=50000;i>0;i--);
		for(i=50000;i>0;i--);
        LCD_WriteReg(0x0050,0x0000);        
        LCD_WriteReg(0x0051,0x00ef);   
        LCD_WriteReg(0x0052,0x0000);     
        LCD_WriteReg(0x0053,0x013f);
        LCD_WriteReg(0x0060,0xa700);        
        LCD_WriteReg(0x0061,0x0001); 
        LCD_WriteReg(0x006a,0x0000);
        LCD_WriteReg(0x0080,0x0000);
        LCD_WriteReg(0x0081,0x0000);
        LCD_WriteReg(0x0082,0x0000);
        LCD_WriteReg(0x0083,0x0000);
        LCD_WriteReg(0x0084,0x0000);
        LCD_WriteReg(0x0085,0x0000);  
        LCD_WriteReg(0x0090,0x0010);     
        LCD_WriteReg(0x0092,0x0000);  
        LCD_WriteReg(0x0093,0x0003);
        LCD_WriteReg(0x0095,0x0110);
        LCD_WriteReg(0x0097,0x0000);        
        LCD_WriteReg(0x0098,0x0000);  
        //display on sequence     
        LCD_WriteReg(0x0007,0x0133);
        LCD_WriteReg(0x0020,0x0000);                                                            
        LCD_WriteReg(0x0021,0x0000);
	}
	else if(DeviceCode==0x9320||DeviceCode==0x9300)
	{
        LCD_WriteReg(0x00,0x0000);
		LCD_WriteReg(0x01,0x0100);	//Driver Output Contral.
		LCD_WriteReg(0x02,0x0700);	//LCD Driver Waveform Contral.
		LCD_WriteReg(0x03,0x1018);	//Entry Mode Set.
	
		LCD_WriteReg(0x04,0x0000);	//Scalling Contral.
		LCD_WriteReg(0x08,0x0202);	//Display Contral 2.(0x0207)
		LCD_WriteReg(0x09,0x0000);	//Display Contral 3.(0x0000)
		LCD_WriteReg(0x0a,0x0000);	//Frame Cycle Contal.(0x0000)
		LCD_WriteReg(0x0c,(1<<0));	//Extern Display Interface Contral 1.(0x0000)
		LCD_WriteReg(0x0d,0x0000);	//Frame Maker Position.
		LCD_WriteReg(0x0f,0x0000);	//Extern Display Interface Contral 2.
	
		for(i=50000;i>0;i--);
		for(i=50000;i>0;i--);
		LCD_WriteReg(0x07,0x0101);	//Display Contral.
		for(i=50000;i>0;i--);
		for(i=50000;i>0;i--);
	
		LCD_WriteReg(0x10,(1<<12)|(0<<8)|(1<<7)|(1<<6)|(0<<4));	//Power Control 1.(0x16b0)
		LCD_WriteReg(0x11,0x0007);								//Power Control 2.(0x0001)
		LCD_WriteReg(0x12,(1<<8)|(1<<4)|(0<<0));					//Power Control 3.(0x0138)
		LCD_WriteReg(0x13,0x0b00);								//Power Control 4.
		LCD_WriteReg(0x29,0x0000);								//Power Control 7.
	
		LCD_WriteReg(0x2b,(1<<14)|(1<<4));
		
		LCD_WriteReg(0x50,0);		//Set X Start.
		LCD_WriteReg(0x51,239);	//Set X End.
		LCD_WriteReg(0x52,0);		//Set Y Start.
		LCD_WriteReg(0x53,319);	//Set Y End.
		LCD_WriteReg(0x60,0x2700);	//Driver Output Control.
		LCD_WriteReg(0x61,0x0001);	//Driver Output Control.
		LCD_WriteReg(0x6a,0x0000);	//Vertical Srcoll Control.
		LCD_WriteReg(0x80,0x0000);	//Display Position? Partial Display 1.
		LCD_WriteReg(0x81,0x0000);	//RAM Address Start? Partial Display 1.
		LCD_WriteReg(0x82,0x0000);	//RAM Address End-Partial Display 1.
		LCD_WriteReg(0x83,0x0000);	//Displsy Position? Partial Display 2.
		LCD_WriteReg(0x84,0x0000);	//RAM Address Start? Partial Display 2.
		LCD_WriteReg(0x85,0x0000);	//RAM Address End? Partial Display 2.
		LCD_WriteReg(0x90,(0<<7)|(16<<0));	//Frame Cycle Contral.(0x0013)
		LCD_WriteReg(0x92,0x0000);	//Panel Interface Contral 2.(0x0000)
		LCD_WriteReg(0x93,0x0001);	//Panel Interface Contral 3.
		LCD_WriteReg(0x95,0x0110);	//Frame Cycle Contral.(0x0110)
		LCD_WriteReg(0x97,(0<<8));	//
		LCD_WriteReg(0x98,0x0000);	//Frame Cycle Contral.
		LCD_WriteReg(0x07,0x0173);	//(0x0173)
	}
	else if(DeviceCode==0x9331)
	{
		LCD_WriteReg(0x00E7, 0x1014);
		LCD_WriteReg(0x0001, 0x0100); // set SS and SM bit   0x0100
		LCD_WriteReg(0x0002, 0x0200); // set 1 line inversion
		LCD_WriteReg(0x0003, 0x1030); // set GRAM write direction and BGR=1.     0x1030
		LCD_WriteReg(0x0008, 0x0202); // set the back porch and front porch
		LCD_WriteReg(0x0009, 0x0000); // set non-display area refresh cycle ISC[3:0]
		LCD_WriteReg(0x000A, 0x0000); // FMARK function
		LCD_WriteReg(0x000C, 0x0000); // RGB interface setting
		LCD_WriteReg(0x000D, 0x0000); // Frame marker Position
		LCD_WriteReg(0x000F, 0x0000); // RGB interface polarity
		//*************Power On sequence ****************//
		LCD_WriteReg(0x0010, 0x0000); // SAP, BT[3:0], AP, DSTB, SLP, STB
		LCD_WriteReg(0x0011, 0x0007); // DC1[2:0], DC0[2:0], VC[2:0]
		LCD_WriteReg(0x0012, 0x0000); // VREG1OUT voltage
		LCD_WriteReg(0x0013, 0x0000); // VDV[4:0] for VCOM amplitude
		//ili9320_Delay(200); // Dis-charge capacitor power voltage
		OSTimeDly(50);
		LCD_WriteReg(0x0010, 0x1690); // SAP, BT[3:0], AP, DSTB, SLP, STB
		LCD_WriteReg(0x0011, 0x0227); // DC1[2:0], DC0[2:0], VC[2:0]
		OSTimeDly(50); // Delay 50ms
		LCD_WriteReg(0x0012, 0x000C); // Internal reference voltage= Vci;
		OSTimeDly(50); // Delay 50ms
		LCD_WriteReg(0x0013, 0x0800); // Set VDV[4:0] for VCOM amplitude
		LCD_WriteReg(0x0029, 0x0011); // Set VCM[5:0] for VCOMH
		LCD_WriteReg(0x002B, 0x000B); // Set Frame Rate
		OSTimeDly(50); // Delay 50ms
		LCD_WriteReg(0x0020, 0x0000); // GRAM horizontal Address
		LCD_WriteReg(0x0021, 0x0000); // GRAM Vertical Address
		// ----------- Adjust the Gamma Curve ----------//
		LCD_WriteReg(0x0030, 0x0000);
		LCD_WriteReg(0x0031, 0x0106);
		LCD_WriteReg(0x0032, 0x0000);
		LCD_WriteReg(0x0035, 0x0204);
		LCD_WriteReg(0x0036, 0x160A);
		LCD_WriteReg(0x0037, 0x0707);
		LCD_WriteReg(0x0038, 0x0106);
		LCD_WriteReg(0x0039, 0x0707);
		LCD_WriteReg(0x003C, 0x0402);
		LCD_WriteReg(0x003D, 0x0C0F);
		//------------------ Set GRAM area ---------------//
		LCD_WriteReg(0x0050, 0x0000); // Horizontal GRAM Start Address
		LCD_WriteReg(0x0051, 0x00EF); // Horizontal GRAM End Address
		LCD_WriteReg(0x0052, 0x0000); // Vertical GRAM Start Address
		LCD_WriteReg(0x0053, 0x013F); // Vertical GRAM Start Address
		LCD_WriteReg(0x0060, 0x2700); // Gate Scan Line
		LCD_WriteReg(0x0061, 0x0001); // NDL,VLE, REV
		LCD_WriteReg(0x006A, 0x0000); // set scrolling line
		//-------------- Partial Display Control ---------//
		LCD_WriteReg(0x0080, 0x0000);
		LCD_WriteReg(0x0081, 0x0000);
		LCD_WriteReg(0x0082, 0x0000);
		LCD_WriteReg(0x0083, 0x0000);
		LCD_WriteReg(0x0084, 0x0000);
		LCD_WriteReg(0x0085, 0x0000);
		//-------------- Panel Control -------------------//
		LCD_WriteReg(0x0090, 0x0010);
		LCD_WriteReg(0x0092, 0x0600);
		LCD_WriteReg(0x0007,0x0021);		
		OSTimeDly(50);
		LCD_WriteReg(0x0007,0x0061);
		OSTimeDly(50);
		LCD_WriteReg(0x0007,0x0133);  // 262K color and display ON
		OSTimeDly(50);
	}
	else if(DeviceCode==0x9919)
	{
		//*********POWER ON &RESET DISPLAY OFF
		 LCD_WriteReg(0x28,0x0006);	
		 LCD_WriteReg(0x00,0x0001);
		 LCD_WriteReg(0x10,0x0000);
		 LCD_WriteReg(0x01,0x72ef);
		 LCD_WriteReg(0x02,0x0600);
		 LCD_WriteReg(0x03,0x6a38);
		 LCD_WriteReg(0x11,0x6874);
		 //  RAM WRITE DATA MASK
		 LCD_WriteReg(0x0f,0x0000); 
		 //  RAM WRITE DATA MASK
		 LCD_WriteReg(0x0b,0x5308); 
		 LCD_WriteReg(0x0c,0x0003);
		 LCD_WriteReg(0x0d,0x000a);
		 LCD_WriteReg(0x0e,0x2e00); 
		 LCD_WriteReg(0x1e,0x00be);
		 LCD_WriteReg(0x25,0x8000);
		 LCD_WriteReg(0x26,0x7800);
		 LCD_WriteReg(0x27,0x0078);
		 LCD_WriteReg(0x4e,0x0000);
		 LCD_WriteReg(0x4f,0x0000);
		 LCD_WriteReg(0x12,0x08d9);
		 // -----------------Adjust the Gamma Curve----//
		 LCD_WriteReg(0x30,0x0000);	
		 LCD_WriteReg(0x31,0x0104);	  
		 LCD_WriteReg(0x32,0x0100);	
		 LCD_WriteReg(0x33,0x0305);	  
		 LCD_WriteReg(0x34,0x0505);	  
		 LCD_WriteReg(0x35,0x0305);	
		 LCD_WriteReg(0x36,0x0707);	
		 LCD_WriteReg(0x37,0x0300);	
		 LCD_WriteReg(0x3a,0x1200);	
		 LCD_WriteReg(0x3b,0x0800);		 
		 LCD_WriteReg(0x07,0x0033);
	}
	else if(DeviceCode==0x1505)
	{
 		//*********************************************Start intial Sequence	  	
		LCD_WriteReg(0x00e5,0x8000);
		LCD_WriteReg(0x0000,0x0001);
		LCD_WriteReg(0x0001,0x0100);
		LCD_WriteReg(0x0002,0x0700);
		LCD_WriteReg(0x0003,0x1030);
		LCD_WriteReg(0x0004,0x0000);
		LCD_WriteReg(0x0008,0x0202);
		LCD_WriteReg(0x0009,0x0000);
		LCD_WriteReg(0x000a,0x0000);	
		LCD_WriteReg(0x000c,0x0000);
		LCD_WriteReg(0x000d,0x0000);
		LCD_WriteReg(0x000f,0x0000);
        //*********************************************Power On
		LCD_WriteReg(0x0010,0x0000);
		LCD_WriteReg(0x0011,0x0000);
		LCD_WriteReg(0x0012,0x0000);
		LCD_WriteReg(0x0013,0x0000);
		for(i=50000;i>0;i--);
		LCD_WriteReg(0x0010,0x17b0);
		LCD_WriteReg(0x0011,0x0037);
		for(i=50000;i>0;i--);
		LCD_WriteReg(0x0012,0x0138);
		for(i=50000;i>0;i--);
		LCD_WriteReg(0x0013,0x1700);
		LCD_WriteReg(0x0029,0x000d);
		for(i=50000;i>0;i--);
		LCD_WriteReg(0x0020,0x0000);
		LCD_WriteReg(0x0021,0x0000);
        //*********************************************Set gamma
		LCD_WriteReg(0x0030,0x0001);
		LCD_WriteReg(0x0031,0x0606);
		LCD_WriteReg(0x0032,0x0304);
		LCD_WriteReg(0x0033,0x0202);
		LCD_WriteReg(0x0034,0x0202);
		LCD_WriteReg(0x0035,0x0103);
		LCD_WriteReg(0x0036,0x011d);
		LCD_WriteReg(0x0037,0x0404);
		LCD_WriteReg(0x0038,0x0404);
		LCD_WriteReg(0x0039,0x0404);
		LCD_WriteReg(0x003c,0x0700);
		LCD_WriteReg(0x003d,0x0a1f);
        //**********************************************Set Gram aera
		LCD_WriteReg(0x0050,0x0000);
		LCD_WriteReg(0x0051,0x00ef);
		LCD_WriteReg(0x0052,0x0000);
		LCD_WriteReg(0x0053,0x013f);
		LCD_WriteReg(0x0060,0x2700);
		LCD_WriteReg(0x0061,0x0001);
		LCD_WriteReg(0x006a,0x0000);
        //*********************************************Paratial display
		LCD_WriteReg(0x0090,0x0010);
		LCD_WriteReg(0x0092,0x0000);
		LCD_WriteReg(0x0093,0x0003);
		LCD_WriteReg(0x0095,0x0101);
		LCD_WriteReg(0x0097,0x0000);
		LCD_WriteReg(0x0098,0x0000);
        //******************************************** Plan Control
		LCD_WriteReg(0x0007,0x0021);
		for(i=50000;i>0;i--);
		LCD_WriteReg(0x0007,0x0031);
		for(i=50000;i>0;i--);
		LCD_WriteReg(0x0007,0x0173);
		for(i=50000;i>0;i--);
	}							 
	
    for(i=50000;i>0;i--);
}




void Ili9320_Init1( void )
{
	u16 DeviceCode;
	u16 i;
	LCDx_Init();
	FSMC_Init();
	OSTimeDly(50);
//	delay_ms(50);
	DeviceCode = LCD_ReadReg(0x0000);
	while( DeviceCode != 0x9328 )
	{
		DeviceCode = LCD_ReadReg(0x0000);
		//delay_ms(100);
		OSTimeDly(100);
	}
	
	LCD_WriteReg(0x00e7,0x0010);
	LCD_WriteReg(0x0000,0x0001);  			     //start internal osc
	
/*********************************************************************/	
	#if _XTOY_OPEN_==0
	LCD_WriteReg(0x0001,0x0100); 
	LCD_WriteReg(0x0002,0x0700); 				 //power on sequence   
	LCD_WriteReg(0x0003,(1<<12)|(1<<5)|(1<<4)|(0<<3) ); //65K  竖 (1<<12)|(1<<5)|(1<<4)|(0<<3) 
	#else
	LCD_WriteReg(0x0001,0x0000);
	LCD_WriteReg(0x0002,0x0700); 				 //power on sequence   
	LCD_WriteReg(0x0003,(1<<12)|(1<<5)|(1<<4)|(1<<3) ); //65K  竖 (1<<12)|(1<<5)|(1<<4)|(0<<3) 
	#endif
	
	/*************************************************/
	
                   
	LCD_WriteReg(0x0004,0x0000);                                   
	LCD_WriteReg(0x0008,0x0202);				//Display Contral 2.(0x0207)	           
	LCD_WriteReg(0x0009,0x0000);         
	LCD_WriteReg(0x000a,0x0000); 				 //display setting         
	LCD_WriteReg(0x000c,0x0001);				 //display setting          
	LCD_WriteReg(0x000d,0x0000); 				 //0f3c          
	LCD_WriteReg(0x000f,0x0000);
	//Power On sequence //
	LCD_WriteReg(0x0010,0x0000);   
	LCD_WriteReg(0x0011,0x0007);
	LCD_WriteReg(0x0012,0x0000);                                                                 
	LCD_WriteReg(0x0013,0x0000); 
	
	for(i=50000;i>0;i--);
	for(i=50000;i>0;i--);
	
	LCD_WriteReg(0x0010,0x1590);   
	LCD_WriteReg(0x0011,0x0227);
	
	for(i=50000;i>0;i--);
	for(i=50000;i>0;i--);
	
	LCD_WriteReg(0x0012,0x009c);   
	
	for(i=50000;i>0;i--);
	for(i=50000;i>0;i--);
	
	LCD_WriteReg(0x0013,0x1900);   
	LCD_WriteReg(0x0029,0x0023);
	LCD_WriteReg(0x002b,0x000e);
	
	for(i=50000;i>0;i--);
	for(i=50000;i>0;i--);
	
	LCD_WriteReg(0x0020,0x0000);                                                            
	LCD_WriteReg(0x0021,0x0000);           
	
	///////////////////////////////////////////////////////      
	for(i=50000;i>0;i--);
	for(i=50000;i>0;i--);
	
	LCD_WriteReg(0x0030,0x0007); 
	LCD_WriteReg(0x0031,0x0707);   
	LCD_WriteReg(0x0032,0x0006);
	LCD_WriteReg(0x0035,0x0704);
	LCD_WriteReg(0x0036,0x1f04); 
	LCD_WriteReg(0x0037,0x0004);
	LCD_WriteReg(0x0038,0x0000);        
	LCD_WriteReg(0x0039,0x0706);     
	LCD_WriteReg(0x003c,0x0701);
	LCD_WriteReg(0x003d,0x000f);
	
	for(i=50000;i>0;i--);
	for(i=50000;i>0;i--);
	
	LCD_WriteReg(0x0050,0x0000);       //X起始地址 
	LCD_WriteReg(0x0051,0x00ef);   		 //X终止地址
	LCD_WriteReg(0x0052,0x0000);     	 //Y起始地址 
	LCD_WriteReg(0x0053,0x013f);			 //Y终止地址
	LCD_WriteReg(0x0060,0xa700);        
	LCD_WriteReg(0x0061,0x0001); 
	LCD_WriteReg(0x006a,0x0000);
	LCD_WriteReg(0x0080,0x0000);
	LCD_WriteReg(0x0081,0x0000);
	LCD_WriteReg(0x0082,0x0000);
	LCD_WriteReg(0x0083,0x0000);
	LCD_WriteReg(0x0084,0x0000);
	LCD_WriteReg(0x0085,0x0000);  
	LCD_WriteReg(0x0090,0x0010);     
	LCD_WriteReg(0x0092,0x0000);  
	LCD_WriteReg(0x0093,0x0003);
	LCD_WriteReg(0x0095,0x0110);
	LCD_WriteReg(0x0097,0x0000);        
	LCD_WriteReg(0x0098,0x0000);  
	//display on sequence     
	LCD_WriteReg(0x0007,0x0133);
	LCD_WriteReg(0x0020,0x0000);                                                            
	LCD_WriteReg(0x0021,0x0000);
	//delay_ms(50);
	OSTimeDly(50);
//	LCD_Clear( BLACKGROUND );
}

void LCDx_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE |
                         RCC_APB2Periph_GPIOF | RCC_APB2Periph_GPIOG |
                         RCC_APB2Periph_AFIO, ENABLE);
	RCC_AHBPeriphClockCmd( RCC_AHBPeriph_FSMC, ENABLE );
	
// 	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
// 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
// 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
// 	GPIO_Init(GPIOA, &GPIO_InitStructure);
// 	GPIO_SetBits(GPIOA, GPIO_Pin_1);						//打开背光  点亮Lcd 

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_4 | GPIO_Pin_5 |
	                            GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_14 | 
	                            GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | 
	                            GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | 
	                            GPIO_Pin_15;
	GPIO_Init(GPIOE, &GPIO_InitStructure);

	/* Set PF.00(A0 (RS)) as alternate function push pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_Init(GPIOF, &GPIO_InitStructure);

	/* Set PG.12(NE4 (LCD/CS)) as alternate function push pull - CE3(LCD /CS) */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_Init(GPIOG, &GPIO_InitStructure);
}

void FSMC_Init( void )
{
	FSMC_NORSRAMInitTypeDef  FSMC_NORSRAMInitStructure;
	FSMC_NORSRAMTimingInitTypeDef  p;
	
	/*-- FSMC Configuration ------------------------------------------------------*/
	/*----------------------- SRAM Bank 4 ----------------------------------------*/
	/* FSMC_Bank1_NORSRAM4 configuration */
	p.FSMC_AddressSetupTime = 0;
	p.FSMC_AddressHoldTime = 0;
	p.FSMC_DataSetupTime = 10;
	p.FSMC_BusTurnAroundDuration = 0;
	p.FSMC_CLKDivision = 0;
	p.FSMC_DataLatency = 0;
	p.FSMC_AccessMode = FSMC_AccessMode_A;
	
	/* Color LCD configuration ------------------------------------
	 LCD configured as follow:
	    - Data/Address MUX = Disable
	    - Memory Type = SRAM
	    - Data Width = 16bit
	    - Write Operation = Enable
	    - Extended Mode = Enable
	    - Asynchronous Wait = Disable 	*/
	FSMC_NORSRAMInitStructure.FSMC_Bank = FSMC_Bank1_NORSRAM4;
	FSMC_NORSRAMInitStructure.FSMC_DataAddressMux = FSMC_DataAddressMux_Disable;
	FSMC_NORSRAMInitStructure.FSMC_MemoryType = FSMC_MemoryType_SRAM;
	FSMC_NORSRAMInitStructure.FSMC_MemoryDataWidth = FSMC_MemoryDataWidth_16b;
	FSMC_NORSRAMInitStructure.FSMC_BurstAccessMode = FSMC_BurstAccessMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignalPolarity = FSMC_WaitSignalPolarity_Low;
	FSMC_NORSRAMInitStructure.FSMC_AsynchronousWait=FSMC_AsynchronousWait_Disable; 
	FSMC_NORSRAMInitStructure.FSMC_WrapMode = FSMC_WrapMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignalActive = FSMC_WaitSignalActive_BeforeWaitState;
	FSMC_NORSRAMInitStructure.FSMC_WriteOperation = FSMC_WriteOperation_Enable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignal = FSMC_WaitSignal_Disable;
	FSMC_NORSRAMInitStructure.FSMC_ExtendedMode = FSMC_ExtendedMode_Disable;
//	FSMC_NORSRAMInitStructure.FSMC_AsyncWait = FSMC_AsyncWait_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WriteBurst = FSMC_WriteBurst_Disable;
	FSMC_NORSRAMInitStructure.FSMC_ReadWriteTimingStruct = &p;
	FSMC_NORSRAMInitStructure.FSMC_WriteTimingStruct = &p;
	
	FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure);  
	
	/* BANK 4 (of NOR/SRAM Bank 1~4) is enabled */
	FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM4, ENABLE);
	//delay_ms(50);
	OSTimeDly(50);
}

u16 LCD_ReadReg( u8 LCD_Reg)
{
	LCDx->LCD_REG = LCD_Reg;
	return ( LCDx->LCD_RAM );
}

void LCD_WriteReg(u8 LCD_Reg,u16 LCD_RegValue)
{
  /* Write 16-bit Index, then Write Reg */
  LCDx->LCD_REG = LCD_Reg;
  /* Write 16-bit Reg */
  LCDx->LCD_RAM = LCD_RegValue;
}

void LCD_Clear( u16 Color )
{
  u32 index=0;
	ili9320_SetCursor(0,0); 
  LCD_WriteRAM_Prepare(); /* Prepare to write GRAM */
  for(index=0;index<76800;index++)
   {
//		 delay_us(900);
     LCDx->LCD_RAM=Color;
   }
}


/****************************************************************************
* 名    称：void ili9320_SetCursor(u16 x,u16 y)
* 功    能：设置屏幕座标
* 入口参数：x      行座标
*           y      列座标
* 出口参数：无
* 说    明：
* 调用方法：ili9320_SetCursor(10,10);
****************************************************************************/
__inline void ili9320_SetCursor(u16 x,u16 y)
{
	LCD_WriteReg(0x0020,x);        //行x
	LCD_WriteReg(0x0021,y);        //列	0x13f-y
}

void LCD_WriteRAM_Prepare(void)
{
  LCDx->LCD_REG = R34;
}


void ili9320_SetPoint(u16 x,u16 y,u16 point)
{
//  if ( (x>320)||(y>240) ) return;
  ili9320_SetCursor(x,y);

  LCD_WriteRAM_Prepare();
  LCD_WriteRAM(point);
}

void LCD_WriteRAM(u16 RGB_Code)					 
{
  /* Write 16-bit GRAM Reg */
  LCDx->LCD_RAM = RGB_Code;
}

/****************************************************************************
* 名    称：u16 ili9320_GetPoint(u16 x,u16 y)
* 功    能：获取指定座标的颜色值
* 入口参数：x      行座标
*           y      列座标
* 出口参数：当前座标颜色值
* 说    明：
* 调用方法：i=ili9320_GetPoint(10,10);
****************************************************************************/
u16 ili9320_GetPoint(u16 x,u16 y)
{
  ili9320_SetCursor(x,y);
  return (ili9320_BGR2RGB(LCD_ReadRAM()));
}

/****************************************************************************
* 名    称：u16 ili9320_BGR2RGB(u16 c)
* 功    能：RRRRRGGGGGGBBBBB 改为 BBBBBGGGGGGRRRRR 格式
* 入口参数：c      BRG 颜色值
* 出口参数：RGB 颜色值
* 说    明：内部函数调用
* 调用方法：
****************************************************************************/
u16 ili9320_BGR2RGB(u16 c)
{
  u16  r, g, b, rgb;

  b = (c>>0)  & 0x1f;
  g = (c>>5)  & 0x3f;
  r = (c>>11) & 0x1f;
  
  rgb =  (b<<11) + (g<<5) + (r<<0);

  return( rgb );
}

/*******************************************************************************
* Function Name  : LCD_ReadRAM
* Description    : Reads the LCD RAM.
* Input          : None
* Output         : None
* Return         : LCD RAM Value.
*******************************************************************************/
u16 dummy;
u16 LCD_ReadRAM(void)
{
  
  /* Write 16-bit Index (then Read Reg) */
  LCDx->LCD_REG = R34; /* Select GRAM Reg */
  /* Read 16-bit Reg */
  dummy = LCDx->LCD_RAM; 
  return dummy;
}
#if _CHINESE_OPEN_ == 1
	/****************************************************************************
	* 名    称：void GUI_Text(u16 x, u16 y, u8 *str, u16 len,u16 Color, u16 bkColor)
	* 功    能：在指定座标显示汉字文本
	* 入口参数：x      行座标
	*           y      列座标
	*           *str   汉字串
	*           len    字串长度
	*           Color  字符颜色
	*           bkColor字符背景颜色
	* 出口参数：无
	* 说    明：一个汉字两个字符，记得哟，5个汉字len就是10
	* 调用方法：GUI_Text(0,0,"成都贝一特",10,0x0000,0xffff);
	****************************************************************************/
	void GUI_Chinese_Text(u16 x,u16 y, char str[],u16 charColor,u16 bkColor)
	{
		u16 i=0,b;
		u16 j=0;
		u16 x_add,y_add;
		u16 tmp_char=0,index=0;
		u8 len;
		x_add=x;
		y_add=y;
		len = strlen(str);
		for(b=0;b<len/2;b++)
		{
				index=(94*(str[b*2] - 0xa1)+(str[b*2+1] - 0xa1));	    //计算区位
				for (i=0;i<16;i++)
					{
						for (j=0;j<8;j++)
						{
							tmp_char=HzLib[index*32+i*2];					//查字库
								if ( (tmp_char << j) & 0x80)
								{
									ili9320_SetPoint(x_add+j,y_add+i,charColor);  // 字符颜色
								}
								else
								{
									ili9320_SetPoint(x_add+j,y_add+i,bkColor);   // 背景颜色
								}
						}
						for (j=0;j<8;j++)
						{
							tmp_char=HzLib[index*32+i*2+1];				    //查字库
								if ( (tmp_char << j) & 0x80)
								{
									ili9320_SetPoint(x_add+j+8,y_add+i,charColor); // 字符颜色
								}
								else
								{
									ili9320_SetPoint(x_add+j+8,y_add+i,bkColor);   // 背景颜色
								}
						}
					 }
				 x_add=x_add+17;										 //地址累加
		}
	}
#endif

/****************************************************************************
* 名    称：void ili9320_SetWindows(u16 StartX,u16 StartY,u16 EndX,u16 EndY)
* 功    能：设置窗口区域
* 入口参数：StartX     行起始座标
*           StartY     列起始座标
*           EndX       行结束座标
*           EndY       列结束座标
* 出口参数：无
* 说    明：
* 调用方法：ili9320_SetWindows(0,0,100,100)；
****************************************************************************/
void ili9320_SetWindows( u16 StartX,u16 StartY,u16 EndX,u16 EndY)
{
 	#if _XTOY_OPEN_ == 0
  ili9320_SetCursor(StartX,StartY);
  LCD_WriteReg(0x0050, StartX);
  LCD_WriteReg(0x0052, StartY);
  LCD_WriteReg(0x0051, EndX-1);
  LCD_WriteReg(0x0053, EndY-1);
	#else
	ili9320_SetCursor(StartY,StartX);
  LCD_WriteReg(0x0050, StartY);
  LCD_WriteReg(0x0052, StartX);
  LCD_WriteReg(0x0051, EndY-1);
  LCD_WriteReg(0x0053, EndX-1);
	#endif
}

