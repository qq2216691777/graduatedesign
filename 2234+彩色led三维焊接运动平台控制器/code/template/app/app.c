/**
  ***************************************
  *@File     app.c
  *@Auther   YNXF  
  *@date     2017-05-10
  *@Version  v1.0
  *@brief    任务定义和创建函数    
  *@HAUST - RobotLab
  ***************************************
**/
#include "includes.h"
#include "windows.h"
#include "motor.h"
#include "timer.h"

OS_STK TASK_USART_STK[TASK_USART_STK_SIZE];		//设置串口堆栈 用于调试程序
OS_STK TASK_LCD_STK[TASK_LCD_STK_SIZE];				//设置lcd堆栈  这需要大一些 否则显示任务工作不正常
OS_STK TASK_TOUCH_STK[TASK_TOUCH_STK_SIZE];		//设置触摸屏任务堆栈

OS_STK TASK_MOTOR_STK[TASK_MOTOR_STK_SIZE];			//设置电机任务堆栈

void Task_Motor( void *p_arg );			//函数声明

void Task_Start( void *p_arg )		//开始任务  最先执行的函数
{
	p_arg = p_arg;
	SysTick_Init();
	
	#if(OS_TASK_STAT_EN > 0)
	OSStatInit();	//	统计任务初始化函数
	#endif
	
	LED_Init();			//提示系统正在运行.....		

	OSTaskCreate( Task_Usart, (void *)0,&TASK_USART_STK[TASK_USART_STK_SIZE-1],TASK_USART_PRIO);		//创建串口任务 便于调试
	
	OSTaskCreate( Task_LCD, (void *)0,&TASK_LCD_STK[TASK_LCD_STK_SIZE-1],TASK_LCD_PRIO);			//创建lcd任务
	
	OSTaskCreate( Task_Motor, (void *)0,&TASK_MOTOR_STK[TASK_MOTOR_STK_SIZE-1],TASK_MOTOR_PRIO);			//创建电机任务
	while(1)
	{
		GPIO_SetBits(GPIOD,GPIO_Pin_13);			//led闪速  提示系统正在运行
		OSTimeDlyHMSM(0,0,0,30);
		GPIO_ResetBits(GPIOD,GPIO_Pin_13);
		OSTimeDlyHMSM(0,0,1,300);
	}
}

/*//任务模板
void Task_( void *p_arg )
{
	p_arg = p_arg;
	
	while(1)
	{

	}
	
}
*/
/*
*  电机任务函数
*/
void Task_Motor( void *p_arg )
{
	p_arg = p_arg;
	
	MotorGpioInit();			//初始化gpio
	m.dest.x = 0;
	m.dest.y = 0;
	m.dest.z = 0;
	m.dir.x = 0;
	m.dir.y = 0;
	m.dir.z = 0;
	m.MotorCount.x = 0;
	m.MotorCount.y = 0;
	m.MotorCount.z = 0;
	m.err.x = 0;
	m.err.y = 0;
	m.err.z = 0;
	m.step.x = 0;
	m.step.y = 0;
	m.step.z = 0;
	m.MotorSpeed.x = 100;
	m.MotorSpeed.y = 100;
	m.MotorSpeed.z = 100;
	
	m.emergyPause = 0;
	
	m.SafeZoneMin = -1000;
	m.SafeZoneMax = 1000;			//初始化变量

	
	TIM3_Int_Init(99,7199);	//10ms中断  是控制周期
	TIM4_Int_Init(199,71);		//200us  用于输出脉冲 控制电机转动
	
	while(1)
	{
			OSTimeDlyHMSM(0,0,1,10);
	}
	
}


/*
*	 串口任务
*/
void Task_Usart( void *p_arg )
{
	p_arg = p_arg;
	uart_init(115200);
	while(1)
	{	
		if( USART_RX_STA&0x8000 )
		{
			printf("SUCCESSED\n");
			USART_RX_STA=0;
		}   
		OSTimeDlyHMSM(0,0,0,10);
	}
	
}

/*
*		触摸屏任务
*/
void Task_Touch( void *p_arg )
{
		//GUI_CURSOR_Show();
		while(1)
		{
				OSTimeDlyHMSM(0,0,0,20);
				GUI_TOUCH_Exec();			//重复执行这个函数  将会自动刷新gui中的指针坐标
		}
}
/*
*		液晶屏初始化
*/
void Task_LCD( void *p_arg )
{
	
	p_arg = p_arg;
	
	GUI_Init();			//ucGUI初始化
	GUI_SetBkColor( GUI_WHITE );		//设置背景色为白色
	GUI_Clear();				//清屏
	
	OSTaskCreate( Task_Touch, (void *)0,&TASK_TOUCH_STK[TASK_TOUCH_STK_SIZE-1],TASK_TOUCH_PRIO);			//创建触摸屏任务
	
	GUI_Work();		//执行work函数   该函数位于 windows.c文件中  主要是勾画画面

}




