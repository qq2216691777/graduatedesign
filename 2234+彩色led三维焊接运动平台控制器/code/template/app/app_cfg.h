#ifndef _APP_CFG_H__
#define _APP_CFG_H__

/****设置任务优先级  数字越小等级越高****/
#define STARTUP_TASK_PRIO 4
/****设置栈大小****/
#define STARTUP_TASK_STK_SIZE 128


#define TASK_LCD_PRIO 3
#define TASK_LCD_STK_SIZE 8024

#define TASK_TOUCH_PRIO 1
#define TASK_TOUCH_STK_SIZE 128


#define TASK_USART_PRIO 8
#define TASK_USART_STK_SIZE 128

#define TASK_MOTOR_PRIO 2
#define TASK_MOTOR_STK_SIZE 4024







#endif

