#ifndef uchar
#define uchar unsigned char
#define uint unsigned int
#endif

#ifndef _DS1302_H_
#define _DS1302_H_

#include< reg52.h >
#include<intrins.h>

sbit DS1302_SCLK = P3^6;
sbit DS1302_IO = P3^4;
sbit DS1302_CE = P3^5;

typedef struct {
	uint t_nian;
	uchar t_yue;
	uchar t_ri;
	uchar t_shi;
	uchar t_fen;
	uchar t_miao;
}DS_Data;

void DS1302_WW( uchar WW_com, uchar WW_dat);
uchar DS1302_WR( uchar WR_com );
uchar DS1302_R( void );
void DS1302_W( uchar W_dat );
void DS1302_init( void );

void DS1302_read_time( DS_Data *p );


#endif