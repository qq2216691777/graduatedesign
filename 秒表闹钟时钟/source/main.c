#include < reg52.h >
#include" DS1302.H "
#include" key.H "
#ifndef uchar
#define uchar unsigned char
#define uint unsigned int
#endif

sbit BEEP = P1^5;

sbit RS = P2^6;	   //lcd1602 位定义
sbit RW = P2^5;
sbit E = P2^7;
void LCD_1602_init( void );		//lcd1602初始化函数
void LCD_1602_Write_com( uchar w_com );	 // lcd1602 写指令函数
void LCD_1602_Write_data( uchar w_data );	 // lcd1602 写数据函数
void delay(uint y_time);			//延迟函数
void LCD_1602_Write_String( uchar h, uchar *w_data );
void LCD_1602_Write_Num( uchar h,uchar s, int num );
void lcd1602_display_time( DS_Data *p );
void display_now_time( DS_Data *p );
void lcd1602_display_date( DS_Data *p );
void timer0();
void alarm();
void stopwatch();
void alarm_check( DS_Data *p );
void set_time( DS_Data *p );
int sec = 0;

int main()
{
	DS_Data Now_date;
	delay(1000);
	BEEP = 0;
	LCD_1602_init();
	timer0();
	DS1302_init();	//时钟初始化
//	LCD_1602_Write_Num(2,25300);
	LCD_1602_Write_String( 1, "TIME: 2016-12-13");
	while(1)
	{
		if( key_scan() == 2 )
		{
			// 设置时间   和设置闹钟类似
			set_time( &Now_date );

			//miaobiao
			stopwatch();

			//set alarm
			alarm();


			LCD_1602_Write_String( 1, "TIME: 2016-12-13");
			LCD_1602_Write_String( 2, "                ");
		}
		display_now_time( &Now_date );
		alarm_check( &Now_date );
	
	}
}

void set_time( DS_Data *p )
{
	  uchar st_i;
	  uchar key_tmp;
	  uchar set_time_arr[8];
	  uchar set_time_arr2[8];

	  DS1302_WW( 0x8e, 0x00 );   //关闭写保护
	  DS1302_WW( 0x90, 0x01 );   //充电

	  set_time_arr[0] = p->t_nian/10;
	  set_time_arr[1] = p->t_nian%10;

	  set_time_arr[3] = p->t_yue/10;
	  set_time_arr[4] = p->t_yue%10;

	  set_time_arr[6] = p->t_ri/10;
	  set_time_arr[7] = p->t_ri%10;

	  set_time_arr2[0] = p->t_shi/10;
	  set_time_arr2[1] = p->t_shi%10;

	  set_time_arr2[3] = p->t_fen/10;
	  set_time_arr2[4] = p->t_fen%10;


	  LCD_1602_Write_String( 2, "SET: 2016-12-13");
	  LCD_1602_Write_String( 1, "                ");
	  LCD_1602_Write_com( 0x80+0X40+7);
	  LCD_1602_Write_data(set_time_arr[0] + 0x30);
	  LCD_1602_Write_data(set_time_arr[1] + 0x30);
	  LCD_1602_Write_data('-');
	  LCD_1602_Write_data(set_time_arr[3] + 0x30);
	  LCD_1602_Write_data(set_time_arr[4] + 0x30);
	  LCD_1602_Write_data('-');
	  LCD_1602_Write_data(set_time_arr[6] + 0x30);
	  LCD_1602_Write_data(set_time_arr[7] + 0x30);
	  LCD_1602_Write_com( 0x80+0X40+7);
	  LCD_1602_Write_com( 0x0f );
	  st_i = 0;
	  while(1)
	  {
		  key_tmp =  key_scan();
		  if( key_tmp ==2 )
		  	break;
		  else if( key_tmp ==4 )
		  {
			 st_i++;
			 if( (st_i ==2)||( st_i ==5 ) )
			 	st_i++;
			 if( st_i >7 )
			 	break;
			 LCD_1602_Write_com( 0x80+0X40+7+st_i);
		  }
		  else if( key_tmp ==1 )
		  {
				set_time_arr[st_i]++;
				set_time_arr[st_i] = set_time_arr[st_i]%10;	
				LCD_1602_Write_data(set_time_arr[st_i] + 0x30);
				LCD_1602_Write_com( 0x80+0X40+7+st_i);	
		  }
		  else if( key_tmp ==8 )
		  {
		  		set_time_arr[st_i] += 9;
				set_time_arr[st_i] = set_time_arr[st_i]%10;
				LCD_1602_Write_data(set_time_arr[st_i] + 0x30);
				LCD_1602_Write_com( 0x80+0X40+7+st_i);	
		  }
	  }
	  if( st_i>7 )
	  {
		  st_i = set_time_arr[1] | (set_time_arr[0]<<4);
		  DS1302_WW( 0x8c, st_i );	//初始化 年 
		  st_i = set_time_arr[4] | (set_time_arr[3]<<4);
		  DS1302_WW( 0x88, st_i );	//初始化 月 
		  st_i = set_time_arr[7] | (set_time_arr[6]<<4);
		  DS1302_WW( 0x86, st_i );	//初始化 日
	  }

	  LCD_1602_Write_String( 2, "  SET: 00:00      ");
	  LCD_1602_Write_com( 0x80+0X40+7);
	  LCD_1602_Write_data(set_time_arr2[0] + 0x30);
	  LCD_1602_Write_data(set_time_arr2[1] + 0x30);
	  LCD_1602_Write_data(':');
	  LCD_1602_Write_data(set_time_arr2[3] + 0x30);
	  LCD_1602_Write_data(set_time_arr2[4] + 0x30);
	  LCD_1602_Write_com( 0x80+0X40+7);
	  st_i = 0;
	  while(1)
	  {
		  key_tmp =  key_scan();
		  if( key_tmp ==2 )
		  	break;
		  else if( key_tmp ==4 )
		  {
			 st_i++;
			 if( (st_i ==2) )
			 	st_i++;
			 if( st_i >4 )
			 	break;
			 LCD_1602_Write_com( 0x80+0X40+7+st_i);
		  }
		  else if( key_tmp ==1 )
		  {
				set_time_arr2[st_i]++;
				set_time_arr2[st_i] = set_time_arr2[st_i]%10;
				LCD_1602_Write_data(set_time_arr2[st_i] + 0x30);
				LCD_1602_Write_com( 0x80+0X40+7+st_i);	
		  }
		  else if( key_tmp ==8 )
		  {
		  		set_time_arr2[st_i] += 9;
				set_time_arr2[st_i] = set_time_arr2[st_i]%10;
				LCD_1602_Write_data(set_time_arr2[st_i] + 0x30);
				LCD_1602_Write_com( 0x80+0X40+7+st_i);	
		  }
	  }
	  if( st_i>4 )
	  {
		  DS1302_WW( 0x80, 0x00 );	//初始化 秒 为 0
		  st_i = set_time_arr2[4] | (set_time_arr2[3]<<4);
		  DS1302_WW( 0x82, st_i );	//初始化 分 为 0
		  st_i = set_time_arr2[1] | (set_time_arr2[0]<<4);
		  DS1302_WW( 0x84, st_i );	//初始化 时 为 0  24h
	  }
		DS1302_WW( 0xc0, 0xf0 );
	DS1302_WW( 0x8e, 0x80 );
	   LCD_1602_Write_com( 0x0c );

}
void stopwatch_display_time( int sec )
{
	 LCD_1602_Write_com( 0x80+0x40);
	 LCD_1602_Write_String( 2, "     ");
	 LCD_1602_Write_com( 0x80+0x40 + 4);
	 LCD_1602_Write_data((sec/36000)/10+0x30);
	 LCD_1602_Write_data((sec/36000)%10+0x30);
	 LCD_1602_Write_data(':');
	 LCD_1602_Write_data((sec/600%60)/10+0x30);
	 LCD_1602_Write_data((sec/600%60)%10+0x30);
	 LCD_1602_Write_data(':');
	 LCD_1602_Write_data((sec/10%60)/10+0x30);
	 LCD_1602_Write_data((sec/10%60)%10+0x30);
	 LCD_1602_Write_data('.');
	 LCD_1602_Write_data(sec%10+0x30);
}
void stopwatch()
{
	 
	 uchar key_tmp;
	 uchar start_tmp=0;
 	 LCD_1602_Write_String( 1, "      READY     ");
	 sec = 0;
	 stopwatch_display_time(sec);
	 while(1)
	 {
	 	if( start_tmp )
			stopwatch_display_time(sec);
		
		key_tmp =  key_scan();
		if( key_tmp ==2 )
		{
			if( sec ==0 )
			{
				TR0 = 0;
				sec= 0;
				return ;
			}
			else
			{
				LCD_1602_Write_String( 1, "      READY     ");
				sec= 0;	
				stopwatch_display_time(sec);
				TR0 = 0;
			}
		}
		else if( key_tmp ==0x4 )
		{
			if( !start_tmp )
			{
				TR0 = 1;
				LCD_1602_Write_String( 1, "      go...     ");
				start_tmp = 1;
			}
			else
			{
			 	TR0 = 0;
				start_tmp = 0;
				LCD_1602_Write_String( 1, "      stop     ");
			}
		}
		
	 }
	 
}
uchar alarm_dat[3]={0,0,0};

void alarm_display_time( uchar *d )
{
	 LCD_1602_Write_com( 0x80+0x40 + 5);
	 if( d[2])
	 	   LCD_1602_Write_data('Y');
	 else
	 		LCD_1602_Write_data('N');
	 LCD_1602_Write_data(' ');
	 LCD_1602_Write_data(d[1]/10+0x30);
	 LCD_1602_Write_data(d[1]%10+0x30);
	 LCD_1602_Write_data(':');
	 LCD_1602_Write_data(d[0]/10+0x30);
	 LCD_1602_Write_data(d[0]%10+0x30);
	
}

void alarm_check( DS_Data *p )
{
 	if( alarm_dat[2] )
	{
	 	if( (alarm_dat[1] == p->t_shi)&&(alarm_dat[0] == p->t_fen) )
		{
			 BEEP = 1;
			 LCD_1602_Write_String( 1, "   alarm bell    ");
			 LCD_1602_Write_String( 2, "       ");
			 alarm_display_time(&alarm_dat);
			 while( !key_scan())
			 {
			 	BEEP = 1;
				Delay10ms();
			  	BEEP = 0;
				Delay10ms();
			 }
			 alarm_dat[2] = 0;
			 
			 LCD_1602_Write_String( 1, "TIME: 2016-12-13");
		}
	}
}
void alarm_set(  )
{
	 int i=0;
	 uchar key_tmp;
	 uchar tmp_val;
	 uchar alarm_tmpp[3]={0,0};
	 alarm_tmpp[0]=	alarm_dat[0];
	 alarm_tmpp[1]=	alarm_dat[1];
	 alarm_tmpp[2]=	alarm_dat[2];

	 LCD_1602_Write_com( 0x80+0x40 + 5);
	 LCD_1602_Write_com( 0x0f );
	 while(1)
	 {
		  do{
			key_tmp =  key_scan();
		}while(!(key_tmp&0xf));
		if( key_tmp==2 )
		{
			 LCD_1602_Write_com( 0x0c );
			 return ;
		}
		else if( key_tmp==4 )
		{
		 	//write
			i++;
			if( (i==1)||(i==4) )
				i++;
			if( i>6 )
			{
				alarm_dat[0]=	alarm_tmpp[0];
	 			alarm_dat[1]=	alarm_tmpp[1];
	 			alarm_dat[2]=	alarm_tmpp[2];
				LCD_1602_Write_com( 0x0c );
				return ;
			}
			LCD_1602_Write_com( 0x80+0x40 + 5 + i);
		}
		else if( key_tmp==1 )
		{
			  if( i==0 )
			  	alarm_tmpp[2] = ~alarm_tmpp[2];
			  else if( i==2 )
			  {
			   		tmp_val = alarm_tmpp[1]/10;
					tmp_val++;
					alarm_tmpp[1] = (tmp_val%3)*10 +  alarm_tmpp[1]%10;
			  }
			  else if( i==3 )
			  {
			   		tmp_val = alarm_tmpp[1]%10;
					tmp_val++;
					if( alarm_tmpp[1]/10 ==2 )	
						alarm_tmpp[1] = (alarm_tmpp[1]/10)*10 +  tmp_val%4;
					else
						alarm_tmpp[1] = (alarm_tmpp[1]/10)*10 +  tmp_val%10;
			  }else if( i==5 )
			  {
			   		tmp_val = alarm_tmpp[0]/10 + 6;
					tmp_val++;
					alarm_tmpp[0] = (tmp_val%6)*10 +  alarm_tmpp[0]%10;
			  }
			   else if( i==6 )
			  {
			   		tmp_val = alarm_tmpp[0]%10 + 40;
					tmp_val++;
					alarm_tmpp[0] = (alarm_tmpp[0]/10)*10 +  tmp_val%10;
			  }
		}
		else if( key_tmp==8 )
		{
			  if( i==0 )
			  	alarm_tmpp[2] = ~alarm_tmpp[2];
			  else if( i==2 )
			  {
			   		tmp_val = alarm_tmpp[1]/10 + 9;
					tmp_val--;
					alarm_tmpp[1] = (tmp_val%3)*10 +  alarm_tmpp[1]%10;
			  }
			   else if( i==3 )
			  {
			   		tmp_val = alarm_tmpp[1]%10 + 40;
					tmp_val--;
					if( alarm_tmpp[1]/10 ==2 )	
						alarm_tmpp[1] = (alarm_tmpp[1]/10)*10 +  tmp_val%4;
					else
						alarm_tmpp[1] = (alarm_tmpp[1]/10)*10 +  tmp_val%10;
			  }
			   else if( i==5 )
			  {
			   		tmp_val = alarm_tmpp[0]/10 + 6;
					tmp_val--;
					alarm_tmpp[0] = (tmp_val%6)*10 +  alarm_tmpp[0]%10;
			  }
			   else if( i==6 )
			  {
			   		tmp_val = alarm_tmpp[0]%10 + 40;
					tmp_val--;
					alarm_tmpp[0] = (alarm_tmpp[0]/10)*10 +  tmp_val%10;
			  }
		} 
		alarm_display_time(alarm_tmpp);
		LCD_1602_Write_com( 0x80+0x40 + 5 + i);

	 }
}

void alarm()
{
	 uchar key_tmp;
	 LCD_1602_Write_String( 1, "      alarm     ");
	 LCD_1602_Write_String( 2, "               ");
	 alarm_display_time( alarm_dat );
	 while(1)
	 {

		do{
			key_tmp =  key_scan();
		}while(!(key_tmp&0xf));
		if( key_tmp ==2 )
			return ;
		else if( key_tmp ==4)
		{
		 	//set alarm
			alarm_set( );	
		}
		alarm_display_time( alarm_dat );
	 }
}


void display_now_time( DS_Data *p )
{
	
	DS1302_read_time( p );
	lcd1602_display_time( p );
	lcd1602_display_date( p );
}


void LCD_1602_init( void )
{
	E = 0;
	RW = 0;
	LCD_1602_Write_com( 0x38 );
	delay(5);
	LCD_1602_Write_com( 0x0c );
	delay(5);
	LCD_1602_Write_com( 0x06 );
	delay(5);
	LCD_1602_Write_com( 0x01 );
	delay(5);
	LCD_1602_Write_com( 0x02 );
	delay(5);
	LCD_1602_Write_com( 0x80 );
	delay(5);

}

void LCD_1602_Write_com( uchar w_com )
{
	RS = 0;
	RW = 0;
	P0 = w_com;
	delay( 5 );
	E = 1;
	delay( 5 );
	E = 0;


}

void LCD_1602_Write_data( uchar w_data )
{
	RS = 1;
	RW = 0;
	P0 = w_data;
	delay( 5 );
	E = 1;
	delay( 5 );
	E = 0;
}
void LCD_1602_Write_String( uchar h, uchar *w_data )
{
	uchar i = 0;
	LCD_1602_Write_com( 0x80 );
	if( h & 0x2 )
	{
		 LCD_1602_Write_com( 0x80+0x40 );
	}
	while( w_data[i] != '\0')
		LCD_1602_Write_data(w_data[i++]);
}

void LCD_1602_Write_Num( uchar h, uchar s,int num )
{
	uchar i = 0;
	char num_temp[16]={0};
	LCD_1602_Write_com( 0x80 + s );
	if( h & 0x2 )
	{
		 LCD_1602_Write_com( 0x80+0x40 + s);
	}
	i=0;
	while( (num!=0))
	{
		 num_temp[i] = num%10 + 0x30;
		 i++;
		 num = num/10;
	}
	if( i == 0 )
	{
		   LCD_1602_Write_data(0x30);
	}
	while( i )
	{
		LCD_1602_Write_data(num_temp[i-1]);
		i--;	
	}

}

void delay(uint y_time)
{
		y_time*=14;
		while(y_time--);
}

void lcd1602_display_time( DS_Data *p )
{
	LCD_1602_Write_com( 0x80+0x40+4);
	LCD_1602_Write_data(p->t_shi/10 + 0x30);
	LCD_1602_Write_data(p->t_shi%10 + 0x30);
	LCD_1602_Write_data(':');
	LCD_1602_Write_data(p->t_fen/10 + 0x30);
	LCD_1602_Write_data(p->t_fen%10 + 0x30);
	LCD_1602_Write_data(':');
	LCD_1602_Write_data(p->t_miao/10 + 0x30);
	LCD_1602_Write_data(p->t_miao%10 + 0x30);
}

void lcd1602_display_date( DS_Data *p )
{
	LCD_1602_Write_com( 0x80+8);

	LCD_1602_Write_data(p->t_nian/10%10 + 0x30);
	LCD_1602_Write_data(p->t_nian%10 + 0x30);
	LCD_1602_Write_data('-');
	LCD_1602_Write_data(p->t_yue/10 + 0x30);
	LCD_1602_Write_data(p->t_yue%10 + 0x30);
	LCD_1602_Write_data('-');
	LCD_1602_Write_data(p->t_ri/10 + 0x30);
	LCD_1602_Write_data(p->t_ri%10 + 0x30);
}

void timer0()
{
 	TMOD = 0X01;
	TH0 = (65536-45872)/256;
	TL0 = (65536-45872)%256;
	EA = 1;
	ET0 = 1;
	TR0 = 0;
}
void to_t() interrupt 1
{
	static uchar i_n=0;
	TH0 = (65536-45872)/256;
	TL0 = (65536-45872)%256;
	i_n ++;
	if( i_n>1 )
	{
		sec++;
	 	i_n = 0;	
	}
}


