#include < reg52.h >
#include "key.h"

sbit KEY1 = P3^0;
sbit KEY2 = P3^1;
sbit KEY3 = P3^2;
sbit KEY4 = P3^3;

void Delay10ms()		//@11.0592MHz
{
	unsigned char i, j;

	i = 18;
	j = 235;
	do
	{
		while (--j);
	} while (--i);
}

uchar key_scan()
{
	uchar key_value=0;
	static uchar pressed = 0;

	KEY2 = 1;
	if( !KEY2 )
	{
		Delay10ms();
		if( !KEY2 )
		{
			if( !(pressed&0x2) )
				key_value  |= 0x2;
			pressed |= 0x2;
		}else
			pressed &= ~0x2;
	}else
			pressed &= ~0x2;


	KEY1 = 1;
	if( !KEY1 )
	{
		Delay10ms();
		if( !KEY1 )
		{
			if( !(pressed&0x1) )
				key_value  |= 0x1;
			pressed |= 0x1;
		}else
			pressed &= ~0x1;
	}else
			pressed &= ~0x1;

	

	KEY3 = 1;
	if( !KEY3 )
	{
		Delay10ms();
		if( !KEY3 )
		{
			if( !(pressed&0x4) )
				key_value  |= 0x4;
			pressed |= 0x4;
		}else
			pressed &= ~0x4;
	}else
			pressed &= ~0x4;

	KEY4 = 1;
	if( !KEY4 )
	{
		Delay10ms();
		if( !KEY4 )
		{
			if( !(pressed&0x8) )
				key_value  |= 0x8;
			pressed |= 0x8;
		}else
			pressed &= ~0x8;
	}else
			pressed &= ~0x8;
	return 	key_value;
}




