#include" DS1302.H "



void DS1302_init( void )
{
	DS1302_WW( 0x8e, 0x00 );   //关闭写保护
	DS1302_WW( 0x90, 0x01 );   //充电
	
	if( DS1302_WR( 0xc1 ) != 0xf0 )
	{
		DS1302_WW( 0x80, 0x00 );	//初始化 秒 为 0
		DS1302_WW( 0x82, 0x23 );	//初始化 分 为 0
		DS1302_WW( 0x84, 0x20 );	//初始化 时 为 0  24h
		DS1302_WW( 0x86, 0x12 );	//初始化 天 为 1
		DS1302_WW( 0x88, 0x02 );	//初始化 月 为 1
		DS1302_WW( 0x8a, 0x04 );	//初始化 星期 为 1
		DS1302_WW( 0x8c, 0x15 );	//初始化 年 为 2015
	}
	DS1302_WW( 0xc0, 0xf0 );
	DS1302_WW( 0x8e, 0x80 );

}

/******从寄存器中读取数据**********/
uchar DS1302_WR( uchar addr )
{
	uchar n,dat,dat1;
	DS1302_CE = 0;
	_nop_();

	DS1302_SCLK = 0;//先将SCLK置低电平。
	_nop_();
	DS1302_CE = 1;//然后将RST(CE)置高电平。
	_nop_();

	for(n=0; n<8; n++)//开始传送八位地址命令
	{
		DS1302_IO = addr & 0x01;//数据从低位开始传送
		addr >>= 1;
		DS1302_SCLK = 1;//数据在上升沿时，DS1302读取数据
		_nop_();
		DS1302_SCLK = 0;//DS1302下降沿时，放置数据
		_nop_();
	}
	_nop_();
	for(n=0; n<8; n++)//读取8位数据
	{
		dat1 = DS1302_IO;//从最低位开始接收
		dat = (dat>>1) | (dat1<<7);
		DS1302_SCLK = 1;
		_nop_();
		DS1302_SCLK = 0;//DS1302下降沿时，放置数据
		_nop_();
	}

	DS1302_CE = 0;
	_nop_();	//以下为DS1302复位的稳定时间,必须的。
	DS1302_SCLK = 1;
	_nop_();
	DS1302_IO = 0;
	_nop_();
	DS1302_IO = 1;
	_nop_();
	return dat;	
}

/*********从寄存器写入数据******************/
void DS1302_WW( uchar addr, uchar dat)
{
	uchar n;
	DS1302_CE = 0;
	_nop_();

	DS1302_SCLK = 0;//先将SCLK置低电平。
	_nop_();
	DS1302_CE = 1; //然后将RST(CE)置高电平。
	_nop_();

	for (n=0; n<8; n++)//开始传送八位地址命令
	{
		DS1302_IO = addr & 0x01;//数据从低位开始传送
		addr >>= 1;
		DS1302_SCLK = 1;//数据在上升沿时，DS1302读取数据
		_nop_();
		DS1302_SCLK = 0;
		_nop_();
	}
	for (n=0; n<8; n++)//写入8位数据
	{
		DS1302_IO = dat & 0x01;
		dat >>= 1;
		DS1302_SCLK = 1;//数据在上升沿时，DS1302读取数据
		_nop_();
		DS1302_SCLK = 0;
		_nop_();	
	}	
		 
	DS1302_CE = 0;//传送数据结束
	_nop_();
}

void DS1302_read_time( DS_Data *p )
{
	uchar tmp;
	tmp =  DS1302_WR( 0x8d );
	p->t_nian = (( tmp>>4)&0xf )*10 + (tmp&0x0f);
    tmp =  DS1302_WR( 0x89 );
	p->t_yue = 	(tmp>>4)*10+ tmp&0x0f;
	tmp =  DS1302_WR( 0x87 );
	p->t_ri = (( tmp>>4)&0x3 )*10+ (tmp&0x0f);
	tmp =  DS1302_WR( 0x85 );
	if( tmp & 0x80 )
		p->t_shi = (( tmp>>5)&0x1 )*12 +(tmp&0x1f);
	else
		p->t_shi = (( tmp>>4)&0x3 )*10 + (tmp&0x0f);
	tmp =  DS1302_WR( 0x83 );
	p->t_fen = (( tmp>>4)&0x7 )*10 + (tmp&0x0f);
	tmp =  DS1302_WR( 0x81 );
	p->t_miao =  (( tmp>>4)&0x7 )*10 + (tmp&0x0f);   //(tmp&0x0f)+
}
