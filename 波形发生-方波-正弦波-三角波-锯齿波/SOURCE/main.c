#include <reg52.h>
#include <sin_arry.h>

#define uchar unsigned char
#define uint unsigned int

/* 数码管编码 0~9 P 共10个 */
uchar num[] = { 0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f,0x73};

/* 数码管显示的数值变量 */
uchar display_num = 100;

/* 波形种类 0:正弦波  1:方波 2:锯齿波 3:三角波  */
uchar wave_kind = 0;

/* 控制频率变量 0对应100Hz  9对应10hz  */
uint HZ_time = 0;


uchar wave_t = 100; 

/**
  *·定时器0的初始化函数
  * 用于数码管的动态扫描
**/
void Time0_Init()
{
	TMOD &= 0XF0;
	TMOD |= 0X01;		//设置定时器0的工作方式为1
	TH0 = (65536-10000)/256;	  //装初值  定时10ms  @12Mhz
	TL0 = (65536-10000)%256;

	EA = 1;
	ET0 = 1;
	TR0 = 1;
}
/**
  *·定时器1的初始化函数
  * 用于控制波形的周期
**/
void Time1_Init()
{
	TMOD &= 0XF0;
	TMOD |= 0X20;		//设置定时器1的工作方式为2
	TH1 = 256-145;//(65536-100)/256;	  //装初值  定时100us  @12Mhz
	TL1 = 256-145;//(65536-100)%256;	   //这两个地方应该是 256-100的  可是在示波器上看到的输出波形周期不对 我就修改了  这里应该是仿真软件的问题

	EA = 1;
	ET1 = 1;
	TR1 = 1;
}

/**
 * 数码管扫描函数
**/
void digitron_scan()
{
	static uchar dula=1;

	dula = dula<<1;
	if( dula > 11)
		dula = 1;
	
	P2 |= 0x0f;
	P2 &=  ~dula;
	switch( dula )
	{
	 	case 0x01:
			P0 = num[10];
			break;

		case 0x02:
			P0 = num[display_num/100];
		    break;

		case 0x04:
			P0 = num[display_num%100/10];
			break;

		case 0x08:
			P0 = num[display_num%10];
		    break;
	}


}
/* 三角波 */
void triangular_Wave()
{
	if( wave_t > 50 )
		P1 = 100-wave_t;
	else
		P1 = wave_t;	
}
/* 锯齿波 */
void sawtooth_Wave()
{
	P1 = wave_t;	
}

/* 方波 */
void square_Wave()
{
	 if( wave_t > 50 )
		P1 = 100;
	else
		P1 = 0;
}

/* 正弦波 */
void sinusoidal_Wave()
{
	
    P1 = my_sin[wave_t];
}

void delay()
{
 	int x=1000;
	while(x--);
}

sbit cs = P3^7;
sbit wr = P3^6;
sbit LE1 = P3^5;

sbit K1 = P2^5;
sbit K2 = P2^6;
sbit K3 = P2^7;
/**
  *·按键扫描化函数
**/
void key_scan()
{
 	if( !K1 )
	{
	   delay();
	   if( !K1 )
	   {
	   		HZ_time++; //频率加10
			HZ_time = HZ_time%10;
			display_num = 100 - (HZ_time*10);
	   }
		while(!K1);
	}
	else if( !K3 )
	{
	   delay();
	   if( !K3 )
	   {
	   		HZ_time += 10;
	   		HZ_time--; //频率减10
			HZ_time = HZ_time%10;
			display_num = 100 - (HZ_time*10);
	   }
		while(!K3);
	}
	else if( !K2 )
	{
	   delay();
	   if( !K2 )
	   	wave_kind++; //切换波形
	   while(!K2);
	}
}

void main()
{
	Time0_Init();
	Time1_Init();
	cs = 0;
	wr = 0;
   	LE1 = 1;

	while(1)
	{
		switch(wave_kind)
		{
			case 0:
				sinusoidal_Wave();
				break;
			case 1:
				square_Wave();
				break;
			case 2:
				sawtooth_Wave();
				break;
			case 3:
				triangular_Wave();
				break;
			default :
				wave_kind = wave_kind%4;
				break;		
		}
		key_scan();
	}

}



void Time0_handler() interrupt 1
{
	TH0 = (65536-20000)/256;	  //装初值  定时20ms  @12Mhz
	TL0 = (65536-20000)%256;

	digitron_scan();
	

}

void Time1_handler() interrupt 3
{
 	static uchar H_time = 0;
	if( H_time )
	{
		H_time--;	
		return ;	
	}
	else
	{
	   H_time = HZ_time;
	}
	wave_t++;
	if( wave_t > 100 )
		wave_t = 0;


}