#include <reg52.h>

#define uchar unsigned char
#define uint unsigned int

sbit LED1 = P0^0;
sbit LED2 = P0^1;
sbit LED3 = P0^2;
sbit LED4 = P0^3;
sbit LED5 = P0^4;
sbit LED6 = P0^5;
sbit LED7 = P0^6;
sbit LED8 = P0^7;
sbit LED9 = P2^0;
sbit LED10 = P2^1;
sbit LED11 = P2^2;
sbit LED12 = P2^3;
sbit LED13 = P2^4;
sbit LED14 = P2^5;
sbit LED15 = P2^6;
sbit LED16 = P2^7;
sbit LED17= P3^4;
sbit LED18= P3^5;
sbit LED19= P3^6;
sbit LED20= P3^7;


void Delay10ms()		//@12.000MHz
{
	unsigned char i, j;

	i = 20;
	j = 113;
	do
	{
		while (--j);
	} while (--i);
}


void Delay100ms()		//@12.000MHz
{
	unsigned char i, j;

	i = 195;
	j = 138;
	do
	{
		while (--j);
	} while (--i);
}




/**
  *·定时器0的初始化函数
  * 用于数码管的动态扫描
**/
void Time0_Init()
{
	TMOD &= 0XF0;
	TMOD |= 0X01;		//设置定时器0的工作方式为1
	TH0 = (65536-10000)/256;	  //装初值  定时10ms  @12Mhz
	TL0 = (65536-10000)%256;

	EA = 1;
	ET0 = 1;
	TR0 = 1;
}
/**
  *·定时器1的初始化函数
  * 用于
**/
void Time1_Init()
{
	TMOD &= 0XF0;
	TMOD |= 0X10;		//设置定时器1的工作方式为1
	TH1 = (65536-50000)/256;	  //装初值  定时50ms  @12Mhz
	TL1 = (65536-50000)%256;	   

	EA = 1;
	ET1 = 1;
	TR1 = 1;
}




/**
  *·按键扫描化函数
**/
uchar key_scan()
{
	P1 = 0XFF;;;
	if(0xff!=P1)
	{
		P1 = 0XFF;
	 	Delay10ms();
		switch(P1)
		{
			case 0xfe: while(0xff!=P1)P1 = 0XFF; return 1; break;
			case 0xfd: while(0xff!=P1)P1 = 0XFF; return 2; break;
			case 0xfb: while(0xff!=P1)P1 = 0XFF; return 3; break;
			case 0xf7: while(0xff!=P1)P1 = 0XFF; return 4; break;
			case 0xef: while(0xff!=P1)P1 = 0XFF; return 5; break;
			case 0xdf: while(0xff!=P1)P1 = 0XFF; return 6; break;
			case 0xbf: while(0xff!=P1)P1 = 0XFF; return 7; break;
			case 0x7f: while(0xff!=P1)P1 = 0XFF; return 8; break;				
		 	default:	break;
		}
			
	}

	return 0;
}

void Delay500ms()		//@12.000MHz
{
	unsigned char i, j, k;

	i = 4;
	j = 205;
	k = 187;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}


uchar led1316Mode = 0;
uchar led1417Mode = 0;
uchar led1518Mode = 0;

void main()
{
	uchar keyValue = 0;

	/* 设置led初始化状态 */
	LED2 = 0;
	LED4 = 0;
	LED6 = 0;
	LED8 = 0;
	LED10 = 0;
	LED12 = 0;

	Time1_Init();	//定时器1初始化  50ms一次中断 计时

	while(1)
	{
	 	keyValue = key_scan();
		
		switch(keyValue) 
		{
			case 1:
			case 2:
				if(!(LED2||LED8))
				{
				 	LED13 = 0;
					LED16 = 0;
					led1316Mode |= 1;
				}

				if(!(LED1||LED7))
				{
					if(!(LED3||LED9))	   //当 1或2号触发器触发，若1和7号灯亮，3和9号灯亮，则15和18号灯亮，5和11号灯亮，6和12号灯灭；
					{
					 	LED15 = 0;
						LED18 = 0;
						LED5 = 0;
						LED11 = 0;
						LED6 = 1;
						LED12 = 1;
					}
				 	else			  //当 1或2号触发器触发，若1和7号灯亮，则14和17号灯亮，然后3和9号灯亮,4和10号灯灭；
					{
					   	LED14 = 0;
						LED17 = 0;
						led1417Mode |= 0x01;
					}
				} 
				break;
			case 3:
				LED13 = 0;
				LED16 = 0;
				led1316Mode |= 2;
				break;
			case 4:
				if(led1316Mode&0x02 )
				{
				 	LED1 = 1;
					LED7 = 1;

					LED2 = 0;
					LED8 = 0;
					led1316Mode &= 0xfd;
				} 
				break;
			case 5:
				LED14 = 0;
				LED17 = 0;
				led1417Mode |= 2;
				break;
			case 6:
				if(led1417Mode&0x02 )
				{
				 	LED3 = 1;
					LED9 = 1;

					LED4 = 0;
					LED10 = 0;
					led1417Mode &= 0xfd;
				}
				break;
			case 7:
				LED15 = 0;
				LED18 = 0;
				led1518Mode |= 2;
				break;
			case 8:
				if(led1518Mode&0x02 )
				{
				 	LED5 = 1;
					LED11 = 1;

					LED6 = 0;
					LED12 = 0;
					led1518Mode &= 0xfd;
				} 
				break;
		 	default: break;
		}
		
		if(!(LED1||LED3||LED5||LED7||LED9||LED11))	   //若1,3,5,7,9,11号灯全亮，则19和20号灯亮；
		{
		 	LED19 = 0;
			LED20 = 0;
		}
		else
		{
			LED19 = 1;
			LED20 = 1;
		}	
	}	

}



void Time0_handler() interrupt 1
{
	TH0 = (65536-20000)/256;	  //装初值  定时20ms  @12Mhz
	TL0 = (65536-20000)%256;
	

}

int led1316s10=0;
int led1417s10=0;
int led1518s10=0;

void Time1_handler() interrupt 3
{
   	TH1 = (65536-50000)/256;	  //装初值  定时50ms  @12Mhz
	TL1 = (65536-50000)%256;	

  	if(!LED13)
	{
	 	led1316s10++;
		if(led1316s10>200)
		{
		 	LED13 = 1;
			LED16 = 1;
		   	if(led1316Mode&0x01 )
			{
				LED1 = 0;
				LED7 = 0;

				LED2 = 1;
				LED8 = 1;
				led1316Mode &= 0xfe;
			}

			led1316s10 = 0;
		}	
	}
	else
		led1316s10 = 0;

	if(!LED14)
	{
	 	led1417s10++;
		if(led1417s10>200)
		{
		 	LED14 = 1;
			LED17 = 1;

		   	if(led1417Mode&0x01)
			{
				LED3 = 0;
				LED9 = 0;

				LED4 = 1;
				LED10 = 1;
				led1417Mode &= 0xfe;
			}

			led1417s10 = 0;
		}	
	}
	else
		led1417s10 = 0;

	if(!LED15)
	{
	 	led1518s10++;
		if(led1518s10>200)
		{

			LED15 = 1;
			LED18 = 1;

			led1518s10 = 0;
		}	
	}
	else
		led1518s10 = 0;
}