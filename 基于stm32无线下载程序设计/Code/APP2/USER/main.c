#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"

#include "usart.h"	
#include "usmart.h"	 
#include "rtc.h" 
//ALIENTEK战舰STM32开发板实验15
//RTC实时时钟实验  
//技术支持：www.openedv.com
//广州市星翼电子科技有限公司  
 int main(void)
 {	 
 	u8 t=0;	
 
  	SCB->VTOR = FLASH_BASE | 0x10000; /* Vector Table Relocation in Internal FLASH. */

	delay_init();	    	 //延时函数初始化	  
	NVIC_Configuration(); 	 //设置NVIC中断分组2:2位抢占优先级，2位响应优先级
	uart_init(115200);	 	//串口初始化为9600
 	LED_Init();			     //LED端口初始化
		 	
	usmart_dev.init(SystemCoreClock/1000000);	//初始化USMART	
	RTC_Init();	  			//RTC初始化
	/*POINT_COLOR=RED;//设置字体为红色 
	LCD_ShowString(60,50,200,16,16,"Mini STM32");	
	LCD_ShowString(60,70,200,16,16,"RTC TEST");	
	LCD_ShowString(60,90,200,16,16,"ATOM@ALIENTEK");
	LCD_ShowString(60,110,200,16,16,"2012/9/7");	
	//显示时间
	POINT_COLOR=BLUE;//设置字体为蓝色
	LCD_ShowString(60,130,200,16,16,"    -  -  ");	   
	LCD_ShowString(60,162,200,16,16,"  :  :  ");		*/  
	printf("RTC test\r\n");	 
	LED0 = 1;
	LED1 = 1;
	while(1)
	{								    
		if(t!=calendar.sec)
		{
			t=calendar.sec;
			/*LCD_ShowNum(60,130,calendar.w_year,4,16);									  
			LCD_ShowNum(100,130,calendar.w_month,2,16);									  
			LCD_ShowNum(124,130,calendar.w_date,2,16);	 */
			printf("这是测试APP2 sec=%d\r\n",t);
			LED0=!LED0;
		}	
		delay_ms(10);								  
	};  
 }

