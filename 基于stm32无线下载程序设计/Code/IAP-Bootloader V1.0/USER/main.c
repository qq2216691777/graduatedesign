#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "usart.h"
#include "stmflash.h"
#include "iap.h"
//ALIENTEK战舰STM32开发板实验48
//IAP实验 Bootloader V1.0 代码 
//技术支持：www.openedv.com
//广州市星翼电子科技有限公司 

unsigned char runAPP( void );
unsigned char updateAPP( void );


u8 t;
u8 key=0;
u16 oldcount=0;	//老的串口接收数据值
u16 applenth=0;	//接收到的app代码长度
u8 clearflag=0;

	
int main(void)
{		 
	

	uart_init(115200);	//串口初始化为 115200
	delay_init();	   	 	//延时初始化 
	
	delay_ms(1000);
	
	LED_Init();		  		//初始化与LED连接的硬件接口
 	KEY_Init();				//按键初始化

	if( KEY1 && KEY2 )	
		runAPP();
	
	printf("等待下载数据...\r\n");
	printf("按KEY继续执行app \r\n");
	while( !key )
	{
		key=KEY_Scan(0);
		delay_ms(10);
	}
	

	while(1)
	{
	 	if(USART_RX_CNT)
		{
			if(oldcount==USART_RX_CNT)//新周期内,没有收到任何数据,认为本次数据接收完成.
			{
				applenth=USART_RX_CNT;
				oldcount=0;
				USART_RX_CNT=0;
				printf("用户程序接收完成!\r\n");
				printf("代码长度:%dBytes\r\n",applenth);
				
				updateAPP();
				runAPP();
				
			}else oldcount=USART_RX_CNT;			
		}
		delay_ms(10);
		if(t==30)
		{
			LED0=!LED0;
			t=0;
			if(clearflag)
			{
				clearflag--;	
			}
		}
	  	 
		key=KEY_Scan(0);
		if( key )
		{
			runAPP();
		}		 
	}   	   
}

unsigned char runAPP( void )
{
		unsigned char res;
	
		printf("开始执行app\r\n\r\n");
		if(((*(vu32*)(FLASH_APP1_ADDR+4))&0xFF000000)==0x08000000)//判断是否为0X08XXXXXX.
		{	 
			iap_load_app(FLASH_APP1_ADDR);//执行FLASH APP代码
			res = 1;
		}else 
		{
			printf("非FLASH应用程序,无法执行!\r\n");
			res = 0;
			
		}									 
		clearflag=7;//标志更新了显示,并且设置7*300ms后清除显示
		return res;
}


unsigned char updateAPP( void )
{
		unsigned char res;
		if(applenth)
		{
				printf("开始更新固件...\r\n");	
				printf("Copying APP2FLASH...");
				if(((*(vu32*)(0X20001000+4))&0xFF000000)==0x08000000)//判断是否为0X08XXXXXX.
				{	 
						iap_write_appbin(FLASH_APP1_ADDR,USART_RX_BUF,applenth);//更新FLASH代码   
						delay_ms(10);

						printf("固件更新完成!\r\n");	
						res = 1;
				}else 
				{
						printf("非FLASH应用程序!\r\n");
						res = 0;
				}
		}else 
		{
				printf("没有可以更新的固件!\r\n");
				res = 0;

		}
		clearflag=7;//标志更新了显示,并且设置7*300ms后清除显示	
		
		return res;
}













