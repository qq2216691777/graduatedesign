#include <reg52.h>
#include <intrins.h>	 //			 包含头文件

#define uint            unsigned int
#define uchar           unsigned char
#define ulong           unsigned long	 //宏定义
#define LCD_DATA        P0				 //定义P0口为LCD_DATA

sbit LCD_RS =P2^5;
sbit LCD_RW =P2^6;
sbit LCD_E  =P2^7;						 //定义LCD控制引脚

sbit DQ1=P1^6;	                         //ds18b20与单片机连接口

sbit jiare =P1^7 ;					 /*加热控制阀*/
sbit speaker =P2^4;						 //蜂鸣器引脚定义
sbit jinshui = P3^7;				 /*进水控制阀*/

sbit ledY = P3^5;
sbit ledR = P3^6;

void delay5ms(void);   //误差 0us
void LCD_WriteData(uchar LCD_1602_DATA);	 /********LCD1602数据写入***********/
void LCD_WriteCom(uchar LCD_1602_COM);		 /********LCD1602命令写入***********/
void lcd_1602_word(uchar Adress_Com,uchar Num_Adat,uchar *Adress_Data); /*1602字符显示函数，变量依次为字符显示首地址，显示字符长度，所显示的字符*/
void InitLcd();//液晶初始化函数

void Tim_Init();

uchar Key_Change;
uchar Key_Value;		//按键键值
uchar View_Con;			//设置的位（0正常工作，1设置上限，2设置下限）
uchar View_Change;
char chengke_numb=0;
uchar flag1=0;
uchar flag2=0;
uchar flag_up=0;
uchar flag_down=0;


int Tbuf;           //采集到的温度值
uchar Txianshi[2];	//温度显示数组
uchar i=0;
uchar Tbuf1=20;
uchar TemMax=50;		  //温度阈值

uchar Wbuf=1;           //采集到水位
uchar WMax=4;		//水位阈值

uchar new=0x03;

uchar Change = 0;	//0:修改温度阈值  1:修改水位阈值


void Delay10ms()		//@11.0592MHz
{
	unsigned char i, j;

	i = 18;
	j = 235;
	do
	{
		while (--j);
	} while (--i);
}


unsigned char getKeyValue();	//获取按键值
unsigned char getWaterPos();	//获取水位


void main()	  //主函数
{
	uchar wPos = 1;		//默认当前水位是1
	speaker=1;			 //关闭蜂鸣器
	jiare=0;			  //关闭加热
	
	InitLcd();
	Tim_Init();
	
	TR0=1;
	TR1=1;				  //打开定时器
 	lcd_1602_word(0xC0,16,"Temperatue:  /");
	lcd_1602_word(0x80,16,"water level:  /");
	ledY = 0;
	while(1)			  //进入循环
	{
		wPos = getWaterPos();	  //获取水位
		if(wPos)
		{
			Wbuf=wPos;
			new |= 0x02;
		}
		switch(getKeyValue())	 //获取按键值
		{
			case 'U':
				if(Change)
				{
					new |= 0x02;
					WMax++;
				}
				else
				{
					new |= 0x01;
					TemMax++;
				}
				break;
			case 'D':
				if(Change)
				{
					new |= 0x02;
					WMax--;
				}
				else
				{
					new |= 0x01;
					TemMax--;
				}
				break;
			case 'T': Change=0;	break;
			case 'W': Change=1;	break;
		 	default: break;
		}

		if( TemMax>100 )
			TemMax = 100;
		else if( TemMax<1 )
			TemMax = 1;

		if( WMax>6 )
			WMax = 6;
		else if( WMax<1 )
			WMax = 1;
			

		if(Tbuf>TemMax)		//判断温度阈值
			jiare=0;
		else
			jiare=1;

		if(Wbuf>WMax)
		{
			speaker=0;
			ledY = 1;
			ledR = 0;
		}
		else
		{
			speaker=1;
			ledY = 0;
			ledR = 1;
		}

		if(Wbuf>3)
			jinshui=0;
		else
			jinshui=1;
		
		if(new&0x01)   //
		{
			Txianshi[0]=Tbuf1%100/10+0x30;
			Txianshi[1]=Tbuf1%10+0x30;
			
			lcd_1602_word(0xCB,2,Txianshi);

			Txianshi[0]=TemMax%100/10+0x30;
			Txianshi[1]=TemMax%10+0x30;		
			lcd_1602_word(0xCe,2,Txianshi);
			new &= 0xfe;
		}

		if(new&0x02)
		{
			Txianshi[0]=Wbuf%10+0x30;
			
			lcd_1602_word(0x8d,1,Txianshi);

			Txianshi[0]=WMax%10+0x30;		
			lcd_1602_word(0x8f,1,Txianshi);
			new &= 0xfd;
		}
	}
	
	
}

unsigned char getKeyValue()
{

	P3 |= 0X0F;
	if( (P3&0X0F)!= 0X0F )
	{
		Delay10ms();
		switch( P3&0X0F )
		{
		 	case 0x0e:	 while((P3&0X0F)!= 0X0F) P3 |= 0X0F; return 'T'; break;  //温度键被按下
			case 0x0d:   while((P3&0X0F)!= 0X0F) P3 |= 0X0F; return 'W'; break;  //水位键被按下
			case 0x0b:   while((P3&0X0F)!= 0X0F) P3 |= 0X0F; return 'U'; break;  //上键被按下
			case 0x07:   while((P3&0X0F)!= 0X0F) P3 |= 0X0F; return 'D'; break;  //下键被按下	
			default: break;		
		}
	}
	return 0;
}

unsigned char getWaterPos()
{
	P1 |= 0X3F;
	if( !(P1&0x01) )
		return 6;
	if( !(P1&0x02) )
		return 5;
	if( !(P1&0x04) )
		return 4;
	if( !(P1&0x08) )
		return 3;
	if( !(P1&0x10) )
		return 2;
	if( !(P1&0x20) )
		return 1;
			
	return 0;
}


void Time1() interrupt 3		//定时器1服务函数:判断按键是否按下，以及按下的是哪个键
{
 static uchar Key_Con;
 TH1=0xd8;		   //10ms
 TL1=0xf0;		   //重新赋初值

 
}

/**定时器T0工作函数**/
void Time0() interrupt 1
{


 TH0=0xfc;		   //1ms
 TL0=0x18;		   //重新赋初值

 i++;
 if(i==10)
 {
	   
       Tbuf=read_temp();  //读取温度
	   if(Tbuf1!=Tbuf)
	   {
	       new |=0x01;
	   	}
		
	   Tbuf1=Tbuf;
 	   i=0;
	   
 }
}
/**定时器初始化函数**/
void Tim_Init()
{
 EA=1;			  //打开中断总开关
 ET0=1;			  //打开T0中断允许开关
 ET1=1;			  //打开T1中断允许开关
 TMOD=0x11;		  //设定定时器状态
 TH0=0xfc;		   //1ms
 TL0=0x18;		   //赋初值
 
 TH1=0xd8;		   //10ms
 TL1=0xf0;		   //赋初值
}
/**在指定地址显示指定数量的指定字符**/
/**Adress_Com显示地址，Num_Adat显示字符数量，Adress_Data显示字符串内容**/ 
void lcd_1602_word(uchar Adress_Com,uchar Num_Adat,uchar *Adress_Data)
{
 uchar a=0;
 uchar Data_Word;
 LCD_WriteCom(Adress_Com); //选中地址
 for(a=0;a<Num_Adat;a++)   //for循环决定显示字符个数
  {
   Data_Word=*Adress_Data;	  //读取字符串数据
   LCD_WriteData(Data_Word);  //显示字符串
   Adress_Data++;			  //显示地址加一
  }
}

/***************1602函数*******************/
void LCD_WriteData(uchar LCD_1602_DATA)	 /********LCD1602数据写入***********/
{
 delay5ms();  //操作前短暂延时，保证信号稳定
 LCD_E=0;
 LCD_RS=1;
 LCD_RW=0;
 _nop_();
 LCD_E=1;
 LCD_DATA=LCD_1602_DATA;
 LCD_E=0;
 LCD_RS=0;
}

/********LCD1602命令写入***********/
void LCD_WriteCom(uchar LCD_1602_COM)
{
 delay5ms();//操作前短暂延时，保证信号稳定
 LCD_E=0;
 LCD_RS=0;
 LCD_RW=0;
 _nop_();
 LCD_E=1;
 LCD_DATA=LCD_1602_COM;
 LCD_E=0;
 LCD_RS=0;
}


void InitLcd()		   //初始化液晶函数
{
 delay5ms();
 delay5ms();
 LCD_WriteCom(0x38); //display mode
 LCD_WriteCom(0x38); //display mode
 LCD_WriteCom(0x38); //display mode
 LCD_WriteCom(0x06); //显示光标移动位置
 LCD_WriteCom(0x0c); //显示开及光标设置
 LCD_WriteCom(0x01); //显示清屏
 delay5ms();
 delay5ms();
}

void delay5ms(void)   //5ms延时函数
{
    unsigned char a,b;
    for(b=185;b>0;b--)
        for(a=12;a>0;a--);
}


 /*******DS18B20测温程序************/
 void delay_18B20(unsigned int i)//延时1微秒
{
 	while(i--);
}

void ds1820rst() //ds1820复位
{  
	DQ1 = 1;           //DQ复位
	delay_18B20(4);    //延时
	DQ1 = 0;           //DQ拉低
	delay_18B20(100);  //精确延时大于480us
	DQ1 = 1;           //拉高
	delay_18B20(40);	 
}  
  
unsigned char ds1820rd()   //读数据
{ 
	unsigned char i=0;
	unsigned char dat = 0;
	for (i=8;i>0;i--)
	{   
		DQ1 = 0; //给脉冲信号
		dat>>=1;
		DQ1 = 1; //给脉冲信号
		if(DQ1)
		dat|=0x80;
		delay_18B20(10);
	}
 	return(dat);
}

void ds1820wr(unsigned char wdata)//写数据
{
	unsigned char i=0;
    for (i=8; i>0; i--)
    { 
		DQ1 = 0;
     	DQ1 = wdata&0x01;
     	delay_18B20(10);
     	DQ1 = 1;
     	wdata>>=1;
   	}
} 

int read_temp(void) //读取温度值并转换
{
	unsigned char a,b;
	unsigned int tvalue;
  	ds1820rst();    
  	ds1820wr(0xcc);//跳过读序列号
  	ds1820wr(0x44);//启动温度转换
  	ds1820rst();    
  	ds1820wr(0xcc);//跳过读序列号 
    ds1820wr(0xbe);//读取温度
  	a=ds1820rd();
  	b=ds1820rd();
  	tvalue=b;
  	tvalue<<=8;
  	tvalue=tvalue|a;
    /*if(tvalue<0x0fff)//温度为正
   		*tflag=0;
    else			 //温度为负
    {
		tvalue=~tvalue+1;
	 	*tflag=1;
   	}*/
  	tvalue=tvalue*(0.0625);//温度值扩大10倍，精确到1位小数
	return(tvalue);
}
