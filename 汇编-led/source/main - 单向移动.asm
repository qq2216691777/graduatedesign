
led1_out equ P0		;将P0定义为前8个led灯  宏定义
led2_out equ P1

led_time	equ	7ch
led_hz  	equ	7ah

led1_status	equ	7bh
led2_status	equ	78h
org		00h	   		; 程序从此开始执行
ajmp	main		; 跳转到main出开始执行
org     000BH
ljmp	time0_handler
org     001BH
ljmp	time1_handler


main:
		mov sp, #50h   		; 设置sp堆栈的初始位置
		mov acc, #00h	    ; 初始化acc
		
		lcall led_init		; 初始化led灯  关闭

		lcall time1_init	; 初始化定时器1 



loop:	lcall DELAY
		jmp loop		
			


led_init:			   		;led初始化函数  关闭所有的led灯
		mov r1, #0ffh
		mov led1_out, r1
		mov led2_out, r1

		mov led1_status, #01h
		mov led2_status, #00h

		mov led_hz, #05h		;c初始化频率为1s
		ret

time0_init:					;定时器初始化函数  设置波特率 
		push acc
		push psw
		mov a, #0f0h
		anl a,TMOD			;位与运算 
		orl a, #01h			;位或运算  工作方式为1
		mov TMOD, a

		mov TH0, #03Ch		;对TH0寄存器赋值
		mov TL0, #0b0h		;对TL0寄存器赋值

		setb TR0			; 将TR0置1
		setb EA
		setb ET0

		pop psw
		pop acc
		ret

time1_init:					;定时器初始化函数  设置时间
		push acc
		push psw
		mov a, #0fh
		anl a, TMOD			;位与运算 
		orl a, #010h		;位或运算  工作方式为1
		mov TMOD, a

		mov TH1, #03Ch		;对TH0寄存器赋值
		mov TL1, #0b0h		;对TL0寄存器赋值

		setb EA
		setb ET1

		mov r1, #00h
		mov led_time, r1

		setb TR1			; 将TR0置1

		pop psw
		pop acc
		ret
		


DELAY:	MOV R7, #250
D1:		MOV R6, #250
D2:		DJNZ R6,D2
		DJNZ R7,D1
		RET
		

time0_handler:					;定时器0中断服务函数
		push acc
		push psw

		pop psw
		pop acc
		reti

time1_handler:				    ;定时器1中断服务函数
		push acc
		push psw
		push b
		mov TH1, #03Ch		;对TH0寄存器赋值
		mov TL1, #0b0h		;对TL0寄存器赋值
		
		inc led_time			;t_1s加2 
		mov a, led_time 
		cjne a, led_hz, tmp_t1 	; r1>20  cy=0  
tmp_t1:
		jc over_t1				;如果r1 < 014h  将跳转 

		mov a, led1_status		
		rlc a  
		jnc led1_s			  //进位为0 
	    mov led2_status, #00h
		
led1_s:
		mov led1_status, a
		
		mov a, led2_status		
		rlc a
		jnc led2_s
		mov led1_status, #01h
led2_s:
		
		mov led2_status,a
		cpl a

		mov led2_out, a
		mov a, led1_status
		cpl a
		mov led1_out, a



		mov led_time, #00h

over_t1:
		pop b
		pop psw
		pop acc
		reti

		END