;
;	STC89C52RC
;   11.0592Mhz
;引脚	    P0.0    P0.1  -------   P1.6    P1.7
;led编号	  1       2    ------	 15		 16
;

org		00h	   		; 程序从此开始执行
ajmp	main		; 跳转到main出开始执行
org     000BH
ljmp	time0_handler
org     001BH
ljmp	time1_handler

led1_out equ P0		;将P0定义为前8个led灯  宏定义 P0.0~P0.7 1~8
led2_out equ P1		;将P1定义为后8个led灯  宏定义 P1.0~P1.7 9~16

direction equ 74h	  ; led灯移动方向

led_time	equ	7ch
led_hz  	equ	7ah	  ; 控制闪烁周期的变量

led1_status	equ	7bh	  ; 前8个灯的状态设置  
led2_status	equ	78h	  ; 后8个灯的状态设置 

main:
		mov sp, #50h   		; 设置sp堆栈的初始位置
		mov acc, #00h	    ; 初始化acc
		
		lcall led_init		; 初始化led灯  关闭

		lcall DELAY100MS 

		lcall time0_init	; 初始化定时器0

		lcall serial_init	; 串口初始化
		

loop:	lcall delay
		lcall serial_receive   ;等待接收从串口发来的数据  将获取到的数据写入到r7寄存器
		
		lcall serial_process   ;处理从串口发来的数据  从r7寄存器中取出
		jmp loop		
			

;********************************
;	处理串口发来的数据
;********************************
serial_process:
		push acc
		push psw
		push b

		mov a, r7		    ;读取从串口发来的数据
		swap a				
		anl a, #0fh			;获取到前4位  

		cjne a, #01h, next_2	;判断前4位是否等于1  如果不等于则跳转到	next_2 处开始执行
		setb TR0			; 将TR0置1  定时器开始计数
		jmp p_over
next_2:
		cjne a, #02h, next_3	;判断前4位是否等于2  如果不等于则跳转到	next_3 处开始执行
		clr TR0					; 将TR0置0  定时器停止计数
		jmp p_over
next_3:
		cjne a, #03h, next_5
		mov a, direction
		jz p_to_l
		mov led1_status, #00h	  ;设置led灯的状态 位 最左边的一个亮
		mov led2_status, #80h
		mov r1, #0ffh
		mov led1_out, r1
		mov r1, #07fh
		mov led2_out, r1 
		jmp p_over		
	p_to_l:
		mov led1_status, #01h	   ;设置led灯的状态 位 最右边的一个亮
		mov led2_status, #00h
		mov r1, #0feh
		mov led1_out, r1
		mov r1, #0ffh
		mov led2_out, r1  		
		jmp p_over
next_4:
		cjne a, #04h, next_5
		mov a, r7
		anl a, #0fh				  ;获取到底4位 

		cjne a, #01h, speed_2	  ;判断前4位是否等于1  如果不等于则跳转到	speed_2 处开始执行
			mov led_hz, #05h	  ;将调整频率的变量设置led_hz为5  即250ms   
		jmp p_over
	speed_2:
			mov led_hz, #0ah
		jmp p_over
	speed_3:
			mov led_hz, #0fh
		jmp p_over
	speed_4:
			mov led_hz, #14h
	    jmp p_over

next_5:
		cjne a, #05h, p_over
		mov a, direction		 ;改变控制方向变量direction的值	   0左移  1右移
		jz p_to_d 
		mov  direction, #00h
		jmp p_over
	p_to_d:
		mov  direction, #01h
		jmp p_over			
p_over:

		pop b
		pop psw
		pop acc
		ret

;********************************
;	led初始化函数  关闭所有的led灯 
;********************************
led_init:			   		
		mov r1, #0ffh
		mov led1_out, r1
		mov led2_out, r1

		mov led1_status, #01h
		mov led2_status, #00h

		mov led_hz, #14h		;c初始化频率为1s
		mov direction, #00h	    ;初始化方向为左移
		ret
;********************************
;	串口初始化函数  
;********************************
serial_init:	
		push acc
		push psw
		push b

		lcall time1_init

		mov scon,#50h
		mov a, pcon
		anl a, #7fh
		mov pcon, a	

		clr ri

	 	pop b
		pop psw
		pop acc
		ret
;********************************
;	串口发送函数 
;********************************
serial_send:	
		push acc
		push psw
		push b

	    jb ti,$ 			  ;等待上一次的数据发送成功
		mov sbuf, r7		  ;将要发送的数据写入到寄存器中

		jnb ti,$			  ;等待发送完成
		clr ti				  ;清除标志位

	 	pop b
		pop psw
		pop acc
		ret

;********************************
;	串口接收函数 
;********************************
serial_receive:	
		push psw

		jnb ri,$			 ;等待上一次的数据接收成功
		mov r7, sbuf		 ;读取从串口发来的数据到r7寄存器
		clr ri				 ;清除标志位

		pop psw
		ret
;********************************
;	定时器初始化函数  设置波特率 
;********************************
time1_init:				
		push acc
		push psw
		push b
		mov a, TMOD
		anl a,#0fh			;位与运算 
		orl a, #20h			;位或运算  工作方式为1
		mov TMOD, a

		mov TH1, #0fdh		;对TH0寄存器赋值   设置波特率 9600
		mov TL1, #0fdh		;对TL0寄存器赋值

		setb TR1			; 将TR0置1
		

		pop b
		pop psw
		pop acc
		ret

;********************************
;	定时器初始化函数  设置led闪烁时间 
;********************************
time0_init:					
		push acc
		push psw
		mov a, TMOD
		anl a, #0f0h			;位与运算 
		orl a, #01h		;位或运算  工作方式为1
		mov TMOD, a

		mov TH0, #04Ch		;对TH0寄存器赋值   50ms	 12Mhz
		mov TL0, #0d0h		;对TL0寄存器赋值

		setb EA
		setb ET0

		mov r1, #00h
		mov led_time, r1

		setb TR0			; 将TR0置1

		pop psw
		pop acc
		ret
		


DELAY:	MOV R7, #250
D1:		MOV R6, #250
D2:		DJNZ R6,D2
		DJNZ R7,D1
		RET
		
;********************************
;	定时器1中断服务函数 
;********************************
time1_handler:					
		push acc
		push psw

		pop psw
		pop acc
		reti
;********************************
;	定时器0中断服务函数 
;********************************
time0_handler:				    ;定时器0中断服务函数
		push acc
		push psw
		push b
		mov TH0, #04Ch		;对TH0寄存器赋值
		mov TL0, #0d0h		;对TL0寄存器赋值
		
		inc led_time			;t_1s加2 
		mov a, led_time 
		cjne a, led_hz, tmp_t1 	; r1>20  cy=0  
tmp_t1:
		jc over_t1				;如果r1 < 014h  将跳转 

		mov a, direction		;判断direction变量 为0  则允许左移函数 否则运行右移函数
		jz to_left
		lcall led_move_r
		jmp t0_over		
to_left:
		lcall led_move_l		;调用led左移函数
t0_over:
		mov led_time, #00h
over_t1:
		pop b
		pop psw
		pop acc
		reti

;********************************
;	led	左移函数  
;********************************
led_move_l:					;led向左移动
		push acc
		push psw
		push b

		mov a, led1_status	  ;读取前8个灯的led状态	
		rlc a  				  ;带进位循环左移
	   
		mov led1_status, a	   ;将循环后的数据写入到状态变量中
		
		mov a, led2_status		;读取后8个灯的led状态	
		rlc a					;带进位循环左移
		jnc led_l2				;判断是否有进位
		mov led1_status, #01h	;有进位  则将最右边的点亮 
led_l2:
		
		mov led2_status,a		 ;将循环后的数据写入到状态变量中
		
		cpl a					 ;因为是低电平点亮led  但是状态变量中点亮为1  所以这里先取反
		mov led2_out, a			 ;将取反后的数据写入到引脚的控制寄存器中
		
		mov a, led1_status
		cpl a
		mov led1_out, a

		

		pop b
		pop psw
		pop acc
		ret

;********************************
;	led	右移函数  
;********************************
led_move_r:	
		push acc
		push psw
		push b

		mov a, led2_status		
		rrc a  

		mov led2_status, a
		
		mov a, led1_status		
		rrc a
		jnc led_r2
		mov led2_status, #80h
led_r2:
		
		mov led1_status,a
		cpl a

		mov led1_out, a
		mov a, led2_status
		cpl a
		mov led2_out, a

		pop b
		pop psw
		pop acc
		ret

DELAY100MS:			;@12.000MHz
	PUSH 30H
	PUSH 31H
	MOV 30H,#195
	MOV 31H,#133
NEXT:
	DJNZ 31H,NEXT
	DJNZ 30H,NEXT
	POP 31H
	POP 30H
	RET

		END