#ifndef _FPMXX_H__
#define _FPMXX_H__

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include <string.h>


void FPMXX_Cmd_Get_Img(void);
void FPMXX_Cmd_Check(void);
void FPMXX_Receive_Data(unsigned char ucLength);
extern unsigned char FPMXX_RECEICE_BUFFER[52];
void FPMXX_Delete_All_Fingerprint( void );
void FPMXX_Cmd_Search_Finger(void);
void FPMXX_Cmd_Reg_Model( void );
void FPMXX_Cmd_Save_Finger( unsigned int storeID );
void FINGERPRINT_Cmd_Delete_All_Finger(void);



u8 FPXMM_Init( void );
void FPMXX_Add_Fingerprint( unsigned int  );
u8 FPMXX_Find_Fingerprint( unsigned int * );
void FINGERPRINT_Cmd_Delete_Model(unsigned int );
void FINGERPRINT_Update_Model_List( void );

#endif

