#ifndef _GPRSA6_H__
#define _GPRSA6_H__

#include "stm32f10x.h"

#define GPRSA6OLINE		0x001    //已开机

#define GPRSA6RESOK		0x010	 //回复OK

#define GPRSA6RESZ		0x040	 //回复>

extern u32 GPRSA6_status;

u8 SendSms( void );
void Respond( u8 *p );

#endif
