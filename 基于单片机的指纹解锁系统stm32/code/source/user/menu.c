#include "menu.h"
#include "ff.h"

char menulist[][10] = {
					"录入指纹",
					"删除指纹",
					"清空指纹",
					"修改密码",
				};
void cursorShow( int x, int y);

/**
* 显示功能列表前的标号
*/
void showMenuList( u8 index )
{
	static u8 LastIndex=1;
	if( LastIndex == index )
		return ;
	LCD_Print(0, LastIndex*16, " ",TYPE16X16,TYPE8X16);
	LCD_Print(0, index*16, ">",TYPE16X16,TYPE8X16);
	LastIndex = index;
}
/*
	获取用户输入的信息
	用于修改密码
*/
u8 getInput( int *p, u8 Max ) 
{
	int cursorPos=0;
	char IDdata[8];
	u8 tData[8];
	int id=0;
	u8 i;
	
	
	while(keyNowValue);
	while(1)
	{
		cursorShow(42+cursorPos*10,40);
		
		if(keyNowValue>='0' && keyNowValue<='9')
		{
			IDdata[cursorPos] = keyNowValue-0x30;
			sprintf((char *)tData,"%c",keyNowValue);
			LCD_Print(42+cursorPos*10, 40, tData,TYPE16X16,TYPE8X16); 
			while(keyNowValue);
			cursorPos++;
			if(cursorPos==Max)
				break;
			
		}
		else if( keyNowValue == 'A' )
			break;
		else if( keyNowValue == 'C' )
			return 0;
	}
	
	if( cursorPos>0 && cursorPos<=Max )
	{
		for(i=0;i<cursorPos; i++)
		{
			id *= 10;
			id += IDdata[i];
		}
		*p = id;
		return 1;
		
	}
	return 0;
}

/*
*获取用户输入的密码 用*替代
*/
void getInputPd( u8 *passWD )
{
	u8 cursorPos;
		
	
	while(keyNowValue);
	cursorPos = 0;
	while(keyNowValue!='C')
	{
		cursorShow(32+cursorPos*10,40);
		if(keyNowValue>='0' && keyNowValue<='9')
		{
			passWD[cursorPos] = keyNowValue-0X30;
			LCD_Print(32+cursorPos*10, 40, "*",TYPE16X16,TYPE8X16);
			while(keyNowValue);
			cursorPos++;
			if( cursorPos==6 )
			{
				printf("%s\r\n",passWD);
				break;
			}
		}
	}
}

/*
修改密码函数
*/
void changePasswd( )
{
	u8 pd1[8];
	u8 pd2[8];
	u8 i;
	FIL fdst;	
	UINT br;
	
	LCD_CLS();
	LCD_Print(12, 16, "请输入密码:",TYPE16X16,TYPE8X16);  //绘图
	getInputPd( pd1 );
	LCD_CLS();
	LCD_Print(12, 16, "请再输入密码:",TYPE16X16,TYPE8X16);  //绘图
	getInputPd( pd2 );
	
	for(i=0;i<6;i++)
	{
		if( pd1[i]!=pd2[i] )
			break;
	}
	if( i==6 )
	{
		//存储
		f_open( &fdst, "0:.pssW", FA_OPEN_ALWAYS|FA_WRITE );
		br=1;
		f_lseek(&fdst,0);		
		f_write( &fdst, pd2, 6, &br );				
		f_close( &fdst );
		
		LCD_CLS();
		LCD_Print(12, 16, "密码输入成功",TYPE16X16,TYPE8X16);  //绘图
	}
	else
	{
		LCD_CLS();
		LCD_Print(12, 16, "密码输入失败",TYPE16X16,TYPE8X16);  //绘图
	}
	
	delay_ms(1000);
	delay_ms(1000);
}
/*
* 显示功能函数
*/
void showFuncMenu()
{
	int index = 0;
	int id;
	
	LCD_CLS();
	LCD_Print(16, 0, "录入指纹",TYPE16X16,TYPE8X16);
	LCD_Print(16, 16, "删除指纹",TYPE16X16,TYPE8X16);
	LCD_Print(16, 32, "清空指纹",TYPE16X16,TYPE8X16);
	LCD_Print(16, 48, "修改密码",TYPE16X16,TYPE8X16);
	LCD_Print(0, 0, ">",TYPE16X16,TYPE8X16);
	showMenuList(0);
	while( keyNowValue != 'C' )
	{
		if( !keyNowValue )
			continue;
		if( keyNowValue == '2')
		{
			index +=3;
			index = index%4;
		}
		else if( keyNowValue == '8')
		{
			index ++;
			index = index%4;
		}
		else if( keyNowValue == 'A')
		{
			switch(index)
			{
				case 0:
					LCD_CLS();
					LCD_Print(12, 16, "请输入ID:",TYPE16X16,TYPE8X16);  //绘图
					if(getInput(&id,3))
					{
						FPMXX_Add_Fingerprint(id);						
					}
					LCD_CLS();
					LCD_Print(16, 0, "录入指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 16, "删除指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 32, "清空指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 48, "修改密码",TYPE16X16,TYPE8X16);
					LCD_Print(0, 0, ">",TYPE16X16,TYPE8X16);
					
					break;
				case 1:
					showModuleList();
				
					LCD_CLS();
					LCD_Print(16, 0, "录入指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 16, "删除指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 32, "清空指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 48, "修改密码",TYPE16X16,TYPE8X16);
					LCD_Print(0, 16, ">",TYPE16X16,TYPE8X16);
					break;
				case 2:
					
					LCD_CLS();
					LCD_Print(16, 0, "删除...",TYPE16X16,TYPE8X16);
					FPMXX_Delete_All_Fingerprint();
					LCD_Print(16, 0, "删除成功",TYPE16X16,TYPE8X16);
					delay_ms(1000);
					LCD_Print(16, 0, "录入指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 16, "删除指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 32, "清空指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 48, "修改密码",TYPE16X16,TYPE8X16);
					LCD_Print(0, 32, ">",TYPE16X16,TYPE8X16);
					
					break;
				case 3:
					changePasswd();
					LCD_CLS();
					LCD_Print(16, 0, "录入指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 16, "删除指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 32, "清空指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 48, "修改密码",TYPE16X16,TYPE8X16);
					LCD_Print(0, 48, ">",TYPE16X16,TYPE8X16);
					break;
				default:
					
					LCD_CLS();
					LCD_Print(16, 0, "录入指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 16, "删除指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 32, "清空指纹",TYPE16X16,TYPE8X16);
					LCD_Print(16, 48, "修改密码",TYPE16X16,TYPE8X16);
					LCD_Print(0, 48, ">",TYPE16X16,TYPE8X16);
					break;
					
			}
		}		
		showMenuList(index);
		
		while(keyNowValue);
		
		
	}	
}
/**
  * 显示ID列表函数
**/
void showIDList( u8 index, int *p , u8 tatol)
{
	u8 showData[10];
	
	if( tatol <1 )
		return ;
	
	LCD_CLS();
	sprintf((char *)showData,"> ID:%d",p[index%tatol]);
	LCD_Print(0, 0, showData,TYPE16X16,TYPE8X16);
	if( tatol <2 )
		return ;
	
	sprintf((char *)showData,"ID:%d",p[(index+1)%tatol]);
	LCD_Print(16, 16, showData,TYPE16X16,TYPE8X16);
	
	if( tatol <3 )
		return ;
	sprintf((char *)showData,"ID:%d",p[(index+2)%tatol]);
	LCD_Print(16, 32, showData,TYPE16X16,TYPE8X16);
	
	if( tatol <4 )
		return ;
	sprintf(( char *)showData,"ID:%d",p[(index+3)%tatol]);
	LCD_Print(16, 48, showData,TYPE16X16,TYPE8X16);
}
/*
* 以列表的形式显示指纹库中的ID
*/
void showModuleList()
{
	u8 *p = FPMXX_Module_list;
	int List[100]={0};
	int i;
	u8 tempData,j,index;
	u8 nowIndex=0;
	
	FINGERPRINT_Update_Model_List();		//获取指纹库函数
	
	index = 0;
	for( i=0; i<128;i++)
	{
		tempData = p[i];
		for(j=0;j<8;j++)
		{
			if( tempData&0x01 )
			{
				List[index] = j+i*8;
				index++;
			}
			tempData = tempData>>1;
		}
	
	}
	if( index ==0 )
	{
		LCD_CLS();
		LCD_Print(16, 0, "无指纹",TYPE16X16,TYPE8X16);
		delay_ms(1000);
		return;
	}
	printf("num %d\r\n",index);
	showIDList(nowIndex,List,index);
	while(keyNowValue);
	do
	{
		if( keyNowValue == '8' )
		{
			showIDList(nowIndex++,List,index);

			while(keyNowValue);
		}
		else if( keyNowValue == '2' )
		{
			nowIndex = nowIndex+index-1;
			showIDList(nowIndex,List,index);
		
			while(keyNowValue);
		}
		else if( keyNowValue == 'A' )
		{
			while(keyNowValue);
			FINGERPRINT_Cmd_Delete_Model(List[(nowIndex)%index]);
			break;
			
		}
		
		
	}while( keyNowValue != 'C' );
	while(keyNowValue);
	
}
