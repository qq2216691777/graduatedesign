#ifndef _KEY_H__
#define _KEY_H__

#include "stm32f10x.h"


void Key_Init( void );
char getKeyValue( void );

extern int keyNowValue;

#endif
