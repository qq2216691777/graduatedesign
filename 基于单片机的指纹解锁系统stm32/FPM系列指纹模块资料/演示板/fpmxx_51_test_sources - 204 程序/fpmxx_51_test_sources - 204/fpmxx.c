#include <reg52.h>
#include "main.h"
//#include "uart.h"
#include "fpmxx.h"
#include "lcd.h"
#include "keyboard.h"

sbit JIDIANQI = P3^7;



volatile unsigned char FPMXX_RECEICE_BUFFER[24];



//FINGERPRINT通信协议定义

code unsigned char FPMXX_Get_Device[6] = {0x01,0x00,0x03,0x16,0x00,0x1A};
code unsigned char FPMXX_Pack_Head[6] = {0xEF,0x01,0xFF,0xFF,0xFF,0xFF};  //协议包头
code unsigned char FPMXX_Get_Img[6] = {0x01,0x00,0x03,0x01,0x0,0x05};    //获得指纹图像
code unsigned char FPMXX_Get_Templete_Count[6] ={0x01,0x00,0x03,0x1D,0x00,0x21 }; //获得模版总数
code unsigned char FP_Search[11]={0x01,0x0,0x08,0x04,0x01,0x0,0x0,0x03,0xE7,0x0,0xF8}; //搜索指纹搜索范围0 - 999,使用BUFFER1中的特征码搜索
code unsigned char FP_Img_To_Buffer1[7]={0x01,0x0,0x04,0x02,0x01,0x0,0x08}; //将图像放入到BUFFER1
code unsigned char FP_Img_To_Buffer2[7]={0x01,0x0,0x04,0x02,0x02,0x0,0x09}; //将图像放入到BUFFER2
code unsigned char FP_Reg_Model[6]={0x01,0x0,0x03,0x05,0x0,0x09}; //将BUFFER1跟BUFFER2合成特征模版
code unsigned char FPMXX_Delete_All_Model[6]={0x01,0x0,0x03,0x0d,0x00,0x11};//删除指纹模块里所有的模版
volatile unsigned char  FP_Save_Finger[9]={0x01,0x00,0x06,0x06,0x01,0x00,0x0B,0x00,0x19};//将BUFFER1中的特征码存放到指定的位置
//volatile unsigned char FP_Delete_Model[10]={0x01,0x00,0x07,0x0C,0x0,0x0,0x0,0x1,0x0,0x0}; //删除指定的模版
//volatile unsigned char FINGER_NUM;


/*------------------ FINGERPRINT命令字 --------------------------*/


/*
void FPMXX_Cmd_Send_Pack_Head(void) //发送包头
{
		int i;
	
	for(i=0;i<6;i++) //包头
   {
     UART_Send_Byte(FPMXX_Pack_Head[i]);   
    }
		
}*/




//void FPMXX_Cmd_Check(void)
//{
//	  int i=0;

//		FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头
//	
//	  for(i=0;i<6;i++)
//	  {		
//			 UART_Send_Byte(FPMXX_Get_Device[i]);
//	  }

//}




//指纹发送指令
void FPMXX_Send_Cmd(unsigned char length,unsigned char *address,unsigned char returnLength) 
{ 
	unsigned char i = 0;

		for(i=0;i<6;i++) //包头
   {
     UART_Send_Byte(FPMXX_Pack_Head[i]);   
    }
	
	  for(i=0;i<length;i++)
	{
	        
			 UART_Send_Byte(*address);        
			address++;
	}		
	
	
	for (i=0;i<returnLength;i++)
	{
     FPMXX_RECEICE_BUFFER[i] = UART_Receive_Byte();
	}
}
	
	

////FINGERPRINT_获得指纹图像命令
//void FPMXX_Cmd_Get_Img(void)
//{
//   // FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头
//	  FPMXX_Send_Cmd(6,FPMXX_Get_Img);
//}



//搜索全部用户999枚
//void FPMXX_Cmd_Search_Finger(void)
//{
//  //     unsigned char i;	   
//	    
////			 FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头

////       for(i=0;i<11;i++)
////           {
////    	      UART_Send_Byte(FP_Search[i]);   
////   		   }
//					 
//				 
//				  FPMXX_Send_Cmd(11,FP_Search);


//}

//void FPMXX_Cmd_Reg_Model(void)
//{
//   //    unsigned char i;	   
////	    
////			 FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头

////       for(i=0;i<6;i++)
////           {
////    	      UART_Send_Byte(FP_Reg_Model[i]);   
////   		   }
// FPMXX_Send_Cmd(6,FP_Reg_Model);
//				 

//}







//将图像转换成特征码存放在Buffer2中
//void FINGERPRINT_Cmd_Img_To_Buffer2(void)
//{
//  //  FPMXX_Cmd_Send_Pack_Head();
// FPMXX_Send_Cmd(7,FP_Img_To_Buffer2); //发送命令 将图像转换成 特征码 存放在 CHAR_buffer1
//}

/*
//将BUFFER1 跟 BUFFER2 中的特征码合并成指纹模版
void FINGERPRINT_Cmd_Reg_Model(void)
{
    unsigned char i;    

    for(i=0;i<6;i++) //包头
    {
      UART_Send_Byte(FPMXX_Pack_Head[i]);   
    }

    for(i=0;i<6;i++) //命令合并指纹模版
    {
      UART_Send_Byte(FP_Reg_Model[i]);   
    }

}
*/


void LCD_Select_SZ(unsigned char row,unsigned start,unsigned char sz,unsigned char black)
{

  switch(sz)
		{
			case 0:
LCD_Write_Font16x8(row,start,SZ_0,black);
			break;
			
			case 1:
		LCD_Write_Font16x8(row,start,SZ_1,black);
			break;
			
			case 2:
		LCD_Write_Font16x8(row,start,SZ_2,black);
			break;
			
			case 3:
		LCD_Write_Font16x8(row,start,SZ_3,black);
			break;
			
			case 4:
		LCD_Write_Font16x8(row,start,SZ_4,black);
			break;
			
			case 5:
		LCD_Write_Font16x8(row,start,SZ_5,black);
			break;
			
			case 6:
			LCD_Write_Font16x8(row,start,SZ_6,black);
			break;
			
			case 7:
			LCD_Write_Font16x8(row,start,SZ_7,black);
			break;
			
			case 8:
			LCD_Write_Font16x8(row,start,SZ_8,black);
			break;
					
		  case 9:
				LCD_Write_Font16x8(row,start,SZ_9,black);
			break;
		
		}





}

void LCD_Wirte_SZ(unsigned char row,unsigned char start,unsigned int sz)
{
	
	 // switch(sz/100)
  	LCD_Select_SZ(row,start,(sz/100),1);	
		LCD_Select_SZ(row,start + 8,((sz%100)/10),1);
		LCD_Select_SZ(row,start + 16,(sz%10),1);	
		
}


void lcd_input_finger(unsigned char index)
{
	switch(index)
	{
		case 1:
//    LCD_Write_Font16x16(0,16-16,HZ_shu); 
	//	LCD_Write_Font16x16(0,32-16,HZ_ru);
	  LCD_Write_Font16x16(0,48-16,HZ_zhi);
	  LCD_Write_Font16x16(0,64-16,HZ_wen);
	  LCD_Write_Font16x8(0,80-16,ZM_I,0);
		LCD_Write_Font16x8(0,88-16,ZM_D,0);
	  LCD_Write_Font16x8(0,96-16,FH_Maohao,0);
	
		break;
		
		case 2:
			
						
									LCD_Write_Font16x16(0,16-16,HZ_Qing3); 
   								LCD_Write_Font16x16(0,32-16,HZ_An);
									LCD_Write_Font16x16(0,48-16,HZ_Shou);
									LCD_Write_Font16x16(0,64-16,HZ_zhi);
									LCD_Write_Font16x8(0,64,SZ_1,0);
									
			
		break;
		
		
		
		case 3:
			
			LCD_Write_Font16x16(0,16-16,HZ_Qing3); 
									LCD_Write_Font16x16(0,32-16,HZ_An);
									LCD_Write_Font16x16(0,48-16,HZ_Shou);
									LCD_Write_Font16x16(0,64-16,HZ_zhi);
									LCD_Write_Font16x8(0,64,SZ_2,0);
			
		break;
		
		default:
			
		break;
	
	}

}



//添加指纹
void FPMXX_Add_Fingerprint()
{
	
	  unsigned int find_finger_id = 0;

		LCD_Clear_Screen(); //清空显示屏

	  lcd_input_finger(1);
	
//		LCD_Write_Font16x16(0,16-16,HZ_shu); 
//		LCD_Write_Font16x16(0,32-16,HZ_ru);
//	  LCD_Write_Font16x16(0,48-16,HZ_zhi);
//	  LCD_Write_Font16x16(0,64-16,HZ_wen);
//	  LCD_Write_Font16x8(0,80-16,ZM_I,0);
//		LCD_Write_Font16x8(0,88-16,ZM_D,0);
//	  LCD_Write_Font16x8(0,96-16,FH_Maohao,0);

	
	/*
	
	LCD_Wirte_SZ(0);
	
	LCD_Fill(3,46,2,0xFF);
	LCD_Fill(4,46,2,0xFF);
	
		LCD_Fill(3,48+8+8+8,2,0xFF);
	LCD_Fill(4,48+8+8 +8,2,0xFF);
	
	*/
	
	
//	LCD_Write_Font16x16(3,20,ZF_UP);
	//LCD_Write_Font16x16(3,48+8+8+20,ZF_DOWN);
	
	
	  LCD_Write_Font16x8(3,48,SZ_0,1);
	  LCD_Write_Font16x8(3,48+8,SZ_0,1);
	  LCD_Write_Font16x8(3,48+8+8,SZ_0,1);
		
		Delay_Ms(100);
	
	  //LCD_Write_Cancel_button(6,0);
	 // LCD_Write_OK_button(6,96);
	
	
	  while(1)
		{		

			if(KEY_UP==0)
								{
								//	
								//		Delay_Ms(50);
								//	  while(KEY_UP!=0); //等待松开按键
									
									
										if(find_finger_id == 999)
										{
											 find_finger_id = 0;
										}
										else
										{
										find_finger_id = find_finger_id + 1;
										}
									
								   	LCD_Wirte_SZ(3,48,find_finger_id);
								
								}
								
								if(KEY_DOWN == 0)
								{
									if(find_finger_id == 0)
										{
											 find_finger_id = 999;
										}
										else
										{
										find_finger_id = find_finger_id - 1;
										}
									
								   	LCD_Wirte_SZ(3,48,find_finger_id);
													
					
                  }
								
									
									 if(KEY_OK == 0)
						    {	
									
								//		Delay_Ms(50);
								//		while(KEY_OK!=0);
									
									  LCD_Clear_Screen(); //清空显示屏											
									  lcd_input_finger(2);
								
															do
									{
			//UART_Send_Byte(0xAC);
		
		//						FPMXX_Cmd_Get_Img(); //获得指纹图像
		//FPMXX_Receive_Data(12);
										
										
										FPMXX_Send_Cmd(6,FPMXX_Get_Img,12);
		
		//判断接收到的确认码,等于0指纹获取成功
		if(FPMXX_RECEICE_BUFFER[9]==0)
		{
			   Buzzer_On();
			
				Delay_Ms(100);
				//FINGERPRINT_Cmd_Img_To_Buffer1();
			FPMXX_Send_Cmd(7,FP_Img_To_Buffer1,12); //发送命令 将图像转换成 特征码 存放在 CHAR_buffer1
			// FPMXX_Receive_Data(12);
			
			
			  		
									LCD_Clear_Screen(); //清空显示屏
											lcd_input_finger(3);
								
				
			
				do
				{
				   	
			//UART_Send_Byte(0xAC);
		
								FPMXX_Send_Cmd(6,FPMXX_Get_Img,12);
		
		//判断接收到的确认码,等于0指纹获取成功
		if(FPMXX_RECEICE_BUFFER[9]==0)
		{
			   Buzzer_On();
			
				Delay_Ms(100);
				FPMXX_Send_Cmd(7,FP_Img_To_Buffer2,12);
			
			
				 //转换成特征码
     
			
			 FPMXX_Send_Cmd(6,FP_Reg_Model,12);
				//    FPMXX_Receive_Data(12);  
			
			
			
				 FPMXX_Cmd_Save_Finger(find_finger_id);                		         
         //FPMXX_Receive_Data(12);
			   
			
						LCD_Clear_Screen(); //清空显示屏
											
									LCD_Write_Font16x16(0,16-16,HZ_tian); 
									LCD_Write_Font16x16(0,32-16,HZ_jia);
									LCD_Write_Font16x16(0,48-16,HZ_Cheng);
									LCD_Write_Font16x16(0,64-16,HZ_Gong);
								//	LCD_Write_Font16x8(0,64,SZ_2,0);
									
									Delay_Ms(1000);
			
			
			   
			
			   break;
			  
				
				
				}
			
			
		}
				while(KEY_CANCEL == 1);
		    break;
	}
	}
	while(KEY_CANCEL == 1);
									
					


								//返回主要界面
							 LCD_Main_Menu();									
							 Menu_Select = 1;
								
								  
							  break;
								
								
								}
									
								
		
		//退回主要界面
						 if(KEY_CANCEL == 0)
						 {
							 Delay_Ms(100);
							 
							 while(KEY_CANCEL!=0);							 
							 LCD_Main_Menu();									
							 Menu_Select = 1;
							 break;
							 
						 }
						
						
						Delay_Ms(100);
		
	}
}



//搜索指纹
void FPMXX_Find_Fingerprint()
{
	 unsigned int find_finger_id = 0;
	 unsigned char address = 0;
	
	
	
		LCD_Clear_Screen(); //清空显示屏
		
		//搜索指纹
				LCD_Write_Font16x16(0,16-16,HZ_sou); 
			  LCD_Write_Font16x16(0,32-16,HZ_suo);
	      LCD_Write_Font16x16(0,48-16,HZ_zhi);
	      LCD_Write_Font16x16(0,64-16,HZ_wen);
			//	LCD_Write_Font16x16(0,80-16,HZ_Dot);
		  //  LCD_Write_Font16x16(0,88-16,HZ_Dot);
	
	//请按手指
	
//	      LCD_Write_Font16x16(2,48-16,HZ_Qing3);
//	      LCD_Write_Font16x16(2,64-16,HZ_An);
//				LCD_Write_Font16x16(2,80-16,HZ_Shou);
//		    LCD_Write_Font16x16(2,96-16,HZ_zhi);
	
	
	 //按cancel返回
//	  LCD_Write_Font16x16(6,27 - 8 -6,HZ_An);
	//	LCD_Write_Cancel_button(6,45- 8-6);
	//	LCD_Write_Font16x16(6,80+ 9-6,HZ_Fan);
	//	LCD_Write_Font16x16(6,96+ 9-6,HZ_Hui);
		
		
		
		
		
	
	
	//搜索指纹库
	
	do
	{
			//UART_Send_Byte(0xAC);
		
		
				FPMXX_Send_Cmd(6,FPMXX_Get_Img,12);
		
		
		
		
		//判断接收到的确认码,等于0指纹获取成功
		if(FPMXX_RECEICE_BUFFER[9]==0)
		{
			
			
			Delay_Ms(50);
//				FINGERPRINT_Cmd_Img_To_Buffer1();
//			  FPMXX_Send_Cmd(7,FP_Img_To_Buffer1); //发送命令 将图像转换成 特征码 存放在 CHAR_buffer1
//			  FPMXX_Receive_Data(12);
//			
			
			
				FPMXX_Send_Cmd(7,FP_Img_To_Buffer1,12);
		
			  FPMXX_Send_Cmd(11,FP_Search,16);
			
			  if(FPMXX_RECEICE_BUFFER[9] == 0) //搜索到
				{
				   LCD_Fill(2,0,128,0x00);//清空改好
						LCD_Fill(3,0,128,0x00);//清空改好
					  LCD_Write_Font16x16(2,0,HZ_Zhao);
	          LCD_Write_Font16x16(2,16,HZ_Dao);
					  
					//ID:
					LCD_Write_Font16x8(2,32+8,ZM_I,0);
					LCD_Write_Font16x8(2,40+8,ZM_D,0);
					LCD_Write_Font16x8(2,48+8,FH_Maohao,0);
					
					//拼接指纹ID数
					find_finger_id = FPMXX_RECEICE_BUFFER[10]*256 + FPMXX_RECEICE_BUFFER[11];
					
					address = 48+16;
					
					
					
					
					LCD_Wirte_SZ(2,address,find_finger_id);
					
					
					
					
					
					  if(JIDIANQI == 0)
						{
						   JIDIANQI = 1;
						}
						else
						{
							  JIDIANQI = 0;
						}
					
					 
						
						/*
						JIDIANQI = 0;					
					 Delay_Ms(2000);

				 //	Delay_Ms(5000);
						//	 Delay_Ms(5000);

					//Delay_Ms(5000);
					 JIDIANQI = 1;
					
				     */
				
				
				
				
				
				}
				else //没有找到
				{
						LCD_Fill(2,0,128,0x00);//清空改好
						LCD_Fill(3,0,128,0x00);//清空改好
					  LCD_Write_Font16x16(2,0,HZ_Wei); 
					  LCD_Write_Font16x16(2,16,HZ_Zhao);
	          LCD_Write_Font16x16(2,32,HZ_Dao);
					
				}
				
		
		
		}
		
		
		
	
	
	
	
	}
	while(KEY_CANCEL == 1);
	
		
	
	
	
	      
	//
	
	
	
			//	LCD_Write_Font16x16(3,48,HZ_Cheng); 
			//  LCD_Write_Font16x16(3,64,HZ_Gong);
	
	
	 

		




}




//删除所有存贮的指纹库
void FPMXX_Delete_All_Fingerprint()
{
		LCD_Clear_Screen(); //清空显示屏
		
		//清空指纹库
			//	LCD_Write_Font16x16(1,16,HZ_qing); 
			//  LCD_Write_Font16x16(1,32,HZ_kong);
	    //  LCD_Write_Font16x16(1,48,HZ_zhi);
	     // LCD_Write_Font16x16(1,64,HZ_wen);
			//	LCD_Write_Font16x16(1,80,HZ_ku);
			//	LCD_Write_Font16x16(1,96,HZ_Dot);
		  //  LCD_Write_Font16x16(1,104,HZ_Dot);
	
	//添加清空指纹指令

	//
	       FPMXX_Send_Cmd(6,FPMXX_Delete_All_Model,12); //发送命令 将图像转换成 特征码 存放在 CHAR_buffer1
	      //FPMXX_Receive_Data(12); 
	
	
	  //   Buzzer_On();
	  //   Delay_Ms(1000);
		//	 Buzzer_On();
	
					LCD_Write_Font16x16(3,16,HZ_qing); 
			  LCD_Write_Font16x16(3,32,HZ_kong);
				LCD_Write_Font16x16(3,48,HZ_Cheng); 
			  LCD_Write_Font16x16(3,64,HZ_Gong);
	
	  //按OK返回
//	  LCD_Write_Font16x16(6,27 - 8,HZ_An);
//		LCD_Write_OK_button(6,45- 8);
	//	LCD_Write_Font16x16(6,80- 8,HZ_Fan);
//		LCD_Write_Font16x16(6,96- 8,HZ_Hui);

		



					Delay_Ms(2000);
						LCD_Main_Menu();									
									Menu_Select = 1;

}






/*
//删除指纹模块里的指定指纹模版
void FINGERPRINT_Cmd_Delete_Model(unsigned int uiID_temp)
{
    volatile unsigned int uiSum_temp = 0;
	unsigned char i;    
	 
	FP_Delete_Model[4]=(uiID_temp&0xFF00)>>8;
	FP_Delete_Model[5]=(uiID_temp&0x00FF);
	
	for(i=0;i<8;i++)
	    uiSum_temp = uiSum_temp + FP_Delete_Model[i];
	
	//UART0_Send_Byte(uiSum_temp);	
			
	FP_Delete_Model[8]=(uiSum_temp&0xFF00)>>8;
	FP_Delete_Model[9]=uiSum_temp&0x00FF;
	 

    for(i=0;i<6;i++) //包头
      UART_Send_Byte(FPMXX_Pack_Head[i]);   

    for(i=0;i<10;i++) //命令合并指纹模版
      UART_Send_Byte(FP_Delete_Model[i]);   
}
*/



/*

//获得指纹模板数量
void FINGERPRINT_Cmd_Get_Templete_Num(void)
{  
	
	unsigned int i;

	 FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头

   //发送命令 0x1d
   for(i=0;i<6;i++)
	{
     UART_Send_Byte(FPMXX_Get_Templete_Count[i]);
	}
   
  
}
*/

/*

//删除全部指纹
void FINGERPRINT_Cmd_Delete_All_Finger(void)
{  unsigned int i;

	 FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头

   //发送命令 0x1d
   for(i=0;i<6;i++)
	{
     UART_Send_Byte(FPMXX_Delete_All_Model[i]);
	}
   
  
}
*/









/*


//搜索全部用户999枚
void FINGERPRINT_Cmd_Search_Finger_Admin(void)
{
       unsigned char i;	   
			 
	     FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头

       for(i=0;i<11;i++)
           {
    	      UART_Send_Byte(FP_Search_0_9[i]);   
   		   }


}*/

void FPMXX_Cmd_Save_Finger( unsigned int storeID )
{
           unsigned long temp = 0;
		   unsigned char i;

//           SAVE_FINGER[9]={0x01,0x00,0x06,0x06,0x01,0x00,0x0B,0x00,0x19};//将BUFFER1中的特征码存放到指定的位置

           FP_Save_Finger[5] =(storeID&0xFF00)>>8;
           FP_Save_Finger[6] = (storeID&0x00FF);
           
		   for(i=0;i<7;i++)   //计算校验和
		   	   temp = temp + FP_Save_Finger[i];
			    
		   FP_Save_Finger[7]=(temp & 0x00FF00) >> 8; //存放校验数据
		   FP_Save_Finger[8]= temp & 0x0000FF;
		   
//          FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头
//	
//           for(i=0;i<9;i++)  
//            	UART_Send_Byte(FP_Save_Finger[i]);      //发送命令 将图像转换成 特征码 存放在 CHAR_buffer1
	
	FPMXX_Send_Cmd(9,FP_Save_Finger,12);
	
}



////接收反馈数据缓冲
//void FPMXX_Receive_Data(unsigned char ucLength)
//{
//  unsigned char i;

//  for (i=0;i<ucLength;i++)
//     FPMXX_RECEICE_BUFFER[i] = UART_Receive_Byte();

//}






/*

















//指纹添加新用户
unsigned char FP_add_new_user(unsigned char ucH_user,unsigned char ucL_user)
{
		
		       do {	          		     	 
                    FINGERPRINT_Cmd_Get_Img(); //获得指纹图像
	                FINGERPRINT_Recevice_Data(12); //接收12个长度的反馈码
                  }
               while ( UART1_FINGERPRINT_RECEVICE_BUFFER[9]!=0x0 ); //检测是否成功的按了指纹


			  	 FINGERPRINT_Cmd_Img_To_Buffer1(); //将图像转换成特征码存放在Buffer1中
		         FINGERPRINT_Recevice_Data(12);   //接收12个长度的反馈码

                do{ 
				     FINGERPRINT_Cmd_Get_Img(); //获得指纹图像
				     FINGERPRINT_Recevice_Data(12); //接收12个长度的反馈码			 
				 }
				 while( UART1_FINGERPRINT_RECEVICE_BUFFER[9]!=0x0 );
                
				 FINGERPRINT_Cmd_Img_To_Buffer2(); //将图像转换成特征码存放在Buffer2中
		         FINGERPRINT_Recevice_Data(12);   //接收12个长度的反馈码


				 FP_Cmd_Reg_Model();//转换成特征码
                 FINGERPRINT_Recevice_Data(12); 
				 
				 FINGERPRINT_Cmd_Save_Finger(ucH_user,ucL_user);                		         
                 FINGERPRINT_Recevice_Data(12);
              
		         return 0;





}

*/
