#include <reg52.h>
#include <stdio.h>
#include <intrins.h> 
#include "uart.h"
#include "fpmxx.h"
#include "lcd.h"
#include "keyboard.h"
#include "main.h"
#include "eeprom.h"

#define BAUD  0xFF80                  //57600bps @ 22.1184MHz

sfr AUXR = 0x8E;
sbit RXB = P3^0;                        //define UART TX/RX port
sbit TXB = P3^1;

typedef bit BOOL;
typedef unsigned char BYTE;
typedef unsigned int WORD;

BYTE TBUF,RBUF;
BYTE TDAT,RDAT;
BYTE TCNT,RCNT;
BYTE TBIT,RBIT;
BOOL TING,RING;
BOOL TEND,REND;

void UART_INIT();

BYTE t, r;
BYTE buf[16];



void UART_INIT()
{
	  TMOD = 0x00;                        //timer0 in 16-bit auto reload mode
    AUXR = 0x80;                        //timer0 working at 1T mode
    TL0 = BAUD;
    TH0 = BAUD>>8;                      //initial timer0 and set reload value
    TR0 = 1;                            //tiemr0 start running
    ET0 = 1;                            //enable timer0 interrupt
    PT0 = 1;                            //improve timer0 interrupt priority
    EA = 1;                             //open global interrupt switch

	
    TING = 0;
    RING = 0;
    TEND = 1;
    REND = 0;
    TCNT = 0;
    RCNT = 0;
}



void UART_Send_Byte(unsigned char dat)
{
   unsigned char i = 0 ;
	
	do{
		i = TEND;
	
	}

		while(i==0);
   	TEND = 0;
    TBUF = dat;
    TING = 1;
	
}


unsigned char UART_Receive_Byte()
{
	
	 unsigned char i = 0,dat ;
      
	do{
		i = REND;
	}

		while(i==0);
	

            REND = 0;
            dat = RBUF;
        
				return dat;


}


//硬件连接，51单片机需要使用11.0592MHZ的晶振
//指纹模块红色线接 +5V 
//黑色线 接 GND
//黄色线 接 TXD
//绿色线 接 RXD
//如果不成功可以尝试交换 黄色跟绿色线试试

void MENU_SELECT(unsigned char index);

sbit UART_RXD = P3^0;
sbit UART_TXD = P3^1;


sbit LED_1 = P3^4;
sbit LED_2 = P3^5;
sbit BUZZER = P3^6;
sbit JIDIANQI = P3^7;


//olatile int Times_Out = 0;
//volatile int Times_Court = 0;

volatile unsigned char Menu_Max_Item = 3;
volatile unsigned char Menu_Select = 1;



void Delay1ms()		//@22.1184MHz
{
	unsigned char i, j;

	i = 22;
	j = 128;
	do
	{
		while (--j);
	} while (--i);
}



void Delay_Ms(unsigned int t)
{
	unsigned	int k  =0 ;

	for(k=0;k<t;k++)
	{
Delay1ms();
	
}
}


void Delay500us()		//@22.1184MHz
{


unsigned char i, j;

	i = 6;
	j = 93;
	do
	{
		while (--j);
	} while (--i);
}




//void delay_a(int t)
//{
//	int a,b;
//	for(a=2*t;a>0;a--)
//	{
//		for(b=5;b>0;b--)
//		{
//			;
//		}
//	}
//}


void LED_1_On()
{
		LED_1 = 1;
}



void LED_2_Blink()
{
		LED_2 = 1;
	  Delay_Ms(1000);
	  LED_2 = 0;

}

void Buzzer_On()
{
	  unsigned int i;
	
	
		for(i = 0;i<500;i++)
	{
	BUZZER = 1;
		
	 Delay500us();	
		
		BUZZER = 0;
		
		 Delay500us();	
	
	
	
	}




}



//unsigned char UART_Receive_Byte_1()//UART Receive a byteg
//{	
//	unsigned char dat;
//	while(!RI)
//	{
//		
//		 if (Times_Out == 1)
//		 {
//			// UART_Send_Byte(0xAA);
//				break;
//			 
//			 
//		  
//		 }
//	
//	
//	};
//	
//	RI = 0;
//	dat = SBUF;
//	return (dat);
//}



//void Timer0_Start()
//{
//   TL0 = 0;
//	 TH0 = 0;
//	
//	 TR0 = 1;
//	 ET0 = 1;
//}

//void Timer0_End()
//{
//	 TR0 = 0;
//	 ET0 = 0;
//	 TF0 = 0; 

//}


//void FPMXX_Check_Module()
//{
//	 unsigned int i=0;
//	
//	 Timer0_Start();  //检测超时退出，启动定时器0
//	
//	
//	 FPMXX_Cmd_Check();
//	 
//	
//	
//   for(i=0;i<12;i++)
//	 {	
//	    FPMXX_RECEICE_BUFFER[i] = UART_Receive_Byte_1();  //UART Receive a byteg
//   }
//	 
//	
//	//FPMXX_Receive_Data(12);
//	
//	
//	 // UART_Send_Byte(  FPMXX_RECEICE_BUFFER[0] );
//	 // UART_Send_Byte(  FPMXX_RECEICE_BUFFER[1] );
//	 // UART_Send_Byte(  FPMXX_RECEICE_BUFFER[2] );
//	  //UART_Send_Byte(  FPMXX_RECEICE_BUFFER[3] );
//	  //UART_Send_Byte(  FPMXX_RECEICE_BUFFER[4] );
//	 // UART_Send_Byte(  FPMXX_RECEICE_BUFFER[5] );
//		
//		
//	 
//	 
//	 if(FPMXX_RECEICE_BUFFER[8]==0x03)
//	 {
//		 
//		  Timer0_End();//停止超时计数
//			
//			//LCD_Disp_Find_Fingerprint_Module();
//		 
//		  for(i=0;i<556;i++)
//		 {
//			 
//			 switch(i)
//			 {
//				 case 37:
//					   FPMXX_RECEICE_BUFFER[1] = UART_Receive_Byte();
//				 break;
//				 
//				 case 38:
//					 FPMXX_RECEICE_BUFFER[2] = UART_Receive_Byte();
//				 break;
//				 
//				 case 39:
//					 FPMXX_RECEICE_BUFFER[3] = UART_Receive_Byte();
//				 break;
//				 
//				 case 40:
//					 FPMXX_RECEICE_BUFFER[4] = UART_Receive_Byte();
//				 break;
//					 
//				 case 41:
//					 FPMXX_RECEICE_BUFFER[5] = UART_Receive_Byte();
//				 break;
//				 
//				 default:
//				 
//			   FPMXX_RECEICE_BUFFER[0] = UART_Receive_Byte();
//			 
//			 } 
//			 
//			 
//			
//		 
//		 
//		 }
//		 
//		 //判断指纹模块型号
//		 if(FPMXX_RECEICE_BUFFER[1] == 0x46 && +
//			  FPMXX_RECEICE_BUFFER[2] == 0x50 && +
//				FPMXX_RECEICE_BUFFER[3] == 0x4D && + 
//				FPMXX_RECEICE_BUFFER[4] == 0x31 && + 
//				FPMXX_RECEICE_BUFFER[5] == 0x30 
//		 )
//		 {
//		 
//					LCD_Disp_Find_Fingerprint_Module();			 
//			    LCD_Disp_Fingerprint_Type();
//			 
//					while(1)
//					{
//						
//						//UART_Send_Byte(KEY_OK);
//						
//						if(KEY_OK == 0)
//						{							
//								
//								LCD_Main_Menu();
//							  Menu_Select = 1;
//							  break;
//						}
//						Delay_Ms(100);
//					
//					}
//		 
//		 }
//		 
//		 
//		  //UART_Send_Byte(  FPMXX_RECEICE_BUFFER[0] );
//		  //UART_Send_Byte(  FPMXX_RECEICE_BUFFER[1] );
//		  //UART_Send_Byte(  FPMXX_RECEICE_BUFFER[2] );
//		  //UART_Send_Byte(  FPMXX_RECEICE_BUFFER[3] );
//		  //UART_Send_Byte(  FPMXX_RECEICE_BUFFER[4] );
//		  //UART_Send_Byte(  FPMXX_RECEICE_BUFFER[5] );
//		 
//		 
//	 
//	 }
//	 
//	

//	
//	//加入超时退出则模块不存在
//	 if(Times_Out==1)
//	 {
//			 LCD_Disp_Cannot_Find_Module();
//	 
//	 
//	 }
//	
//	
//	

//}


void Jidianqi_Test()
{
				  JIDIANQI = 0;
	
	        Delay_Ms(1000);

JIDIANQI = 1;
}






void main()
{
	
	 unsigned char first=1;
	

	  Buzzer_On();

    LED_1_On();
	  LED_2_Blink();
		Jidianqi_Test();
	
	
	
	
	  Delay_Ms(300);
	 //定时器0
	//  TMOD = 0x00;                        //timer0 in 16-bit auto reload mode
  //  AUXR = 0x80;                        //timer0 working at 1T mode
 //   TL0 = BAUD;
  //  TH0 = BAUD>>8;                      //initial timer0 and set reload value
 //   TR0 = 1;                            //tiemr0 start running
 //   ET0 = 1;                            //enable timer0 interrupt
 //   PT0 = 1;                            //improve timer0 interrupt priority
  //  EA = 1;                             //open global interrupt switch

  
		
	
		//UART_init_57600(); //初始化串口
	  Keyboard_Init();  //初始化键盘
	
	
	
	
	//  UART_Send_Byte(eepromRead(0x2000));
	
	
		LCD_Init(); 
	  LCD_Clear_Screen();
	  LCD_Disp_Init();
	
	
	//LCD_Clear_Screen(); 
	//while(1);
	
	  
	
//	  Delay_Ms(300); //延时500MS，等待指纹模块复位
	
//	  FPMXX_Check_Module();
		  UART_INIT();
		

//	goto ABC;
	 
	//按键判断
	while(1){

		if(first == 1)
		{
				FPMXX_Find_Fingerprint();			
				LCD_Main_Menu();								
			  Menu_Select = 1;
						
				first =2;						
		
		}

	
		  if(KEY_OK == 0)
			{
				
			//	Delay_Ms(100);
				  while(KEY_OK!=0); //等待松开按键
				
				
					switch(Menu_Select)
					{
						
						//查找指纹
						case 1:
							
						
					FPMXX_Find_Fingerprint();				
						LCD_Main_Menu();								
						Menu_Select = 1;
						first =2;						
						break; 
						
						
						//添加指纹
						case 2:				
							FPMXX_Add_Fingerprint();
							break;
						
								
						//清空指纹
						case 3:
							FPMXX_Delete_All_Fingerprint();
						
						
						/*
						//按OK键退出
					   while(1)
					   {					
						
						    if(KEY_OK == 0)
						    {	
										while(KEY_OK!=0);
											
											
								
								LCD_Main_Menu();
									
									Menu_Select = 1;
							  break;
						}
						Delay_Ms(500);
					
					}*/
					
						
						break;
					
					
					
					
					}
			
			
			
			
			}
		
		
	
		  //下
			if(KEY_DOWN == 0)
			{
					//  Delay_Ms(100);
			  while(KEY_DOWN!=0); //等待松开按键
				
				
				//当前箭头+1
			    if(Menu_Select!=Menu_Max_Item)
					{
						Menu_Select++; 	
					
					}
					
					LCD_Main_Menu_Select(Menu_Select);
					
			}
			
		  //上
			if(KEY_UP == 0)
			{
				 	//  Delay_Ms(100);
	
				
				  while(KEY_UP!=0); //等待松开按键

				
				//当前箭头+1
			    if(Menu_Select!=1)
					{
						Menu_Select--; 	
					
					}
					
					LCD_Main_Menu_Select(Menu_Select);
					
					
			}
			
			Delay_Ms(100); //延时判断100MS检测一次
		
		
		
		
	}
}






////1为减去 2为加上
//void MENU_SELECT(unsigned char index)
//{
//	
//	switch(index)
//	{
//		case 1:	
//				//当前箭头+1
//			    if(Menu_Select!=1)
//					{
//						Menu_Select--; 	
//					
//					}
//				break;
//				
//				
//		case 2:
//			
//		  if(Menu_Select!=1)
//					{
//						Menu_Select++; 	
//					
//					}
//					
//						LCD_Main_Menu_Select(Menu_Select);

//			
//		break;
//					
//		default:
//			break;
//				}
//	
//				
//	LCD_Main_Menu_Select(Menu_Select);

//}



////定时器0中断
//void tm0_isr() interrupt 1 using 1
//{
//  TR0= 0;
//	TL0 = 0;
//	TH0 = 0;
//	Times_Court++;
//	TF0 = 0; //清空中断标志位

//	TR0=1;
//	
//	 if(Times_Court==50)
//	
//	{
//		  TR0=  0;
//		  TF0 = 0; //清空中断标志位
//		  Times_Out = 1;
//	
//	}


//}



//定时器串口
void tm0() interrupt 1 using 1
{
    if (RING)
    {
        if (--RCNT == 0)
        {
            RCNT = 3;                   //reset send baudrate counter
            if (--RBIT == 0)
            {
                RBUF = RDAT;            //save the data to RBUF
                RING = 0;               //stop receive
                REND = 1;               //set receive completed flag
            }
            else
            {
                RDAT >>= 1;
                if (RXB) RDAT |= 0x80;  //shift RX data to RX buffer
            }
        }
    }
    else if (!RXB)
    {
        RING = 1;                       //set start receive flag
        RCNT = 4;                       //initial receive baudrate counter
        RBIT = 9;                       //initial receive bit number (8 data bits + 1 stop bit)
    }

    if (--TCNT == 0)
    {
        TCNT = 3;                       //reset send baudrate counter
			
        if (TING)                       //judge whether sending
        {
            if (TBIT == 0)
            {
                TXB = 0;                //send start bit
                TDAT = TBUF;            //load data from TBUF to TDAT
                TBIT = 9;               //initial send bit number (8 data bits + 1 stop bit)
            }
            else
            {
                TDAT >>= 1;             //shift data to CY
                if (--TBIT == 0)
                {
                    TXB = 1;
                    TING = 0;           //stop send
                    TEND = 1;           //set send completed flag
                }
                else
                {
                    TXB = CY;           //write CY to TX port
                }
            }
        }
    }
}