#include <reg52.h>
#include "intrins.h"
#include "eeprom.h"

sfr IAP_DATA = 0xE2; //FLASH数据寄存器
sfr IAP_ADDRH = 0xE3; //FLASH地址高位
sfr IAP_ADDRL = 0xE4; //FLASH地址低位
sfr IAP_CMD = 0xE5; //FLASH指令寄存器
sfr IAP_TRIG = 0xE6; //FLASH触发寄存器
sfr IAP_CONTR = 0xE7; //FLASH控制寄存器

//IAP指令定义
#define CMD_IDLE 0 //待机
#define CMD_READ 1 //BYTE读取 
#define CMD_PROGRAM 2 //BYTE写入
#define CMD_ERASE 3 //块擦除

//等待时间
#define ENABLE_IAP 0x81 //if SYSCLK<20M


//开始地址 STC89C52RC 起始地址 不同芯片起始地址不一样的
#define IAP_ADDRESS 0x2000


//关闭IAP模式,进去空闲模式,让MCU进去空闲状态
void IapIdle()
{
	IAP_CONTR = 0;
	IAP_CMD = 0;
	IAP_TRIG = 0;
	IAP_ADDRH = 0x80;
	IAP_ADDRL = 0;
}



//读取一个数据
unsigned char eepromRead(unsigned int addr)
{
	unsigned char dat;

	IAP_CONTR = ENABLE_IAP; //使能IAP
	IAP_CMD = CMD_READ;
	IAP_ADDRL = addr;
	IAP_ADDRH = addr >> 8;
	IAP_TRIG = 0x46;
	IAP_TRIG = 0xb9;
	_nop_();
	dat = IAP_DATA;
	IapIdle();
	
	return dat;
	
	
	




}


//void eepromWrite(unsigned int addr,unsigned char dat)
//{
//    eepromEraseSector(addr);
//	
//    IapWrite(addr,dat);

//}

//void IapWrite(unsigned int addr,unsigned char dat)
//{
//	IAP_CONTR = ENABLE_IAP;
//	IAP_CMD = CMD_PROGRAM;
//	IAP_ADDRL = addr;
//	IAP_ADDRH = addr>>8;
//	IAP_DATA = dat;
//  IAP_TRIG = 0x46;
//  IAP_TRIG = 0xb9;
//  _nop_();
//  IapIdle();	
//}

//void eepromEraseSector(unsigned int addr)
//{
//	IAP_CONTR = ENABLE_IAP;
//	IAP_CMD = CMD_ERASE;
//	IAP_ADDRL = addr;
//	IAP_ADDRH = addr >> 8;
//	IAP_TRIG = 0x46;
//	IAP_TRIG = 0xb9;
//	_nop_();
//	IapIdle();
//}