#ifndef __MAIN_H__
#define __MAIN_H__

extern volatile unsigned char Menu_Select;
void Delay_Ms(unsigned int t);
void Buzzer_On();
void UART_Send_Byte(unsigned char dat);
unsigned char UART_Receive_Byte();

#endif