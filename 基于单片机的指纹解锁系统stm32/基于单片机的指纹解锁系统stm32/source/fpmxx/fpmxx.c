#include "fpmxx.h"
#include "delay.h"
#include "oled.h"

unsigned char FPMXX_RECEICE_BUFFER[52];

u8 FPMXX_Module_list[128]={0};		//保存列表


//FINGERPRINT通信协议定义
unsigned char FPMXX_Get_Device[10] = {0x01,0x00,0x07,0x13,0xFF,0xFF,0xFF,0xFF,0x00,0x03};  //验证指令
unsigned char FPMXX_Pack_Head[6] = {0xEF,0x01,0xFF,0xFF,0xFF,0xFF};  //协议包头
unsigned char FPMXX_Get_Img[6] = {0x01,0x00,0x03,0x01,0x0,0x05};    //获得指纹图像
unsigned char FPMXX_Get_Templete_Count[6] ={0x01,0x00,0x03,0x1D,0x00,0x21 }; //获得模版总数
unsigned char FP_Search[11]={0x01,0x0,0x08,0x04,0x01,0x0,0x0,0x03,0xE7,0x0,0xF8}; //搜索指纹搜索范围0 - 999,使用BUFFER1中的特征码搜索
unsigned char FP_Search_0_9[11]={0x01,0x0,0x08,0x04,0x01,0x0,0x0,0x0,0x13,0x0,0x21}; //搜索0-9号指纹
unsigned char FP_Img_To_Buffer1[7]={0x01,0x0,0x04,0x02,0x01,0x0,0x08}; //将图像放入到BUFFER1
unsigned char FP_Img_To_Buffer2[7]={0x01,0x0,0x04,0x02,0x02,0x0,0x09}; //将图像放入到BUFFER2
unsigned char FP_Reg_Model[6]={0x01,0x0,0x03,0x05,0x0,0x09}; //将BUFFER1跟BUFFER2合成特征模版
unsigned char FPMXX_Delete_All_Model[6]={0x01,0x0,0x03,0x0d,0x00,0x11};//删除指纹模块里所有的模版
volatile unsigned char  FP_Save_Finger[9]={0x01,0x00,0x06,0x06,0x01,0x00,0x0B,0x00,0x19};//将BUFFER1中的特征码存放到指定的位置
unsigned char FP_Delete_Model[10]={0x01,0x00,0x07,0x0C,0x0,0x0,0x0,0x1,0x0,0x0}; //删除指定的模版

unsigned char FPMXX_Get_List0[7]={0x01, 0x00, 0x04, 0x1F, 0x00, 0x00, 0x24};
unsigned char FPMXX_Get_List1[7]={0x01, 0x00, 0x04, 0x1F, 0x01, 0x00, 0x25};
unsigned char FPMXX_Get_List2[7]={0x01, 0x00, 0x04, 0x1F, 0x02, 0x00, 0x26};
unsigned char FPMXX_Get_List3[7]={0x01, 0x00, 0x04, 0x1F, 0x03, 0x00, 0x27};

//volatile unsigned char FINGER_NUM;
//volatile unsigned char FINGER_NUM;


/*------------------ FINGERPRINT命令字 --------------------------*/


void FPMXX_Cmd_Send_Pack_Head(void) //发送包头
{
	int i;

	for(i=0;i<6;i++) //包头
	{
		Uart3SendChar(FPMXX_Pack_Head[i]);   
	}
		
}

void FPMXX_Cmd_Check(void)
{
	int i=0;

	FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头

	for(i=0;i<10;i++)
	{		
		Uart3SendChar(FPMXX_Get_Device[i]);
	}

}

//FINGERPRINT_获得指纹图像命令
void FPMXX_Cmd_Get_Img(void)
{
    unsigned char i;

    FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头
	
    for(i=0;i<6;i++) //发送命令 0x1d
	{
       Uart3SendChar(FPMXX_Get_Img[i]);
	}
}


//讲图像转换成特征码存放在Buffer1中
void FINGERPRINT_Cmd_Img_To_Buffer1(void)
{
	unsigned char i;

	FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头

	for(i=0;i<7;i++)   //发送命令 将图像转换成 特征码 存放在 CHAR_buffer1
	{
		Uart3SendChar(FP_Img_To_Buffer1[i]);
	}
}


//将图像转换成特征码存放在Buffer2中
void FINGERPRINT_Cmd_Img_To_Buffer2(void)
{
	unsigned char i;
	for(i=0;i<6;i++)    //发送包头
	{
		Uart3SendChar(FPMXX_Pack_Head[i]);   
	}

	for(i=0;i<7;i++)   //发送命令 将图像转换成 特征码 存放在 CHAR_buffer1
	{
		Uart3SendChar(FP_Img_To_Buffer2[i]);
	}
}

//删除指纹模块里的所有指纹模版
void FINGERPRINT_Cmd_Delete_All_Model(void)
{
	unsigned char i;    


	for(i=0;i<6;i++) //包头
	Uart3SendChar(FPMXX_Pack_Head[i]);   

	for(i=0;i<6;i++) //命令合并指纹模版
	{
		Uart3SendChar(FPMXX_Delete_All_Model[i]);   
	}
	
}


//搜索全部用户999枚
void FPMXX_Cmd_Search_Finger(void)
{
	unsigned char i;	   

	FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头

	for(i=0;i<11;i++)
	{
		Uart3SendChar(FP_Search[i]);   
	}


}

void FPMXX_Cmd_Reg_Model(void)
{
	unsigned char i;	   

	FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头

	for(i=0;i<6;i++)
	{
		Uart3SendChar(FP_Reg_Model[i]);   
	}

}


void FPMXX_Cmd_Save_Finger( unsigned int storeID )
{
           unsigned long temp = 0;
		   unsigned char i;

//           SAVE_FINGER[9]={0x01,0x00,0x06,0x06,0x01,0x00,0x0B,0x00,0x19};//将BUFFER1中的特征码存放到指定的位置

           FP_Save_Finger[5] =(storeID&0xFF00)>>8;
           FP_Save_Finger[6] = (storeID&0x00FF);
           
		   for(i=0;i<7;i++)   //计算校验和
		   	   temp = temp + FP_Save_Finger[i];
			    
		   FP_Save_Finger[7]=(temp & 0x00FF00) >> 8; //存放校验数据
		   FP_Save_Finger[8]= temp & 0x0000FF;
		   
          FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头
	
           for(i=0;i<9;i++)  
      		   Uart3SendChar(FP_Save_Finger[i]);      //发送命令 将图像转换成 特征码 存放在 CHAR_buffer1
}




//接收反馈数据缓冲
void FPMXX_Receive_Data(unsigned char ucLength)
{
  u8 res;
  unsigned char i;
  u8 data;

  for (i=0;i<ucLength;i++)
  {
     data = Uart3ReceiveByte(&res); 
	 if(res)
	 {
		 FPMXX_RECEICE_BUFFER[i] = data;
	 }
		 
  }
}

u8 FPXMM_Init( void )			//对应 demo  void FPMXX_Check_Module()
{
	FPMXX_Cmd_Check();
	
	FPMXX_Receive_Data(12);

   if((FPMXX_RECEICE_BUFFER[10]==0x00))
		return 1;			 
   else
	   return 0;
	
}


//添加指纹  传入ID
void FPMXX_Add_Fingerprint( unsigned int saveID )
{
	LCD_CLS();
	LCD_Print(12, 16, "请放手指...1",TYPE16X16,TYPE8X16);
	do
	{
		FPMXX_Cmd_Get_Img(); //获得指纹图像
		FPMXX_Receive_Data(12);
		
	}while(FPMXX_RECEICE_BUFFER[9]!=0);
	LCD_Print(12, 16, "  成功:1        ",TYPE16X16,TYPE8X16);
	FINGERPRINT_Cmd_Img_To_Buffer1();
	FPMXX_Receive_Data(12);
	printf("add 2\r\n");
	delay_ms(1000);
	delay_ms(1000);
	LCD_Print(12, 16, "请放手指...2",TYPE16X16,TYPE8X16);
	do
	{
		FPMXX_Cmd_Get_Img(); //获得指纹图像
		FPMXX_Receive_Data(12);
		
	}while(FPMXX_RECEICE_BUFFER[9]!=0);
	FINGERPRINT_Cmd_Img_To_Buffer2();
	FPMXX_Receive_Data(12);
	
	
	FPMXX_Cmd_Reg_Model();
	FPMXX_Receive_Data(12);
	
	FPMXX_Cmd_Save_Finger(saveID);
	FPMXX_Receive_Data(12);
	LCD_Print(12, 16, " 录入成功       ",TYPE16X16,TYPE8X16);
	delay_ms(1000);
	delay_ms(1000);
}

//搜索指纹
u8 FPMXX_Find_Fingerprint( unsigned int *p )
{
	
	do
	{
		FPMXX_Cmd_Get_Img(); //获得指纹图像
		FPMXX_Receive_Data(12);
		//判断接收到的确认码,等于0指纹获取成功
		if(FPMXX_RECEICE_BUFFER[9]==0)
		{
			printf("get image\r\n");
			delay_ms(50);
			FINGERPRINT_Cmd_Img_To_Buffer1();
			FPMXX_Receive_Data(12);
			
			FPMXX_Cmd_Search_Finger();
			FPMXX_Receive_Data(16);
			
			if(FPMXX_RECEICE_BUFFER[9] == 0) //搜索到
			{
				//拼接指纹ID数
				*p = FPMXX_RECEICE_BUFFER[10]*256 + FPMXX_RECEICE_BUFFER[11];
				printf("found %d\r\n",*p);
				return 1;
				
			}
			else
			{
				printf("no found\r\n");
				break;
			}
			
		}
		
		
		
	}while(1);
	return 0;
	
	
}

//删除所有存贮的指纹库
void FPMXX_Delete_All_Fingerprint()
{
	FINGERPRINT_Cmd_Delete_All_Model();
	FPMXX_Receive_Data(12);
}

void FINGERPRINT_Cmd_Delete_Model(unsigned int uiID_temp)
{
	volatile unsigned int uiSum_temp = 0;
	u8 i;
	
	printf("del id:%d\n",uiID_temp);
	FP_Delete_Model[4]=(u8)((uiID_temp&0xFF00)>>8);
	FP_Delete_Model[5]=(u8)(uiID_temp&0xFF);
	
	uiSum_temp = 0;
	for(i=0;i<8;i++)
	    uiSum_temp = uiSum_temp + FP_Delete_Model[i];
	
	FP_Delete_Model[8]=(u8)((uiSum_temp&0xFF00)>>8);
	FP_Delete_Model[9]=(u8)(uiSum_temp&0x00FF);
	
	FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头

    for(i=0;i<10;i++) //命令合并指纹模版
		Uart3SendChar(FP_Delete_Model[i]);
	

	FPMXX_Receive_Data(12);

	if(FPMXX_RECEICE_BUFFER[9]==0 )
	{
		LCD_CLS();
		LCD_Print(16, 0, "删除成功",TYPE16X16,TYPE8X16);
	}
	else
	{
		LCD_CLS();
		LCD_Print(16, 0, "删除失败",TYPE16X16,TYPE8X16);
	}
	delay_ms(1000);
}

void FINGERPRINT_Update_Model_List()
{
	u8 i;
	for(i=0;i<128;i++)
		FPMXX_Module_list[i]=0;
	
	FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头  0

	for(i=0;i<7;i++)   //发送命令 
		Uart3SendChar(FPMXX_Get_List0[i]);
	
	FPMXX_Receive_Data(44);
	if( FPMXX_RECEICE_BUFFER[9]==0)
	{
		for(i=0;i<32;i++)
		{
			FPMXX_Module_list[i] = FPMXX_RECEICE_BUFFER[10+i];
		}
		
	}
	
	FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头  1

	for(i=0;i<7;i++)   //发送命令 
		Uart3SendChar(FPMXX_Get_List1[i]);
	
	FPMXX_Receive_Data(44);
	if( FPMXX_RECEICE_BUFFER[9]==0)
	{
		for(i=0;i<32;i++)
		{
			FPMXX_Module_list[i+32] = FPMXX_RECEICE_BUFFER[10+i];
		}
		
	}
	
	FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头  2

	for(i=0;i<7;i++)   //发送命令 
		Uart3SendChar(FPMXX_Get_List2[i]);
	
	FPMXX_Receive_Data(44);
	if( FPMXX_RECEICE_BUFFER[9]==0)
	{
		for(i=0;i<32;i++)
		{
			FPMXX_Module_list[i+64] = FPMXX_RECEICE_BUFFER[10+i];
		}
		
	}
	
	FPMXX_Cmd_Send_Pack_Head(); //发送通信协议包头  3

	for(i=0;i<7;i++)   //发送命令 
		Uart3SendChar(FPMXX_Get_List3[i]);
	
	FPMXX_Receive_Data(44);
	if( FPMXX_RECEICE_BUFFER[9]==0)
	{
		for(i=0;i<32;i++)
		{
			FPMXX_Module_list[i+96] = FPMXX_RECEICE_BUFFER[10+i];
		}
		
	}
	
	
	
}





