#include "gprsa6.h"
#include "usart.h"
#include "delay.h"

u32 GPRSA6_status=0;

char cmd1Data[]="AT+CMGF=1\r\n";
char cmd2Data[]="AT+CSCS=\"GSM\"\r\n";
char cmd3Data[]="AT+CMGS=18437953612\r\n";
char cmd4Data[]="Warning!\r\n    A stranger to verify fingerprints! Please pay attention to safety.";

char cmdz1Data[]="AT+CMGF=0\r\n";
char cmdz2Data[]="AT+CMGS=61\r\n";
char cmdz3Data[]="0011000D91688134973516F20008AA2E8B66544AFF1A6709672A77E563077EB98BD556FE901A8FC77CFB7EDF9A8C8BC130028BF76CE8610F5B895168FF01";

char respondStart[]="+CREG: 1";

/*
等待 OK
*/
u8 WaitForOK( void )
{
	int i=100;
	while(i-->0)
	{
		delay_ms(2);
		if( GPRSA6_status&GPRSA6RESOK)
		{
			GPRSA6_status &= ~GPRSA6RESOK;
			break;
		}
	}
	return i;
}
/*
* 等待 > 
*/
u8 WaitForZCN( void )
{
	int i=100;
	while(i-->0)
	{
		delay_ms(2);
		if( GPRSA6_status&GPRSA6RESZ)
		{
			GPRSA6_status &= ~GPRSA6RESZ;
			break;
		}
	}
	return i;
}
/*
* 发送短信
*/
u8 SendSms( void )
{
	if( !(GPRSA6_status & GPRSA6OLINE) )
		return 0;
	Uart2SendString(cmdz1Data);
	WaitForOK();
	
	Uart2SendString(cmdz2Data);
	WaitForZCN();
	
	Uart2SendString(cmdz3Data);
	Uart2SendChar(0x1a);
	
	return WaitForOK();
	
}

/*
*等待gprs a6 反馈的信息 从而得到模块的状态
*/
void Respond( u8 *p )
{
	if( !(GPRSA6_status & GPRSA6OLINE) )	//首先检测是否开机
	{
		if( (p[0]=='+')&&(p[2]=='R')&&( p[5] == ':')&&(p[7]=='1') )
			GPRSA6_status|= GPRSA6OLINE;
	}
	switch(p[0])
	{
		
		case 'O':
		case 'o':
			if( (p[1]=='K') || (p[1]=='k') )
				GPRSA6_status |= GPRSA6RESOK;	
			break;
		case '>':
			GPRSA6_status |=GPRSA6RESZ;
			break;
		
	}
}
