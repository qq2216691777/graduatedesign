/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
	***************************************
			@作者     叶念西风 
			@作用     
			@日期     2017—3-27
			@备注   
	***************************************
**/
/*
	USART 1  PA9  PA10
	USART 2  PA2  PA3
	USART 3  PB10 PB11
	
	OLED    PB1  PB2  PB3  PB4

*/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "sd_sdio.h"
#include "ff.h"
#include "oled.h"
#include "key.h"
#include "timer.h"
#include "gprsa6.h"
#include "fpmxx.h"
#include "menu.h"
#include "rtc.h"


FATFS fs;			//记录文件系统盘符信息的结构体    //注意  需要定义为全局变量  也就是在MAIN函数之前定义
int res;			//记录读写文件的返回值
FIL fdst;	    //文件系统结构体，包含文件指针等成员
BYTE textFileBuffer[] = "\t指纹解锁系统\r\nID\t时间\t\t\t备注\r\n";
u8 defaultPasswd[]={1,2,3,4,5,6};		//默认密码
u8 recoveData[100];		//数据缓冲数组  用于在向sd卡写入数据时使用
BYTE buffer[512];		//数据缓冲数组  用于从sd卡读取数据时使用
int a;
UINT br,bw;

/*
*显示主菜单函数
*/
void showMainMenu()
{
	LCD_CLS();		//清屏
	
	LCD_Print(12, 8, "指纹解锁系统",TYPE16X16,TYPE8X16);
	LCD_Print(16, 32, "验证  管理",TYPE16X16,TYPE8X16);
	LCD_Print(22, 50, "[D]   [A]",TYPE16X16,TYPE8X16);
}

int menuTime=0;		//用于光标闪烁时间的计数  20ms在定时器中断中加1
/*
* 显示光标    600ms闪烁一次
*/
void cursorShow( int x, int y)
{
	static u8 temp=0;
	if( menuTime>30 )	//如果时间大于600ms
	{
		menuTime = 0;
		temp = ~temp;		//状态位取反
	
		if(temp)
			LCD_Print(x, y, "-",TYPE16X16,TYPE8X16);
		else
			LCD_Print(x, y, " ",TYPE16X16,TYPE8X16);
	}
}



int main( void )
{
	int res=0;

	unsigned int rec_ID;
	unsigned int saveSdId;
	char numDat[4];		//显示ID时的缓存数组
	int cursorPos=0;	//保存光标当前的位置
	u8 passWD[6];		// 密码缓存数组
	int presserror=0;	//在一个小时内指纹验证错误次数
	u8 pressTime[2];	//保存最后一次验证错误的小时和分钟
	int pressTimeHour;  //保存时间的临时变量
	int pressTimeMin;	//保存分钟的临时变量
	
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );	//串口1初始化 用于调试
	uart2_init( 115200 );	//用于 gprs a6
	uart3_init( 57600 );	//用于指纹模块
	
	RTC_Init();	  		//时钟初始化
	Key_Init();			//键盘初始化
	delay_ms(500);
	
	printf("uart 1\r\n");
	
	LCD_Init();
	LCD_Print(0, 16, "Waitting for",TYPE16X16,TYPE8X16);
	LCD_Print(0, 32, "GPRS A6...",TYPE16X16,TYPE8X16);
	
	TIM3_Int_Init(199,7199);	//定时器初始化  20ms
	
	if(0==FPXMM_Init())		//指纹模块初始化
	{
		LCD_CLS();
		LCD_Print(20, 16, "Not Found",TYPE16X16,TYPE8X16);
		LCD_Print(28, 32, "FPM A10",TYPE16X16,TYPE8X16);
		while(1);
	}
	printf("FPM init successed\r\n");
	
	if( f_mount( 0, &fs ) == FR_OK )
	{
		printf("注册工作空间成功\n");
	}
	else
	{
		LCD_CLS();
		LCD_Print(0, 16, "sd f_mount error",TYPE16X16,TYPE8X16);
		while(1);
	}
	
	/***************判断密码文件和记录文件是否存在 如果不存在则新建 **************/
	
	res = f_open( &fdst,"0:.pssW",FA_CREATE_NEW|FA_WRITE );
	if( res == FR_OK )
	{
		printf("文件已新建  默认密码\n");
		f_write( &fdst, defaultPasswd, 6, &bw );
		f_close( &fdst );
	}
	else if( res == FR_EXIST )
	{
		f_close( &fdst );
		printf("文件已经存在\n");
	}
	else
	{
		f_close( &fdst );
	}
	
	res = f_open( &fdst,"0:Record.txt",FA_CREATE_NEW|FA_WRITE );
	if( res == FR_OK )
	{
		printf("文件已新建 \n");
		f_write( &fdst, textFileBuffer, strlen((char *)textFileBuffer), &bw );
		
		f_close( &fdst );
	}
	else if( res == FR_EXIST )
	{
		f_close( &fdst );
		printf("文件已经存在\n");
	}
	else
	{
		f_close( &fdst );
	}
	

	while(!(GPRSA6_status&GPRSA6OLINE));   //等待gprs a6初始化成功  必须要有电话卡  中国移动
	
	showMainMenu();
	
	while(1)
	{
		if('D'==keyNowValue)   // 验证
		{
			LCD_CLS();
			LCD_Print(12, 16, "请放手指...",TYPE16X16,TYPE8X16);
			res = FPMXX_Find_Fingerprint(&rec_ID);	//识别指纹
			LCD_CLS();
			if(res)
			{
				saveSdId = rec_ID;
				sprintf(numDat,"ID: %d",rec_ID);
				LCD_Print(24, 16, (u8 *)numDat,TYPE16X16,TYPE8X16);
				LCD_Print(24, 32, "验证成功",TYPE16X16,TYPE8X16);
				
				//存储到sd卡
				res = f_open( &fdst, "0:Record.txt", FA_OPEN_ALWAYS|FA_WRITE );
				br=1;
				sprintf((char *)recoveData,"%d\t%d-%d-%d %d:%d:%d\r\n",saveSdId,calendar.w_year,calendar.w_month,\
										calendar.w_date,calendar.hour,calendar.w_date,calendar.sec);	

				f_lseek(&fdst,f_size(&fdst));
				res = f_write( &fdst, recoveData, strlen((char *)recoveData), &br );				
				f_close( &fdst );
			}
			else
			{
				if(!presserror) 
				{    //第一次验证错误  记录时间
					presserror=1;
					pressTime[0]=calendar.min;
					pressTime[1]=calendar.hour;
				}
				else
				{
					pressTimeHour = calendar.min - pressTime[0];
					pressTimeMin = calendar.hour - pressTime[1];
					if( pressTimeHour<0 ) //如果小于0  说明是从 23到0 
					{
						pressTimeMin = pressTimeMin+(pressTimeHour+24)*60;
					}
					else
						pressTimeMin += pressTimeHour*60;
					
					if( pressTimeMin >59 )
					{
						presserror=0;
						pressTime[0]=calendar.min;
						pressTime[1]=calendar.hour;
						
					}
					presserror++;
					if(presserror==3)
					{
						
						LCD_CLS();
						LCD_Print(12, 16, "Warning...",TYPE16X16,TYPE8X16);
						//报警 
						//发短信 
						printf("send a sms\r\n");
						SendSms();
						printf("sms over\r\n");
						while(1);
					}
				}
			//	printf("min:%d %d\r\n",pressTimeMin,presserror);
				LCD_CLS();
				LCD_Print(0, 16, "验证失败",TYPE16X16,TYPE8X16);
				delay_ms(1000);
			}
			
			delay_ms(1000);
			delay_ms(1000);
			delay_ms(1000);
			showMainMenu();
		}
		else if('A'==keyNowValue)  //管理
		{
			LCD_CLS();
			LCD_Print(12, 16, "请输入密码:",TYPE16X16,TYPE8X16);  //绘图
			while(keyNowValue);
			cursorPos = 0;
			while(keyNowValue!='C')
			{
				cursorShow(32+cursorPos*10,40);
				if(keyNowValue>='0' && keyNowValue<='9')
				{
					passWD[cursorPos] = keyNowValue-0x30;
					LCD_Print(32+cursorPos*10, 40, "*",TYPE16X16,TYPE8X16);
					while(keyNowValue);
					cursorPos++;
					if( cursorPos==6 )
					{
						break;
					}
				}
			}
			
			if( cursorPos==6 )
			{
				//密码验证 
				res = f_open( &fdst, "0:.pssW", FA_OPEN_EXISTING|FA_READ );
				br=1;
				a=0;
				
				res = f_read( &fdst, buffer, 6, &br );				
				f_close( &fdst );
				for( a=0; a<6; a++ )
				{
					if(buffer[a]!=passWD[a])
						break;
				}
				if( a== 6)
					showFuncMenu();//功能菜单
				else
				{
					LCD_CLS();
					LCD_Print(0, 16, "密码验证失败",TYPE16X16,TYPE8X16);
					delay_ms(1000);
				}
				
				
				
			}
			
			showMainMenu();
		}
		
	}
	

	
}

