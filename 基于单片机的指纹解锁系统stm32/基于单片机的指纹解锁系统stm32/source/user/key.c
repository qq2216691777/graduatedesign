#include "key.h"

int keyNowValue = 0;

void Key_Init( void )
{
	GPIO_InitTypeDef  GPIOStru;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD,ENABLE);

	GPIOStru.GPIO_Mode = GPIO_Mode_Out_PP;  
	GPIOStru.GPIO_Speed = GPIO_Speed_50MHz;
	GPIOStru.GPIO_Pin = GPIO_Pin_8|GPIO_Pin_9|GPIO_Pin_10|GPIO_Pin_11;

	
	GPIO_Init(GPIOD,&GPIOStru);

	GPIOStru.GPIO_Mode = GPIO_Mode_IPD;  
	GPIOStru.GPIO_Speed = GPIO_Speed_50MHz;
	GPIOStru.GPIO_Pin = GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;

	GPIO_Init(GPIOD,&GPIOStru);	
	
}

static int scan_key(void)
{
	static u8 press=0;
	int KeyVal=0;
	int i;
	
	GPIO_Write(GPIOD,(GPIOD->ODR | 0x0f00));

	if( (GPIOD->IDR & 0xf000)==0x0000 )
	{
		press = 0;
		return 0;
	}
	else
	{
		if(0==press)
		{
			press = 1;
			return 0;
		}
	}
	for( i=0; i<4; i++ )
	{
		GPIO_Write(GPIOD,((GPIOD->ODR & 0xf0fF) | (0x1<<(8+i))));
		switch(GPIOD->IDR & 0xf000)		               
		{
			case 0x8000: KeyVal=4-i;	break;
			case 0x4000: KeyVal=8-i;	break;
			case 0x2000: KeyVal=12-i;	break;
			case 0x1000: KeyVal=16-i;	break;
		}
		
	}
	return KeyVal;
}

char getKeyValue( void )
{
	int res;
	
	res = scan_key();
	if(res==0)
		return 0;
	if(res<4)
		return res+0x30;
	if(res>4 && res <8 )
		return res + 0x2f;
	
	if(res>8 && res <12 )
		return res + 0x2e;
	
	switch(res)
	{
		case 4:
			return 'A';
		case 8:
			return 'B';
		case 12:
			return 'C';
		case 16:
			return 'D';
		case 15:
			return '#';
		case 14:
			return '0';
		case 13:
			return '*';
	}
	
	return 0;
}
