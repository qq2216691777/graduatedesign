#ifndef _MENU_H__
#define _MENU_H__

#include "oled.h"
#include "key.h"
#include "timer.h"
#include "gprsa6.h"
#include "fpmxx.h"

void showFuncMenu( void );
void showModuleList( void );

extern u8 FPMXX_Module_list[128];


#endif

