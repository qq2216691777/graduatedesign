#ifndef	__AT24C02_H
#define	__AT24C02_H

#include	"STC15Fxxxx.H"

void i2c_init();
void delayms(u16 xms);
void start(); //
void stop(); //??i2c
void ack();
void nack();
void send_byte(u8 date);
u8 read_byte();
void write_at24c02(u8 address, u8 date);
u8 read_at24c02(u8 address);


#endif
