/**************************************************************
         果云科技  GPRS A6  模块      
				 
	  51 单片机例程       适用于 STC15F 系列
		
		实验内容 ：  发送PDU短信息


    UART1 映射到 P36 P37口      KEY1 ： 触发发送目标短信
		
PS： A6模块上电注册是要一定的时间的！

在这里也解释下 AT+CMGS=39    39是怎么来的？  AT+CMGS=xx   xx是正文的长度  在PDU模式下，8位真实TP数据单位的长度（即RP层的SMSC地址中的8位字符将不计算在该长度内！）
所以我们转换过来的PDU码长度是40，要减掉SMSC地址中的8位字符,所以是40-1=39。

串口1全双工中断方式收发通讯程序。本例程使用11.0592MHZ时钟，如要改变，请修改下面的"定义主时钟"的值并重新编译。

串口设置为：115200,8,n,1.

通过PC向MCU发送数据, MCU收到后通过串口把收到的数据原样返回.

******************************************/



/*************	本地常量声明	**************/
#define MAIN_Fosc		11059200L	//定义主时钟
#define	BaudRate1		115200UL	//选择波特率


#define	Timer1_Reload	(65536UL -(MAIN_Fosc / 4 / BaudRate1))		//Timer 1 重装值， 对应300KHZ
#define	Timer2_Reload	(65536UL -(MAIN_Fosc / 4 / BaudRate1))		//Timer 2 重装值， 对应300KHZ

#include "STC15Fxxxx.H"
#include "delay.h"
#include "string.h"
#include "Exti.h"

/*************	本地变量声明	**************/

#define Automatic_Startup 1     //定义自启动 V1.2版本起有自启动功能
#define Buf1_Max 60 					  //串口1缓存长度
#define Buf2_Max 60 					  //串口2缓存长度
//串口1发送回车换行
#define UART1_SendLR() UART1_SendData(0X0D);\
											 UART1_SendData(0X0A)


/*************	本地常量声明	**************/
//sbit RUNING_LED = P1^2;					//运行指示灯
sbit LEDA  			= P1^3;					//测试用LED
sbit LEDB       = P1^0;
sbit LEDC       = P1^5;
sbit KEY1       = P3^3;   //发送短信

/*************  本地变量声明	**************/
xdata u8 Uart1_Buf[Buf1_Max];
u8 Register_Flag=0;  //A6  注册标志位  1 ：注册成功  3：注册失败   5：注册成功，但处于漫游状态下，无法拨打电话发短信
u8 Times=0,First_Int = 0,shijian=0;


bdata u8 Flag;//定时器标志位
sbit Timer0_start =Flag^0;	//定时器0延时启动计数器

//长度是40 的PDU码值   内容  ：果云科技-GPRS-A6
static unsigned char *message="0011000D91683145821957F90008AA18679C4E9179D16280002D0047005000520053002D00410036";//发送PDU短信
/*************	本地函数声明	**************/
void USART1_Init(void);   //串口1初始化
void Timer0Init(void);		//20毫秒@11.0592MHz
void CLR_Buf1(void);      //清串口接收缓存
u8 Find(u8 *a);           //查找字符串
void Second_AT_Command(u8 *b,u8 *a,u8 wait_time) ;//a6 at命令发送
void Set_PDU_Mode(void);
void Check_New_Message(void);
void UART1_SendData(u8 dat);//串口1发送 1字节
void UART1_SendString(char *s);//串口1发送 字符串
void Wait_CREG(void);
void Send_Message(void);

/**********************************************/
void main(void)
{
  USART1_Init();//串口初始化
	Timer0Init();//TIM0初始化
	Wait_CREG();//等待GSM注册成功
	Set_PDU_Mode();//设置为PDU模式
  
	while (1)
	{
    if(KEY1==0)
		{	
    Send_Message();
		while(!KEY1);
    }		
		 
	}
}

/********************* UART1中断函数************************/
void UART1_int (void) interrupt UART1_VECTOR
{
	if (RI)
    {
      RI = 0;                           //清除RI位
			Uart1_Buf[First_Int] = SBUF;  	  //将接收到的字符串存到缓存中
			First_Int++;                			//缓存指针向后移动
			if(First_Int > Buf1_Max)       		//如果缓存满,将缓存指针指向缓存的首地址
			{
				First_Int = 0;
			}
    }
    if (TI)
    {
        TI = 0;                          //清除TI位
    }
}

void USART1_Init(void)
{
	S1_8bit();				//8位数据
	//S1_USE_P30P31();		//UART1 使用P30 P31口	默认
	S1_USE_P36P37();		//UART1 使用P36 P37口
	//S1_USE_P16P17();		//UART1 使用P16 P17口
	AUXR &= ~(1<<4);	//Timer stop		波特率使用Timer2产生
	AUXR |= 0x01;		//S1 BRT Use Timer2;
	AUXR |=  (1<<2);	//Timer2 set as 1T mode
	TH2 = (u8)(Timer2_Reload >> 8);
	TL2 = (u8)Timer2_Reload;
	AUXR |=  (1<<4);	//Timer run enable

	REN = 1;	//允许接收
	ES  = 1;	//允许中断

	EA = 1;		//允许全局中断
	

}


/*******************************************************************************
* 函数名 : Timer0Init
* 描述   : 定时器0初始化，20ms定时
* 输入   : 
* 输出   : 
* 返回   : 
* 注意   : 
*******************************************************************************/
void Timer0Init(void)		//20毫秒@11.0592MHz
{
	AUXR &= 0x7F;		//定时器时钟12T模式
	TMOD &= 0xF0;		//
	TMOD |= 0x01;		//设置定时器模式，16位定时器
	TL0 = 0x70;		  //设置定时器初值
	TH0 = 0xFC;		  //设置定时器初值
	TF0 = 0;		    //清TF0标志
	TR0 = 1;		    //定时器0开始计时
	ET0 = 1;    	  //使能定时器0中断
}

/*******************************************************************************
* 函数名 : CLR_Buf1
* 描述   : 清除串口2缓存数据
* 输入   : 
* 输出   : 
* 返回   : 
* 注意   : 
*******************************************************************************/
void CLR_Buf1(void)
{
	u16 k;
	for(k=0;k<Buf1_Max;k++)      //将缓存内容清零
	{
		Uart1_Buf[k] = 0x00;
	}
    First_Int = 0;              //接收字符串的起始存储位置
}



/*******************************************************************************
* 函数名 : Find
* 描述   : 判断缓存中是否含有指定的字符串
* 输入   : 
* 输出   : 
* 返回   : unsigned char:1 找到指定字符，0 未找到指定字符 
* 注意   : 
*******************************************************************************/

u8 Find(u8 *a)
{ 
  if(strstr(Uart1_Buf,a)!=NULL)
	    return 1;
	else
			return 0;
}

/*******************************************************************************
* 函数名 : Second_AT_Command
* 描述   : 发送AT指令函数
* 输入   : 发送数据的指针、希望收到的应答、发送等待时间(单位：S)
* 输出   : 
* 返回   : 
* 注意   : 
*******************************************************************************/

void Second_AT_Command(u8 *b,u8 *a,u8 wait_time)         
{
	u8 i;
	u8 *c;
	c = b;										//保存字符串地址到c
	CLR_Buf1(); 
  i = 0;
	while(i == 0)                    
	{
		
		if(!Find(a))            //查找需要应答的字符
		{
			if(Timer0_start == 0)
			{
				b = c;							//将字符串地址给b
				for (b; *b!='\0';b++)
				{
					UART1_SendData(*b);
				}
				UART1_SendLR();	
				Times = 0;
				shijian = wait_time;
				Timer0_start = 1;
		   }
    }
 	  else
		{
			i = 1;
			Timer0_start = 0;
		}
	}
	CLR_Buf1(); 
}
/*******************************************************************************
* 函数名 : Set_PDU_Mode
* 描述   : 设置短信为PDU模式
* 输入   : 
* 输出   : 
* 返回   : 
* 注意   : 
*******************************************************************************/
void Set_PDU_Mode(void)
{
	u8 buf[30]="AT+CSCS="; 
	Second_AT_Command("ATE0","OK",3);										//取消回显	
	Second_AT_Command("AT+CMGF=0","OK",3);				  //配置短信为PDU模式
	Second_AT_Command("AT+CPMS=\"SM\",\"SM\",\"SM\"","OK",3);//所有操作都在SIM卡中进行

	
 
}

/*******************************************************************************
* 函数名 : Send_Message
* 描述   : 发送PDU信息
* 输入   : 
* 输出   : 
* 返回   : 
* 注意   : 
*******************************************************************************/
void Send_Message(void)
{

	Second_AT_Command("AT+CMGS=39",">",3);//发送指令后直到出现 ">"  后发送内容  
	UART1_SendString(message);//发送内容
	UART1_SendData(0x1A);//结束符
 
}
/*******************************************************************************
* 函数名 : Check_Message_rec
* 描述   : 检查是否有新信息，并执行信息内容指令
* 输入   : 
* 输出   : 
* 返回   : 
* 注意   : 
******************************************************************************/

void Check_New_Message(void)
{
	u8 temp=0;
	if(strstr(Uart1_Buf,"+CMT")!=NULL)   		//若缓存字符串中含有"+CMT"就表示有新的短信
	{
		delay_ms(5);//等待数据全部接收完成
		if(strstr(Uart1_Buf,"R00")!=NULL)
		{ 
		}
			
		CLR_Buf1();
		Second_AT_Command("AT+CMGD=1,4","OK",3);//删除短信
	}
}

/*******************************************************************************
* 函数名 : Wait_CREG
* 描述   : 等待模块注册成功
* 输入   : 
* 输出   : 
* 返回   : 
* 注意   : 
*******************************************************************************/
void Wait_CREG(void)
{
	u8 i;
	u8 k;
	i = 0;
	CLR_Buf1();
  while(i == 0)        			
	{
    
		CLR_Buf1();        
		UART1_SendString("AT+CREG?");
		UART1_SendLR();
		delay_ms(100); 
		
	    for(k=0;k<Buf1_Max;k++)      			
    	{
			if((Uart1_Buf[k] == 'C')&&(Uart1_Buf[k+1] == 'R')&&(Uart1_Buf[k+2] == 'E')&&(Uart1_Buf[k+3] == 'G')&&(Uart1_Buf[k+4] == ':'))
			{
					 
				if((Uart1_Buf[k+8] == '1')&&(Uart1_Buf[k+9] != '3'))
				{
					i = 1;
					Register_Flag=1;
					LEDB =0;
				  break;
				}
        if((Uart1_Buf[k+8] == '1')&&(Uart1_Buf[k+9] == '3')) 
				{
					i = 1;
					Register_Flag=3;
					LEDA =0;
				  break;
				}	
			  if(Uart1_Buf[k+8] == '5') 
				{
					i = 1;
					Register_Flag=5;
					LEDC =0;
				  break;
				}	
				
			}
		}
		
	}
}



/*----------------------------
UART1 发送串口数据
-----------------------------*/
void UART1_SendData(u8 dat)
{
	ES=0;					//关串口中断
	SBUF=dat;			
	while(TI!=1);	//等待发送成功
	TI=0;					//清除发送中断标志
	ES=1;					//开串口中断
}
/*----------------------------
UART1 发送字符串
-----------------------------*/
void UART1_SendString(char *s)
{
	while(*s)//检测字符串结束符
	{
		UART1_SendData(*s++);//发送当前字符
	}
}



/*******************************************************************************
* 函数名 : Timer0_ISR
* 描述   : 定时器0中断服务入口函数,1ms中断一次
* 输入   : 
* 输出   : 
* 返回   : 
* 注意   : 
*******************************************************************************/
void Timer0_ISR() interrupt 1
{
	static u8 Time_count=0; 
	TR0=0;//关定时器
	TL0 = 0x70;		//重设定时器初值
	TH0 = 0xFC;		//重设定时器初值
	
	
	
	Time_count++;
	if(Time_count>=50)
	{
		Time_count = 0;
		if(count_20ms) //20ms延时计数器
		count_20ms--;
	}
	
	if(Timer0_start)
	  Times++;
	if(Times > (50*shijian))
	{
		Timer0_start = 0;
		Times = 0;
	}
	TR0=1;//开定时器
}





