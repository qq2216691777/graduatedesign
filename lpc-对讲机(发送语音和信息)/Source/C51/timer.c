#include "timer.h"
#include <REGX52.H>

void Display( unsigned char num );

extern unsigned int Time_100ms;
extern unsigned int Time_10ms;
extern unsigned char Temp_TX;
extern unsigned char Temp_RX;

void Timer_Init( void )
{
	 TMOD &= 0XF0;
	 TMOD |= 0X01;
	 TH0 = 0xdc;
	 TL0 = 0;
	 EA = 1;
	 ET0 = 1;
	 TR0 = 1;
}

extern unsigned char Stop_time;
extern unsigned char Rece_Data;
void T0_Time() interrupt 1
{
	 TH0 = 0xdc;
	 TL0 = 0;
	 Time_10ms++;
	 if( !Stop_time ) 
	 {
		 if( Time_10ms >9 )
		 {
			Time_10ms = 0;
			Time_100ms++;	
		 } 
		 
		 if( 0==Temp_TX && 0==Temp_RX )
	 		  Time_100ms=0;
	 }
	 else
		Stop_time--;

	
	if( Rece_Data )
	{
		if( Rece_Data==10 )
			Display( 0 );
		else
			Display( Rece_Data );
	}
	else
		Display( Time_100ms );

}

