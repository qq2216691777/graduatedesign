#include <REGX52.H>
#include "timer.h"
#include "uart.h"

sbit led1=P0^0;
sbit led2=P0^1;
sbit led3=P0^2;

sbit Key1=P0^4;
sbit Key2=P0^5;
sbit Key3=P0^6;

sbit GPIO_TX=P1^0;
sbit GPIO_RX=P1^1;

const unsigned char Num_Data[] = { 0x0a, 0xfa, 0x8c, 0xa8, 0x78, 0x29, 0x09,0xba, 0x08, 0x28 };
unsigned int Time_10ms = 0;
unsigned int Time_100ms = 0;
unsigned char Temp_TX=0;
unsigned char Temp_RX=0;
unsigned char Stop_time=0;

void Delay( void )
{
	unsigned char x,y;
	for(x=0;x<250; x++)
	{
	 	y=255;
		while(--y);
	}
}

void Display( unsigned char num )
{
	static unsigned char di=0;
	switch(di++)
	{
	   case 0: P2 = Num_Data[num/100]; led1 = 1; led2 = 0; led3 = 0;  
	   		break;

	   case 1: P2 = Num_Data[(num/10)%10] & 0xf7; led2 = 1;led1 = 0;  led3 = 0;  
	   		break;

	   case 2: P2 = Num_Data[num%10]; led3 = 1; led1 = 0; led2 = 0;  
	   		break;
	}
	if( di>2)
		di=0; 
}

unsigned char Rece_Data = 0;
void main( void )
{
	unsigned char Press_Temp=0;
	unsigned char Press_RTemp=0;
	unsigned char Key1_temp = 0;
	unsigned char Key2_temp = 0;
	unsigned char Uart_Data[]={ 0x41, 0x54, 0x2B, 0x44, 0x4D, 0x4F, 0x4D, 0x45, 0x53, 0x3D, 0x04, 0x35, 0xaf, 0xcd, 0x00, 0x0D, 0x0A  };  //��� 2��	 ����Ƶ��450.05
	unsigned char i;
	
	

	GPIO_TX = 1;
	Timer_Init();
	Uart_Init();			   
	while(1)
	{
		if( Key3 ==0 && Press_Temp==0)
		{
			ES = 0;
			Rece_Data = 0;
			Uart_Sendstring( Uart_Data, 17 );
			for( i=0; i<10; i++ )
			{
				while(!RI);
				RI = 0;
			}
			ES = 1;
			GPIO_TX = 0;
			Temp_TX = 1;
			Press_Temp = 1;
			Stop_time=0;
		}
		else if( Key3 != 0 && Press_Temp==1 )
		{
			GPIO_TX = 1;
			Temp_TX = 0;
			Press_Temp = 0;
			Stop_time=100;
		}

		if( Key1 == 0 && Key1_temp==0 )
		{
			Rece_Data = 0;
			Key1_temp = 1;
			Uart_Data[14]=9;
			ES = 0;
			Uart_Sendstring( Uart_Data, 17 );
			for( i=0; i<10; i++ )
			{
				while(!RI);
				RI = 0;
			}
			ES = 1;
			Uart_Data[14]=0;

		}
		else if( Key1 != 0)
		{
			  Key1_temp = 0;
		}

		if( Key2 == 0 && Key2_temp==0 )
		{
			ES = 0;
			Rece_Data = 0;
			Key2_temp = 1;
			Uart_Data[14]=3;
			Uart_Sendstring( Uart_Data, 17 );
			for( i=0; i<10; i++ )
			{
				while(!RI);
				RI = 0;
			}

			Uart_Data[14]=0;
			ES = 1;
		}
		else if( Key2 != 0)
		{
			  Key2_temp = 0;
		}
		
		if( (!Rece_Data) && GPIO_RX ==0 && Press_RTemp==0 )
		{
			Rece_Data = 0;
			Time_10ms = 0;
			Time_100ms=0;
			Temp_RX = 1;
			Press_RTemp = 1;
					
		}
		else if( GPIO_RX ==1 && Press_RTemp==1 )
		{
			Temp_RX = 0;
			Press_RTemp = 0;
			Stop_time=100;
		}

		
	}

}

