#include "uart.h"
#include <REGX52.H>

void Uart_Init( void )
{
	 TMOD &= 0X0F;
	 TMOD |= 0X20;
	 TH1 = 0XFD;
	 TL0 = 0XFD;
	 TR1 = 1;
	 REN = 1;
	 SM0 = 0;
	 SM1 = 1;
	 EA = 1;
	 ES = 1;
	 
}

void Uart_Sendstring( unsigned char *pData, unsigned char len )
{
	unsigned char i;
	for( i=0; i<len; i++ )
	{
		SBUF = *pData++;
		while( !TI );
		TI = 0;
	}
}

extern unsigned char Rece_Data;
void Uart_Interrupt( void ) interrupt 4
{
	static unsigned char Uart_SData[7];
	RI = 0;
	Uart_SData[6] = Uart_SData[5];
	Uart_SData[5] = Uart_SData[4];
	Uart_SData[4] = Uart_SData[3];
	Uart_SData[3] = Uart_SData[2];
	Uart_SData[2] = Uart_SData[1];
	Uart_SData[1] = Uart_SData[0];
	Uart_SData[0] = SBUF;
	if( Uart_SData[5] == 0x3d && Uart_SData[6] == 0x53 )
	{	
		if( Uart_SData[0] == 0 )	
			Rece_Data = 0;
		else	
			Rece_Data = Uart_SData[0]%100;	
	}



}



