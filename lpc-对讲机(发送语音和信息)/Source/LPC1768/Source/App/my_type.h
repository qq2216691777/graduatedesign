/**
  ******************************************************************************
  *@file      my_type.h
  *@author    Ҷ������
  *@date 	  2016.1.3
  *@version      
  *@brief              
  *@HAUST - RobotLab           
  ******************************************************************************
**/
#ifndef _MY_TYPE_H__
#define _MY_TYPE_H__

/**
 * @ Status type definition
 */
typedef enum {ERROR = 0, SUCCESS = !ERROR} Status;

/**
 * @brief Flag Status and Interrupt Flag Status type definition
 */
typedef enum {RESET = 0, SET = !RESET} FlagStatus, IntStatus, SetState;
#define PARAM_SETSTATE(State) ((State==RESET) || (State==SET))


/**
 * @brief Functional State Definition
 */
typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;
#define PARAM_FUNCTIONALSTATE(State) ((State==DISABLE) || (State==ENABLE))

#undef _SBF
/* Set bit field macro */
#define _SBF(f,v) (v<<f)


#endif
