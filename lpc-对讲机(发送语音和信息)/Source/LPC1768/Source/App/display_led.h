#ifndef _IIC_H__
#define _IIC_H__

void iic0_Init( void );
unsigned char Display_led( unsigned long D_Data );
unsigned char iic_read( unsigned char rEG );
unsigned char Display_Time( unsigned long T_Data, unsigned char  Dir );
unsigned char Display_All( unsigned long D_Data, unsigned char  Dir );

#endif

