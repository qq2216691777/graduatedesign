/*****************************************************************************
 *   i2stest.c:  main C entry file for NXP LPC17xx Family Microprocessors
 *
 *   Copyright(C) 2009, NXP Semiconductor
 *   All rights reserved.
 *
 *   History
 *   2009.05.26  ver 1.00    Prelimnary version, first Release
 *
******************************************************************************/
#include "lpc17xx.h"
#include "uart.h"
#include "lpc1768_i2c.h"
#include "display_led.h"
#include "key.h"

unsigned long Time_Data=0;
unsigned char Temp_TX=0;
unsigned char Temp_RX=0;
unsigned char Key_num=0;

extern unsigned char Uart2Recv_i;
/**
  *******************************
  提供一定的时间延迟等待外设稳定
  *******************************
**/
void Delay_mms( unsigned int mm )
{
	unsigned int iii=0;
	for( ; mm>1; mm-- )
	{
		for(iii=0;iii<65530; iii++);
	}
}

#define TX_OUT  (LPC_GPIO2->FIOPIN&0x01)  //p2.0  1为发射
#define RX_IN  ((LPC_GPIO2->FIOPIN>>1)&0x01)  //p2.1

#define GPIO_TX_1()  LPC_GPIO2->FIOPIN |= (0x01<<3)
#define GPIO_TX_0()  LPC_GPIO2->FIOPIN &= ~(0x01<<3)

#define TX_LED_OFF()   LPC_GPIO2->FIOPIN &= ~(  (0x01<<7) )
#define TX_LED_ON()  LPC_GPIO2->FIOPIN |= (  (0x01<<7) )

#define RX_LED_OFF()   LPC_GPIO2->FIOPIN &= ~( (0x01<<6) )
#define RX_LED_ON()  LPC_GPIO2->FIOPIN |= ( (0x01<<6) )

unsigned char Uart_Data[]={ 0x41, 0x54, 0x2B, 0x44, 0x4D, 0x4F, 0x4D, 0x45, 0x53, 0x3D, 0x04, 0x31, 0xaf, 0xcd, 0x0D, 0x00, 0x0A  };  //编号 1号
/*******************************************************************************
**   Main Function  main()
*******************************************************************************/
int main (void)
{
	
	char Uart_Rx_Data[11];
	unsigned char Press_Temp=0;
	unsigned char Uart_SData[7]={0};
	unsigned char Dis_led=0;
	unsigned char RX_TEMP=0;
	unsigned char TX_NUM=0;
	unsigned char Uart_i;
	unsigned short Rate_RX=0;
	unsigned char Rece_Data = 0;
	unsigned char Rec_Key_Value=0;
	
	
	SystemInit();
	
	UART2_Init(9600); 
	
	LPC_GPIO2->FIODIR |= (1<<3)|(1<<5)|(1<<6)|(1<<7);
	GPIO_TX_1();
	LPC_GPIO2->FIOPIN &= ~( (0x01<<5) );
	
	iic0_Init();
	TX_LED_OFF();
	RX_LED_OFF();
	Delay_mms(50);  //等待外设工作稳定
	
	xprintf("key Init Begin\r\n");
	if( Display_led( Dis_led ) == ERROR)
	{
		xprintf("Key Init Error\r\n");
		while(1);
	}
	xprintf("key Init OK\r\n");
	
	
	SysTick_Config( SystemCoreClock/100 - 1 ); //100ms
	Display_Time( 0,0 );

	while(1)
	{
		if( Key_num ==28 && Press_Temp==0)
		{	
			Display_Time( 0, 2 );
			
			TX_LED_ON();
			for( Uart_i=0; Uart_i<17; Uart_i++)
				UART2_SendByte( Uart_Data[Uart_i] );
			get_line( Uart_Rx_Data, 12 );
			TX_LED_OFF();
			
			GPIO_TX_0();
			Temp_TX = 1;
			Press_Temp = 1;
			TX_NUM = 5;
			Display_led( Uart_Data[11]-0x30 );
		}
		else if( Key_num == 0 && !(--TX_NUM) )
		{
			GPIO_TX_1();
			Temp_TX = 0;
			Press_Temp = 0;
		}
		else if( Key_num==20)    //按键2 查看接收的频率
		{
			Display_All( Rate_RX,1 );
		}
		else if( Key_num==12)	//按键3 查看发射的频率
		{
			Display_All( 45005,2 );
		}
		Rec_Key_Value = KEY_Value( Key_num);
		if( Rec_Key_Value )
		{
			Uart_Data[14] = (Rec_Key_Value%10)*10;
			while( KEY_Value( Key_num) );
			do
			{
				Rec_Key_Value = KEY_Value( Key_num);
			}
			while( !Rec_Key_Value);
			Uart_Data[14] += Rec_Key_Value;
			TX_LED_ON();
			for( Uart_i=0; Uart_i<17; Uart_i++)
				UART2_SendByte( Uart_Data[Uart_i] );
			get_line( Uart_Rx_Data, 12 );
			TX_LED_OFF();
			Uart_Data[14] = 0;
		}		
		
		if( (Rece_Data==0) && RX_IN ==0 && RX_TEMP==0 )
		{
			Temp_RX = 1;
			RX_TEMP = 1;
			
			Display_led( Dis_led );			
		}
		else if( RX_IN !=0 )
		{
			Temp_RX = 0;
			RX_TEMP = 0;
		}
		
		if( ((LPC_UART2->LSR) & 0x01) ==1 ) 
		{
			Uart_SData[6] = Uart_SData[5];
			Uart_SData[5] = Uart_SData[4];
			Uart_SData[4] = Uart_SData[3];
			Uart_SData[3] = Uart_SData[2];
			Uart_SData[2] = Uart_SData[1];
			Uart_SData[1] = Uart_SData[0];
			Uart_SData[0] = LPC_UART2->RBR;
			if( Uart_SData[5] == 0x3d && Uart_SData[6] == 0x53 )
			{	
				RX_LED_ON();
				Dis_led = Uart_SData[3]-0x30;
				Rate_RX = (Uart_SData[2]<<8) | Uart_SData[1];
				Rece_Data = Uart_SData[0];
			
				Display_led( Rece_Data );	
				RX_LED_OFF();
			}
			
		}
	}
}

/**
  **********************************************
	滴答定时器中断  为系统提供时钟
  **********************************************
**/
void SysTick_Handler (void) 
{       
	static unsigned char i=0;
	static unsigned char KEY_temp=0;
	if( Temp_TX ==1 )
	{
		if(++i>9)
		{
			i=0;
			Time_Data++;
			Key_num = iic_read(0x01);
			KEY_temp = 1;
			Display_Time( Time_Data, 2 );
		}
	}
	else if( Temp_RX ==1 )
	{
		if(++i>9)
		{
			i=0;
			Time_Data++;
			Key_num = iic_read(0x01);
			KEY_temp = 1;
			Display_Time( Time_Data, 1 ) ;	
		}
	}
	else
	{
		Key_num = iic_read(0x01);
		i=0;
		Time_Data=0;
		if( Key_num == 27 && KEY_temp == 1 )
		{
			KEY_temp = 0;
			Display_Time( 0, 0 );
			Display_led( 0 );
		}
	}
}

/******************************************************************************
**                            End Of File
******************************************************************************/
