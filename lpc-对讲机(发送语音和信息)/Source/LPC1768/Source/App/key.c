#include "key.h"

unsigned char KEY_Value( unsigned char Value )
{
	if( Value <4 && Value>0 )
		return Value;
	
	
	if( Value <12 && Value>8 )
		return Value-5;
	
	if( Value <20 && Value>16 )
		return Value-10;
	
	if( Value == 26 )
		return 100;	
	
	return 0;
}
