#include "display_led.h"
#include "lpc17xx.h"
#include "lpc1768_i2c.h"

uint8_t Dir_Data[] = {0x02, 0x0e, 0x62 };

void iic0_Init( void )
{
	I2C_InitTypeDef I2C_InitStruct;  
	// 2. I2C initialize
    I2C_InitStruct.Mode = I2C_MASTER;
    I2C_InitStruct.ClockRate = 50000;
    I2C_InitStruct.InterruptMode = I2C_INTERRUPT_MODE;
    I2C0_Init( &I2C_InitStruct );
}

unsigned char iic_read( uint8_t rEG )
{
	uint8_t EData[3];
	uint8_t RData[2];
	EData[0]=0x70;
	EData[1]=rEG;
	EData[2]=(0x70)|0X01;
	if( I2C_OK == I2C_ReadWrite(LPC_I2C0, (uint8_t *) EData, 2, RData, 1) )
		return RData[0] ;
	else
		return 0xfe;
}

unsigned char LED_Data[] ={0xfc, 0x60, 0xda, 0xf2, 0x66, 0xb6, 0xbe, 0xe0, 0xfe, 0xf6,0x0}; 
unsigned char Display_led( unsigned long D_Data )
{  
    uint8_t i = 0;
    uint8_t data[]={0x70, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    uint32_t s;

	data[++i] = 0x10;
	
	data[++i] = LED_Data[10];
	data[++i] = LED_Data[D_Data/10];
	data[++i] = LED_Data[D_Data%10];
	data[++i] = LED_Data[10];
	
	s = I2C_ReadWrite(LPC_I2C0, data, 6, data, 0);
	if(I2C_OK != s )
			return ERROR;
	
	return SUCCESS;
}

unsigned char Display_All( unsigned long D_Data, unsigned char  Dir )
{  
    uint8_t i = 0;
    uint8_t data[]={0x70, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    uint32_t s;

	data[++i] = 0x10;
	
	data[++i] = Dir_Data[Dir];
	data[++i] = LED_Data[D_Data/10000];
	data[++i] = LED_Data[(D_Data/1000)%10];
	data[++i] = LED_Data[(D_Data/100)%10]|0x01;
	data[++i] = Dir_Data[Dir];
	data[++i] = LED_Data[(D_Data/10)%10];
	data[++i] = LED_Data[D_Data%10];
	data[++i] = LED_Data[0];
	
	s = I2C_ReadWrite(LPC_I2C0, data, 10, data, 0);
	if(I2C_OK != s )
			return ERROR;
	
	return SUCCESS;
}
/*
	1 ����
	2 ���
*/
unsigned char Display_Time( unsigned long T_Data, unsigned char  Dir )
{  
    uint8_t i = 0;
	uint8_t Time_s = 0;
	uint8_t Time_m = 0;
    uint8_t data[]={0x70, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    uint32_t s;
	
	Time_s = T_Data%600;
	Time_m = T_Data/600;

	data[++i] = 0x14;
	
	data[++i] = Dir_Data[Dir];
	data[++i] = LED_Data[Time_s/100];
	data[++i] = LED_Data[(Time_s/10)%10]|0x01;
	data[++i] = LED_Data[Time_s%10];
	
	s = I2C_ReadWrite(LPC_I2C0, data, 6, data, 0);
	if(I2C_OK != s )
			return ERROR;
	
	return SUCCESS;
}
