/****************************************Copyright (c)****************************************************
**								   http://www.OpenMCU.com
**--------------File Info---------------------------------------------------------------------------------
** File name:           uart.h
** Last modified Date:  2010-05-12
** Last Version:        V1.07
** Descriptions:        
**
**--------------------------------------------------------------------------------------------------------
** Created by:          OpenMCU
** Created date:        2010-05-10
** Version:             V1.07
** Descriptions:        ��дʾ������
**
**--------------------------------------------------------------------------------------------------------       
*********************************************************************************************************/
#ifndef __UART_H
#define __UART_H
#include "monitor.h"
#include "stdio.h"
void UART0_Init ( unsigned long UART0_BPS);
int  UART0_SendByte (int ucData);
int  UART0_GetChar (void);
void UART0_SendString (unsigned char *s); 

void UART2_Init (unsigned long UART2_BPS);
void  UART2_SendByte ( unsigned char ucData);
unsigned char  UART2_GetChar (void);
void UART2_SendString (unsigned char *s);
void UART2_SendNum (unsigned long num);
void UART0_SendChar(unsigned int disp);  

#endif
