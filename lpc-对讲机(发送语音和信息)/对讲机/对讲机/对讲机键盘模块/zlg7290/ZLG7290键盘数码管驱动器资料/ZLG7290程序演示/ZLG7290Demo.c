/*
	ZLG7290演示程序
*/

#include "I2C.h"
#include "ZLG7290.h"

//定义键盘中断标志，FlagINT=1表示有键按下
volatile bit FlagINT = 0;

/*
函数：INT0_SVC()
功能：ZLG7290键盘中断服务程序
说明：中断触发方式选择负边沿触发，因此不必等待中断请求信号恢复为高电平
*/
void INT0_SVC() interrupt 0
{
	FlagINT = 1;
}

/*
函数：Delay()
功能：延时10ms～655.36s
参数：
	t>0时，延时(t*0.01)s
	t=0时，延时655.36s
说明：
	晶振采用11.0592MHz
*/
void Delay(unsigned int t)
{
	do
	{
		TH0 = 0xDC;
		TL0 = 0x00;
		TR0 = 1;
		while ( !TF0 );
		TF0 = 0;
		TR0 = 0;
	} while (--t);
}

/*
函数：SystemInit()
功能：系统初始化
*/
void SystemInit()
{
	I2C_Init();
	TMOD = 0x01;
	Delay(30);		//等待ZLG7290复位完毕
}

/*
函数：ClearAll()
功能：清除所有显示
*/
void ClearAll()
{
	unsigned char x;
	for ( x=0; x<8; x++ )
	{
		ZLG7290_Download(x,0,0,31);
	}
}

/*
函数：Test_DispBuf()
功能：测试直接写显存
*/
void Test_DispBuf()
{
	code char DispDat[16] =
	{//字母AbCdEFgHiJkLoPqr的字形数据
		0xEE,0x3E,0x9C,0x7A,0x9E,0x8E,0xF6,0x6E,
		0x20,0x70,0x0E,0x1C,0x3A,0xCE,0xE6,0x0A
	};
	unsigned char n;
	unsigned char x;
	unsigned char reg;
	unsigned char dat;
	for ( n=0; n<16; n++ )
	{
		for ( x=0; x<8; x++ )
		{
			reg = ZLG7290_DpRam + x;
			dat = DispDat[n];
			ZLG7290_WriteReg(reg,dat);
		}
		Delay(50);
	}
}

/*
函数：Test_Download()
功能：测试下载数据功能
*/
void Test_Download()
{
	unsigned char x;
	bit dp;
	bit flash;
	char dat;
//点亮所有数码管
	dp = 1;
	flash = 0;
	dat = 8;
	for ( x=0; x<8; x++ )
	{
		ZLG7290_Download(x,dp,flash,dat);
	}
	Delay(100);
//依次显示所有字型
	dp = 0;
	flash = 0;
	for ( dat=0; dat<32; dat++ )
	{
		for ( x=0; x<8; x++ )
		{
			ZLG7290_Download(x,dp,flash,dat);
		}
		Delay(50);
	}
}

/*
函数：Test_ScanNum()
功能：测试不同扫描位数
说明：扫描位数越少，数码管就越亮
*/
void Test_ScanNum()
{
	unsigned char x;
	for ( x=0; x<8; x++ )
	{
		ZLG7290_Download(x,1,0,8);
	}
	Delay(100);
	for ( x=0; x<8; x++ )
	{
		ZLG7290_WriteReg(ZLG7290_ScanNum,x);
		Delay(100);
	}
}

/*
函数：Test_Flash()
功能：测试闪烁功能
*/
void Test_Flash()
{
	char dat = 0x01;
	unsigned char x;
//显示01234567
	for ( x=0; x<8; x++ )
	{
		ZLG7290_Download(x,0,0,x);
	}
//设置闪烁控制寄存器
	ZLG7290_WriteReg(ZLG7290_FlashOnOff,0x11);	
//闪烁演示
	for ( x=0; x<8; x++ )
	{
		ZLG7290_Flash(dat);
		dat <<= 1;
		Delay(300);
	}
//数码管的8个位一起闪烁
	ZLG7290_Flash(0xFF);
	Delay(350);
//停止闪烁
	ZLG7290_Flash(0x00);
	Delay(50);
}

/*
函数：Test_SegOnOff()
功能：测试段寻址功能
*/
void Test_SegOnOff()
{
	unsigned char seg;
	ClearAll();
	Delay(100);
	for ( seg=0; seg<64; seg++ )
	{
		ZLG7290_SegOnOff(seg,1);
		Delay(30);
	}
	Delay(100);
	for ( seg=0; seg<64; seg++ )
	{
		ZLG7290_SegOnOff(seg,0);
		Delay(30);
	}
	Delay(100);
}

/*
函数：DispValue()
功能：显示100以内的数值
参数：
	x：显示位置，取值0～6
	dat：要显示的数据，取值0～99
*/
void DispValue(char x, unsigned char dat)
{
	unsigned char d;
	d = dat / 10;
	ZLG7290_Download(x,0,0,d);
	d = dat - d * 10;
	ZLG7290_Download(x+1,0,0,d);
}

/*
函数：DispHexValue()
功能：以16进制方式显示数值
参数：
	x：显示位置，取值0～6
	dat：要显示的数据，取值0～255
*/
void DispHexValue(char x, unsigned char dat)
{
	unsigned char d;
	d = dat / 16;
	ZLG7290_Download(x,0,0,d);
	d = dat - d * 16;
	ZLG7290_Download(x+1,0,0,d);
}

/*
函数：Test_Key()
功能：测试按键功能
*/
void Test_Key()
{
	unsigned char KeyValue;
	unsigned char RepeatCnt;
	unsigned char FnKeyValue;
	ClearAll();
	EA = 0;
	IT0 = 1;	//负边沿触发中断
	EX0 = 1;	//允许外部中断
	EA = 1;
	for (;;)
	{
		if ( FlagINT )		//如果有键按下
		{
		//清除中断标志
			FlagINT = 0;
		//读取键值、连击计数器值、功能键值
			ZLG7290_ReadReg(ZLG7290_Key,&KeyValue);
			ZLG7290_ReadReg(ZLG7290_RepeatCnt,&RepeatCnt);
			ZLG7290_ReadReg(ZLG7290_FunctionKey,&FnKeyValue);
		//显示键值、连击计数器值、功能键值
			DispValue(0,KeyValue);
			DispHexValue(3,RepeatCnt);
			DispHexValue(6,FnKeyValue);
		}
		PCON |= 0x01;	//使CPU进入空闲状态，任一中断可唤醒
	}
}

void main()
{
	SystemInit();		//系统初始化
	Test_DispBuf();		//测试直接写显存
	Test_Download();	//测试下载数据
	Test_ScanNum();	//测试不同扫描位数
	Test_Flash();		//测试闪烁功能
	Test_SegOnOff();	//测试段寻址功能
	Test_Key();		//测试键盘功能
	while (1);
}
