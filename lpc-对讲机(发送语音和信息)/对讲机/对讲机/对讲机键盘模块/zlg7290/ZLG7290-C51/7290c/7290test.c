/****************************************Copyright (c)**************************************************
**                               广州周立功单片机发展有限公司
**                                     研    究    所
**                                        产品一部 
**
**                                 http://www.zlgmcu.com
**
**--------------文件信息--------------------------------------------------------------------------------
**文   件   名: PCF8563.C
**创   建   人: 叶皓贲
**最后修改日期:  2003-3-4
**描        述: DP-1581的8563T及7289a键盘LED演示程序
**
**--------------历史版本信息----------------------------------------------------------------------------
** 创建人: 叶皓贲
** 版  本: 1.0
** 日　期: 2003-3-4
** 描　述: DP-1581演示程序
**
**------------------------------------------------------------------------------------------------------
** 修改人:
** 版  本:
** 日　期:
** 描　述:
**
**--------------当前版本修订------------------------------------------------------------------------------
** 修改人: 
** 日　期:
** 描　述:
**
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
#include "REG52.h"
#include <intrins.h>
#include <absacc.h>
#include "VIIC_C51.h"
#include "ZLG7290.h"


sbit KEY_INT=P3^2;

/* 全局变量 */

unsigned char disp_buf[8]={0,0,0,0,0,0,0,0};

/*********************************************************************************************************
** 函数名称: 	display
** 功能描述: 	7298A led显示
** 输　入: *sd :	显示缓冲区的头地址
**         
** 输　出: 0 :	OK;
**         1 :	FAIL;
** 全局变量:	无
** 调用模块: 	ZLG7289_SendBuf
**
** 作　者: 叶皓贲
** 日　期: 2003-3-4
**-------------------------------------------------------------------------------------------------------
** 修改人:
** 日　期:
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
unsigned char display(unsigned char  *sd)
{ 
  	disp_buf[0] = (disp_buf[0]&0xf0)|(sd[0]%16);			// 装载" dp-932"
    disp_buf[1] = (disp_buf[1]&0xf0)|(sd[0]/16);		
    disp_buf[2] = 31;
    disp_buf[3] = (disp_buf[3]&0xf0)|(sd[1]%16);
    disp_buf[4] = (disp_buf[4]&0xf0)|(sd[1]/16);
    disp_buf[5] = 31;
    disp_buf[6] = (disp_buf[6]&0xf0)|(sd[2]%16);	
    disp_buf[7] = (disp_buf[7]&0xf0)|(sd[2]/16);
    ZLG7290_SendBuf(disp_buf,8);
	return 0;
}

/*********************************************************************************************************
** 函数名称: 	DelayNS
** 功能描述: 	长软件延时
** 输　入: no :	延时参数，值越大时延时越久
**         
** 输　出: 0 :	OK;
**         1 :	FAIL;
** 全局变量:	无
** 调用模块: 	无
**
** 作　者: 叶皓贲
** 日　期: 2003-3-4
**-------------------------------------------------------------------------------------------------------
** 修改人:
** 日　期:
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
unsigned char DelayNS(unsigned char  no)
{ 
	unsigned char  i,j;					//延时参数

  	for(; no>0; no--)
  	{ 
		for(i=0; i<100; i++)
     		for(j=0; j<100; j++);
  	}
  	return 0; 
}

/*********************************************************************************************************
** 函数名称: 	delay
** 功能描述: 	短软件延时
** 输　入: j :	延时参数，值越大时延时越久
**         
** 输　出: 0 :	OK;
**         1 :	FAIL;
** 全局变量:	无
** 调用模块: 	无
**
** 作　者: 叶皓贲
** 日　期: 2003-3-4
**-------------------------------------------------------------------------------------------------------
** 修改人:
** 日　期:
**------------------------------------------------------------------------------------------------------
********************************************************************************************************/
unsigned char  delay(unsigned char  j)
{
  	unsigned char  k,l;
  	for(l=0;l<=j;l++)
    	for(k=0;k<=250;k++);
	return 0;
}

void  main()
{	
	/*定义发送缓冲区*/
  	unsigned char  rd[5]={0x12,0x48,0x30,0x03,0x04};           	
	unsigned char  key,flash,i,k;  	

	flash=1;
	DelayNS(10);
  	while(1)
  	{
  		delay(100); 
  		display(rd);
  		delay(100);
		if(KEY_INT==0)
		{
			key=ZLG7290_GetKey();
			switch(key)
			{
				case 1 ://确定
					for(k=0;k<8;k++)
					{
						disp_buf[k] = disp_buf[k]&0xbf;//去除闪烁
					}                        
					break;
				case 2 :                        //光标左移
					disp_buf[flash]= disp_buf[flash]&0xbf;
					flash=flash+1;
					if(flash==8)
						flash=0;
					disp_buf[flash]= disp_buf[flash]|0x40;
					break;
				case 3 :                        //光标右移
					disp_buf[flash]= disp_buf[flash]&0xbf;
					flash=flash-1;
					if(flash==0)
						flash=8;
					disp_buf[flash]= disp_buf[flash]|0x40;
					break;
				case 4 :                        //光标值加一
					if(flash==2||flash==5)	break;
					if((flash%3)==0) 
 				    	{
							i=flash;
							i=(i/3);
							rd[i]=rd[i]+1;
						}
					else if((flash%3)!=0)
						{
							i=flash;
							i=(i/3);
							rd[i]=rd[i]+16;	
						}
						delay(50);
						break;
				case 5 :                            //光标值减一
					if(flash==2||flash==5)	break;
					if((flash%3)==0) 
 				    	{
							i=flash;
							i=(i/3);
							rd[i]=rd[i]-1;
						}
					else if((flash%3)!=0)
						{
							i=flash;
							i=(i/3);
							rd[i]=rd[i]-16;	
						}
						
					break;
				case 6 :
					
					break;
				case 7 :
				
					break;
				case 8 :
					break;
				default :
					break;
			}
			while(KEY_INT==0);
		}
  	}
}



