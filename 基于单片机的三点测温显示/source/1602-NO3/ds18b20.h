#ifndef uchar

#define uchar unsigned char
#define uint unsigned int

#endif


#ifndef _DS18B20_H_
#define _DS18B20_H_

#include< reg52.h >
#include< intrins.h>


sbit DS18B20 = P3^3;   /*******DS18B20�ź�����*******/

void DS18B_delay( uint );
void delay60us(void);
int DS18B20_Read_dat( void );


#endif