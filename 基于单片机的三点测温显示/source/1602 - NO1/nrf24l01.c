#include < reg52.h >
#include "nrf2401.h"
#include "nrf24l01.h"




sbit CE = P1^4;
sbit CSN = P1^7;
sbit SCK = P1^3;
sbit IRQ = P1^5;
sbit MOSI = P1^6;
sbit MISO = P1^2;

sbit key = P3^2;  		/**********************注意********************/
sbit LED = P3^3;  		/**********************注意********************/

uchar LOCAL_address[5] = { 0x01, 0x01, 0x01, 0x01, 0x02 };
uchar RX_address[5] = { 0x01, 0x01, 0x01, 0x01, 0x01 };

uchar send_dat[TX_PLOAD_WIDTH] = { 0x20 };
uchar rece_dat[TX_PLOAD_WIDTH] = { 0 };

void NRF_delay( uchar d_time);

 /*
void main(void)
{
	uchar send_da[TX_PLOAD_WIDTH] = { 0x20 };	     //要发送的数据
	uchar xx ;
	LED = 0;
	nrf_init();		 							// nrf24L01初始化
	delay( 100 );
	while( 1 )	  								// 发送
	{
		key = 1;
		if( 1 )
		{
			nrf_tx_mode( send_dat );			//发送数据

			xx = SPI_read_reg( STATUS );		//读取状态
			SPI_RW_REG( 0x20 + STATUS, 0xff);	//写0xff到状态寄存器  方便下次读取
			
			if( ( xx & 0x20 ) || ( xx & 0x10 ) )	  //判断xx的值  如果为0001 1110 则为发送到最高次数
			{
				P0 = xx;
			 	LED = 1;
				delay(1000);
				delay(1000);
				xx = 0;
				nrf_clear_tFIFO();			   //清除TX_FIFO寄存器
			}
		}
		LED = 0;
	}		
  	   
  
	nrf_rx_mode( );			//	进入接收模式	
	while( 1 )
	{
		nrf_rx_mode( );					//不可缺少此语句
		xx = SPI_read_reg( STATUS );
		SPI_RW_REG( 0x20 + STATUS, 0xff);
		P0 = xx;
		if( (xx & 0x40) == 0x40 )
		{
			SPI_READ_BUF(RD_RX_PLOAD,rece_dat,TX_PLOAD_WIDTH);	// 将接收到的数据读到rece_dat数组中 
				
			P0 = xx ;
			if( rece_dat[0] == 0x20 )
			{
				LED = 1;
			}
			xx = SPI_read_reg( STATUS );
			SPI_RW_REG( 0x20 + STATUS, 0xff);
			nrf_clear_rFIFO();			   //清除TX_FIFO寄存器
			xx = 0;
			while(1);
		} 
	 } 	
}*/

void nrf_init( void )
{
	SCK = 0;
	CE = 0;
	CSN = 1;
	IRQ = 1;

	SPI_RW_REG( WRITE_REG + EN_AA, 0x01 );  	 // 打开数据通道自动应答允许位
	SPI_RW_REG( WRITE_REG + EN_RXADDR, 0x01 );	 // 接收通道允许位
	SPI_RW_REG( WRITE_REG + SETUP_RETR, 0x1a );	 //自动重发设置
	SPI_RW_REG( WRITE_REG + RF_CH, 40 );		 //工作频率通道设置
	SPI_RW_REG( WRITE_REG + RX_PW_P0, TX_PLOAD_WIDTH );	// 设置接收数据通道0有效宽度
	SPI_RW_REG( WRITE_REG + RF_SETUP, 0x0f );	 //射频设置
}

void nrf_clear_tFIFO( void )
{
 	CSN = 0;
	SPI_RW(FLUSH_TX);
	CSN = 1;
	SPI_RW_REG(WRITE_REG+STATUS,0xff);
	IRQ=1;
}

void nrf_clear_rFIFO( void )
{
 	CSN = 0;
	SPI_RW(FLUSH_RX);
	CSN = 1;
	SPI_RW_REG(WRITE_REG+STATUS,0xff);
	IRQ=1;

}

void nrf_tx_mode( uchar *tx_data )	  //	tx_data 为需要发送的数据的数组
{
	CE = 0;							  
	SPI_WRITE_BUF( WRITE_REG + TX_ADDR, LOCAL_address, 5);		//设置本地地址
	SPI_WRITE_BUF( WRITE_REG + RX_ADDR_P0, RX_address, 5 ); //设置接收端地址
	SPI_WRITE_BUF( WR_TX_PLOAD, tx_data, TX_PLOAD_WIDTH ); //将要发送的数据写入寄存器

	SPI_RW_REG( WRITE_REG + CONFIG, 0x0e );				   //设置为 发送模式
	CE = 1;
	NRF_delay(7000);   //需要至少 150us 的延迟 
}

void nrf_rx_mode( void )
{
	CE = 0;
	SPI_WRITE_BUF( WRITE_REG + RX_ADDR_P0, LOCAL_address, 5 ); //设置本地地址

	SPI_RW_REG( WRITE_REG + CONFIG, 0x0f );						//设置为接收模式
	CE = 1;
	NRF_delay( 400 );

}

uchar SPI_WRITE_BUF(uchar reg, uchar *WB_value, uchar WB_num )
{
	uchar WB_status, WB_i;
	CSN = 0;
	WB_status = SPI_RW( reg );
	NRF_delay( 100 );
	for( WB_i = 0; WB_i < WB_num; WB_i ++ )
	{
		SPI_RW( *WB_value ++ );

	}
	CSN = 1;
	return WB_status;
}

uchar SPI_READ_BUF( uchar reg, uchar *RB_value, uchar RB_num )
{
	uchar RB_sta, RB_i;
	CSN = 0;
	RB_sta = SPI_RW( reg );
	for( RB_i = 0; RB_i < RB_num; RB_i ++ )
	{
		RB_value[RB_i] = SPI_RW(0);

	}
	CSN = 1;
	return 	RB_sta;

}

uchar SPI_read_reg( uchar r_reg)
{
	uchar reg_sta;
	CSN = 0;
	SPI_RW( r_reg );
	reg_sta = SPI_RW( 0x00 );
	CSN = 1;
	return reg_sta;

}

uchar SPI_RW_REG( uchar rw_reg, uchar rw_value )
{
	uchar rreg_sta;
	CSN = 0;
	rreg_sta = SPI_RW( rw_reg );
	SPI_RW( rw_value );
	CSN = 1;
	return rreg_sta;
}

uchar SPI_RW( uchar rw_data)
{
	uchar rw_i;
	for( rw_i = 0; rw_i < 8; rw_i ++)
	{
		MOSI = ( rw_data & 0x80 );
		SCK = 1;
		rw_data = ( rw_data << 1 );
		rw_data = ( rw_data | MISO );
		SCK = 0;
	}
	return rw_data;
}

void NRF_delay( uint d_time)
{
 	uint i;
	for( i = 0; i<d_time; i++);
	for( i = 0; i<d_time; i++);
	for( i = 0; i<d_time; i++);

}