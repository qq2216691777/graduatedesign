#include < reg52.h >
#include "ds18b20.h"
#include "nrf24l01.h"

sbit RS = P3^6;	   //lcd1602 位定义
sbit RW = P3^5;
sbit E = P3^4;
void LCD_1602_init( void );		//lcd1602初始化函数
void LCD_1602_Write_com( uchar w_com );	 // lcd1602 写指令函数
void LCD_1602_Write_data( uchar w_data );	 // lcd1602 写数据函数
void delay(uint y_time);			//延迟函数
void LCD_1602_Write_String( uchar h, uchar *w_data );
void Set_No1_tem( float t );
void Set_No2_tem( float t );
void Set_No3_tem( float t );

void main( void )
{
	int tr=0;
	uchar xx ;
	int tr2=0;
	int tr3=0;

	delay(1000);
	LCD_1602_init();
	LCD_1602_Write_String( 1, "NO:1  NO:2  NO:3");
	Set_No1_tem(0);
	Set_No2_tem(0);
	Set_No3_tem(0);
	nrf_init();		 							// nrf24L01初始化
	delay( 100 );
//	DS18B20_Read_dat();	
	while(1)
	{
		tr =   DS18B20_Read_dat();
		Set_No1_tem((float)(tr/100.0));
		Set_No2_tem((float)(tr2/100.0));
		Set_No3_tem((float)(tr3/100.0));
		
		nrf_rx_mode( );					//不可缺少此语句
		xx = SPI_read_reg( STATUS );
		SPI_RW_REG( 0x20 + STATUS, 0xff);
		
		if( (xx & 0x40) == 0x40 )
		{
			SPI_READ_BUF(RD_RX_PLOAD,rece_dat,TX_PLOAD_WIDTH);	// 将接收到的数据读到rece_dat数组中 
				
			if( rece_dat[0] == 2 )
			{
				tr2 =  (rece_dat[2]<<8)|rece_dat[1];
			}
			else if( rece_dat[0] == 3 )
			{
				tr3 =  (rece_dat[2]<<8)|rece_dat[1];
			}
			xx = SPI_read_reg( STATUS );
			SPI_RW_REG( 0x20 + STATUS, 0xff);
			nrf_clear_rFIFO();			   //清除TX_FIFO寄存器
			
		} 
	}

}

void Set_No1_tem( float t )
{
	uint t_num;
	LCD_1602_Write_com( 0x80+0x40 );
 	if( t<0 )
		t = 0;
	else if(t>99.9 )
		t = 99.9;
	t_num = t*10;
	LCD_1602_Write_data(t_num/100 + 0x30);
	LCD_1602_Write_data(t_num/10%10 + 0x30);
	LCD_1602_Write_data('.');
	LCD_1602_Write_data(t_num%10 + 0x30); 
}

void Set_No2_tem( float t )
{
	uint t_num;
	LCD_1602_Write_com( 0x80+0x46 );
	if( t<0 )
		t = 0;
	else if(t>99.9 )
		t = 99.9;
	t_num = t*10;
	LCD_1602_Write_data(t_num/100 + 0x30);
	LCD_1602_Write_data(t_num/10%10 + 0x30);
	LCD_1602_Write_data('.');
	LCD_1602_Write_data(t_num%10 + 0x30); 
}

void Set_No3_tem( float t )
{
	uint t_num;
	LCD_1602_Write_com( 0x80+0x4c );
 	if( t<0 )
		t = 0;
	else if(t>99.9 )
		t = 99.9;
	t_num = t*10;
	LCD_1602_Write_data(t_num/100 + 0x30);
	LCD_1602_Write_data(t_num/10%10 + 0x30);
	LCD_1602_Write_data('.');
	LCD_1602_Write_data(t_num%10 + 0x30); 
}

void LCD_1602_init( void )
{
	E = 0;
	RW = 0;
	LCD_1602_Write_com( 0x38 );
	delay(5);
	LCD_1602_Write_com( 0x0c );
	delay(5);
	LCD_1602_Write_com( 0x06 );
	delay(5);
	LCD_1602_Write_com( 0x01 );
	delay(5);
	LCD_1602_Write_com( 0x02 );
	delay(5);
	LCD_1602_Write_com( 0x80 );
	delay(5);

}

void LCD_1602_Write_com( uchar w_com )
{
	RS = 0;
	RW = 0;
	P2 = w_com;
	delay( 5 );
	E = 1;
	delay( 5 );
	E = 0;


}

void LCD_1602_Write_data( uchar w_data )
{
	RS = 1;
	RW = 0;
	P2 = w_data;
	delay( 5 );
	E = 1;
	delay( 5 );
	E = 0;
}
void LCD_1602_Write_String( uchar h, uchar *w_data )
{
	uchar i = 0;
	LCD_1602_Write_com( 0x80 );
	if( h & 0x2 )
	{
		 LCD_1602_Write_com( 0x80+0x40 );
	}
	while( w_data[i] != '\0')
		LCD_1602_Write_data(w_data[i++]);
}

void delay(uint y_time)
{
		y_time*=14;
		while(y_time--);
}


