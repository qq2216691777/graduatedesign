#ifndef _NRF24L01_H__
#define _NRF24L01_H__
#include "nrf2401.h"

#define TX_PLOAD_WIDTH 3  // 设置发送数据的位数
#ifndef uchar

#define uchar unsigned char
#define uint unsigned int

#endif

uchar SPI_RW( uchar );
void nrf_init( void );
uchar SPI_RW_REG( uchar rw_reg, uchar rw_value );
uchar SPI_WRITE_BUF(uchar reg, uchar *WB_value, uchar WB_num );
uchar SPI_READ_BUF( uchar reg, uchar *RB_value, uchar RB_num );
void nrf_tx_mode( uchar *tx_data );
uchar SPI_read_reg( uchar r_reg);
void nrf_rx_mode( void );
void nrf_clear_tFIFO( void );
void nrf_clear_rFIFO( void );

extern uchar rece_dat[TX_PLOAD_WIDTH];
#endif
