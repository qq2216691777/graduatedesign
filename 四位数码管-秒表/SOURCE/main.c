#include <reg52.h>

#define uchar unsigned char
#define uint unsigned int

/* 数码管编码 0~9 共10个 */
uchar num[] = { 0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f};

/* 数码管显示的数值变量 0~9999 */
uint display_num = 0;

/* 10ms级变量 */
uint ms10  = 0;

/* 1s级变量 */
uint s1  = 0;


/**
  *·定时器0的初始化函数
  * 用于数码管的动态扫描
**/
void Time0_Init()
{
	TMOD &= 0XF0;
	TMOD |= 0X01;		//设置定时器0的工作方式为1
	TH0 = (65536-10000)/256;	  //装初值  定时10ms  @12Mhz
	TL0 = (65536-10000)%256;

	EA = 1;
	ET0 = 1;
	TR0 = 1;
}
/**
  *·定时器1的初始化函数
  * 用于
**/
void Time1_Init()
{
	TMOD &= 0XF0;
	TMOD |= 0X10;		//设置定时器1的工作方式为1
	TH1 = (65536-10000)/256;	  //装初值  定时10ms  @12Mhz
	TL1 = (65536-10000)%256;	   

	EA = 1;
	ET1 = 1;
	TR1 = 0;
}

/**
 * 数码管扫描函数
**/
void digitron_scan()
{
	static uchar dula=1;

	dula = dula*2;
	if( dula > 11)
		dula = 1;
	
	P2 |= 0x0f;
	P2 &=  ~dula;
	switch( dula )
	{
	 	case 0x01:
			P0 = num[s1/10%10];
			break;

		case 0x02:
			P0 = num[s1%10]|0x80;
		    break;

		case 0x04:
			P0 = num[ms10/10%10];
			break;

		case 0x08:
			P0 = num[ms10%10];
		    break;
	}


}


void delay()
{
 	int x=1000;
	while(x--);
}

sbit cs = P3^7;
sbit wr = P3^6;
sbit LE1 = P3^5;

sbit K1 = P2^5;
sbit K2 = P2^6;
sbit K3 = P2^7;
/**
  *·按键扫描化函数
**/
void key_scan()
{
 	if( !K1 )
	{
	   delay();
	   if( !K1 )
	   {
	   		TR1 = 1;
	   }
		while(!K1);
	}
	else if( !K2 )
	{
	   delay();
	   if( !K2)
	   {
	   	   TR1 = 0;
	   }
		while(!K2);
	}
	else if( !K3 )
	{
	   delay();
	   if( !K3)
	   {
	   		TR1 = 0;
		   s1 = 0;
		   ms10 = 0;
	   }
	   while(!K3);
	}
}

void main()
{
	Time0_Init();
	Time1_Init();
	cs = 0;
	wr = 0;
   	LE1 = 1;

	while(1)
	{
	
		key_scan();
	}

}



void Time0_handler() interrupt 1
{
	TH0 = (65536-20000)/256;	  //装初值  定时20ms  @12Mhz
	TL0 = (65536-20000)%256;

	digitron_scan();
	

}

void Time1_handler() interrupt 3
{
   	TH1 = (65536-10000)/256;	  //装初值  定时10ms  @12Mhz
	TL1 = (65536-10000)%256;	

   ms10++;
   if( ms10 > 99 )
   {
		ms10 = 0;
		s1 ++;
   }
}