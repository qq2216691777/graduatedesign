#include<reg51.h>
#include<intrins.h>
#include<stdio.h>
#define uint unsigned int
#define uchar unsigned char
sbit CLK=P2^0;
sbit ST=P2^1;
sbit EOC=P2^2;
sbit OE=P2^3;

sbit beep=P2^7;

sbit X1=P3^4;
sbit X2=P3^5;
sbit X3=P3^6;
sbit P07 = P0^7;


sbit LED=P3^3;
sbit jidianqi = P3^7;

sbit adA = P3^1;
sbit adB = P3^2;

sbit waterP = P2^5;
sbit airP = P2^6;

uint temp;
int getdata;
float waterData = 0;  //保存水压的数值
float airData = 0;    //保存气压的数值
uchar b;
uchar s;
uchar g;
uchar code table[10]={0xC0,0xF9,0xA4,0xB0,0x99,0x92,0x82,0xF8,0x80,0x90};
void delayus(uchar us){
	uchar i;
	while(us--)
	for(i=0;i<120;i++)	
	;
}

void Delay25ms()		//@11.0592MHz
{
	unsigned char i, j;

	i = 45;
	j = 208;
	do
	{
		while (--j);
	} while (--i);
}

void Delay35ms()		//@11.0592MHz
{
	unsigned char i, j;

	i = 63;
	j = 190;
	do
	{
		while (--j);
	} while (--i);
}




void init(){
	TMOD=0x21;
	TH0=(65536-200)/256;
	TL0=(65536-200)%256;
	TH1=0XFD;
	TL1=0XFD;
	PCON=0X00;
	SCON=0X50;
	ES=1;
	ET0=1;
	EA=1;
	TR1=1;
	TR0=1;
}
void timer0()interrupt 1
{	TH0=(65535-200)/256; 
	TL0=(65535-200)%256; 
	CLK=~CLK;
}

void disp(){
		X1=1;
		X2=0;
		X3=0;
		P0=table[b];
	//	P07=0;
		X1=1;
		X2=0;
		X3=0;
		delayus(5);
		P0=0XFF;
	
		P0=table[s];
		X1=0;
		X2=1;
		X3=0;
		P07=0;
		delayus(5);
		P0=0XFF;

		P0=table[g];
		X1=0;
		X2=0;
		X3=1;
		delayus(5);
		P0=0XFF;
}

unsigned char getTemValue()
{
	unsigned char dat1;
	
	//Delay20ms();
  	ST=0;
	OE=0;
	ST=1;
	ST=0;
	while(EOC==0);
	OE=1;
	dat1=P1;

	OE=0;
	return dat1;
}

unsigned char getWaterValue()
{
	unsigned char dat2;
	
  	ST=0;
	OE=0;
	ST=1;
	ST=0;
	while(EOC==0);
	OE=1;
	dat2=P1;

	OE=0;
	return dat2;
}

unsigned char getAirValue()
{
	unsigned char dat3;
	
	
  	ST=0;
	OE=0;
	ST=1;
	ST=0;
	while(EOC==0);
	OE=1;
	dat3=P1;

	OE=0;
	return dat3;
}

void main()
{
	unsigned char warning=0;
	unsigned char index=0;
	P1=0XFF;
	beep = 0;
	init();
	
	while(1)
	{
		switch(index)
		{
		 	case 0:
				airData = getAirValue()*50/255;	
				adA = 0;
				adB = 0;
	
				break;
			case 1:
				getAirValue();	
				break;

			case 2:
				getdata = getTemValue();
				adA = 1;
				adB = 0;
			   break;
			case 3:
				getAirValue();	
				break;
			case 4:
				waterData =  getWaterValue()*50/255;
				adA = 0;
				adB = 1;
			   break;
			case 5:
				getAirValue();	
				break;
			default:
				break;	
		}
		index = (index+1)%6;
		
		temp=getdata*1.6;
		b=temp/100;
		s=temp/10%10;
		g=temp%10;

		disp();

		if( temp>=250 )
		{
		 	jidianqi = 0;
		}
		else
			jidianqi = 1;

		
		if(waterData<10 || waterData>40 )
		  	warning=1;
		else
			warning=0;	

		
		if(airData<5 || airData>10 )
		{
		  	warning++;
		}  	  

		if(warning)
		{
		  	beep = 1;
			LED = 0;
		}
		else
		{
			beep = 0;
			LED = 1;
		}
		Delay25ms();
	//	Delay25ms();
	}
}
void ser()interrupt 4
{	
	if(RI==1)
	{
		RI=0;

	}
}

