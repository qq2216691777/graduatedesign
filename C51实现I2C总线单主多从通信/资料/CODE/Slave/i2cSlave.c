#include "i2cslave.h"
#include "delay.h"
#define u8 unsigned char
#define uchar unsigned char

void LCD_1602_Write_String( uchar h, uchar *w_data );
void LCD_1602_Write_Num( uchar h,uchar s, int num );

char regData[12]={8};


sbit SCL = P1^0;
sbit SDA = P1^1;

u8 startSIGNAL();
u8 receiveData( u8 *);
void sendACK();
void sendData( u8 dat);

unsigned char pos=0;

void loop(void)
{
	u8 res;
	u8 p;
 	if( !startSIGNAL() )
		return ;

	res = receiveData(0);   			 //device address
	if( (res&0xfe) == 0xd0 )		 //�ӻ���ַΪ0xd0
	{
		
		sendACK();
		if( res&0x01 )	 //0:write  1:read
		{
			LCD_1602_Write_Num(0,0,99);
			while(1);
		}
		else
		{
		 	res = receiveData(0);
			sendACK();   
		
			if((res>=0)&&(res<12))
				pos = res;
		
		    res = receiveData(&p);
			if(0==p)
			{
				 while(SCL);
				 res = receiveData(0);   			 //device address
				 sendACK();
				 	
				 if( res == 0xd1 )
				 {
				 	 // regData[pos] = 2;
					  sendData( regData[pos] );
					  while(SCL);
		//			  LCD_1602_Write_Num(0,0,pos);
					  LCD_1602_Write_Num(0,5,regData[pos]);
					 
				 }
			}
			else
			{
				sendACK();
				regData[pos] = res;
			}
		//	pos=(pos++)%12;
		}
	//	LCD_1602_Write_Num(0,0,pos);	
	//	LCD_1602_Write_Num(0,5,regData[2]);
	}
	while(!(SDA&&SCL));

	
}

u8 startSIGNAL()
{
	if(!(SCL&&SDA))
	{
	 	return 0;
	}
	while( SCL );
	Delay100us();
	if(SDA)
		return 0;
	else
		return 1;
}

u8 receiveData( u8 *p)
{
	u8 rdata=0x00;
	u8 i=0;
	u8 lastSDA = 0;
	 
	for(i=0;i<8;i++)
	{
	    rdata = rdata<<1;
	  	while(!SCL);
		;;
		lastSDA = SDA;
		while(SCL)
		{
		  	if(SDA!=lastSDA)
			{
				*p = 0;
				return 0;
			}
		}

		if(SDA)
			rdata++;

	}
//	LCD_1602_Write_Num( 0,0, i);
	*p = 1;
	return 	rdata;
}

void sendData( u8 dat)
{
	u8 i;
	while(!SCL);
	for(i=0;i<8;i++)
  	{
		while(SCL)
		{
		 	SDA = (dat>>7);
		}
		while(!SCL);
		dat = dat<<1;
	}
	SDA = 1;
}

void sendACK()
{
 	while(!(SCL&&SDA));
	while(SCL)
	{
		SDA = 0;
	}
	SDA = 1;
}

u8 stopSIGNAL()
{
 	if(SCL&&SDA)
		return 1;
	else
		return 0;
}