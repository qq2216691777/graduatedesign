 
 /*************************此部分为AT2402的驱动程序使用I2C总线连接*************************************/

#include<i2c_51.h>

/***************write one byte***********************************************************
功能：将ch写到地址add处																	*
输入：地址add,待写字节数据ch															*
输出：无																				*
****************************************************************************************/

void e2promWriteByte(unsigned char add,unsigned char ch)
{
	I2C_Start();									//start

	WriteI2CByte(0xa0);	while(!Check_Acknowledge());//control byte:1010+A2+A1+A0+1(write)/0(read)
	
	WriteI2CByte(add);	while(!Check_Acknowledge());//address
	WriteI2CByte(ch);	while(!Check_Acknowledge());//data
	I2C_Stop();									    //stop
}


/***************Read one byte************************************************************
功能：从地址ch处读取一字节数据															*
输入：地址add																			*
输出：从地址add处读得的一字节数据														*
****************************************************************************************/
char  e2promReadByte(unsigned char add)
{
	char b;
	I2C_Start();                                     //start
	WriteI2CByte(0xa0);	 while(!Check_Acknowledge());//control byte,
	WriteI2CByte(add);   while(!Check_Acknowledge());//address
	I2C_Start();									 //start
	WriteI2CByte(0xa1);  while(!Check_Acknowledge());//data
	b=ReadI2CByte();        		//N0 Acknowledge
	I2C_Stop();										 //stop	
	return b;
}



