#include <REG52.H>
#include<stdlib.h>
#include "digitron.h"
#include "Function.h"
#include "delay.h"
#include "slave.h"

#define  AT24C02 0xa0  //AT24C02 地址

extern unsigned int displayNum;		 //数码管显示的数据

unsigned char pDat[8];
unsigned char slaveData;

sbit KEY=P3^6;
void main()
{
	unsigned char wData;
	Delay500ms();
	Delay500ms();

  	time0Init();

	pDat[0] = 0;
	
	while(1)
	{
	
		wData = rand()%10;
		slaveWriteByte( 2, wData );	  	//向从机写入一个数
		slaveData = slaveReadByte(2);	 //从从机读取一个数

	//	wData = rand()%10;
		e2promWriteByte(1,wData);
		pDat[0]=e2promReadByte(1);
		displayNum = slaveData;
	  	Delay500ms();
		Delay500ms();
		Delay500ms();
	
		while(!KEY);

	}


}
