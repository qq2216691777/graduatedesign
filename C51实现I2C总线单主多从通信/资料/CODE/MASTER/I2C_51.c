/*************************************
    单片机模拟I2C总线C语言代码
*************************************/
///#include<string.h>
#include<reg52.h>
#include "I2C_51.h"
//#include<intrins.h>


/********** Function Definition 函数定义 ***************/
void DELAY(unsigned int t) /*延时函数*/
{
	while(t!=0)
	t--;
}
/*启动I2C总线的函数，当SCL为高电平时使SDA产生一个负跳变*/
void I2C_Start(void)
{
	SDA=1;
	SCL=1;
	DELAY(DELAY_TIME);
	SDA=0;
	DELAY(DELAY_TIME);
	SCL=0;
	DELAY(DELAY_TIME);
}
/*终止I2C总线，当SCL为高电平时使SDA产生一个正跳变******/
void I2C_Stop(void)  
{
	SCL=0;
	SDA=0;
	SCL=1;
	DELAY(DELAY_TIME);
	SDA=1;
	DELAY(DELAY_TIME);
	SCL=0;
	DELAY(DELAY_TIME);
}
/*发送0，在SCL为高电平时使SDA信号为低*****************/
void SEND_0(void) /* SEND ACK */
{
	SDA=0;
	SCL=1;
	DELAY(DELAY_TIME);
	SCL=0;
	DELAY(DELAY_TIME);
}
/*发送1，在SCL为高电平时使SDA信号为高****************/
void SEND_1(void)
{
	SDA=1;
	SCL=1;
	DELAY(DELAY_TIME);
	SCL=0;
	DELAY(DELAY_TIME);
}
/*发送完一个字节后检验设备的应答信号****************/
bit Check_Acknowledge(void)
{
	SDA=1;
	SCL=1;
	DELAY(DELAY_TIME/2);
	F0=SDA;
	DELAY(DELAY_TIME/2);
	SCL=0;
	DELAY(DELAY_TIME);
	if(F0==1)
		return FALSE;
	return TRUE;
}
/*向I2C总线写一个字节*******************************/
void WriteI2CByte(char b)reentrant
{
	char i;
	for(i=8;i!=0;i--){   //for(i=0;i<8;i++)
		if(b&0x80)
			SEND_1();
		else
			SEND_0();
		b<<=1;
		/*if((b<<i)&0x80)  //原来写法
			SEND_1();
		else
			SEND_0();*/
	}

}
/*从I2C总线读一个字节******************************/
char ReadI2CByte(void)reentrant
{
	char b=0,i;
	for(i=0;i<8;i++)
	{
		SDA=1;     /*释放总线*/
		SCL=1;     /*接受数据*/
		DELAY(10);
		F0=SDA;
		DELAY(10);
		SCL=0;
		if(F0==1)
		{
			b=b<<1;
			b=b|0x01;
		}
		else
		   b=b<<1;
	}
	return b;
}

