#include<reg51.h>	//头文件
#include <intrins.h>
#define uint unsigned int
#define uchar unsigned char
uchar code table[]={0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f}; uint i,j;
uchar l;	//l 为地址的数据
uchar dizhi; uchar z;
uchar a;	//a 为信息码
uchar d;	//编码后的数据
uchar e;	//监督码
sbit sda=P3^1;	//数据线
sbit scl=P3^0;	//时钟线
void display(e); void delay1s(z);
void delay()	//微秒级延时子程序
{;;}

void delay1(unsigned int m)	//延时子程序 1
{
unsigned int n; for(n=0;n<m;n++);
}


void delay1s(z)	//延时子程序 2
{
for(i=z;i>0;i--)
for(j=110;j>0;j--);
}

//==================================I2C         总线程序===========================================

void init()	//初始化
{
scl=1;	//I2C 总线进行数据传送时，



delay();	//时钟信号为高电平期间，
sda=1;	//数据线上的数据必须保持稳定。
delay();
}


void start()	//起始信号
{
sda=1;	//SCL 线为高电平期间，
delay();	//SDA    线由高电平向低电平的变化表示起始信号
scl=1; delay(); sda=0; delay(); scl=0;
delay();	//在起始信号产生后，总线就处于被占用的状态
}





void stop()	//终止信号
{
sda=0;	//SCL 线为高电平期间，
delay();	//SDA    线由低电平向高电平的变化表示终止信号
scl=1; delay(); sda=1;
delay();	//在终止信号产生后，总线就处于空闲状态
}




void respons()	//应答信号
{
unsigned char i=0; scl=1;
delay(); while((sda==1)&&(i<255))
i++;
scl=0; delay();
}

//=============================向  24C02     写入数据===========================================




void writebyte(unsigned char j)	//写一个字节
{
unsigned char i,temp; temp=j;
for (i=0;i<8;i++)
{
temp=temp<<1;	//数据左移一位，把次高位放在最高位
scl=0;	//时钟线上的信号为低电平期间，数据才允许变化 delay();
sda=CY;	//temp 左移时，移出的值放入了 CY 中
delay();
scl=1;	//待 sda 线上的数据稳定后，将 scl 拉高
delay();
}

scl=0; delay();
sda=1;	//主机发送完数据后释放 SDA
delay();
}





void write24c02(unsigned char add,unsigned char date) //向 24c02 的 add 地址中写入一字节数据 date//
{
start();	// 起始和终止信号都是由主机发出的
writebyte(0xa0);	//发送该器件的 7 位地址码和写方向位“0”
respons();
writebyte(add);	//写入器件的首地址
respons();
writebyte(date);	//开始写入数据
respons(); stop();
delay1(5000);	//24c02 在从 sda 上取得数据后，需要一定时间的烧录过程。

}





//=============================从  24C02     读出数据===========================================

unsigned char readbyte()	//读一个字节



{
unsigned char i,j,k=0; scl=0;
delay(); sda=1;

for (i=0;i<8;i++)
{
delay();
scl=1;	//释放 SCL,准备接收数据
delay();

if(sda==1)	//如果数据线为高电平
j=1;

else
j=0;
k=(k<<1)|j;	//则将字节的数据左移一位，最低位写 1，否则写 0
scl=0;	//将 SCL 线拉低，使主机处于等待状态
}
delay();
return(k);	//返回读取的一个字节数据
}







unsigned char read24c02(unsigned char add)	//从 24c02 的地址 add 中读取一个字节数据//
{
unsigned char i; start(); writebyte(0xa0); respons(); writebyte(add); respons(); start(); writebyte(0xa1); respons(); i=readbyte(); stop(); delay1(100);
return(i);	//返回读取的一个字节数据
}



//=============================编码子程序===========================================

void	encoding()
{
d=a+4;	//0011 0110=0x36;
}


void supervision()	//生成监督码子程序
{
int f=4;
e=(a&0xc7)+f;	//0000 0010	--0000 0110=0x06;(显示数字 1)

}



//=============================监督码显示子程序===========================================

void display(e)
{

uint g;
uchar ten,one; ten=e/10; one=e%10;

for(g=1000;g>0;g--)
{
P0=table[ten]; P2=0xfb; delay1s(1); P2=0xff;




P0=table[one]; P2=0xf7;
delay1s(1); P2=0xff;
}
}


//=============================主程序===========================================
void main()



{
dizhi=0;	//地址位数据归零
init();	//初始化 24C02
l=0x11;	//给出地址位	从机 1 地址位	***选择从机位（0x11,0x12,0x13）

write24c02(2,l);	//向从机写入地址位数据 dizhi=read24c02(3);
if(dizhi!=0)	//如果地址位匹配成功，主机根据对应的地址位给该从机发送数据

{
a=0x55;	//信息码：0011 0010=0x32=50
supervision();	//生成监督码
display(e);	//数码管显示监督码(数字 6) encoding();		//编码	结果为 0x36
write24c02(4,d);	//向 24c02 中的地址为 4 的存储单元写入数据 d write24c02(5,e);		//向从机发送监督码，与从机生成的监督码相比较

}
}