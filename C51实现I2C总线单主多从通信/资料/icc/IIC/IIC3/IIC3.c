//==================================从机  3    程序===========================================

#include<reg51.h>	//头文件
#include <intrins.h>
#define uint unsigned int
#define uchar unsigned char 
uchar code table1[]="Data sent";
uchar code table2[]="successfully!!!";

uchar code table3[]="Data send "; uchar code table4[]="failure...";
uchar code table5[]="Whether to host"; uchar code table6[]="the request";

uchar t,command,info,num; sbit rs=P2^5;
sbit rw=P2^6; sbit e=P2^7;

void	lcd_start();
void write_com(command); void write_info(info);
uchar l;	//地址位发出的应答信号
uchar a;	//地位位数据
uchar d;	//接收到的数据
uchar p;	//解码后的数据
uchar s;	//监督码
uchar n;	//接收到的主机监督码

sbit alarm=P2^0;	//蜂鸣器
sbit sda=P0^5;	//数据线
sbit scl=P0^4;	//时钟线
void delay2(t);
void lcd_display1();	//液晶显示发送成功
void lcd_display2();	//液晶显示发送失败
void delay()	//微秒级延时函数。
{;;}

void delay1(unsigned int m)	//延时函数
{
unsigned int n; for(n=0;n<m;n++);
}




//=================================I2C        总线==========================================


void init()	//初始化
{
scl=1;	//I2C 总线进行数据传送时，
delay();	//时钟信号为高电平期间，
sda=1;	//数据线上的数据必须保持稳定。
delay();
}


void start()	//起始信号
{
sda=1;	//SCL 线为高电平期间，
delay();	//SDA    线由高电平向低电平的变化表示起始信号
scl=1; delay(); sda=0; delay(); scl=0;
delay();	//在起始信号产生后，总线就处于被占用的状态
}





void stop()	//终止信号
{
sda=0;	//SCL 线为高电平期间，
delay();	//SDA    线由低电平向高电平的变化表示终止信号
scl=1; delay(); sda=1;
delay();	//在终止信号产生后，总线就处于空闲状态
}




void respons()	//应答信号
{
unsigned char i=0; scl=1;
delay();



while((sda==1)&&(i<255)) i++;
scl=0; delay();
}


//==================================写===========================================

void writebyte(unsigned char j)	//写一个字节
{
unsigned char i,temp; temp=j;
for (i=0;i<8;i++)
{
temp=temp<<1;	//数据左移一位，把次高位放在最高位
scl=0;	//时钟线上的信号为低电平期间，数据才允许变化 delay();
sda=CY;	//temp 左移时，移出的值放入了 CY 中
delay();
scl=1;	//待 sda 线上的数据稳定后，将 scl 拉高
delay();
}

scl=0; delay();
sda=1;	//主机发送完数据后释放 SDA
delay();
}





void write24c02(unsigned char add,unsigned char date) //向 24c02 的 add 地址中写入一字节数据 date//
{
start();	// 起始和终止信号都是由主机发出的
writebyte(0xa0);	//发送该器件的 7 位地址码和写方向位“0”
respons();
writebyte(add);	//写入器件的首地址
respons();
writebyte(date);	//开始写入数据
respons(); stop();
delay1(5000);	//24c02 在从 sda 上取得数据后，需要一定时间的烧录过程。
}







//==================================读===========================================

unsigned char readbyte()	//读一个字节
{
unsigned char i,j,k=0; scl=0;
delay(); sda=1;

for (i=0;i<8;i++)
{
delay();
scl=1;	//释放 SCL,准备接收数据
delay();

if(sda==1)	//如果数据线为高电平
j=1;

else
j=0;
k=(k<<1)|j;	//则将字节的数据左移一位，最低位写 1，否则写 0
scl=0;	//将 SCL 线拉低，使主机处于等待状态
}
delay();
return(k);	//返回读取的一个字节数据
}







unsigned char read24c02(unsigned char add)	//从 24c02 的地址 add 中读取一个字节数据//
{
unsigned char i; start(); writebyte(0xa0); respons(); writebyte(add); respons(); start(); writebyte(0xa1); respons();



i=readbyte(); stop(); delay1(100);
return(i);	//返回读取的一个字节数据
}




//==============================监督码生成函数============================================


void supervision()
{	int f=4;
s=(p&0xc7)+f;	//0000 0110=0x06;(传送成功显示数字 6)
}




//==================================解码子程序=========================================


void decode()
{

p=d+(-4);
}

//==================================液晶显示=========================================


void lcd_display1()	//比较监督码后，若与主机相同，
{	//则认为发送成功，液晶显示发送成功内容
lcd_start(); delay2(50); write_com(0x80); delay2(50);
for(num=0;num<9;num++)
{
write_info(table1[num]); delay2(50);

}
write_com(0xc0); delay2(50); for(num=0;num<15;num++)



{
write_info(table2[num]); delay2(50);

}
}




void lcd_display2()	//比较监督码后，若与主机不同
{	//则认为发送失败，液晶显示发送失败内容
lcd_start(); delay2(50); write_com(0x80); delay2(50);
for(num=0;num<9;num++)
{
write_info(table3[num]); delay2(50);

}
write_com(0xc0); delay2(50); for(num=0;num<18;num++)
{
write_info(table4[num]); delay2(50);

}
}





void lcd_display3()	//比较监督码后，若与主机不同
{	//则认为发送失败，液晶显示发送失败内容
lcd_start(); delay2(50); write_com(0x80); delay2(50);
for(num=0;num<15;num++)
{
write_info(table5[num]); delay2(50);



}
write_com(0xc0); delay2(50); for(num=0;num<11;num++)
{
write_info(table6[num]); delay2(50);

}
}




void write_com(command)	//写指令函数//
{
P3=command; rs=0;
rw=0; e=0; e=1;
}


void write_info(info)	//写数据函数//
{
P3=info; rs=1; rw=0; e=0; e=1;
}




void lcd_start()	//开始端函数//
{
write_com(0x38);	//显示模式设置//
delay2(50);
write_com(0x0c);	//显示开及光标设置//
delay2(50);
write_com(0x06);	//显示光标移动设置//
delay2(50);
write_com(0x01);	//显示清屏//
delay2(50);

}



void delay2(t)	//延时函数//
{
uint i,j; for(i=t;i>0;i--)
for(j=110;j>0;j--);
}
//=================================主程序=========================================

void main()	//主函数//
{
delay1(500);	//等待数据传送完闭再接收
init();	//初始化 24C02
while(1)
{
a=read24c02(2);	//读出地址位数据
l=0x33;	//应答信号的数据 （可将其用于请求信号）
if(a==0x13)	//从机 C 的地址位
{
write24c02(3,l);	//地址位正确发出的应答信号
d=read24c02(4);	//从 24c02 存储单元里的子地址 4 里读取数据。
n=read24c02(5);	//将主机生成的监督码接收过来，以做比较
decode();	//解码
supervision();	//生成监督码
P1=s;	//把生成的监督码后的数据通过 P1 口输出

if(s==n)
{
lcd_display1();	//液晶显示成功内容 
alarm=1;		//蜂鸣器不工作
}

else
{

lcd_display2();	//液晶显示失败内容
alarm=0;	//否则，蜂鸣器报警
delay2(5000);
}

lcd_display3();	//液晶显示是否发送请求内容
}
}
}

