#include "slave.h"


void slaveWriteByte(unsigned char add,unsigned char ch)
{
	I2C_Start();									//start

	WriteI2CByte(0xd0);		while(!Check_Acknowledge());//control byte:1010+A2+A1+A0+1(write)/0(read)
//	displayNum = 77;
	WriteI2CByte(add);	while(!Check_Acknowledge());//address
//	displayNum = 88;

	WriteI2CByte(ch);	while(!Check_Acknowledge());//data
	I2C_Stop();									    //stop

}



/***************Read one byte************************************************************
功能：从地址ch处读取一字节数据															*
输入：地址add																			*
输出：从地址add处读得的一字节数据														*
****************************************************************************************/
char  slaveReadByte(unsigned char add)
{
	char b;
	I2C_Start();                                     //start
	WriteI2CByte(0xd0);	 while(!Check_Acknowledge());//control byte,
	WriteI2CByte(add);   while(!Check_Acknowledge());//address
	I2C_Start();									 //start
	WriteI2CByte(0xd1);  while(!Check_Acknowledge());//data
	b=ReadI2CByte();        		//N0 Acknowledge
	I2C_Stop();	
										 //stop	
	return b;
}