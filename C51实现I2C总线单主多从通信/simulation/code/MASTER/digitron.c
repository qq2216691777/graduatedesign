#include "digitron.h"

unsigned int displayNum = 0;
unsigned char digiData[10] = {0xc0,0xf9,0xa4,0xb0,0x99,0x92,0x82,0xf8,0x80,0x90};

void time0Init(void)
{
	TMOD &= 0XF0;
	TMOD |= 0X01;
	TH0 = TH0NUM;
	TL0 = TL0NUM;

	EA = 1;
	ET0 = 1;
	TR0 = 1;	

}



unsigned char disbit=0;
void T0timer() interrupt 1
{

	TH0 = TH0NUM;
	TL0 = TL0NUM;


	switch(disbit)
	{
	 	case 0:
			P2 &= 0XF0;
			P2 |= 0X01;
			P1 = digiData[displayNum/1000%10];
				break;	

		case 1:
			P2 &= 0XF0;
			P2 |= 0X02;
			P1 = digiData[(displayNum/100)%10];
				break;
 
		case 2:
			P2 &= 0XF0;
			P2 |= 0X04;
			P1 = digiData[(displayNum/10)%10];
				break;

		case 3:
			P2 &= 0XF0;
			P2 |= 0X08;
			P1 = digiData[(displayNum)%10];
				break;	 
		default: break;

	}
	disbit = (disbit+1)%4;

}


