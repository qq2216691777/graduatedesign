#ifndef _I2C_51_H__
#define _I2C_51_H__
#include <REG52.H>

#define DELAY_TIME 60 /*只有不小于50！才不会造成时序混乱*/
#define TRUE 1
#define FALSE 0

sbit SCL=P3^0;    /*串行时钟*/
sbit SDA=P3^1;   /*串行数据*/

/***函数原型声明*****************************************************************************/
void I2C_Start(void);                /*启动I2C总线的函数，当SCL为高电平时使SDA产生一个负跳变*/
void I2C_Stop(void);                 /*终止I2C总线，当SCL为高电平时使SDA产生一个正跳变**** **/
bit Check_Acknowledge(void);         /*发送完一个字节后检验设备的应答信号***********    *****/
void WriteI2CByte(char b)reentrant;  /*向I2C总线写一个字节************************    *******/
char ReadI2CByte(void)reentrant;     /*从I2C总线读一个字节***********************     *******/
void DELAY(unsigned int t);
/********************************************************************************************/


#endif
