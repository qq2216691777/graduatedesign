#include "main.h"

void My_display( void );

elevaror My_ele;


void Delay500ms()		//@11.0592MHz
{
	unsigned char i, j, k;

	i = 4;
	j = 129;
	k = 119;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}

void My_ele_Init( void )
{
	uchar i=ELEVATOR_FLOAR_MAX;
	My_ele.now_floor = 1; 					//启动电梯后默认在1楼
	My_ele.now_status = 0;					//启动电梯后默认静止 
	My_ele.door_status = 0;					//启动后门默认为关闭状态  0关闭  1打开
	My_ele.speed = 0;
	for( ;i>(ELEVATOR_FLOAR_MIN-1);i-- )    //把任务置零
	{
		My_ele.up_floor[i-1]=0;
		My_ele.down_floor[i-1]=0;
	 	My_ele.tar_floor[i-1]=0;

	}
	My_ele.run = My_ele_fun;
	My_ele.display = My_display;
	My_ele.input = My_input;
}
extern int real_pwm;
uchar My_ele_fun( void )
{
	uchar i,ii;
	uchar res=0;
	while( My_ele.now_status == 0 )   //STOP
	{	 
		 for( i=0; i<ELEVATOR_FLOAR_MAX; i++ )
		 {
		  	if( My_ele.up_floor[i]==1 )
			{
				My_ele.tar_floor[i] = 1;
				 My_ele.up_floor[i]=0;
			}
			if( My_ele.down_floor[i]==1 )
			{
				My_ele.tar_floor[i] = 1;
				 My_ele.down_floor[i]=0;
			}
			
		 }
		for( i=My_ele.now_floor; i<ELEVATOR_FLOAR_MAX; i++ )
		 {
		  	if( My_ele.tar_floor[i]==1 )
			{
				My_ele.now_status = 1;
				break;
			}
		 }

		 for( i=My_ele.now_floor; i>1; i-- )
		{
				if( My_ele.tar_floor[i-2]==1 )
				{
					My_ele.now_status = 2;
					break;
				}
		}
		 break;
	}

	while( My_ele.now_status == 1 )  //UP
	{
		 // 判断当前楼层之上有没有任务
		 for( i=My_ele.now_floor; i<ELEVATOR_FLOAR_MAX; i++ )
		 {
		  	if( My_ele.up_floor[i]==1 )
			{
				My_ele.tar_floor[i] = 1;
				My_ele.up_floor[i] = 0;
			}
		 }
		 if( My_ele.speed == 0 )
		 {
			 ttime = 0;
			 My_ele.speed = 5;
			 My_ele.display();
			 //1S加速  				
	  		 while(ttime<10)
			 {
			 	My_ele.input();
			 }
			 if(!M_Speed)
			 {
			  	Beep = 0; 
				while(1);
			 }
			 My_ele.speed = 10;
			 ttime = 0;
			 while(ttime<10)
			 {
			 	My_ele.input();
			 }
			 My_ele.speed = 14;
		} 
		  	// 有则上移 并结束函数
		 
		//2s匀速
		ttime = 0;
		My_ele.speed = 14;
		while(ttime<40)
		{
		 	My_ele.input();
		}
		//检查楼层是否需要停止
		if( My_ele.tar_floor[My_ele.now_floor]==1)
		{
			//1.1 需要停止 1s减速
			ttime = 0;
			My_ele.speed = 0;
			while(ttime<40)
			{
			 	My_ele.input();
			}
			ttime = 0;
			My_ele.tar_floor[My_ele.now_floor] = 0;
			My_ele.now_floor++;
			do
			{

				//1.2 开门  2s后关闭电梯
				My_ele.door_status = 1;
				My_ele.display();
				
				while(ttime<20)
				{
				 	My_ele.input();
					if( (ttime%2) )
						step_M(1);
				}
				ttime = 0;
				while((ttime<80)&&(key_v!=2));
				{
				 	My_ele.input();
				}
				ttime = 0;
				while((ttime<20)&&(key_v!=2));
				{
				 	My_ele.input();
				}
				//2.3 关门
				My_ele.door_status = 0;
				My_ele.display();
				ttime = 0;
				ii=0;
				while(ttime<20)
				{
				 	My_ele.input();
					if(!K37)
						ii=1;
					if( (ttime%2) )
						step_M(0);
				}
			}while(ii);
			for( i=My_ele.now_floor; i<ELEVATOR_FLOAR_MAX; i++ )
			 {
			  	if( My_ele.tar_floor[i]==1 )
					break;
			 }
			 if( i<ELEVATOR_FLOAR_MAX )
			 	My_ele.now_status = 1;
			else
				My_ele.now_status = 0;
		}
		else
		{
			ttime = 0;
			My_ele.now_floor++;
			My_ele.display();
			//1.2 不需要  0.5+2.5s匀速
			My_ele.speed = 14;
			while(ttime<20)
			{
			 	My_ele.input();
			}
		}	
		 break;	
	}

	while( My_ele.now_status == 2 )   //DOWN
	{
		// 判断当前楼层之下有没有任务
		for( i=My_ele.now_floor; i>1; i-- )
		{
			if( My_ele.down_floor[i-2]==1 )
			{
				My_ele.tar_floor[i-2]=1;
			 	My_ele.down_floor[i-2]=0;
			}
		}
		if( My_ele.speed == 0 )
		{			 	
			//1S加速
			ttime = 0;
			My_ele.speed = -5;
			My_ele.display();
			while(ttime<10)
			{
			 	My_ele.input();
			}
			if(!M_Speed)
			 {
			  	Beep = 0; 
				while(1);
			 }
			My_ele.speed = -10;
			ttime = 0;
			while(ttime<10)
			{
			 	My_ele.input();
			}	 
			My_ele.speed = -14;
			
		}
			
		
		//2s匀速
		ttime = 0;
		My_ele.speed = -14;
		while(ttime<40)
		{
		 	My_ele.input();
		}
		//检查楼层是否需要停止
		if( My_ele.tar_floor[My_ele.now_floor-2]==1)
		{
			//1.1 需要停止 1s减速
			ttime = 0;
			My_ele.speed = 0;
			while(ttime<40)
			{
			 	My_ele.input();
			}
			ttime = 0;
			My_ele.tar_floor[My_ele.now_floor-2] = 0;
			My_ele.now_floor--;
			do
			{
				key_v=0;
				My_ele.door_status = 1;
				My_ele.display();
				//1.2 开门
				while(ttime<20)
				{
				 	My_ele.input();
					if( (ttime%2) )
						step_M(1);
				}
				ttime = 0;
				while((ttime<80)&&(key_v!=2));
				{
				 	My_ele.input();
				}
				ttime = 0;
				while((ttime<20)&&(key_v!=2));
				{
				 	My_ele.input();
				}
	
				//2.3 关门
				My_ele.door_status = 0;
				My_ele.display();
				ttime = 0;
				ii=0;
				while(ttime<20)
				{
				 	My_ele.input();
					if(!K37)
						ii=1;
					if( (ttime%2) )
						step_M(0);
				}
			}while(ii);
			for( i=My_ele.now_floor; i>1; i-- )
			{
				if( My_ele.tar_floor[i-2]==1 )
					break;
			}
			if( i>1 )
				My_ele.now_status = 2;
			else
				My_ele.now_status = 0;	
		}
		else
		{
			//1.2 不需要  0.5+2.5s匀速
			ttime = 0;
		//	while(ttime<10);
			My_ele.now_floor--;
			My_ele.display();
			My_ele.speed = -14;
			//1.2 不需要  0.5+2.5s匀速
			while(ttime<20)
			{
			 	My_ele.input();
			}
		}
		  
		break;		 
		
	}
	

		
	return res;
}

void My_input( void )
{
	uchar i,j,ii;
	if( key_v )
	{
		if( (key_v==1) && (My_ele.speed == 0) && (My_ele.door_status != 1) )
		{
			do
			{
				key_v=0;
				ttime = 0;
				My_ele.door_status = 1;
				My_ele.display();
				//1.2 开门
				while(ttime<20)
				{
					if( (ttime%2) )
						step_M(1);
				}
				ttime = 0;
				while((ttime<80)&&(key_v!=2));
				ttime = 0;
				while((ttime<20)&&(key_v!=2));
	
				//2.3 关门
				My_ele.door_status = 0;
				My_ele.display();
				ttime = 0;
				ii=0;
				while(ttime<20)
				{
					if((!K37)|| (key_v==1))
						ii=1;
					if( (ttime%2) )
						step_M(0);
				}
			}while(ii);

		}
		else if( (key_v==2) && (My_ele.speed == 0) && (My_ele.door_status == 1) )
		{
			key_v = 0;
			//2.3 关门
			My_ele.door_status = 0;
			My_ele.display();
			ttime = 0;
			ii=0;
			while(ttime<20)
			{
			 	
				if((!K37)|| (key_v==1))
					ii=1;
				if( (ttime%2) )
					step_M(0);
			}
		
			while(ii)
			{
				key_v = 0;
				ttime = 0;
				My_ele.door_status = 1;
				My_ele.display();
				//1.2 开门
				while(ttime<20)
				{
					if( (ttime%2) )
						step_M(1);
				}
				ttime = 0;
				while((ttime<80)&&(key_v!=2));
				ttime = 0;
				while((ttime<20)&&(key_v!=2));
	
				//2.3 关门
				My_ele.door_status = 0;
				My_ele.display();
				ttime = 0;
				ii=0;
				while(ttime<20)
				{
					if((!K37)|| (key_v==1))
						ii=1;
					if( (ttime%2) )
						step_M(0);
				}
			}

		}
		switch( key_v )
		{
		 /*	case 4:	My_ele.tar_floor[8] = 1;
					break;
			case 5:	My_ele.tar_floor[7] = 1;
					break;
			case 6:	My_ele.tar_floor[6] = 1;
					break;
			case 7:	My_ele.tar_floor[5] = 1;
					break;*/
			case 8:	My_ele.tar_floor[4] = 1;
					break;
			case 9:	My_ele.tar_floor[3] = 1;
					break;
			case 10:My_ele.tar_floor[2] = 1;
					break;
			case 11: My_ele.tar_floor[1] = 1;
					break;
			case 12: My_ele.tar_floor[0] = 1;
					break;
			case 13: My_ele.up_floor[4] = 1;	//上
					break;
			case 14: My_ele.down_floor[4] = 1;	  //下
					break;
			default:   key_v= 0; break;
		}
		j=0;
		for( i=0; i<9; i++ )
		{
			if( My_ele.tar_floor[i] )
			{
				Draw_word( i+4,8+((j/5)*2),2,(j%5)*16,16);
				j++;
			}	
		}
		for(; j<9; j++ )
		{
			Draw_word( 14,8+((j/5)*2),2,(j%5)*16,16);	
		}
		key_v= 0;
//		My_ele.display();
//		My_ele.tar_floor[My_ele.now_floor-1] = 0;		
	}
}


void My_display( void )
{
	uchar i,j;
	Draw_word(My_ele.now_floor+3,4,2,65,16);
	if( My_ele.now_status == 1 )
		Draw_word( 2,4,2,85,16);
	else if( My_ele.now_status == 2 )
		Draw_word( 3, 4, 2, 85, 16 );
	else
		Draw_word( 14, 4, 2, 85, 16 );
	j=0;
	for( i=0; i<9; i++ )
	{
		if( My_ele.tar_floor[i] )
		{
			Draw_word( i+4,8+((j/5)*2),2,(j%5)*16,16);
			j++;
		}	
	}
	for(; j<9; j++ )
	{
		Draw_word( 14,8+((j/5)*2),2,(j%5)*16,16);	
	}

	if( My_ele.door_status )
		Draw_word( 15,8,2,105,16);//kai
	else
		Draw_word( 16,8,2,105,16);//kai

}
