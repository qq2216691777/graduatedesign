#ifndef _ELEVATOR_H__
#define _ELEVATOR_H__

#define 	ELEVATOR_FLOAR_MAX		9	   //最高层为9
#define 	ELEVATOR_FLOAR_MIN 		1	   //最底层为1

typedef struct 
{
	uchar now_floor;	 //当前所在楼层
	uchar tar_floor[9];	 //所需要到达的楼层
	uchar up_floor[9];
	uchar down_floor[9];
	uchar now_status;	 //当前电梯前进状态 0停止  1上升  2下降
	uchar door_status;	 //当前门的状态
	int	   speed;
	void (*input)( void );
	uchar (*run)( void ); //输入按键内容 正常返回0
	void (*display)( void );
}elevaror;

extern elevaror My_ele;

void My_ele_Init( void );
uchar My_ele_fun( void );
void My_input( void );

#endif
