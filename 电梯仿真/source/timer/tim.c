#include "main.h"

void Tim0_Init()
{
	TMOD &= 0XF0;
	TMOD |= 0X01;
	TH0 = 0x3c;
	TL0 = 0xb0;
	EA = 1;
	ET0 = 1;
	TR0 = 1;
}

void Tim1_Init()
{
	TMOD &= 0X0f;
	TMOD |= 0X10;
	TH1 = 0xf2;
	TL1 = 0x3c;
	EA = 1;
	ET1 = 1;
	TR1 = 1;
}

void Delay10ms_temp()		//@11.0592MHz
{
	unsigned char i, j;

	i = 49;
	j = 235;
	do
	{
		while (--j);
	} while (--i);
}

int real_pwm=0;
int time_v=0;
extern char d_l1[];
extern char d_l2[];
extern elevaror My_ele;
extern int M_Speed_temp;
void T0_time() interrupt 1
{
	uchar key_temp;
	int temp_v;
	static last_v;
	TH0 = 0x3c;
	TL0 = 0xb0;

	key_temp = Key_scan_4x3();
	if(key_temp)
		last_v = key_temp;
	else if( last_v )
	{		
		while( last_v==3 )
		{
		   Beep = 0;
		   Delay10ms_temp();
		   Beep = 1;
		   Delay10ms();
		}
		key_v=last_v;
		last_v= 0;
	}

	if(My_ele.speed != 0)
		time_v++;
	else
		time_v = 0;
	if( time_v>10 )
	{
		time_v = 10;
		 if(!M_Speed)
		 {
		  	Beep = 0; 
			while(1);
		 }
	}
	ttime++;
	M_Speed = (M_Speed_temp/2.1);
	M_Speed_temp=0;
	temp_v = Motor_PID( My_ele.speed, M_Speed ) + My_ele.speed;
	if(temp_v>30)
		temp_v = 30;
	else if(temp_v<(-30))
		temp_v = -30;
  	
	real_pwm = temp_v ;	
}



int Motor_PID( int Tar_v, int Now_v )
{
	const float P=2.5;
	const float I=0.1;
	const float D=0.0;
	int error;
	int output;
	static float I_error=0;
	static float LI_error=0;
	error =  Tar_v-Now_v;
	I_error += error;
	

	if( I_error>5 )
		I_error = 5;
	else if( I_error<-5 )
		I_error = -5;
	 output = error*P+I_error*I+(error-LI_error)*D;
	 LI_error = error;
	 return output;
}

void T1_time() interrupt 3
{
	static uchar v=0;
	TH1 = 0xfa;
	TL1 = 0x3c;
	v++;
	if(v>30)
		v=0;
	if( real_pwm==0 )
	{
	 	M_PWM = 0;
	}
	else if( real_pwm>0 )
	{
		MOTOR_DIR(1);
		if( v>real_pwm )
		   M_PWM = 0;
		else
			M_PWM = 1;
	}
	else
	{
		MOTOR_DIR(0);
		if( v>(-real_pwm) )
		   M_PWM = 0;
		else
			M_PWM = 1;
	}
	


}



