#ifndef _MAIN_H__
#define _MAIN_H__

#include <REGX52.H>

#define uchar unsigned char
#define uint unsigned int


#include "lcd1602.h"
#include "elevator.h"
#include "tim.h"
#include "12864.h"
#include "interrupt.h"

void Delay10ms();
uchar Key_scan_4x3( void );
int Motor_PID( int Tar_v, int Now_v );
void step_M( char d );

sbit Beep=P2^3;
sbit K37=P3^7;

extern uchar ttime;
extern uchar key_v;
extern int M_Speed;

#define	 MOTOR_DIR(n)  	M_IN1=n;M_IN2=!n;

sbit M_IN1=P2^4;
sbit M_IN2=P2^5;
sbit M_PWM=P3^6;
sbit M_DIR=P3^4;

sbit MA=P0^5;
sbit MB=P0^6;
sbit MC=P0^7;
sbit MD=P3^0;

#endif
