
#ifndef _LCD1602_C51_H__
#define _LCD1602_C51_H__

#include "main.h"

void Lcd1602_init(void);
void write_com(uchar c_dat);
void write_data(uchar d_dat);
void lcd1602_delay(uint y_time);

void lcd1602_dischar( uchar x, uchar y, char *c_data );

#endif
