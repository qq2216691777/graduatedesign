/**
  ***************************************************
  @filename: 	lcd1602.c
  @project:		sheji.uvproj
  @author:		Yenianxifeng
  @brief:		lcd1602 for 89c52
  ***************************************************
**/

#include "lcd1602.h"


/*** Lcd1602 ***/
sbit LCD_E = P2^2;
sbit LCD_RS = P2^0;
sbit LCD_RW = P2^1;	




void Lcd1602_init(void)
{
	LCD_E=0; 				 //	RS  0-1为读
	write_com(0X38);
	lcd1602_delay(1);
	write_com(0X0c);
	lcd1602_delay(1);	
	write_com(0X06);   //写入一个字节后 0X06光标加1 	 0X07光标减1 
	lcd1602_delay(1);
	write_com(0X01);
	lcd1602_delay(1);
	write_com(0X80);
}


void write_com(uchar c_dat)
{
	LCD_RS=0;
	LCD_RW=0;
	P0=c_dat;
	lcd1602_delay(1);
	LCD_E=1;
	lcd1602_delay(1);
	LCD_E=0;

}

void lcd1602_dischar( uchar y, uchar x, char *c_data )
{
	write_com(0X80+0x40*y+x);
	lcd1602_delay(5);
	while( *c_data != 0 )
	{
	   write_data( *c_data );
	   c_data++;
	}
}


/**
  *****************************************
	函数名：void write_data(uchar d_dat)
	作用：	在当前坐标下显示一个字节 
  *****************************************
**/
void write_data(uchar d_dat)
{
	LCD_RS=1;
	LCD_RW=0;
	P0=d_dat;
	lcd1602_delay(1);
	LCD_E=1;
	lcd1602_delay(1);
	LCD_E=0;
}

void lcd1602_delay(uint y_time)
{
		y_time*=18;
		while(y_time--);
}

