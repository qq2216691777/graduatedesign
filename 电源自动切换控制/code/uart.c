#include "uart.h"

void uart_Init()
{
	TMOD &=0x0f;
	TMOD=0X20;
	TH1=0XFD;
	TL1=0XFD;
	TR1=1;
	REN=1;
	
	SM0=0;
	SM1=1;
	
	EA = 1;
	ES = 1;
}

void SendData( unsigned char dat )
{
	SBUF=dat;
	while(!TI);
	TI = 0;
}

void SendString( unsigned char *s )
{
 	while(*s)
	{
	 	 SendData(*s++);
	}
}

void SendNum( long n )
{
	long n_flag = (long)n;
	char Num[15]={0};
	BYTE i=13;

	do
	{
	 	Num[i--] = n_flag%10 + 0x30;
		n_flag = n_flag/10;
	}while( n_flag );
	if(n<0)
		Num[i] = '-';
	else
		Num[i] = ' ';
	

	SendString(&Num[i]);
}

void ser() interrupt 4
{
    unsigned char a;
 	RI = 0;
	a=	SBUF;
}

