#include <reg52.h>
#include "adc0809.h"
#include "tim.h"
#include "uart.h"
																		  
/**
  ***************************************************
  @filename: 	main.c
  @project:		template.uvproj
  @author:		Yenianxifeng
  @brief:		main for 89c52
  @xtal			11.0592MHz
  ***************************************************
**/

/*
* IN0  主电源
* IN1  备用电源
*/
sbit j = P3^5;
sbit in = P3^7;

void delay1()
{
	unsigned int i,y;
	for( i=200;i>2;i--)
	{
		for(y=20;y>1;y--){}
	}
}

void delay2()
{
	unsigned int i,y;
	for( i=50;i>2;i--)
	{
		for(y=20;y>1;y--){}
	}
}


sbit keys = P3^2;
sbit led1 = P2^4;
sbit led2 = P2^3;
sbit beep = P2^6;

unsigned char key()
{
	keys = 1;
	return keys;
}

void main()
{
	uchar res1;
	uchar res2;
	uchar temp=0;

	beep = 0;
	Tim0_Init();
	uart_Init();
	j = 1;
	led1 = 0;
	led2 = 0;
	
	while(1) 
	{
		if(j)
		{
		 	led1 = 0;
			led2 = 1;
		}	
		else
		{
		 	led1 = 1;
			led2 = 0;
		}
		if(temp)
		{
		 	if( !key() )
			{
				delay2();
				if( !key() )
				{
				   temp = 0;
				   if(j)
					{
					 	led1 = 0;
						led2 = 1;
					}	
					else
					{
					 	led1 = 1;
						led2 = 0;
					}
				   while( !key() );
				}
			}
		}
		else
		{
			if( !key() )
			{
				delay2();
				if( !key() )
				{
				   temp = 1;
				   j = 0;
				   led1 = 1;
				   led2 = 0;
				   while( !key() );
				}
			}
		}
		if( temp )
			continue;	
	   if(1)
	   {
	   		in = 1;
 
	   
	   		in = 1;
	   		res2 = ADC0809();
			if( res2<106)
			{
			   j = 0;	
				beep = 1;
			}
			else
			{
				j = 1;	
				beep = 0;
			}
	   }
//	  delay1();
	}

}



