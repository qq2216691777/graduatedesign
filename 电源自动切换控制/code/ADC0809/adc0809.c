/**
  ***************************************************
  @filename: 	adc0809.c
  @project:		sheji.uvproj
  @author:		Yenianxifeng
  @brief:		adc0809 for 89c52
  ***************************************************
**/
#include "adc0809.h"
sbit ADC_START=P3^4; 
sbit ADC_ALE =P2^5; 
sbit ADC_OE =P2^1; 
sbit ADC_EOC =P3^3; 



void ADC0809_Init( void )
{	
	_nop_(); 
	_nop_(); 
	_nop_();
	ADC_START=0;
	ADC_OE=0;
	_nop_();
	 
	ADC_ALE=0; 
	_nop_(); 
	_nop_(); 
	_nop_();
	 
	ADC_ALE=1; 
	_nop_(); 
	_nop_(); 
	_nop_();
	  
	ADC_ALE=0; 
	_nop_(); 
	_nop_(); 
	_nop_(); 


}

void delayus(i) 
{  
for(i;i>0;i--); 

} 


uchar ADC0809( void )
{
	uchar r_data;

	ADC0809_Init();

	ADC_START=1; 
	_nop_(); 
	_nop_(); 
	_nop_();  
	ADC_START=0; 
	_nop_(); 
	_nop_(); 
	_nop_();  
	while(!ADC_EOC); 
	
	ADC_OE =1;  
	r_data = P1;	
	delayus(5); 
	ADC_OE =0;

	return r_data;
}
