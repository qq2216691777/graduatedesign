#ifndef _ADC0809_H__
#define _ADC0809_H__

#include< reg52.h >
#include< intrins.h >

#ifndef uchar

#define uchar unsigned char
#define uint unsigned int

#endif

sbit ADC_A =P0^2; 
sbit ADC_B =P0^1; 
sbit ADC_C =P0^0;

//void ADC0809_Init( void );
uchar ADC0809( void );

#endif
