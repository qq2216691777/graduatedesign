#include "tim.h"


sbit CLK = P2^2;

void Tim0_Init()
{
	TMOD &= 0XF0;
	TMOD |= 0X02;
	TH0 = 240;
	TL0 = 240;
	EA = 1;
	ET0 = 1;
	TR0 = 1;
}
sbit watchdog = P2^0;
void T0_time() interrupt 1
{	
	CLK = !CLK;
	watchdog = !watchdog;
	
}