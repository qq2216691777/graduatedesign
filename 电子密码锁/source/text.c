#include "text.h"
#include "key.h"


void LCD_1602_Write_String( uchar h, uchar *w_data );
void LCD_1602_Write_Num( uchar h,uchar s, int num );
void LCD_1602_Write_com( uchar w_com );
void LCD_1602_Write_data( uchar w_data );

uchar text_code[13]="            ";	  		//记事本保存数组   里面保存记事本的信息 默认为空

void Text()
{
	uchar pos = 0;
	uchar key_value;
	uchar pressed = 1;
    uchar text_temp[13]="            ";
	uchar i;

	LCD_1602_Write_String( 1, "Text:              ");
	LCD_1602_Write_String( 2, "                   "); 
	LCD_1602_Write_String(2,text_code);
	while(1)
	{
	 	key_value = get_value_key();
		if( key_value==41 )
		{
			LCD_1602_Write_String(2,text_temp);
			LCD_1602_Write_com( 0x80+0x40 + pos);
			LCD_1602_Write_com( 0x0f );			//打开光标
			break;
		}
		else if( key_value==42 )
		{
			return ;

		}
	}
	pos = 0;
	while(1)
	{
		key_value = get_value_key();
		if( key_value<200 )
		{
			if( pressed )
				continue;
			pressed = 1;

			if( key_value<20 )
			{
				if( pos<13 )
				{
					LCD_1602_Write_data(key_value+0x30);
					text_temp[pos] = 	key_value+0x30;
					pos++;
				}
			}
			else if( key_value==43 )	//退格
			{
				if( pos>0)
				{	
					pos--;
					LCD_1602_Write_com( 0x80+0x40 +pos );
					LCD_1602_Write_data(' ');
					LCD_1602_Write_com( 0x80+0x40 +pos );
					text_temp[pos] = ' ';
				}
			}
			else if( key_value==42 )	//取消
			{
				LCD_1602_Write_String( 2, "                ");
				LCD_1602_Write_com( 0x0c );
				return ;
			
			}
			else if( key_value==41 )	//确定
			{
			//	LCD_1602_Write_String( 2, "                ");
				LCD_1602_Write_com( 0x0c );
				for(i=0; i<13; i++)
					text_code[i]=text_temp[i];	
				return ;
			
			}
		}
		else
		{
			pressed = 0;
		}
	}

}

