#include "passwd.h"
#include "key.h"

void LCD_1602_Write_String( uchar h, uchar *w_data );
void LCD_1602_Write_Num( uchar h,uchar s, int num );
void LCD_1602_Write_com( uchar w_com );
void LCD_1602_Write_data( uchar w_data );

void pss_delay(uint d );

uchar True_psswd[6]={1,2,3,4,5,6};	//密码保存数组     存储真正的密码  默认是123456
/*
	输入密码函数。将输入的数据保存在传入的数组中
	可以用在密码验证  密码修改   密码重置功能中
*/
uchar input_passwd( uchar * psswd )
{
	int key_value;
	uchar pressed = 1;
	uchar input_num=0;
	

	LCD_1602_Write_String( 2, "                ");	   //勾画显示界面
	LCD_1602_Write_com( 0x80+0x40 + 5);
	LCD_1602_Write_com( 0x0f );			//打开光标
//	LCD_1602_Write_com( 0x0c );
	while(input_num<6)
	{
		key_value = get_value_key();
		if( key_value<200 )
		{
			if( pressed )
				continue;
			pressed = 1;

			if( key_value<10 )			 //如果	输入的数据为0~9  则是密码数据
			{
				LCD_1602_Write_data('*'); 	//用*号替换
				psswd[input_num] = 	key_value;	  //将用户输入的数据临时保存起来  便于修改以及和原密码匹配
				input_num++;
			}
			else if( key_value==43 )	//退格	  //如果用户输入退格  则将数组的下标变量减一   并将之前输入的 * 删除
			{
				if( input_num>0)
				{
					input_num--;
					LCD_1602_Write_com( 0x80+0x40 + 5 +input_num );
					LCD_1602_Write_data(' ');
					LCD_1602_Write_com( 0x80+0x40 + 5 +input_num );
				}
			}
			else if( key_value==42 )	//取消				如果用户取消输入  则关闭光标  清屏   直接返回即可
			{
				LCD_1602_Write_String( 2, "                ");
				LCD_1602_Write_com( 0x0c );
				return 0;
			
			}
		}
		else
		{
			pressed = 0;
		}
	}
	LCD_1602_Write_String( 2, "                ");
	LCD_1602_Write_com( 0x0c );

	return 1;

}
/*
密码验证函数。
从密码输入函数中获取用户输入的数据和正确的密码进行匹配  如果输入正确 则返回1  输入错误 返回0   用户取消 则返回3
*/
uchar passwd_Verification( uchar * psswd )
{
	uchar i;
	if(!input_passwd(  psswd ))				   //获取用户输入的密码数据
	   return 3;
	
	for( i=0; i<6; i++ )					  //和正确的密码进行匹配  如果匹配成功  则i=6  如果不成功 则i<6
	{
	   if( True_psswd[i]!=psswd[i] )
	   		break;
	}
	if( i<6 )
	{
	 	return 0;
	}
	else
		return 1;
}
/*
	设置密码   首先验证密码的正确性  如果正确  
	则连续获取两次的密码输入状态 并判断是否一致，若一致 则保存新的密码
*/
void set_passwd()
{
	int key_value;
	uchar psswd1[6];
	uchar psswd2[6];
	uchar *p;
	uchar i;
	uchar res;



	 LCD_1602_Write_String( 1, "Old passwd:           ");
	 while(1)
	 {
	 	res =  passwd_Verification(psswd1);			   //验证用户输入的密码是否正确
	 	if( 0==res )
	 	{
	 		  LCD_1602_Write_String( 2, "    passwd error           ");
			  pss_delay(3000);
			  pss_delay(3000);
	 	}
		else if( 3==res )
			return ;
		else if( 1==res )
			break;
	 }
	 
	 p = psswd1; 											//将第一次输入的密码所保存的数组指针赋值给P   以提高代码的复用性
	 LCD_1602_Write_String( 1, "New passwd:           ");
	 while( 1 )
	 {
		
		if( input_passwd(  p ) )
		{
			if( p==psswd1 )
			{
				LCD_1602_Write_String( 1, "New passwd again:           ");		   //如果密码输入完成  则判断是否是第二次输入  
																					//若是  则直接比较两次输入是否一致
		 		p = psswd2;
			}
			else
				break;
		}
		else
			return ;
	 }
	 for( i=0; i<6; i++ )			  //比较两次输入的密码是否一致
	 {
	  	if( psswd2[i] != psswd1[i] )
			break;
	 }
	 if( i<6 )
	 {
	 	LCD_1602_Write_String( 1, "New passwd not Same    ");
		return ;
	 }
	 else
	 	LCD_1602_Write_String( 1, "New passwd is ok    ");
	for( i=0; i<6; i++ )		 		//如果一致  则保存新的密码到数组
	{
		True_psswd[i] = psswd2[i];	
	}

	pss_delay(3000);
	pss_delay(3000);

}
/*
	重置密码    在设置密码的基础上  删掉密码验证即可
*/
void modify_passwd()
{
	int key_value;
	uchar psswd1[6];
	uchar psswd2[6];
	uchar *p;
	uchar i;
	uchar res;

 	 LCD_1602_Write_String( 1, "Are you sure?       ");
	 LCD_1602_Write_String( 2, "    Yes or Cancel       ");
	 while(1)
	 {
		 key_value = get_value_key();
		 if( key_value==42 )
		 	return ;
		 else if( key_value==41 )
			break;
	 }
	 while( get_value_key()<200 );

	  p = psswd1; 
	 LCD_1602_Write_String( 1, "New passwd:           ");
	 while( 1 )
	 {
		
		if( input_passwd(  p ) )
		{
			if( p==psswd1 )
			{
				LCD_1602_Write_String( 1, "New passwd again:           ");
		 		p = psswd2;
			}
			else
				break;
		}
		else
			return ;
	 }
	 for( i=0; i<6; i++ )
	 {
	  	if( psswd2[i] != psswd1[i] )
			break;
	 }
	 if( i<6 )
	 {
	 	LCD_1602_Write_String( 1, "New passwd not Same    ");
		return ;
	 }
	 else
	 	LCD_1602_Write_String( 1, "New passwd is ok    ");
	for( i=0; i<6; i++ )
	{
		True_psswd[i] = psswd2[i];	
	}

	pss_delay(3000);
	pss_delay(3000);

}

void pss_delay(uint d )
{
	uint dtime=100*d;
	while( dtime --);
}
