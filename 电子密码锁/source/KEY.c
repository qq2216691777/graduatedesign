#include "key.h"
#include < reg52.h >

#define KEY_PORT P1

void key_delay(uint y_time);


int key_scan( void );

/******按键接口定义********/
sbit KEY_H4 = P1^0;
sbit KEY_H3 = P1^1;
sbit KEY_H2 = P1^2;
sbit KEY_H1 = P1^3;
sbit KEY_L4 = P1^4;
sbit KEY_L3 = P1^5;
sbit KEY_L2 = P1^6;
sbit KEY_L1 = P1^7;

unsigned char bdata bit8;//位定义
sbit keyFlag = bit8^0;
sbit key1 = bit8^1;
sbit key2 = bit8^2;
sbit key3 = bit8^3;
sbit key4 = bit8^4;

//获取按键信息函数
//将硬件信息转化为我们需要的信息
int get_value_key( void )
{
	uchar  key_val;

	key_delay(500);
	key_delay(500);
	KEY_PORT = 0XFF;
	key_val = key_scan();
	KEY_PORT = 0;

	switch( key_val )
	{
		case 1:
		case 2:
		case 3:
			return key_val;
			break;
		case 5:
		case 6:
		case 7:
			return key_val-1;
			break;
		case 9:
		case 10:
		case 11:
			return key_val-2;
			break;
		case 14:
			return 0;
			break;

		case 4:
			return 41;
			break;
		case 8:
			return 42;
			break;
		case 12:
			return 45;
			break;
		case 16:
			return 44;
			break;
		case 15:
			return 43;
			break;

		default:
			break;

	}
	return 255;
}
/*  键盘扫描函数   获取具体的按键值 1-16 */
int key_scan( void )
{

#if 0
	uchar i=8;
	uchar key_v ;
	while(i>4)
	{
		i--;
		KEY_PORT=~(1<<i);
		if( 0x0f !=(KEY_PORT & 0x0f) )
		{
			KEY_PORT = 0x00;
			key_delay(14);
			KEY_PORT=~(1<<i);
			key_v =  KEY_PORT & 0x0f;
			KEY_PORT= 0;
			if( 0x0f==key_v )
				continue;
		}
		else
			continue;

	

		switch( key_v )
		{
			case 0x07:
					return (1+4*(7-i));
				break;
			case 0x0b:
					return (2+4*(7-i));
				break;
			case 0x0d:
					return (3+4*(7-i));
				break;
			case 0x0e:
					return (4+4*(7-i));
				break;	 
					
			default: break;

		}
	}
	return 0;
#endif
   		KEY_L1 = 0;                       //扫描第一行
		key1=1;
		if(!KEY_H1)        return 1;
		else if(!KEY_H2)   return 2;
		else if(!KEY_H3)   return 3;
		else if(!KEY_H4)   return 4;
		else key1=0;
    	KEY_L1 = 1;

		KEY_L2 = 0;						 //扫描第二行
		key2=1;
		if(!KEY_H1)        return 5;
		else if(!KEY_H2)   return 6;
		else if(!KEY_H3)   return 7;
		else if(!KEY_H4)   return 8;
		else key2=0;
   	    KEY_L2 = 1;

		KEY_L3 = 0;					      //扫描第三行
		key3=1;
		if(!KEY_H1)        return 9;
		else if(!KEY_H2)   return 10;
		else if(!KEY_H3)   return 11;
		else if(!KEY_H4)   return 12;
		else key3=0;
    	KEY_L3 = 1;

		KEY_L4 = 0;					      //扫描第四行
		key4=1;
		if(!KEY_H1)        return 13;
		else if(!KEY_H2)   return 14;
		else if(!KEY_H3)   return 15;
		else if(!KEY_H4)   return 16;
		else key4=0;
    	KEY_L4 = 1;	    	

}


void key_delay(uint y_time)
{
		y_time*=14;
		while(y_time--);
}
