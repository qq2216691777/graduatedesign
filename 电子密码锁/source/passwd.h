#ifndef _PASSWD_H__
#define _PASSWD_H__

#ifndef uchar
#define uchar unsigned char
#define uint unsigned int
#endif


uchar input_passwd( uchar * psswd );
uchar passwd_Verification( uchar * psswd );
void modify_passwd();
void set_passwd();

#endif
