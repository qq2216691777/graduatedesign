#include < reg52.h >
#include "key.h"
#include "passwd.h"
#include "text.h"

#ifndef uchar
#define uchar unsigned char
#define uint unsigned int
#endif

sbit BEEP = P1^5;

sbit RS = P2^6;	   //lcd1602 位定义
sbit RW = P2^5;
sbit E = P2^7;
void LCD_1602_init( void );		//lcd1602初始化函数
void LCD_1602_Write_com( uchar w_com );	 // lcd1602 写指令函数
void LCD_1602_Write_data( uchar w_data );	 // lcd1602 写数据函数
void delay(uint y_time);			//延迟函数
void LCD_1602_Write_String( uchar h, uchar *w_data );
void LCD_1602_Write_Num( uchar h,uchar s, int num );



int main()
{
	uchar key_val;
	uchar tmp_psswd[6];
	uchar res;
	P1 = 0X0;
	delay(1000);
	
	LCD_1602_init();				  //LCD1602初始化
	LCD_1602_Write_String( 1, "    Welcome");
	

	while(1)
	{
	  	 key_val = get_value_key();			//获取按键信息
		 switch( key_val )
		 {
		  	case 41:						//判断是否按下确定键
				LCD_1602_Write_String( 1, "passwd:           ");
				res =  passwd_Verification(tmp_psswd);		//输入密码并验证正确性
				if( 3==res )
				{
					 break;
				}
				else if( 0==res )
				{
					
				  	LCD_1602_Write_String( 2, "    failed          "); 
					BEEP = 1;
					delay(3000);
					delay(3000);
					delay(3000);
					delay(3000);
					delay(3000);
					delay(3000);
					BEEP = 0;
				}
				else 
				{
					Text();					  //输入正确则进入记事本功能
					
				}		
				break;
			 case 44:
			 	 modify_passwd();			   //重置密码
				 break;

			case 45:
			 	 set_passwd();				   //修改密码
				 break;

				
		 }

		 LCD_1602_Write_String( 1, "    Welcome      ");
		 LCD_1602_Write_String( 2, "                  ");

		
	
	}
}

/* 以下是lcd1602硬件设置和初始化函数 */

void LCD_1602_init( void )
{
	E = 0;
	RW = 0;
	LCD_1602_Write_com( 0x38 );
	delay(5);
	LCD_1602_Write_com( 0x0c );
	delay(5);
	LCD_1602_Write_com( 0x06 );
	delay(5);
	LCD_1602_Write_com( 0x01 );
	delay(5);
	LCD_1602_Write_com( 0x02 );
	delay(5);
	LCD_1602_Write_com( 0x80 );
	delay(5);

}

void LCD_1602_Write_com( uchar w_com )
{
	RS = 0;
	RW = 0;
	P0 = w_com;
	delay( 5 );
	E = 1;
	delay( 5 );
	E = 0;


}

void LCD_1602_Write_data( uchar w_data )
{
	RS = 1;
	RW = 0;
	P0 = w_data;
	delay( 5 );
	E = 1;
	delay( 5 );
	E = 0;
}
void LCD_1602_Write_String( uchar h, uchar *w_data )
{
	uchar i = 0;
	LCD_1602_Write_com( 0x80 );
	if( h & 0x2 )
	{
		 LCD_1602_Write_com( 0x80+0x40 );
	}
	while( w_data[i] != '\0')
		LCD_1602_Write_data(w_data[i++]);
}

void LCD_1602_Write_Num( uchar h, uchar s,int num )
{
	uchar i = 0;
	char num_temp[16]={0};
	LCD_1602_Write_com( 0x80 + s );
	if( h & 0x2 )
	{
		 LCD_1602_Write_com( 0x80+0x40 + s);
	}
	i=0;
	while( (num!=0))
	{
		 num_temp[i] = num%10 + 0x30;
		 i++;
		 num = num/10;
	}
	if( i == 0 )
	{
		   LCD_1602_Write_data(0x30);
	}
	while( i )
	{
		LCD_1602_Write_data(num_temp[i-1]);
		i--;	
	}

}

void delay(uint y_time)
{
		y_time*=14;
		while(y_time--);
}





