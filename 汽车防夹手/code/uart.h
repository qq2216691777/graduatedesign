#ifndef _UART_H__
#define _UART_H__
#include <reg52.h>

#define BYTE char

void uart_Init();
void SendNum( long n );
void SendData( unsigned char dat );
void SendString( unsigned char *s );

#endif
