#include <reg52.h>
#include "uart.h"

sbit Relay1 = P3^4;		 //电机控制A相
sbit Relay2 = P3^5;		 //电机控制B相

sbit  key3 = P2^3;
sbit  key4 = P2^4;


sbit DIANJI = P3^2;


void time0Init(); //定时器0初始化

int speed=0;	 //电机转速

void Delay10ms()		//@12.000MHz
{
	unsigned char i, j;

	i = 20;
	j = 113;
	do
	{
		while (--j);
	} while (--i);
}


void Delay100ms()		//@12MHz
{
	unsigned char i, j, k;

	i = 5;
	j = 144;
	k = 71;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}



void Delay500ms()		//@12MHz
{
	unsigned char i, j, k;

	i = 23;
	j = 205;
	k = 120;
	do
	{
		do
		{
			while (--k);
		} while (--j);
	} while (--i);
}

int averData[30]={0};
int index = 0;

#define  MOTORFOR()	{ Relay1 = 1; Relay2 = 0;}		//电机正向旋转
#define  MOTORBACK()	{ Relay1 = 0; Relay2 = 1;}	//电机反向旋转


void main()
{
	int ii=0;
	char dir = 1;	  //当前电机转向

    
//	time0Init();
//	uart_Init();

 //   IT0 = 1;	//打开外部中断0
//	EX0 = 1;                                                    
//	EA = 1;
	MOTORBACK();
	Delay100ms();
	MOTORFOR();
	while(1) 
	{
	 	MOTORBACK();
	Delay500ms();
	Delay500ms();
	Delay500ms();
	Delay500ms();
	MOTORFOR();
	Delay500ms();
	Delay500ms();
	Delay500ms();
	Delay500ms();
	

	}


}

void time0Init() 
{
	TMOD &= 0XFC;
	TMOD |= 0X02;  //8位自动装载
	TH0	 = (256-50);
	TL0	 = (256-50);
	ET0 = 1;
	TR0 = 1;
	EA = 1;

	
}

int timei=0;
void tim0Int() interrupt 1			  //定时器0中断服务函数  50us一次
{
	timei++;
}

void int0() interrupt 0
{

	averData[index%20] = timei;			//读取距离上次中断的时间
	index++;
	timei = 0; 
	
}