#include "reg52.h"
#include <intrins.h>
#include "codetab.h"
#include "LQ12864.h"
#include "delay.h"
#include "uart.h"
#include "device.h"

sbit LED1 = P2^3; 	//led1 引脚定义
sbit LED2 = P2^4;
sbit key1 = P2^1;	//key1 引脚定义
sbit key2 = P2^7;

sbit beep = P1^3;		//蜂鸣器引脚定义
sbit keyup = P1^5;
sbit keydown = P1^6;

char addr[2]={1,0};

char setAddress();		//设置地址函数
void showWarning( char id );  //显示报警信息函数  声明


unsigned char uartBuf = 0;	 //串口接收缓存变量
unsigned char model=0;	 	 //当前模式变量  0未收到消息  00000001 收到led1的消息  00000002  收到led2的消息
unsigned char warning=0;
void main()
{
 	Delay500ms();  	//复位延时  保证外设在初始化的时候电压稳定
	OLED_Init();    //OLED 初始化函数
	uart_Init();	//串口初始化函数
	beep = 0;
//	SendString("Init ok\r\n");

	LED1 = 0;		//打开led
	LED2 = 0;
	OLED_P8x16Str(40,3,"Welcome");		   //显示欢迎信息
	Delay500ms();						   //等待2S
	Delay500ms();
	Delay500ms();
	Delay500ms();
	beep = 1;
	LED1 = 1;
	LED2 = 1;
	OLED_P8x16Str(5,3,"System running");		//显示正常启动信息 
	
	while(1)
	{
		if( uartBuf )		//判断有没有接收到数据
		{
		 	if( uartBuf==LED1_WARNNING )		   //判断是否是模块1发来的警告信息 
			{
			 	LED1 = 0;						//如果是警告信息则立即点亮LED灯
			//	SendData(0X33);
			}else if( uartBuf==LED2_WARNNING )	  		//判断是否是模块2发来的 
			{
				LED2 = 0;
			}else if( uartBuf==LED1_CANCELS )  	 //判断是否是模块1发来的取消信息 
			{
				LED1 = 1;						  //如果是取消信息则立即关闭LED灯
				OLED_P8x16Str(5,2,"               ");   //清空显示
			}else if( uartBuf==LED2_CANCELS )
			{
				LED2 = 1;
				OLED_P8x16Str(5,5,"               ");
			}
			uartBuf = 0;			   //清空数据
					
		}
	    warning = 0;
		if( !LED1 )		//如果灯是点亮的 	则进入判断
		{
			if( !(model & 0x01) )	 //如果model的最低位是0
			{	
			 	if(model==0)	     //如果model所有位都为0   则说明第一次报警信息
				{
				  	OLED_P8x16Str(5,3,"               ");		//清空显示
					//OLED_P8x16Str(5,2,"1:N2 Warnning");
					warning |= 0x01;			   		//报警信息最低位置1
					showWarning(warning);				//显示
					model = 1;						   //设置当前为模式1
				//	SendData(0X32);
				}
				else
				{
				 	//OLED_P8x16Str(5,2,"NO 1 Warnning");	  //否则说明已经不是第一次收到信息了
					warning |= 0x01;
					showWarning(warning);
					model |= 1;
				}
			}		
		}

		if( !LED2 )
		{
		 	if( !(model & 0x02) )
			{	
			 	if(model==0)
				{
				  	OLED_P8x16Str(5,3,"               ");
					warning |= 0x02;
					showWarning(warning);
					model = 2;
				}
				else
				{
				 	warning |= 0x02;
					showWarning(warning);
					model |= 2;
				}
			}		
		}
		if( LED2 && LED1 )		 //如果有任意一个led是被点亮的  则打开蜂鸣器
			beep = 1;
		else
			beep = 0;


		if( model )	   //如果不是模式0  则说明有报警信息
		{
		   if(LED1&&LED2)		 	//如果两个灯都被关闭  则显示正常信息
		   {
	//	   		OLED_P8x16Str(5,2,"               ");
	//			OLED_P8x16Str(5,5,"               ");
				OLED_P8x16Str(5,3,"System running");
				model = 0;	
		   }
		   else 
		   {
		   		if(!LED1)	   //如果led灯被点亮  
				{
					key1 = 1;	
			   		if( key1==0  )		 //判断是否有按键被按下
					{
					 	Delay10ms();		  //按键消抖
						if( key1==0  )		  //如果确实被按下
						{
						 //	model &=  ~0X01;		  //关闭led
							LED1 = 1;
							OLED_P8x16Str(5,2,"               ");
							SendData(LED1_RESPOND);				  //通过无线发聩数据
						}
					}
				}


				if(!LED2)
				{
					key2 = 1;	
			   		if( key2==0  )
					{
					 	Delay10ms();
						if( key2==0  )
						{
						 //	model &=  ~0X01;
							LED2 = 1;
							OLED_P8x16Str(5,5,"               ");
							SendData(LED2_RESPOND);
						}
					}
				}

		   } 
		}

		if(setAddress())
		{
			OLED_Fill(0x00);
			OLED_P8x16Str(5,3,"System running");	
		}

	}
}
/*
	获取按键值
*/
char getKeyValue()
{
	keyup = 1;	   //先将按键都拉高  如果按键被按下  则会立即被拉低
	keydown = 1;
	key1 = 1;
	key2 = 1;
	Delay10ms();
	if( !keyup )
	{
		Delay10ms();	 //按键消抖
	  	if( !keyup )
			return 4;
	}

	if( !keydown )
	{
		Delay10ms();
	  	if( !keydown )
			return 3;
	}

	if( !key2 )
	{
		Delay10ms();
	  	if( !key2 )
			return 2;
	}

	if( !key1 )
	{
		Delay10ms();
	  	if( !key1 )
			return 1;
	}

	return 0;
	

}

void showWarning( char id )
{
	if(id&0x01)
	{
	 	switch(addr[1])
		{
		 	case 0:
			   	OLED_P8x16Str(5,2,"1:N1 Warnning");
				break;
			case 1:
				OLED_P8x16Str(5,2,"1:N2 Warnning");
				break;
			case 2:
				OLED_P8x16Str(5,2,"1:N3 Warnning");
				break;
			case 3:
				OLED_P8x16Str(5,2,"1:N4 Warnning");
				break;
			case 4:
				OLED_P8x16Str(5,2,"1:N5 Warnning");
			 	break;

			default:	break;
		}	
	}
	if( id&0x02 )
	{
	 	switch(addr[0])
		{
		 	case 0:
			   	OLED_P8x16Str(5,5,"2:N1 Warnning");
				break;
			case 1:
				OLED_P8x16Str(5,5,"2:N2 Warnning");
				break;
			case 2:
				OLED_P8x16Str(5,5,"2:N3 Warnning");
				break;
			case 3:
				OLED_P8x16Str(5,5,"2:N4 Warnning");
				break;
			case 4:
				OLED_P8x16Str(5,5,"2:N5 Warnning");
			 	break;

			default:	break;
		}
	}

}

void showDevice( char index )
{
 	if( index )
	{
		OLED_P8x16Str(5,1,"select device:");
		OLED_P8x16Str(5,3," >  1");
		OLED_P8x16Str(5,5,"    2");
	}
	else
	{
	 	OLED_P8x16Str(5,1,"select device:");
		OLED_P8x16Str(5,3,"    1");
		OLED_P8x16Str(5,5,">   2");
	}
}

void showAddress( char index )
{
 	if( index==0 )
	{
		OLED_P8x16Str(5,0,"select Address:");
		OLED_P8x16Str(5,2," >  1");
		OLED_P8x16Str(5,4,"    2");
		OLED_P8x16Str(5,6,"    3");
	}
	else  if( index==1 )
	{
	 	OLED_P8x16Str(5,0,"select Address:");
		OLED_P8x16Str(5,2,"    1");
		OLED_P8x16Str(5,4,">   2");
		OLED_P8x16Str(5,6,"    3");
	}
	else  if( index==2 )
	{
	 	OLED_P8x16Str(5,0,"select Address:");
		OLED_P8x16Str(5,2,"    1");
		OLED_P8x16Str(5,4,"    2");
		OLED_P8x16Str(5,6,">   3");
	}
	else  if( index==3 )
	{
	 	OLED_P8x16Str(5,0,"select Address:");
		OLED_P8x16Str(5,2,">   4");
		OLED_P8x16Str(5,4,"    5");
		OLED_P8x16Str(5,6,"     ");
	}
	else  if( index==4 )
	{
	 	OLED_P8x16Str(5,0,"select Address:");
		OLED_P8x16Str(5,2,"    4");
		OLED_P8x16Str(5,4,">   5");
		OLED_P8x16Str(5,6,"     ");
	}
}

 /*
 设置从机地址函数
 */
char setAddress()
{
	char index = 1;
	char Aindex = 0;
	char temp;
	keyup = 1;
	keydown = 1;

	if( !(keyup && keydown) )
	{
		Delay10ms();
		if( !(keyup && keydown) )
		{
			 OLED_Fill(0x00); //初始清屏
			 showDevice(index%2);
			 while( !(keyup && keydown))
			 {
			  	keyup = 1;
				keydown = 1;
				;
			 }
			 while(1)
			 {
			 	temp = getKeyValue();
				while(!(keydown&keyup&&key1&&key2));
				if(temp == 2)
					return 1;
			   	if( temp == 1 )
				{
				   	break;
				}
				if( temp )
				{
					index++;	
					index = index%2;
					showDevice(index);
				}
			 }
			 OLED_Fill(0x00); //初始清屏
			 showAddress(Aindex%2);
			 while(1)
			 {
				temp = getKeyValue();
				while(!(keydown&keyup&&key1&&key2));
				if(temp == 2)
					return 1;
			   	if( temp == 1 )
				{
					addr[index] = Aindex;
				   	break;
				}
				if( temp==3 )
				{
					Aindex++;	
					Aindex = Aindex%5;
					showAddress(Aindex);
				}
				else if( temp==4 )
				{
					Aindex+=4;	
					showAddress(Aindex%5);
				}

			 }
				 
		}
		else
			return 0;
	
	}	
	else
		return 0;

	return 1;
}
