#include < reg52.h >

#ifndef uchar
#define uchar unsigned char
#define uint unsigned int
#endif


sbit RS = P2^6;	   //lcd1602 位定义
sbit RW = P2^5;
sbit E = P2^7;

sbit key = P2^0;
sbit aver_key = P2^1;

void LCD_1602_init( void );		//lcd1602初始化函数
void LCD_1602_Write_com( uchar w_com );	 // lcd1602 写指令函数
void LCD_1602_Write_data( uchar w_data );	 // lcd1602 写数据函数
void delay(uint y_time);			//延迟函数
void LCD_1602_Write_String( uchar h, uchar *w_data );
void LCD_1602_Write_Num( uchar h,uchar s, int num );

void display_speed();
void display_mile();
void display_aver_speed();


uint speed_num = 0;		//当前速度

unsigned long mile_num = 0;		//当前路程

unsigned long total_time = 0;    //当前的总时间(s)	 = 	total_time * 0.5;
unsigned long begin_mile = 0;	 //

void int0_init();				//外部中断初始化
void time0_init();

int main()
{
	delay(1000);

	LCD_1602_init();

	LCD_1602_Write_String( 1, "speed: 00.00km/h");
	LCD_1602_Write_String( 2, "miles: 0000.00km");

	int0_init();
	time0_init();
	while(1)
	{
		
		display_speed();
		display_mile();
		if(!key)
		{
		 	delay(10);
			if( !key )
			{
				mile_num = 0;
			}
		}

		if(!aver_key)				//是否按下显示平均速度的按键
		{
		 	delay(10);
			if( !aver_key )
			{
				LCD_1602_Write_String( 1, "   aver speed   ");
				LCD_1602_Write_String( 2, "    00.00km/h    ");
			  	while( !aver_key )		 //判断是否松手
				{
				  	display_aver_speed();
				}
				LCD_1602_Write_String( 1, "speed: 00.00km/h");
				LCD_1602_Write_String( 2, "miles: 0000.00km");
			}
		}
	
	
	}
}

void time0_init()
{
	
	TMOD = 0X01;

	TH0 = (65536-37500)/256; 		//	 理论值是(65536-50000)/256;
	TL0 = (65536-37500)%256;
	ET0 = 1;
	TR0 = 1;
	EA  = 1;

}
/**
  * 定时器0 的中断服务函数 
  * 用于计算当前的实时速度
**/
void T0_time() interrupt 1		  //50ms一次    12Mhz
{
	static unsigned long last_mile = 0;
	static unsigned long stop_mile = 0;	//上次停车时走过的里程
	static uchar i=0;

	TH0 = (65536-37500)/256;
	TL0 = (65536-37500)%256;
	i++;
	
	if(i<10 )
		return ;
	i = 0; 				//500ms执行一次
	total_time ++;
	if( mile_num >= last_mile )
		speed_num = (uint)((mile_num - last_mile)*7.2);	 // m/0.5s * 3.6 = h/km
	if( !speed_num )
	{
		total_time = 1;
		stop_mile = mile_num;
	}
	begin_mile = mile_num-stop_mile;	
	last_mile = mile_num;
}
/**
  * 外部中断0 初始化函数 
  * 实时获取外部的脉冲数
**/
void int0_init()
{
  	IT0 = 1;
	EA = 1;
	EX0 = 1;
}

void int0_handler() interrupt 0
{
	mile_num += 10;	   //一个脉冲一米	
}
/**
  * 显示当前速度
**/
void display_speed()
{
	 LCD_1602_Write_com( 0x80 + 7 );
	 LCD_1602_Write_data((speed_num/1000)%10 + 0x30);
	 LCD_1602_Write_data((speed_num/100)%10 + 0x30 );
	 LCD_1602_Write_data('.');
	 LCD_1602_Write_data((speed_num/10)%10 + 0x30 );
	 LCD_1602_Write_data((speed_num)%10 + 0x30 );
}
/**
  * 显示当前里程
**/
void display_mile()
{
	 LCD_1602_Write_com( 0x80 + 0x40 + 7 );
	 LCD_1602_Write_data((mile_num/1000000)%10 + 0x30);
	 LCD_1602_Write_data((mile_num/100000)%10 + 0x30 );
	 LCD_1602_Write_data((mile_num/10000)%10 + 0x30);
	 LCD_1602_Write_data((mile_num/1000)%10 + 0x30 );
	 LCD_1602_Write_data('.');
	 LCD_1602_Write_data((mile_num/100)%10 + 0x30 );
	 LCD_1602_Write_data((mile_num/10)%10 + 0x30 );
}

void display_aver_speed()
{
	uint tmp_dat = 0;
	
	LCD_1602_Write_com( 0x80 + 0x40 + 4 );
	tmp_dat = (int)((begin_mile / total_time) *7.2); 
	LCD_1602_Write_data((tmp_dat/1000)%10 + 0x30);
	 LCD_1602_Write_data((tmp_dat/100)%10 + 0x30 );
	 LCD_1602_Write_data('.');
	 LCD_1602_Write_data((tmp_dat/10)%10 + 0x30 );
	 LCD_1602_Write_data((tmp_dat)%10 + 0x30 );
}


void LCD_1602_init( void )
{
	E = 0;
	RW = 0;
	LCD_1602_Write_com( 0x38 );
	delay(5);
	LCD_1602_Write_com( 0x0c );
	delay(5);
	LCD_1602_Write_com( 0x06 );
	delay(5);
	LCD_1602_Write_com( 0x01 );
	delay(5);
	LCD_1602_Write_com( 0x02 );
	delay(5);
	LCD_1602_Write_com( 0x80 );
	delay(5);

}

void LCD_1602_Write_com( uchar w_com )
{
	RS = 0;
	RW = 0;
	P0 = w_com;
	delay( 5 );
	E = 1;
	delay( 5 );
	E = 0;


}

void LCD_1602_Write_data( uchar w_data )
{
	RS = 1;
	RW = 0;
	P0 = w_data;
	delay( 5 );
	E = 1;
	delay( 5 );
	E = 0;
}
void LCD_1602_Write_String( uchar h, uchar *w_data )
{
	uchar i = 0;
	LCD_1602_Write_com( 0x80 );
	if( h & 0x2 )
	{
		 LCD_1602_Write_com( 0x80+0x40 );
	}
	while( w_data[i] != '\0')
		LCD_1602_Write_data(w_data[i++]);
}

void LCD_1602_Write_Num( uchar h, uchar s,int num )
{
	uchar i = 0;
	char num_temp[16]={0};
	LCD_1602_Write_com( 0x80 + s );
	if( h & 0x2 )
	{
		 LCD_1602_Write_com( 0x80+0x40 + s);
	}
	i=0;
	while( (num!=0))
	{
		 num_temp[i] = num%10 + 0x30;
		 i++;
		 num = num/10;
	}
	if( i == 0 )
	{
		   LCD_1602_Write_data(0x30);
	}
	while( i )
	{
		LCD_1602_Write_data(num_temp[i-1]);
		i--;	
	}

}

void delay(uint y_time)
{
		y_time*=14;
		while(y_time--);
}




