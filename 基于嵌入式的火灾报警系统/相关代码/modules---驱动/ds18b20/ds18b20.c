#include <linux/module.h>
#include <linux/init.h>
#include <linux/miscdevice.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <asm/irq.h>
#include <linux/random.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/delay.h>
#include <mach/gpio.h>
#include <linux/mutex.h>


#define GPH3_0CON 0xE0200C60	//GPIO 控制寄存器地址
#define GPH3_0DAT 0xE0200C64	//GPIO 数据寄存器地址
#define GPH3_0PUD 0xE0200C68	//GPIO pud寄存器地址

unsigned int *gpio_config;
unsigned char *gpio_data;
unsigned int *gpio_pud;

static struct class *fog_class;     //Linux内核中的类 用来创建设备文件
static struct class_device *fog_class_devs;   //

int major;

struct mutex res_mutex;

void Ds18b20_Pin_Init(void)
{
	unsigned int pin_val;

	gpio_request(S5PV210_GPH3(0),"my_ds1802");	//向内核申请使用GPIO  函数gpio_request由Linux内核提供  可百度查找具体内容
	gpio_config = ioremap(GPH3_0CON,4);		//地址映射   在使用内存器地址之前，必需要映射  内核会生成一个虚拟地址   通过对虚拟地址的操作来完成对实际地址的操作
	gpio_data = ioremap(GPH3_0DAT,1);
	gpio_pud = ioremap(GPH3_0PUD,2);

	pin_val = readl(gpio_pud);			//读取pud寄存器
	pin_val &=~(0x0003);				//清空底2位
	pin_val |= 0x2;				        //将底2位配置为 01
	writel(pin_val,gpio_pud);			//写入到寄存器

    pin_val = readl(gpio_data);
    writel(pin_val|0x1,gpio_data);

}

/*
	配置 GPH3.0引脚为输出模式
*/
void DS18B20_OUT( unsigned char value)
{

	if( value == 1)
	{
		gpio_direction_output( S5PV210_GPH3(0), 1);	//输出高电平  gpio_direction_output由内核提供
	}
	else
	{
		gpio_direction_output( S5PV210_GPH3(0), 0);
	}
}

/*
	配置gpio引脚为输入模式  读取ds18b20的数据
*/
unsigned char DS18B20_IN( void )
{
	unsigned int pin_val;

	gpio_direction_input( S5PV210_GPH3(0));			//gpio_direction_input 内核提供
	pin_val = readl(gpio_data);
	return pin_val&0x1;
}


/* ds18b20初始化  可参考18b20数据手册中的时序图  */
static void Init_DS18B20(void)
{
    gpio_direction_output( S5PV210_GPH3(0), 1);
    udelay(200);
    gpio_direction_output( S5PV210_GPH3(0), 0);
    udelay(600);
    gpio_direction_output( S5PV210_GPH3(0), 1);
    udelay(480);

}
/* ds18b20写一个字节  可参考18b20数据手册中的时序图  */
static void WriteCode(unsigned char dat)
{
    unsigned char temp,i;

    for(i=0;i<8;i++)
    {
        temp = dat&0x01;
        gpio_direction_output( S5PV210_GPH3(0), 1);
        udelay(2);
        gpio_direction_output( S5PV210_GPH3(0), 0);

        if(temp == 0x01)
        {
            udelay(2);
            gpio_direction_output( S5PV210_GPH3(0), 1);
            udelay(100);
        }else{
            udelay(100);
            gpio_direction_output( S5PV210_GPH3(0), 1);
            udelay(3);
        }
        dat = dat>>1;
    }
}
/* ds18b20复位  可参考18b20数据手册中的时序图  */
static void Reset_DS18B20( void )
{
    gpio_direction_output( S5PV210_GPH3(0), 0);
    udelay(500);
    gpio_direction_output( S5PV210_GPH3(0), 1);
    udelay(480);
}
/* ds18b20读取数据 返回的是原始数据  可参考18b20数据手册中的时序图  */
static unsigned int ReadData(void)
{
    unsigned int rec,data,i;
    data = 0;

    for(i=0;i<16;i++)
    {
        gpio_direction_output( S5PV210_GPH3(0), 0);
        udelay(5);

        udelay(3);
        rec = DS18B20_IN();
            udelay(20);
        if(rec){
        data |= 0x8000;
        }else{
        data &= 0x7fff;
        }
        if(i<15)
        data >>=1;
        udelay(20);

        gpio_direction_output( S5PV210_GPH3(0), 1);
        udelay(5);
    }
    return (data);
}

/* 设备文件的open函数 */
int ds18b20_open(struct inode *node, struct file *filp)
{
    return 0;
}

/* 设备文件的读函数  用来返回温度数据*/
static int ds18b20_read(struct file * file, char * buffer, size_t count, loff_t *ppos)
{
    int tem;
    int ds_value;

    mutex_lock_interruptible(&res_mutex);  //添加互斥锁 保护数据 

    Ds18b20_Pin_Init();

    Init_DS18B20();
    WriteCode(0xcc);
    WriteCode(0x44);
    gpio_direction_input( S5PV210_GPH3(0));
    udelay(100);
    tem = DS18B20_IN();
    if(tem)
    {
        gpio_direction_output( S5PV210_GPH3(0), 1);
        Reset_DS18B20();
        WriteCode(0xcc);
        WriteCode(0xbe);
        ds_value = ReadData();
    }else{
        udelay(50);
        ds_value = 0xaaaa;
    }
    mutex_unlock(&res_mutex);		//解锁

    copy_to_user(buffer, &ds_value, 4);		//将数据从内核空间拷贝到用户空间

    return sizeof ds_value;
}

static struct file_operations ds18b20_fops =		//设备文件操作函数集
{
    .open = ds18b20_open,
    .read = ds18b20_read,
};

/* 内核模块初始化函数 */
static int Ds18b20_init(void)
{

    major = register_chrdev( 0,"ds18b20_drv", &ds18b20_fops );  //当指定的设备号为0时，系统会自动生成一个设备号
    fog_class = class_create(THIS_MODULE,"ds18b20_class");
    fog_class_devs = device_create(fog_class,NULL,MKDEV(major,0),NULL,"my_ds1802");  //创建设备文件

    mutex_init(&res_mutex);

    printk("install module successed\n");

    return 0;
}

/*内核模块卸载函数*/
void Ds18b20_exit(void)
{
    unregister_chrdev( major, "ds18b20_drv" );  //注销设备文件

    device_unregister(fog_class_devs);
    class_destroy(fog_class);  //销毁类

}


module_init(Ds18b20_init);
module_exit(Ds18b20_exit);


MODULE_LICENSE("GPL");
