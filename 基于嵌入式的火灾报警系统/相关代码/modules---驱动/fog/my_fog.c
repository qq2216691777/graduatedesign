#include <linux/module.h>
#include <linux/init.h>
#include <linux/miscdevice.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <asm/irq.h>
#include <linux/random.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <mach/gpio.h>



#define GPH2CON 0xE0200c60		//gpio�Ĵ���Ӳ����ַ
#define GPH2DAT 0xE0200c64

static struct class *fog_class;     //创建类
static struct class_device *fog_class_devs;   //创建类对应的设备


unsigned int *gpio_data;




int fog_open(struct inode *node,struct file *filp)
{
    return 0;
}
/*
�豸�ļ��Ķ�����   
*/
ssize_t fog_read(struct file *filp, char __user *buf, size_t size, loff_t *pos)
{

    unsigned int fog_num = 0;

    gpio_direction_input( S5PV210_GPH3(3));		//����gpioΪ����ģʽ

    fog_num = gpio_get_value(S5PV210_GPH3(3));		//��ȡgpio��״̬

    copy_to_user(buf, &fog_num, 4);		//�����ݴ��ں˿ռ俽�����û��ռ�


    return 4;
}

struct file_operations fog_fops =	//�豸�ļ�����������
{
    .open = fog_open,
    .read = fog_read,
};
/*
struct miscdevice fog_miscdev = {
    .minor = 200,
    .name = "fog",
    .fops = &fog_fops,
};*/

int major;
static int fog_init()
{
    int ret;
    major = register_chrdev( 0,"fog_drv", &fog_fops );
    fog_class = class_create(THIS_MODULE,"fog_class");
    fog_class_devs = device_create(fog_class,NULL,MKDEV(major,0),NULL,"my_fog");

    if (ret !=0)
        printk("register fail!\n");

    gpio_request(S5PV210_GPH3(3),"my_fog");	//���ں�����GPIOʹ��Ȩ

    gpio_direction_input( S5PV210_GPH3(3));

    return 0;
}


static void fog_exit()
{
    unregister_chrdev( major, "fog_drv" );
    device_unregister(fog_class_devs);
    class_destroy(fog_class);


}


module_init(fog_init);
module_exit(fog_exit);


MODULE_LICENSE("GPL");
