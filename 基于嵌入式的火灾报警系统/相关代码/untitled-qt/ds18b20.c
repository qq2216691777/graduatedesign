#include "ds18b20.h"
#include "interface.h"


#define DS18B20_devname "/dev/my_ds1802"   //Please init the value

void DS18B20_app()
{
    int tem;
    int DS18B20_fd = 0;
    float tempreture_temp;
    float last_tem=0;
    

    if((DS18B20_fd = open(DS18B20_devname, O_RDWR)) < 0)
    {
        printf("Open ds18b20 dev error ! \n");
        exit(1);
    }
    while(1)
    {
	    read(DS18B20_fd,&tem,4);		//读取温度原始数据 
	    tempreture_temp = tem*0.0625+0.5;	//将原始数据转化为正常的温度信息  转换公式参考 ds18b20数据手册


	    if(tempreture_temp<last_tem)
	    	Stat0.tempreture =tempreture_temp;		//将温度信息保存起来
	    else
	    	Stat0.tempreture =last_tem;
	    	
	    last_tem = tempreture_temp;
	    usleep(500000);
	}
}
