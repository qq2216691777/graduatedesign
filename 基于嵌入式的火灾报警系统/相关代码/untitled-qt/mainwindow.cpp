#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QPalette>
#include "env.h"
#include <QProcess>
#include <QDebug>

extern "C"{			//C语言函数声明   （这里是c++调用C函数的声明）
    int interface(void);	//接口函数声明 注意完成线程的初始化
    float return_tem(void);	//返回温度值
    int return_fog(void);	//返回烟雾状态
    int Multimedia_message( void );	//发送彩信函数
}

extern float tempreture_limit;		//声明变量   本变量存储的是传递进来的报警温度值

MainWindow *w_p;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    setMinimumSize(1024, 600);		//设置窗口最大尺寸
    setMaximumSize(1024, 600);		//设置串口最小尺寸
    setWindowFlags(windowFlags() &~ Qt::WindowMinMaxButtonsHint);	
    this->setWindowFlags(Qt::FramelessWindowHint);	//隐藏标题栏

    this->timer = new QTimer;				//定义一个定时器
    QObject::connect(this->timer,SIGNAL(timeout()),SLOT(TimeSlot()));		//连接一个槽

    QObject::connect(this,SIGNAL(custsignal()),this,SLOT(SendMess_Slot()));

    QObject::connect(this,SIGNAL(AppendText(QString)),this,SLOT(SlotAppendText(QString)));

    w_p = this;

   // text_show("system start...\n");
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

class SleeperThread : public QThread		//自定义延时类
{
    public:
    static void msleep(unsigned long msecs)
    {
        QThread::msleep(msecs);
    }
};

int interface_sta = 1;

void MainWindow::on_startButton_clicked()	//点击开始按钮时  执行的内容
{
    ui->quitButton->setEnabled(true);		//打开停止按钮
    ui->startButton->setEnabled(false);		//关闭开始按钮 防止重复点击
    ui->label_temperate->setEnabled(false);	//使能温度显示
    ui->label_fog->setEnabled(false);
    ui->label_status->setEnabled(false);

    if(  1==interface_sta )		//interface  只执行一次
    {
    	interface_sta = 0;
    	interface();				//启动线程
    }
    this->timer->start(1000);			//开启定时器  


}

void MainWindow::on_quitButton_clicked()
{
    ui->quitButton->setEnabled(false);
    ui->startButton->setEnabled(true);

    ui->label_temperate->setEnabled(true);
    ui->label_fog->setEnabled(true);
    ui->label_status->setEnabled(true);

    this->timer->stop();		//关闭定时器
}

char t_dat[20];

extern int send_temp;
QString text("00.00");
int fog;
bool system_stat=true;
bool send_stat = true;
void MainWindow::TimeSlot()	//定时器执行函数  1S 一次
{
    QFont ft1;
    QPalette pa;
    float dat;



    ft1.setPointSize(16);		//设置字体大小
    ui->label_temperate->setFont(ft1);
    ui->label_fog->setFont(ft1);
    ui->label_status->setFont(ft1);

    dat = return_tem();						//读取温度值
    sprintf(t_dat,"%.2f/%.2f",dat,tempreture_limit );		//将温度信息保存到字符串
    text = t_dat;
    ui->label_temperate->setText(text);				//更新温度显示

    if( dat > tempreture_limit)					//如果当前温度大于设置的值
    {
        pa.setColor(QPalette::WindowText,Qt::red);		//字体设置为红色
        ui->label_temperate->setPalette(pa);
        system_stat = false;
    }
    else
    {
        pa.setColor(QPalette::WindowText,Qt::black);		
        ui->label_temperate->setPalette(pa);
        system_stat=true;
    }

    fog = return_fog();					//读取烟雾状态

    if(fog==1)
    {
        pa.setColor(QPalette::WindowText,Qt::black);
        ui->label_fog->setPalette(pa);
        ui->label_fog->setText(trUtf8("正常"));
    }
    else
    {
        pa.setColor(QPalette::WindowText,Qt::red);
        ui->label_fog->setPalette(pa);
        ui->label_fog->setText(trUtf8("警告"));
    }

    if((1==fog)&&system_stat)
    {
        pa.setColor(QPalette::WindowText,Qt::black);
        ui->label_status->setPalette(pa);
        ui->label_status->setText(trUtf8("系统正常"));
    }
    else
    {
        pa.setColor(QPalette::WindowText,Qt::red);
        ui->label_status->setPalette(pa);
        ui->label_status->setText(trUtf8("系统异常"));

        if(send_stat)
        {
            emit this->custsignal();
            send_stat=false;
        }
    }

    if(0==send_temp)
        send_stat=true;
}


void MainWindow::SendMess_Slot()
{
    system("/usr/share/camera -qws &");			//启动摄像头程序
    this->hide();

    SleeperThread::msleep(1000);
    SleeperThread::msleep(1000);

    this->pixmap = QPixmap::grabWindow(QApplication::desktop()->winId());   //Get the picture		获取图片
    system("killall camera");  //kill
    QPixmap pixmap1 = this->pixmap.copy(QRect( 12, 140, 478, 355) );
    pixmap = pixmap1.scaled(350,259,Qt::KeepAspectRatio);
    this->show();
    QString filename = "./RES.bmp";			//保存图片为bmp
    pixmap.save(filename);
    system("/usr/share/bmp_jpg ./RES.bmp");		//压缩文件为jpg
     //message
   // Multimedia_message();
    send_temp = 1;					//准备发送彩信


}





void MainWindow::on_pushButton_clicked()
{
    system("reboot");
}

void MainWindow::text_show(char *c)
{

    emit AppendText(c);


}


extern "C" void text_printf( char *c );
void text_printf( char *c )
{
    //printf(c);
   w_p->text_show(c);

}


void MainWindow::SlotAppendText(const QString &text)
{
    ui->textEdit->append(text);
}
