#-------------------------------------------------
#
# Project created by QtCreator 2017-03-09T22:59:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = untitled
TEMPLATE = app

QTPLUGIN += qjpeg

SOURCES += main.cpp\
        mainwindow.cpp \
    fog.c \
    interface.c \
    ds18b20.c \
    gsm800a.c

HEADERS  += mainwindow.h \
    fog.h \
    interface.h \
    ds18b20.h \
    gsm800a.h \
    env.h

FORMS    += mainwindow.ui
