#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPixmap>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QFile>
#include <QFileDialog>
#include <QDesktopServices>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);

    void text_show(char *c);



    ~MainWindow();
    
private slots:
    void on_startButton_clicked();

    void on_quitButton_clicked();

    void TimeSlot();

    void SendMess_Slot();

    void on_pushButton_clicked();

    void SlotAppendText(const QString &text);

private:
    Ui::MainWindow *ui;
    QPixmap pixmap;
    QTimer *timer;

signals:
    void custsignal();
    void AppendText(const QString &text);



};

#endif // MAINWINDOW_H
