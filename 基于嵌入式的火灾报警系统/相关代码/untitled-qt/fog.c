#include "fog.h"

#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "interface.h"

#define Fog_devname "/dev/my_fog"

void fog_app( void )
{
    int fog_flag = 0;
    int fog_fd = 0;
    if((fog_fd = open(Fog_devname,O_RDWR)) < 0)
    {
        printf("Open Fog dev error ! \n");
        pthread_exit(NULL);
    }
    while(1)
    {

    	read(fog_fd,&fog_flag,4);		//读取传感器的状态

        if(fog_flag == 1)
        {
        	Stat0.fog_stat = 1;		//将状态保存到全局变量中  
        }
        else
        	Stat0.fog_stat = 0;

        usleep(500000);
    }
}
