#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sys/stat.h>

#include "gsm800a.h"
#include "interface.h"
#include "env.h"

char phone_num[20];

void text_printf( char *c );

int send_temp=0;

char print_data[128];

#define SERIAL_DEV		"/dev/ttySAC0"		//串口设备名称

#define TITLE_CONTENT	"Fire Warning! Please view this message!"		//发送彩信的主题 

#define TEXT_CONTENT	"Fire Warning: \n  Fire may occur at Monitoring site.You" \
							"can view the scene through the picture."		//发送彩信的文本内容

char cmd[][100]= {
		/*0*/		"AT\r\n",	//ok
		/*1*/		"AT+CMMSINIT\r\n",		//ok
		/*2*/		"AT+CMMSCURL=\"mmsc.monternet.com\"\r\n", //
	    /*3*/		"AT+CMMSCID=1\r\n",
    	/*4*/		"AT+CMMSPROTO=\"10.0.0.172\",80\r\n",
		/*5*/		"AT+CMMSSENDCFG=6,3,0,0,2,4\r\n",
		/*6*/		"AT+SAPBR=3,1,\"Contype\",\"GPRS\"\r\n",
		/*7*/		"AT+SAPBR=3,1,\"APN\",\"cmwap\"\r\n",
		/*8*/		"AT+SAPBR=1,1\r\n",
		/*9*/		"AT+CMMSEDIT=1\r\n",	//8
		/*10*/		"AT+CMMSDOWN=\"PIC\",0,500000\r\n",
					//connect
		/*11*/		"image data\r\n",
					//send image data

					//ok
		/*12*/		"AT+CMMSDOWN=\"TEXT\",0,5000\r\n",
					//connect
					/*MMS信息内容*/
		/*13*/		TEXT_CONTENT,

		/*14*/		"AT+CMMSDOWN=\"TITLE\",0,5000 \r\n",
					//connect
		/*15*/		TITLE_CONTENT,	//ok

		/*16*/		"AT+CMMSRECP=\"00000\"\r\n",

					//"AT+CMMSVIEW\r\n",

		/*17*/		"AT+CMMSSEND\r\n",

		/*18*/		"AT+CMMSEDIT=0\r\n",

		/*19*/		"AT+CPOWD=1\r\n"
				};

typedef struct $
{
	int fd;
	char *image_name;
	int image_len;

}Gsm_t;

Gsm_t *g_gsm;
/*
串口波特率设置
*/
int set_opt(int fd,int nSpeed,int nBits,char nEvent,int nStop)

{

    struct termios newtio,oldtio;

    if(tcgetattr(fd,&oldtio)!=0)

    {

        perror("error:SetupSerial 3\n");

        return -1;

    }

    bzero(&newtio,sizeof(newtio));


    newtio.c_cflag |= CLOCAL | CREAD;

    newtio.c_cflag &= ~CSIZE;



    newtio.c_lflag &=~ICANON;



    switch(nBits)

    {

        case 7:

            newtio.c_cflag |= CS7;

            break;

        case 8:

            newtio.c_cflag |=CS8;

            break;

    }


    switch(nEvent)



    {

        case 'O':

            newtio.c_cflag |= PARENB;

            newtio.c_cflag |= PARODD;

            newtio.c_iflag |= (INPCK | ISTRIP);

            break;

        case 'E':

            newtio.c_iflag |= (INPCK | ISTRIP);

            newtio.c_cflag |= PARENB;

            newtio.c_cflag &= ~PARODD;

            break;

        case 'N':

            newtio.c_cflag &=~PARENB;

			newtio.c_iflag |= INPCK;

			newtio.c_iflag &= ~(INLCR|ICRNL|IXON|ECHO|ISIG);

			newtio.c_oflag &= ~OPOST;

            break;

    }


    switch(nSpeed)

    {

        case 2400:

            cfsetispeed(&newtio,B2400);

            cfsetospeed(&newtio,B2400);

            break;

        case 4800:

            cfsetispeed(&newtio,B4800);

            cfsetospeed(&newtio,B4800);

            break;

        case 9600:

            cfsetispeed(&newtio,B9600);

            cfsetospeed(&newtio,B9600);

            break;

        case 115200:

            cfsetispeed(&newtio,B115200);

            cfsetospeed(&newtio,B115200);

            break;

        case 460800:

            cfsetispeed(&newtio,B460800);

            cfsetospeed(&newtio,B460800);

            break;

        default:

            cfsetispeed(&newtio,B9600);

            cfsetospeed(&newtio,B9600);

            break;

    }


    if(nStop == 1)

        newtio.c_cflag &= ~CSTOPB;

    else if(nStop == 2)

        newtio.c_cflag |= CSTOPB;

    newtio.c_cc[VTIME] = 1;

    newtio.c_cc[VMIN] = 0;

    tcflush(fd,TCIFLUSH);



    if(tcsetattr(fd,TCSANOW,&newtio)!=0)

    {

        perror("com set error\n");

        return -1;

    }

    return 0;

}

/*
获取图片文件大小
*/
int file_size2(char* filename)
{
    struct stat statbuf;
    stat(filename,&statbuf);
    int size=statbuf.st_size;

    return size;
}
/*
发送图片数据到gsm
*/
int up_pic( char *pic_path )
{
    char buffer[1024];
    int count = 0 ;
    int pic_size = 0;
    int ffd = 0;
    int tmp =0 ;
    int fd=g_gsm->fd;

    ffd = open(pic_path,O_RDWR);
    if( ffd== -1)
        text_printf("open pic error !\n");
    else
        printf("Open pic successed ! ffd is :%d\n",ffd);

    while((count = read(ffd,buffer,1024)) > 0) //每次读取1024个字节的数据对外发送
    {
    	tmp = write(fd,buffer,count);

    	pic_size=pic_size + tmp;
        if(tmp != count){
                text_printf("Send pic error !\n");
    	        exit(0);
    	    }
    }

    close(ffd);  //关闭打开的图片文件

    return pic_size;
}

int send_message( Gsm_t *gsm );

/*
发送彩信函数
*/
int Multimedia_message( void )
{
	int fd;
	int ret;
	int i;
    char *image_file=IMAGE_PATH;

	g_gsm = calloc(1,sizeof(Gsm_t));
	g_gsm->image_name = calloc(1,strlen(image_file));
	memcpy(g_gsm->image_name, image_file,strlen(image_file));		//获取到图片文件名
    sprintf(print_data,"the image name is: %s\n",g_gsm->image_name );
    text_printf(print_data);

	fd = open( SERIAL_DEV, O_RDWR|O_NOCTTY );		//打开串口设备
	if( -1==fd )
	{
		perror("serial device open");
		return -1;
	}
	g_gsm->fd = fd;

	if( set_opt(fd,115200,8,'N',1)== -1)		//设置波特率
    {
    	perror("set_opt");
    	return -1;
    }
    /* 更新短信发送内容信息 */
    g_gsm->image_len = file_size2(g_gsm->image_name);
    sprintf(cmd[10],"AT+CMMSDOWN=\"PIC\",%d,500000\r\n",g_gsm->image_len);
    sprintf(cmd[12],"AT+CMMSDOWN=\"TEXT\",%d,5000\r\n",(int)strlen(cmd[13]));
    sprintf(cmd[14],"AT+CMMSDOWN=\"TITLE\",%d,5000\r\n",(int)strlen(cmd[15]));
	sprintf(cmd[16],"AT+CMMSRECP=\"%s\"\r\n",phone_num);

/*
	打印需要发送的参数
*/
	for(i=0; i<20; i++ )
	{
        sprintf(print_data,"%d:%s",i,cmd[i] );
        text_printf(print_data);

	}
	/* 发送彩信 */
    ret = send_message(g_gsm);

	return ret;
}
/*
发送一条命令后  等待模块回复信息
*/
int wait_for_respond( int fd, int index )
{
	char data_tmp[200];
	char *rx_data;
	int ret;
	int time = 0;
	int i;

	while(1)
	{
		rx_data = data_tmp;
		ret = read(fd,rx_data,40);
		if(ret>0)
		{
			rx_data[ret] = 0;
			for( i=0;i<ret;i++)
			{
				if( *rx_data)
				{
					if( (*rx_data!=0x0a) && (*rx_data!=0x0d))
						break;
				}
				rx_data++;
			}
			if((strlen(rx_data)==4)&&(*rx_data=='O'))	//收到OK
			{
                		text_printf("ok\n");
				return 1;
			}
			else if( (strlen(rx_data) ==9)&&(*rx_data=='C') )	//收到connect
			{
				return 2;
			}
			else if( (strlen(rx_data) ==7)&&(*rx_data=='E'))	//收到error
			{
				return -1;
			}
		}
		sleep(1);
		time++;
		if(time>10)	//如果等待时间过长
		{
            sprintf(print_data,"time:%d\n",time );
            text_printf(print_data);
			if((index<16)||(time > 80))
				break;
		}
	}
	return 0;
}

int send_message( Gsm_t *gsm )
{
	int fd = gsm->fd;
	int ret;
	int i=0;

	while(1)
	{
		if( i==11)
		{
			ret = up_pic(gsm->image_name);		//如果发送第11条命令  则为发送图片数据信息
            		sprintf(print_data,"send image size:%d\n",ret );
            		text_printf(print_data);
		}
		else
		{
            		sprintf(print_data,"send cmd No:%d\n",i );
            		text_printf(print_data);
			ret = write(fd, cmd[i], sizeof(cmd[i]));	//按照cmd数组依次发送命令信息
		}
		if(ret == -1 )						//发送命令失败
		{
            		sprintf(print_data,"send cmd [%d] error\r\n",i);
            		text_printf(print_data);
			return -2;
		}
		if(i==19)
			break;
		ret = wait_for_respond(fd, i);		//发送成功 等待数据返回
		if(ret == 2 )
		{
            		sprintf(print_data,"connect: %d\n",i);
            		text_printf(print_data);
		}
		else if( 0==ret )
		{
            		sprintf(print_data,"time out: %d  please waitting...\n",i);
            		text_printf(print_data);
			write(fd, cmd[19], sizeof(cmd[19]));
			sleep(3);
			return -1;
		}
		else if(-1==ret)
		{
            		sprintf(print_data,"receive error: %d\n",i);
            		text_printf(print_data);
			write(fd, cmd[19], sizeof(cmd[19]));
			sleep(3);
		/*	if(i<4)
			{
				sleep(2);
				i = 0;
				continue;
			}*/
			return -1;
		}
		i++;
	}
    sprintf(print_data,"message send successed!\n");
    text_printf(print_data);
	return 0;
}


void gsm800a_pthread(void)
{
    int lastmin=-6;
    time_t timer;
    struct tm *tbclock;
    int min;

    while(1)
    {
        timer = time(NULL);
        tbclock = localtime(&timer);
        min = tbclock->tm_min;

        sleep(1);
        if(send_temp)
        {
            if(min>lastmin+4)
            {
                Multimedia_message();
                send_temp = 0;
                lastmin = min;
            }
            else
            {
                sprintf(print_data,"if you want to send a message, please wait 5 minutes\n");
                text_printf(print_data);
                send_temp = 0;
            }
        }
    }
}





