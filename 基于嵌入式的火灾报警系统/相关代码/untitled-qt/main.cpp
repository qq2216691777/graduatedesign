#include <QApplication>
#include "mainwindow.h"
#include <QStringList>
#include <QDebug>

extern "C"{void init_env(char *argg[] );}
int main(int argc, char *argv[])
{
    QApplication::addLibraryPath("/lib");

    QApplication a(argc, argv);
    
    init_env(argv);		//将传递进来的参数保存起来  以便于下次使用
    
    
    MainWindow w;

    w.show();
    
    return a.exec();
}

