#include "interface.h"


#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "gsm800a.h"
#include "fog.h"
#include "ds18b20.h"

int txt_fd;

struct Message Stat0;

void text_printf( char *c );


float tempreture_limit = 0;

char iargv[3][20]={		//用于保存 命令行参数  获取手机号码和温度值
        "123",
        "18437953612",
        "2332",
        };

void set_t( float dat );
void set_fog( int i );

void init_env(char *argg[] )		//在mian调用  获取手机号码和温度
{
    memcpy(iargv[0],argg[0],12);
    memcpy(iargv[1],argg[1],12);
    memcpy(iargv[2],argg[2],12);

    tempreture_limit = atoi(iargv[2])*0.01;
    memcpy(phone_num,iargv[1],strlen(iargv[1]));

    printf("the message will be sent to :%s\n",phone_num );
    printf("the tempreture max:%f\n", tempreture_limit);
}

/*
点击开始按钮后  执行此函数  做相应的系统初始化
*/
int interface(void)
{
    pthread_t pthread_Fog_id;
    pthread_t pthread_DS18b20_id;
    pthread_t pthread_gsm800a_id;
    int message = 0;
    int message_temp=0;
    int s_delay = 0;


    text_printf("System Start...\n");
    if(pthread_create(&pthread_Fog_id,NULL,(void *) fog_app,NULL) != 0)		//创建烟雾传感器进程
    {
        text_printf("Creat Fog thread error !\n");
        exit(1);
    }

    if(pthread_create(&pthread_DS18b20_id,NULL,(void *) DS18B20_app,NULL) != 0)		//创建ds18b20进程
    {
        text_printf("Creat DS18B20 thread error ! \n");
        exit(1);
    }

    if(pthread_create(&pthread_gsm800a_id,NULL,(void *) gsm800a_pthread,NULL) != 0)		//创建gsm模块进程
    {
        text_printf("Creat DS18B20 thread error ! \n");
        exit(1);
    }

    text_printf("System Start successed\n");

    return 0;
}


float return_tem(void)
{
    return Stat0.tempreture;		//返回温度值
}

int return_fog(void)
{
    return Stat0.fog_stat;		//返回烟雾传感器状态
}
