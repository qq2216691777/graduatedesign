#include <reg52.h>

#define uchar unsigned char
#define uint unsigned int

/* 数码管编码 0~9 P 共10个 */
uchar num[] = { 0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f,0x73};

/* 数码管显示的实时速度 */
int real_num = 456;

/* 数码管显示的测量速度 */
int set_num = 123;

/**
  *·定时器0的初始化函数
  * 用于数码管的动态扫描
**/
void Time0_Init()
{
	TMOD &= 0XF0;
	TMOD |= 0X01;		//设置定时器0的工作方式为1
	TH0 = (65536-5000)/256;	  //装初值  定时10ms  @12Mhz
	TL0 = (65536-5000)%256;

	EA = 1;
	ET0 = 1;
	TR0 = 1;
}
/**
  *·定时器1的初始化函数
  * 用于控制波形的周期
**/
void Time1_Init()
{
	TMOD &= 0XF0;
	TMOD |= 0X20;		//设置定时器1的工作方式为2
	TH1 = 256-145;//(65536-100)/256;	  //装初值  定时100us  @12Mhz
	TL1 = 256-145;//(65536-100)%256;	   //这两个地方应该是 256-100的  可是在示波器上看到的输出波形周期不对 我就修改了  这里应该是仿真软件的问题

	EA = 1;
	ET1 = 1;
	TR1 = 1;
}

/**
 * 数码管显示函数  - 当前速度
**/
uchar dula=1;
void digitron_display_now()
{

	dula = dula>>1;
	if( 0==dula )
		dula = 0x80;
	
	P2 =  ~dula;

	switch( dula )
	{
	/* 前4个数码管 */
	 	case 0x10:
			P0 = num[real_num%10];
			break;

		case 0x20:
			P0 = num[(real_num/10)%10];
		    break;

		case 0x40:
			P0 = num[real_num/100];
			break;

		case 0x80:
			P0 = num[10];
		    break;
	}
}


/**
 * 数码管显示函数  - 设置速度
**/
void digitron_display_set()
{

	if( 0==dula )
		dula = 0x80;
	
	P2 =  ~dula;

	switch( dula )
	{
	/* 后个数码管 */
	 	case 0x01:
			P0 = num[set_num%10];
			break;

		case 0x02:
			P0 = num[(set_num/10)%10];
		    break;

		case 0x04:
			P0 = num[set_num/100];
			break;

		case 0x08:
			P0 = num[10];
		    break;
	}
}


void delay()
{
 	int x=1000;
	while(x--);
}

sbit cs = P3^7;
sbit wr = P3^6;
sbit LE1 = P3^5;
/**
  *·按键扫描化函数
**/
void key_scan()
{

}

void main()
{
   Time0_Init();
   while(1);
}



void Time0_handler() interrupt 1
{
	TH0 = (65536-10000)/256;	  //装初值  定时20ms  @12Mhz
	TL0 = (65536-10000)%256;	
	digitron_display_now();
	digitron_display_set();

}

void Time1_handler() interrupt 3
{



}