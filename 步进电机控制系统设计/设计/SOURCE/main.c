#include<reg51.h>
#include<intrins.h>
sbit clk=P3^0;						   //传输数据
sbit din=P3^1;
sbit c0=P3^2;						  //测速，测放向
sbit c1=P3^3;
sbit key1=P2^4;						  //数码管位选
sbit key2=P2^5;
sbit key3=P2^6;
sbit key4=P2^7;
unsigned int num0;					  //转速

int set_num=100;

unsigned char cs;					 //测速定时器时间控制
unsigned  char b=0; 				 //静态数码管显示
unsigned  char s=0;
unsigned  char g=0;
unsigned  char w=0;
unsigned char key_numb[4]=0;		 //矩阵键盘获取数值
unsigned char code table_smg[]={0xfc,0x60,0xda,0xf2,0x66,0xb6,0xbe,0xe0,0xfe,0xf6,0xce,0xec};		//共阴静态数码管段码

unsigned char code table[]={0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f,0x73,0x37};		//共阴数码管段码

void delay_5ms()		  //约5ms延时
{
	unsigned char i,j;
	for(i=0;i<25;i++)
		for (j=0;j<=200;j++);
}
void delay()			 //根据转速延时，用于测方向
{	unsigned  int i,n;
	unsigned char j;
	n=key_numb[1]*100+key_numb[2]*10+key_numb[3];	
	for(i=0;i<=60000/(n*24)*5;i++)
		for (j=0;j<=200;j++);
} 
void delay_200us()		//约200us延时
{
	unsigned char j;
	for (j=0;j<=200;j++);
	j=0;
}
unsigned char LED(unsigned char key_num1,key_num2,key_num3,key_num4)	 //动态数码管显示
{
	key1=0;key2=1;key3=1;key4=1;
	P0=table[key_num1];
	delay_200us();
	key1=1;key2=0;key3=1;key4=1;
	P0=table[key_num2];
	delay_200us();	
	key1=1;key2=1;key3=0;key4=1;
	P0=table[key_num3];
	delay_200us();	
	key1=1;key2=1;key3=1;key4=0;	
	P0=table[key_num4];
	delay_200us();
	key1=1;key2=1;key3=1;key4=1;	
	return 0;
}
void suocun()						 //  按键锁存
{
	unsigned  char key_temp;
	delay_5ms();
	P1=0x0f;
	key_temp=P1;
	do 
	{ 
		delay_5ms();
		P1=0x0f;
		key_temp=P1;
		LED(key_numb[0],key_numb[1],key_numb[2],key_numb[3]);
	}while(key_temp!=0x0f);	
}
unsigned char key_scan()			 //扫描键盘
{
	unsigned char key_temp0,key_temp1;
	unsigned char key_num=20; 
	P1=0x0f;
	key_temp0=P1;
	if(key_temp0!=0x0f)
	{
	delay_5ms();
	key_temp0=P1;
		if(key_temp0!=0x0f)
		{
			P1=0xf0;
			key_temp1=P1;
			if(key_temp0==0x0e)
			{
				switch(key_temp1)
				{
					case 0x70:key_num=1;break;
					case 0xb0:key_num=2;break;
					case 0xd0:key_num=3;break;
					default:key_num=20;break;
				}
			}
			else if(key_temp0==0x0d)
			{
				switch(key_temp1)
				{
					case 0x70:key_num=4;break;
					case 0xb0:key_num=5;break;
					case 0xd0:key_num=6;break;
					case 0xe0:key_num=15;break;
					default:key_num=20;break;
				}
			}
			else if(key_temp0==0x0b)
			{
				switch(key_temp1)
				{
					case 0x70:key_num=7;break;
					case 0xb0:key_num=8;break;
					case 0xd0:key_num=9;break;
					case 0xe0:key_num=12;break;
					default:key_num=20;break;
				}
			}
			else if(key_temp0==0x07)
			{
				switch(key_temp1)
				{
					case 0x70:key_num=10;break;
					case 0xb0:key_num=0;break;
					case 0xd0:key_num=11;break;
					case 0xe0:key_num=13;break;
					default:key_num=20;break;
				}
			}
		}
	}
	return key_num;
}
void bitsend( bit i)				   //位传送
{
	clk=0;
	din=i;
	clk=1;
}
void unitsend(unsigned char unit)		  //字传送
{
	clk=0;
	bitsend((bit)(table_smg[unit]&0x80));
	bitsend((bit)(table_smg[unit]&0x40));
	bitsend((bit)(table_smg[unit]&0x20));
	bitsend((bit)(table_smg[unit]&0x10));
	bitsend((bit)(table_smg[unit]&0x08));
	bitsend((bit)(table_smg[unit]&0x04));
	bitsend((bit)(table_smg[unit]&0x02));
	bitsend((bit)(table_smg[unit]&0x01));
	clk=0;
}

void Time1_handler() interrupt 3					 //定时器1中断服务子程序	，用于测转速
{	
	unsigned int num1;
	TH1=(65536-50000)/256;				//装初值  定时50ms  @12Mhz
	TL1=(65536-50000)%256;
	if(cs==0)
	{
		cs=80;
		num1=num0*2.5;
		num0=0;
		g=num1/100;
		s=num1%100;
		s=s/10;
		b=num1%100;
		b=b%10;

		if( 10 == key_numb[0] )
			w = 10;
		else
			w = 11;	

		unitsend(b);
		unitsend(s);
		unitsend(g);
		unitsend(w);
	}
	cs--;
}

void Int_0() interrupt 0				   //外部中断0，用于测速
{
	num0++;
}

/**
  *·定时器0的初始化函数
**/
void Time0_Init()
{
	TMOD &= 0XF0;
	TMOD |= 0X01;		//设置定时器0的工作方式为1
	TH0 = (65536-10000)/256;	  //装初值  定时10ms  @12Mhz
	TL0 = (65536-10000)%256;

	EA = 1;
	ET0 = 1;
	TR0 = 1;
}

void Time0_handler() interrupt 1
{
	static int t0_n=0;
	static unsigned char p2_tmp = 0xee;

	TH0 = (65536-1000)/256;	  //装初值  定时10ms  @12Mhz
	TL0 = (65536-1000)%256;
	t0_n++;
	if( t0_n >= 2500/set_num )
	{
		
		t0_n = 0;
		if( 10 == key_numb[0] )
		{
			p2_tmp = _crol_(p2_tmp,1);
			
			P2 =  (p2_tmp& 0x0f) | (P2 &0xf0);
		}
		else
		{
			p2_tmp = _cror_(p2_tmp,1);
			P2 =  (p2_tmp& 0x0f) | (P2 &0xf0);	
		}
	}
}

/**
  *·定时器1的初始化函数
**/
void Time1_Init()
{
	TMOD &= 0X0f;
	TMOD |= 0X10;		//设置定时器0的工作方式为1
	TH1 = (65536-10000)/256;	  //装初值  定时10ms  @12Mhz
	TL1 = (65536-10000)%256;

	EA = 1;
	ET1 = 1;
	TR1 = 1;
}

void Int0_init()
{
   EX0=1;
   IT0=1;
}

	
void main()
{
	unsigned  char key_num=0;
	unsigned  char t;
	Int0_init();
	Time0_Init();
	Time1_Init();
	P2=0xee;
	key_numb[0] = 10;
	key_numb[1] = 1;
	while(1)
	{  
		key_num=key_scan();
		  
		if(key_num==13)	//修改 	  
		{ 
		   EX0 = 0;	
		   unitsend(8);
		   unitsend(8);
		   unitsend(8);
		   unitsend(8);
		   t = 0; 
		   		   		   	
		   while(1)
		   {
			   do
			   {
				  key_num=key_scan();	
			   	  LED(key_numb[0],key_numb[1],key_numb[2],key_numb[3]);
			   }while( key_num<19 );

			   while( key_num>19 )
			   {
				   key_num=key_scan();	
			   	  LED(key_numb[0],key_numb[1],key_numb[2],key_numb[3]);
			   }

			   if(key_num==12)	 //确定	 	
			   { 	 	
				   break;
			   }
			   if( 10 == key_num )
				   key_numb[0] = 10;   
			   else if( 11 == key_num )
			   		key_numb[0] = 11;


			   if( key_num >=0 && key_num<10 )
			   {
			   		if( t>2 )
						break ;
			   		t++;
				   key_numb[t] = key_num;
			   }
			  	   	
		   }
		   set_num = key_numb[1]*100;
		   set_num += key_numb[2]*10;
		   set_num += key_numb[3];
		   if( set_num > 300)
		   		set_num = 300;
		   else if( set_num < 100)
		   		set_num = 100;
		   key_numb[1] = set_num/100;
		   key_numb[2] = (set_num/10)%10;
		   key_numb[3] = set_num%10;		
		   EX0 = 1;
		} 
		

		LED(key_numb[0],key_numb[1],key_numb[2],key_numb[3]);
	}
}





