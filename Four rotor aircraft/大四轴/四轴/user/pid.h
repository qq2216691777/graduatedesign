/**
  ***************************************
  *@FileName pid.h
  *@Auther   YNXF  
  *@date     2015-11-23
  *@Version  v1.0
  *@Action      
  ***************************************
**/
#ifndef _PID_H__
#define _PID_H__
void PID_Control( float R, float P, float Y, float G_x, float G_y, float G_z, int YM );

#endif


