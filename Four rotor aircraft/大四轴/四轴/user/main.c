/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
  ***************************************
  *@FileName main.c
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@Action      
  ***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "stm32f10x_conf.h"
#include "nrf24l01.h"
#include "mpu6050.h"
#include "imu.h"
#include "tim_pwm.h"

void LED_Init()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC, ENABLE );
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; //GPIO_Mode_IPU; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOC, &GPIO_InitStructure );
}

Angle O_EAng;

int main( void )
{
	
	unsigned char NRF_Send_Data[20];
	
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	while(1);
	Motot_Init();				//电机初始化
	
	LED_Init();
	
	NRF24L01_Init();
	if( NRF_Check() )
		printf("NRF24L01 Init Successed!\r\n");
	else
		printf("NRF24L01 Init Failed!\r\n");
	
	MPU6050_Init();
		
	Handler_TIM2_Init( 2000,72 );
	
	NRF_RT_Mode(1);		//发送模式
	
	while(1)
	{
		NRF_Send_Data[0] = ((short)(O_EAng.Roll*100))>>8;
		NRF_Send_Data[1] = ((short)(O_EAng.Roll*100))%256;
		
		NRF_Send_Data[2] = ((short)(O_EAng.Pitch*100))>>8;
		NRF_Send_Data[3] = ((short)(O_EAng.Pitch*100))%256;
		
		NRF_Send_Data[4] = ((short)(O_EAng.Yaw*10))>>8;
		NRF_Send_Data[5] = ((short)(O_EAng.Yaw*10))%256;
		
		if(NRF_Send_Dat( NRF_Send_Data ) == ERROR)
		{
			printf( "NRF24L01 ERROR\r\n");
		}

	}
}

