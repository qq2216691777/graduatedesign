/**
  ***************************************
  *@FileName imu.h
  *@Auther   YNXF  
  *@date     2015-11-20
  *@Version  v1.0
  *@Action     
  ***************************************
**/
#include "mpu6050.h"
#ifndef _IMU_H__
#define _IMU_H__

void IMUupdate(float gx, float gy, float gz, float ax, float ay, float az, Angle* Out );

#endif


