/**
  ***************************************
  *@FileName pid.c
  *@Auther   YNXF  
  *@date     2015-11-23
  *@Version  v1.0
  *@Action      
  ***************************************
**/
#include "pid.h"
#include "mpu6050.h"
#include "stm32f10x.h"                  // Device header
#include "tim_pwm.h"

 Angle I_Err_Out={ 0,0,0 };
 Angle I_Err_In={ 0,0,0 };

 Angle PID_Out={ 0,0,0 };

 Angle Old_Out={ 0,0,0 };
 Angle Old_In={ 0,0,0 };

 Angle PID_Finish={ 0,0,0 };
 
 
 /******************** PID参数设定 *******/
#define OUT_P_KP 0.00		 
#define OUT_P_KI 0.0	    
#define OUT_P_KD 0.0	    

#define OUT_R_KP 0.027		//0.022
#define OUT_R_KI 0.00026	//0.0002
#define OUT_R_KD 0.40		//0.30

#define OUT_Y_KP 0
#define OUT_Y_KI 0
#define OUT_Y_KD 0

#define IN_P_KP 0.00		 
#define IN_P_KI 0.0			 
#define IN_P_KD 0.0		 

#define IN_R_KP 85.00		//85.0
#define IN_R_KI 5.5			//6.0
#define IN_R_KD 90.0		//100.0

#define IN_Y_KP 0
#define IN_Y_KD 0
/****************************************/
#define OUT_ROLL_I 200
#define OUT_PITCH_I 200
#define IN_ROLL_I 400
#define IN_PITCH_I 400

//  求float型数据绝对值
float FL_ABS(float x)
{
   if(x < 0.00001)  return -x;
	 else return x; 
}

void PID_Control( float R, float P, float Y, float G_x, float G_y, float G_z, int YM )
{
	int Motor1;
	int Motor2;
	int Motor3;
	int Motor4;
	
	Angle Out_Dev={ 0,0,0 };  //偏差 外环
	Angle In_Dev={ 0,0,0 };  //偏差 内环
	
	Out_Dev.Pitch = 0-P;
	Out_Dev.Roll = 0-R;
	
	if( YM>1000 ) YM=990;		//油门限幅
	
	if( FL_ABS(P)>= 30 || FL_ABS(R )>= 30 )	//如果倾角大于50°  放弃控制  停机
	{
		TIM_Cmd(TIM2, DISABLE);

		Motor(0,0,0,0);//停机
		while(1);
	}
	
	if( FL_ABS(P)<= 2 && FL_ABS(R )<= 2 ) //当倾角小于2° 认为平衡
	{
		return;
	}
/*******	外环PID 	*****/	
	if( YM > 20 )
	{
		I_Err_Out.Pitch += Out_Dev.Pitch;
		I_Err_Out.Roll += Out_Dev.Roll;
	}
	else
	{
		I_Err_Out.Pitch = 0;
		I_Err_Out.Roll = 0;
	}
	
	if( I_Err_Out.Pitch > OUT_PITCH_I )
		I_Err_Out.Pitch=OUT_PITCH_I;
	else if( I_Err_Out.Pitch < -OUT_PITCH_I ) 
		I_Err_Out.Pitch = -OUT_PITCH_I;
	
	if( I_Err_Out.Roll > OUT_ROLL_I)
		I_Err_Out.Roll=OUT_ROLL_I;
	else if( I_Err_Out.Roll < -OUT_ROLL_I ) 
		I_Err_Out.Roll = -OUT_ROLL_I;
	
	PID_Out.Pitch = Out_Dev.Pitch*OUT_P_KP + I_Err_Out.Pitch*OUT_P_KI + OUT_P_KD*( Out_Dev.Pitch-Old_Out.Pitch );
	PID_Out.Roll = Out_Dev.Roll*OUT_R_KP + I_Err_Out.Roll*OUT_R_KI + OUT_R_KD*( Out_Dev.Roll-Old_Out.Roll );
	PID_Out.Yaw = Y*OUT_Y_KP;
	
	Old_Out.Pitch = Out_Dev.Pitch;
	Old_Out.Roll = Out_Dev.Roll;
	
	/***********************		内环PID  *************************/
	In_Dev.Pitch = PID_Out.Pitch-G_y;
	In_Dev.Roll = PID_Out.Roll-G_x;
	if(YM>10) 
	{
		I_Err_In.Roll += In_Dev.Roll;
		I_Err_In.Pitch += In_Dev.Pitch ;
	}
	else
	{
		I_Err_In.Roll =0;
		I_Err_In.Pitch =0;
	}
	
	if( I_Err_In.Roll > IN_ROLL_I) I_Err_In.Roll=IN_ROLL_I;
	else if( I_Err_In.Roll < -IN_ROLL_I) I_Err_In.Roll=-IN_ROLL_I;
	
	if( I_Err_In.Pitch > IN_PITCH_I) I_Err_In.Pitch=IN_PITCH_I;
	else if( I_Err_In.Pitch < -IN_PITCH_I) I_Err_In.Pitch=-IN_PITCH_I;
	
	
	PID_Finish.Roll = In_Dev.Roll*IN_R_KP + IN_R_KI*I_Err_In.Roll + IN_R_KD * ( In_Dev.Roll - Old_In.Roll );
	
	PID_Finish.Pitch = In_Dev.Pitch*IN_P_KP + IN_P_KI*I_Err_In.Pitch +IN_P_KD * ( In_Dev.Pitch - Old_In.Pitch );
	//PID_Finish.Yaw = PID_Out.Yaw*IN_Y_KP + IN_Y_KD * ( G_z - Old_In.Yaw );
	PID_Finish.Yaw = 0;		//YAW积分  暂时不调
	
	Old_In.Pitch = In_Dev.Pitch;
	Old_In.Roll = In_Dev.Roll;
	Old_In.Yaw = G_z;
	
	if( YM>10 ) //如果油门的数值足够大的话 开启电机 
	{
		Motor2 = YM - PID_Finish.Pitch + PID_Finish.Roll;	
		Motor1 = YM - PID_Finish.Pitch - PID_Finish.Roll;
		
		Motor4 = YM + PID_Finish.Pitch + PID_Finish.Roll;			
		Motor3 = YM + PID_Finish.Pitch - PID_Finish.Roll;	
		
		if(1)		//检查飞行开关是否被打开
		{
			Motor( Motor1,Motor2,Motor3,Motor4 );
		}
		else	//如果油门没有开  则关闭电机
		{
			Motor(0,0,0,0);
		}
	}
	else	//如果油门没有开  则关闭电机
	{
		Motor(0,0,0,0);
	}
	
}


