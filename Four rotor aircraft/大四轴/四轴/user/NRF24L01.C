/**
  ***************************************
  *@FileName NRF24L01.c
  *@Auther   YNXF  
  *@date     2015-11-20
  *@Version  v1.0
  *@Action   Four rotor aircraft   
  ***************************************
**/
#include "nrf24l01.h"
#include "stm32f10x.h"                  // Device header
#include "sys.h"
#include "delay.h"

u8  TX_ADDRESS[TX_ADR_WIDTH]= {0xE1,0xE2,0xE3,0xE4,0xE5};	//本地地址
u8  RX_ADDRESS[RX_ADR_WIDTH]= {0xE1,0xE2,0xE3,0xE4,0xE5};	//接收地址

void NRF24L01_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	SPI_InitTypeDef SPI_InitStructure;
	
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB , ENABLE );
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_SPI1, ENABLE );
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init( GPIOA, &GPIO_InitStructure );				   			//初始化gpiob
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;		  		//CE
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  		//推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init( GPIOA, &GPIO_InitStructure );

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_Init( GPIOA, &GPIO_InitStructure );

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;		 		//IRQ
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU ;  		//上拉输入
	GPIO_Init( GPIOB, &GPIO_InitStructure ); 
	
	NRF_CSN = 1;

	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;	 //设置SPI单向或者双向的数据模式:SPI设置为双线双向全双工
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;						 //设置SPI工作模式:设置为主SPI
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;					 //设置SPI的数据大小:SPI发送接收8位帧结构
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;							 //串行同步时钟的空闲状态为低电平
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;						 //串行同步时钟的第一个跳变沿（上升或下降）数据被采样
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;							 //NSS信号由硬件（NSS管脚）还是软件（使用SSI位）管理:内部NSS信号有SSI位控制
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;   //定义波特率预分频的值:波特率预分频值为8
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;					 //指定数据传输从MSB位还是LSB位开始:数据传输从MSB位开始
	SPI_InitStructure.SPI_CRCPolynomial = 7;							 //CRC值计算的多项式
	SPI_Init( SPI1, &SPI_InitStructure );								 //根据SPI_InitStruct中指定的参数初始化外设SPIx寄存器
	SPI_Cmd( SPI1, ENABLE );
	
}
/*

*/
unsigned char NRF_Check( void )
{
	unsigned char buf1[5];
	unsigned char i;	
	
	SPI_NRF_WriteBuf( NRF_WRITE_REG+TX_ADDR, TX_ADDRESS, 5 );	 /*写入5个字节的地址.  */ 	
	SPI_NRF_ReadBuf( TX_ADDR,buf1, 5);					 /*读出写入的地址 */
   	for( i=0; i<5; i++ )								 /*比较*/  
	{
		if( buf1[i] != TX_ADDRESS[i] )
		break;
	}
	if( i==5 )
		return 1 ;        //MCU与NRF成功连接 
	else
		return 0 ;        //MCU与NRF不正常连接  
}


unsigned char SPI_NRF_WriteBuf( unsigned char reg, unsigned char *pBuf, unsigned char bytes)
{
	u8 status,byte_cnt;
	NRF_CE = 0;										//CE置底
	NRF_CSN = 0;									//CSN置底
	status = SPI_NRF_RW( reg );						/*发送寄存器号*/
	for( byte_cnt=0; byte_cnt<bytes; byte_cnt++ )
	{
		SPI_NRF_RW( *pBuf++ );						//写数据到缓冲区
	}
	NRF_CSN = 1;									//CSN置高
	return( status );
}

unsigned char SPI_NRF_ReadBuf( unsigned char reg, unsigned char *pBuf, unsigned char bytes )
{
 	u8 status, byte_cnt;
	NRF_CE = 0;											//CE置底
	NRF_CSN = 0;										//CSN置底			
	status = SPI_NRF_RW( reg ); 						/*发送寄存器号*/	
	 for(byte_cnt=0; byte_cnt<bytes; byte_cnt++)		/*读取缓冲区数据*/
	 {		  
	     pBuf[byte_cnt] = SPI_NRF_RW(NOP);          //从NRF24L01读取数据
	 }  
	NRF_CSN = 1;									//CSN置高			
 	return status;									//返回寄存器状态值
}

unsigned char SPI_NRF_RW( unsigned char dat )
{  	  
  while ( SPI_I2S_GetFlagStatus( SPI1, SPI_I2S_FLAG_TXE ) == RESET ); /* 当 SPI发送缓冲器非空时等待 */   
  SPI_I2S_SendData( SPI1, dat );									  /* 通过 SPI2发送一字节数据 */ 
  while ( SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET );  /* 当SPI接收缓冲器为空时等待 */
  return SPI_I2S_ReceiveData( SPI1 );								   /* Return the byte read from the SPI bus */
}

unsigned char SPI_NRF_WriteReg( unsigned char reg, unsigned char dat )
{
 	unsigned char status;
	NRF_CE = 0;										//CE置底
    NRF_CSN = 0;									//CSN置底	
	status = SPI_NRF_RW(reg);						/*发送命令及寄存器号 */	 
    SPI_NRF_RW(dat); 								/*向寄存器写入数据*/   
  	NRF_CSN = 1;			    					//CSN置高	
   	return(status);
}

unsigned char SPI_NRF_ReadReg( unsigned char reg )
{
 	unsigned char reg_val;
	NRF_CE = 0;										//CE置底
    NRF_CSN = 0;									//CSN置底	 
	SPI_NRF_RW(reg); 								/*发送寄存器号*/
	reg_val = SPI_NRF_RW(NOP);						/*读取寄存器的值 */  	
	NRF_CSN = 1;					 /*CSN拉高，完成*/   	
	return reg_val;
}

void NRF_RT_Mode( unsigned char Temp )
{
   NRF_CE = 0;												//CE置底	
   SPI_NRF_WriteBuf( NRF_WRITE_REG+RX_ADDR_P0, RX_ADDRESS, RX_ADR_WIDTH );//写RX节点地址
   SPI_NRF_WriteBuf( NRF_WRITE_REG+TX_ADDR, TX_ADDRESS, TX_ADR_WIDTH );//写TX节点地址
   SPI_NRF_WriteReg( NRF_WRITE_REG+EN_AA, 0x01 );    		//使能通道0的自动应答    
   SPI_NRF_WriteReg( NRF_WRITE_REG+EN_RXADDR, 0x01 );		//使能通道0的接收地址    
   SPI_NRF_WriteReg( NRF_WRITE_REG+SETUP_RETR,0x0a );		//设置自动重发间隔时间:250us;最大自动重发次数:10次 
   SPI_NRF_WriteReg( NRF_WRITE_REG+RF_CH, CHANAL );      	//设置RF通信频率    
   SPI_NRF_WriteReg( NRF_WRITE_REG+RX_PW_P0, RX_PLOAD_WIDTH );//选择通道0的有效数据宽度      
   SPI_NRF_WriteReg( NRF_WRITE_REG+RF_SETUP, 0x0f ); 		//设置TX发射参数,0db增益,2Mbps,低噪声增益开启   
	
   if( Temp )
   {
	   SPI_NRF_WriteReg( NRF_WRITE_REG+NRF_CONFIG, 0x0e );
   }
   else
   {
	   SPI_NRF_WriteReg( NRF_WRITE_REG+NRF_CONFIG, 0x0f );  		//配置基本工作模式的参数;PWR_UP,EN_CRC,16BIT_CRC,接收模式 
   }
} 

void NRF_Tx_Dat( unsigned char *txbuf )
{
	NRF_CE = 0;											//CE置底		
	SPI_NRF_WriteBuf(NRF_WRITE_REG + RX_ADDR_P0, TX_ADDRESS, TX_ADR_WIDTH); // 装载接收端地址	
    SPI_NRF_WriteBuf( WR_TX_PLOAD, txbuf, TX_PLOAD_WIDTH );   
 	NRF_CE = 1;											//CE置高                           
}


unsigned char NRF_Rece_Dat( unsigned char* R_data )
{
	unsigned char NRF_status;
	unsigned char RX_Len;
	
	NRF_status = SPI_NRF_ReadReg( NRF_READ_REG + NRF_STATUS );
	if( NRF_status & RX_DR )
	{
		RX_Len = SPI_NRF_ReadReg( 0x60 );
		if( RX_Len == TX_PLOAD_WIDTH )
		{
			SPI_NRF_ReadBuf( RD_RX_PLOAD, R_data, TX_PLOAD_WIDTH );
			return 1;
		}
		else
		{
			SPI_NRF_WriteReg( FLUSH_RX, 0xff );
		}
	}
	return 0;
} 


unsigned char NRF_Send_Dat( unsigned char* S_data )
{
	unsigned char state;
	NRF_CE = 0;											//CE置底						
    SPI_NRF_WriteBuf( WR_TX_PLOAD, S_data, TX_PLOAD_WIDTH );   
 	NRF_CE = 1;
	while( NRF_IRQ != 0 );
	state = SPI_NRF_ReadReg( NRF_STATUS );			   		/*读取状态寄存器的值 */	                   
	SPI_NRF_WriteReg( NRF_WRITE_REG+NRF_STATUS, state); 	/*清除TX_DS或MAX_RT中断标志*/	
	SPI_NRF_WriteReg( FLUSH_TX, NOP );    					//清除TX FIFO寄存器  
	if( state&MAX_RT )                     				    //达到最大重发次数
			 return MAX_RT; 
	else if( state&TX_DS )                  				//发送完成
		 	return TX_DS;
	else						  
			return ERROR;                 				//其他原因发送失败
}




