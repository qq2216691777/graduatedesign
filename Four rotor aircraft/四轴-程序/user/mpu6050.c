/**
  ***************************************
  *@Project  Air mouse
  *@FileName mpu6050.c
  *@Auther   YNXF  
  *@date     2015-11-4
  *@Version  v1.0
  *@Action   for stm32f103  
  ***************************************
**/
#include "mpu6050.h"
#include "iic_sim.h"
#include "usart.h"

void MPU6050_Init( void )
{
	unsigned char temp;
	IIC_SIM_Init();
	temp = MPU6050_Read_OneReg( WHO_AM_I );
	while( temp != 0x68 )
	{
		printf("MPU6050 Init Failed! Retrying\r\n");
		IIC_SIM_Init();
		temp = MPU6050_Read_OneReg( WHO_AM_I );
	}
	printf("MPU6050  Init Successed!\r\n");
	
	MPU6050_Write_OneReg( PWR_MGMT_1, 0x00 );
	
	MPU6050_Write_OneReg( SMPLRT_DIV, 0x07 );
	MPU6050_Write_OneReg( CONFIG, 0x06 );
	MPU6050_Write_OneReg( GYRO_CONFIG, 0x18 );
	MPU6050_Write_OneReg( ACCEL_CONFIG, 0x01 );
	
}

static void MPU6050_Write_OneReg( unsigned char RegAddr, unsigned char Value )
{
	IIC_SIM_Start();
	IIC_SIM_Write_Byte( SlaveAddress );
	IIC_SIM_Write_Byte( RegAddr );
	IIC_SIM_Write_Byte( Value );
	IIC_SIM_Stop();
}

static unsigned char MPU6050_Read_OneReg( unsigned char RegAddr )
{
	unsigned char Get_data;
	IIC_SIM_Start();
	IIC_SIM_Write_Byte( SlaveAddress );
	IIC_SIM_Write_Byte( RegAddr );
	
	IIC_SIM_Start();
	IIC_SIM_Write_Byte( SlaveAddress+1 );
	Get_data = IIC_SIM_Read_Byte();
	IIC_SIM_Send_Ack( 1 );
	IIC_SIM_Stop();
	return Get_data;
}

static void MPU6050_All_Data( unsigned char *pData )
{
	unsigned char i;
	IIC_SIM_Start();
	IIC_SIM_Write_Byte( SlaveAddress );
	IIC_SIM_Write_Byte( ACCEL_XOUT_H );
	
	IIC_SIM_Start();
	IIC_SIM_Write_Byte( SlaveAddress+1 );
	
	for( i=0; i<14; i++ )
	{
		pData[i] = IIC_SIM_Read_Byte();
		if( i==13 )
			IIC_SIM_Send_Ack( 1 );
		else
			IIC_SIM_Send_Ack( 0 );
	}
	IIC_SIM_Stop();
}

void Get_Acc_Ang_Data( Angle* Acc, Angle* Ang )				//����һ��  240us
{
	unsigned char Pri_data[14];
	static unsigned char err_temp = 0;
	
	MPU6050_All_Data( Pri_data ); 
	Acc->Roll = ((short)((( (short)Pri_data[0] << 8) ) | Pri_data[1] ))/16384.0 ;
	Acc->Pitch = ((short)((( (short)Pri_data[2] << 8) ) | Pri_data[3] ))/16384.0 ;
	Acc->Yaw = ((short)((( (short)Pri_data[4] << 8) ) | Pri_data[5] ))/16384.0 ;
	
	Ang->Roll = ((short)((( (short)Pri_data[8] << 8) ) | Pri_data[9] ))* 0.001065;
	Ang->Pitch = ((short)((( (short)Pri_data[10] << 8) ) | Pri_data[11] )) * 0.001065;
	Ang->Yaw = ((short)((( (short)Pri_data[12] << 8) ) | Pri_data[13] )) * 0.001065;
	
//	if( !( Acc->Roll || Acc->Pitch || Acc->Yaw) )
//		err_temp++;
//	else
//		err_temp = 0;
//	
//	while( err_temp>4 );
	
}



