/**
  ***************************************
  *@Project  Air mouse
  *@FileName iic_sim.c
  *@Auther   YNXF  
  *@date     2015-11-4
  *@Version  v1.0
  *@Action   for stm32f103  
  ***************************************
**/

#include "iic_sim.h"
#include "stm32f10x.h"                  // Device header
#include "sys.h"

void IIC_SIM_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB, ENABLE );
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOB, &GPIO_InitStructure );
	
	
	IIC_SCL = 1;
	IIC_SDA = 1;
	
}

void IIC_SIM_Start( void )
{
	SDA_OUT();
	IIC_SDA = 1;
	IIC_SCL = 1;
	
	IIC_SIM_Delay( 18 );
	IIC_SDA = 0;
	IIC_SIM_Delay( 18 );
	IIC_SCL = 0;
}

void IIC_SIM_Write_Byte( unsigned char txd )
{
	unsigned char i;
	SDA_OUT();
	IIC_SCL = 0;
	for( i=0; i<8; i++ )
	{
		IIC_SDA = (txd&0x80)>>7;
		txd <<= 1;
		IIC_SIM_Delay( 14 );
		IIC_SCL = 1;
		IIC_SIM_Delay( 14 );
		IIC_SCL = 0;
		IIC_SIM_Delay( 14 );
	}
	while( IIC_SIM_Wait_Ack() );	
}

unsigned char IIC_SIM_Read_Byte( void )
{
	unsigned char i;
	unsigned char Receive=0;
	
	SDA_IN();
	for( i=0; i<8; i++ )
	{
		IIC_SCL = 0;
		IIC_SIM_Delay( 14 );
		IIC_SCL = 1;
		Receive <<= 1;
		if( READ_SDA )
			Receive++;
		IIC_SIM_Delay( 14 );
	}
	return Receive;
}

void IIC_SIM_Send_Ack( unsigned char Ack )
{
	IIC_SCL = 0;
	SDA_OUT();
	IIC_SDA = Ack;
	IIC_SIM_Delay( 16 );
	IIC_SCL = 1;
	IIC_SIM_Delay( 16 );
	IIC_SCL = 0;
}

unsigned char IIC_SIM_Wait_Ack( void )
{
	unsigned char ERRtime;
	SDA_IN();
	IIC_SDA = 1;
	IIC_SIM_Delay( 16 );
	IIC_SCL = 1;
	IIC_SIM_Delay( 16 );
	ERRtime = 0;
	while( READ_SDA )
	{
		ERRtime++;
		if( ERRtime > 250 )
		{
			IIC_SIM_Stop();
			return 1;
		}
	}
	IIC_SCL = 0;
	return 0;
}
void IIC_SIM_Stop( void )
{
	SDA_OUT();
	IIC_SDA = 0;
	IIC_SIM_Delay( 12 );
	IIC_SCL = 1;
	IIC_SIM_Delay( 12 );
	IIC_SDA = 1;
	IIC_SIM_Delay( 12 );
}

void IIC_SIM_Delay( unsigned char d )
{
	for( ; d<1; d--);
}


