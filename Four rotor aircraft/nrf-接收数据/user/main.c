/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
  ***************************************
  *@FileName main.c
  *@Auther   YNXF  
  *@date     2015-10-29
  *@Version  v1.0
  *@Action   Racing_Robot   
  ***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "stm32f10x_conf.h"
#include "nrf24l01.h"

void Serial_send( float x, float y, float z  )
{
	u8 sum;
	u8 i;
	u8 Buf[33];
	
	Buf[0]=0x88;
	Buf[1]=0xaf;
	Buf[2]=0x1c;
	Buf[3]=0;//(short)(Acc_X)>>8;			
	Buf[4]=0;//(short)(Acc_X)%256;//Serial 1原始数据
	Buf[5]=0;//(short)(Acc_Y)>>8;			
	Buf[6]=0;//(short)(Acc_Y)%256;//Serial 2
	Buf[7]=0;//(short)(Acc_Z)>>8;//(short)(Angle_Z_Temp)>>8;
	Buf[8]=0;//(short)(Acc_Z)%256;//(short)(Angle_Z_Temp)%256;//Serial 3
	
	Buf[9]=0;//(short)Groy_X>>8;
	Buf[10]=0;//(short)Groy_X%256;
	Buf[11]=0;//(short)Groy_Y>>8;			//Serial 5	H		
	Buf[12]=0;//(short)Groy_Y%256;
	Buf[13]=0;//(short)Groy_Z>>8;			//Serial 6	H	
	Buf[14]=0;//(short)Groy_Z%256;
	
	Buf[15]=0;		//Serial 7	H	
	Buf[16]=0x0;
	Buf[17]=0x0;	//Serial 8	H	
	Buf[18]=0x0;
	Buf[19]=0x0;		//Serial 9	H	
	Buf[20]=0x0;
	Buf[21]=((short)( x ))>>8;		//Serial 10	H		
	Buf[22]=((short)( x ))%256;
	Buf[23]=((short)( y ))>>8;		//Serial 11	H	
	Buf[24]=((short)( y ))%256;
	Buf[25]=((short)( z ))>>8;
	Buf[26]=((short)( z ))%256;
	Buf[27]=0x0;
	Buf[28]=0x0;
	Buf[29]=0x0;
	Buf[30]=0x0;	
	sum = 0; 
	for(i=0;i<31;i++)
			sum += Buf[i];
	Buf[31]=sum;	
	for(i=0;i<32;i++)
	{

		USART_SendData(USART1,Buf[i]);//向串口1发送数据
		 while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);
	}

}

int main( void )
{
	unsigned char rx_d[TX_PLOAD_WIDTH];
	
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	
	NRF24L01_Init();
	
	if( NRF_Check() )
		printf("NRF24L01 Init Successed!\r\n");
	else
		printf("NRF24L01 Init Failed!\r\n");
	
	NRF_RT_Mode( 0 ); //接收模式	
	Serial_send( 0,0,0 );
	while(1)
	{
		
		if( NRF_Rece_Dat( rx_d ) )
		{
		//	printf("OK\r\n");
			Serial_send( rx_d[0]*256+rx_d[1],rx_d[2]*256+rx_d[3],rx_d[4]*256+rx_d[5] );
		}

	}
}

