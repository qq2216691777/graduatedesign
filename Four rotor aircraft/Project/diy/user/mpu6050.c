#include "mpu6050.h"
#include "imu.h"

#define A_Y (0)

#define Q_X (-27)
#define Q_Y (-41)
#define Q_Z (48)

#define KALMAN_Q        0.20
#define KALMAN_R        0.80

float g_xx;
float g_yy;
float g_zz;


void MPU6050_Init( void )
{
	u8 i;
	IIC_SIM_GPIO_Init();

	i = MPU6050_ReadOneReg( WHO_AM_I );
	while( i != 0x68 )
	{
		printf("MPU6055 error !\r\n");
		i = MPU6050_ReadOneReg( WHO_AM_I );
	}
	printf("                      MPU6055 SUCESS! !\r\n");

	MPU6050_WriteOneData( PWR_MGMT_1, 0x00);		//解除休眠状态
	MPU6050_WriteOneData( SMPLRT_DIV, 0x07);
	MPU6050_WriteOneData( CONFIG, 0x06);
	MPU6050_WriteOneData( GYRO_CONFIG, 0x18 );		//陀螺仪量程 +- 2000
	MPU6050_WriteOneData( ACCEL_CONFIG, 0x01 );		//加速度量程 +- 2g
}
void MPU6050_WriteOneData( u8 RegAddr, u8 W_Data )
{
	IIC_Sim_Start();
	IIC_Send_Byte(SlaveAddress);	    //发送写命令
	IIC_Send_Byte(RegAddr);	        //内部寄存器地址
	IIC_Send_Byte(W_Data);	        //内部寄存器数据
	IIC_Stop();						//发送停止信号
}

short Get_MPU6050_Data( u8 RegAddr )
{
	u8 H,L;
	H = MPU6050_ReadOneReg( RegAddr );
	L = MPU6050_ReadOneReg( RegAddr+1 );
	return (H<<8)+L;	 //合成数据
}

u8 MPU6050_ReadOneReg( u8 RegAddr )
{
	u8 Reg_Data;
	IIC_Sim_Start();					//起始信号
	IIC_Send_Byte(SlaveAddress);	    //发送设备地址+写信号

	IIC_Send_Byte(RegAddr);	    		//发送存储单元地址，从0开始
	IIC_Sim_Start();
	IIC_Send_Byte(SlaveAddress+1);	    //发送设备地址+读信号
	Reg_Data = IIC_Read_Byte();			//读出寄存器数据
	IIC_Send_ACK(1);					//发送应答信号
	IIC_Stop();							//发送停止信号
	return Reg_Data;
}

/*****************************读取角度********************************/
void MPU6050_ReadData( void  )
{
	float Groy_x,Groy_y,Groy_z;
	float Speed_X,Speed_Y,Speed_Z;				//角速度
	float Y_Angle_X,Y_Angle_Y,Y_Angle_Z;				//角  度
	float I_Angle_X,I_Angle_Y,I_Angle_Z;				//角  度
	float Angle_X,Angle_Y,Angle_Z;				//角  度
	u8 Buf[33];
	u8 i;
	u32 sum;
	Speed_X = ( Get_MPU6050_Data( GYRO_XOUT_H ) + Q_X );
	Speed_Y = ( Get_MPU6050_Data( GYRO_YOUT_H ) + Q_Y );
	Speed_Z = ( Get_MPU6050_Data( GYRO_ZOUT_H ) + Q_Z );

	Y_Angle_X = Get_MPU6050_Data( ACCEL_XOUT_H ) ;		//加速度
	Y_Angle_Y = Get_MPU6050_Data( ACCEL_YOUT_H ) + A_Y;
	Y_Angle_Z = Get_MPU6050_Data( ACCEL_ZOUT_H ) ;
	
	if( !(Speed_X || Speed_Y || Speed_Z) )
	{
		Motor(0,0,0,0);
		while(1);
		return ;
	}
	
	Groy_x = Speed_X*0.001065f;
	Groy_y = Speed_Y*0.001065f;
	Groy_z = Speed_Z*0.001065f;
	
	I_Angle_X = KalmanFilter_x( Y_Angle_X ,KALMAN_Q, KALMAN_R );
	I_Angle_Y = KalmanFilter_y( Y_Angle_Y ,KALMAN_Q, KALMAN_R );
	I_Angle_Z = KalmanFilter_z( Y_Angle_Z ,KALMAN_Q, KALMAN_R );
	
//	I_Angle_X = Y_Angle_X;
//	I_Angle_Y = Y_Angle_Y;
//	I_Angle_Z = Y_Angle_Z;
//	
	Angle_X = I_Angle_X/16384;
	Angle_Y = I_Angle_Y/16384;
	Angle_Z = I_Angle_Z/16384;
	
	BS004_IMU_Update( Angle_X, Angle_Y, Angle_Z,Groy_x,Groy_y,Groy_z);

	PID_Control( Roll, Pitch, Yaw, Groy_x,Groy_y,Groy_z);	
	
	
	g_xx = atan(Y_Angle_X/sqrt(Y_Angle_Y*Y_Angle_Y+Y_Angle_Z*Y_Angle_Z))*57.2957795f;
	g_yy=atan(Y_Angle_Y/sqrt(Y_Angle_X*Y_Angle_X+Y_Angle_Z*Y_Angle_Z))*57.2957795f;
	Buf[0]=0x88;
	Buf[1]=0xaf;
	Buf[2]=0x1c;
	Buf[3]=(short)(g_yy)>>8;			
	Buf[4]=(short)(g_yy)%256;//Serial 1原始数据
	Buf[5]=(short)(Speed_Z)>>8;			
	Buf[6]=(short)(Speed_Z)%256;//Serial 2
	Buf[7]=0;//(short)(Angle_Z_Temp)>>8;
	Buf[8]=0;//(short)(Angle_Z_Temp)%256;//Serial 3
	Buf[9]=0x0;
	Buf[10]=0x0;
	Buf[11]=0;			//Serial 5	H		
	Buf[12]=0;
	Buf[13]=0;			//Serial 6	H	
	Buf[14]=0x0;
	Buf[15]=0;		//Serial 7	H	
	Buf[16]=0x0;
	Buf[17]=0x0;	//Serial 8	H	
	Buf[18]=0x0;
	Buf[19]=0x0;		//Serial 9	H	
	Buf[20]=0x0;
	Buf[21]=((short)(Roll*100))>>8;		//Serial 10	H		
	Buf[22]=((short)(Roll*100))%256;
	Buf[23]=((short)(Pitch*100))>>8;		//Serial 11	H	
	Buf[24]=((short)(Pitch*100))%256;
	Buf[25]=((short)(Yaw*10))>>8;
	Buf[26]=((short)(Yaw*10))%256;
	Buf[27]=0x0;
	Buf[28]=0x0;
	Buf[29]=0x0;
	Buf[30]=0x0;	
	sum = 0; 
	for(i=0;i<31;i++)
			sum += Buf[i];
	Buf[31]=sum;	
	for(i=0;i<32;i++)
	{

		USART_SendData(USART1,Buf[i]);//向串口1发送数据
		 while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);
	}

	
}




/******************************* ********************/
static void IIC_SIM_GPIO_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(	RCC_APB2Periph_GPIOB, ENABLE );

//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;   //推挽输出
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(GPIOC, &GPIO_InitStructure);
//	GPIO_SetBits(GPIOC,GPIO_Pin_13); 

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;   //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	GPIO_SetBits(GPIOB,GPIO_Pin_6 | GPIO_Pin_7); 
	
	IIC_SCL=1;
	IIC_SDA=1;
}

static void IIC_Sim_Start(void)		 //I2C起始信号
{
	SDA_OUT();     //sda线输出
	IIC_SDA=1;
	IIC_SCL=1;	  	  
	
	delay_us(1);
 	IIC_SDA=0;//START:when CLK is high,DATA change form high to low 
	delay_us(1);
	IIC_SCL=0;//钳住I2C总线，准备发送或接收数据 
}

static void IIC_Send_Byte(u8 txd)			//向I2C总线发送一个字节数据
{                        
  u8 t,temp;   
	SDA_OUT(); 	    
    for(t=0;t<8;t++)
    {   
			temp = (txd&0x80)>>7;           
					IIC_SDA=temp;
					txd<<=1; 	  
			delay_us(1);   
			IIC_SCL=1;
			delay_us(1); 
			IIC_SCL=0;	
			delay_us(1);
    }
	IIC_Wait_Ack();	 
}

static u8 IIC_Read_Byte(void)			   //从I2C总线接收一个字节数据
{
	u8 i,receive=0;
	SDA_OUT();
	IIC_SDA = 1;
	SDA_IN();//SDA设置为输入
    for(i=0;i<8;i++ )
	{     
		IIC_SCL=1;
		delay_us(1);
        receive<<=1;
        if(READ_SDA)receive++;   
		IIC_SCL=0;
		delay_us(1); 
    }					    
    return receive;
}

static u8 IIC_Wait_Ack(void)		   //I2C接收应答信号
{
	u8 ucErrTime=0;
	SDA_IN();      //SDA设置为输入  	   
	IIC_SCL=1;
	delay_us(1);	 
	while(READ_SDA)
	{
		ucErrTime++;
		if(ucErrTime>250)
		{
			IIC_Stop();
			return 1;
		}
	}
	IIC_SCL=0;//时钟输出0 	   
	return 0;  
}

void IIC_Send_ACK( u8 Ack)		   //I2C发送应答信号
{

	IIC_SDA=Ack;
	delay_us(2);
	IIC_SCL=1;
	delay_us(5);
	IIC_SCL=0;
	delay_us(2);	
}

static void IIC_Stop(void)			//I2C停止信号
{
	SDA_OUT();//sda线输出
	IIC_SDA=0;//STOP:when CLK is high DATA change form low to high
 	delay_us(4);
	IIC_SCL=1;
	delay_us(5); 
	IIC_SDA=1;//发送I2C总线结束信号
	delay_us(5);							   	
}

/*	
	Q:????,Q??,??????,???????
	R:????,R??,??????,???????	
*/

static double KalmanFilter_x(const double ResrcData,double ProcessNiose_Q,double MeasureNoise_R)
{
   double R = MeasureNoise_R;
   double Q = ProcessNiose_Q;
   static double x_last;
   double x_mid = x_last;
   double x_now;
   static double p_last;
   double p_mid ;
   double p_now;
   double kg;        

   x_mid=x_last; //x_last=x(k-1|k-1),x_mid=x(k|k-1)
   p_mid=p_last+Q; //p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=??
   kg=p_mid/(p_mid+R); //kg?kalman filter,R???
   x_now=x_mid+kg*(ResrcData-x_mid);//???????
                
   p_now=(1-kg)*p_mid;//??????covariance       
   p_last = p_now; //??covariance?
   x_last = x_now; //???????
   return x_now;                
 }
static double KalmanFilter_y(const double ResrcData,double ProcessNiose_Q,double MeasureNoise_R)
{
   double R = MeasureNoise_R;
   double Q = ProcessNiose_Q;
   static double y_last;
   double y_mid = y_last;
   double y_now;
   static double p_last;
   double p_mid ;
   double p_now;
   double kg;        

   y_mid=y_last; //x_last=x(k-1|k-1),x_mid=x(k|k-1)
   p_mid=p_last+Q; //p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=??
   kg=p_mid/(p_mid+R); //kg?kalman filter,R???
   y_now=y_mid+kg*(ResrcData-y_mid);//???????
                
   p_now=(1-kg)*p_mid;//??????covariance       
   p_last = p_now; //??covariance?
   y_last = y_now; //???????
   return y_now;                
 }
static double KalmanFilter_z(const double ResrcData,double ProcessNiose_Q,double MeasureNoise_R)
{
   double R = MeasureNoise_R;
   double Q = ProcessNiose_Q;
   static double z_last;
   double z_mid = z_last;
   double z_now;
   static double p_last;
   double p_mid ;
   double p_now;
   double kg;        

   z_mid=z_last; //x_last=x(k-1|k-1),x_mid=x(k|k-1)
   p_mid=p_last+Q; //p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=??
   kg=p_mid/(p_mid+R); //kg?kalman filter,R???
   z_now=z_mid+kg*(ResrcData-z_mid);//???????
                
   p_now=(1-kg)*p_mid;//??????covariance       
   p_last = p_now; //??covariance?
   z_last = z_now; //???????
   return z_now;                
 }


