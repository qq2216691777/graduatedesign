#ifndef _SI_PID__H___
#define _SI_PID__H___

#include "stm32f10x.h"
#include "motor_pwm.h"
#include "imu.h"


	//-----PID积分最大值-----//
#define PITCH_I_MAX	600
#define ROLL_I_MAX	600

	//外环PID参数
#define OUT_Roll_KP  0
#define OUT_Roll_KI  0

	//内环PID参数
#define In_Roll_KP  2
#define In_Roll_KI  0
#define In_Roll_KD  0


	//外环PID参数
#define OUT_Pitch_KP  0
#define OUT_Pitch_KI  0

	//内环PID参数
#define In_Pitch_KP  1
#define In_Pitch_KI  0
#define In_Pitch_KD  0




void PID_Control( float Err_Roll, float Err_Pitch, float Err_Yaw, float gx, float gy, float gz );


#endif

