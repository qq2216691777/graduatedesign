/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
	***************************************
			@作者     叶念西风 
			@作用     工程模板   
			@日期     2015—6-23
			@备注   
	***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "stm32f10x_conf.h"
#include "motor_pwm.h"
#include "mpu6050.h"
#include "nrf24l01_spi.h"

float Roll;
float Pitch;
float Yaw;
int Tim3_temp;

u8 NRF_Status;
u8 NRF_Rxbuf[RX_PLOAD_WIDTH];
u8 NRF_Txbuf[TX_PLOAD_WIDTH];
u8 NRF_Data_num;

int YM;//油门

void LED_Init( void );

int main( void )
{
	u8 ii;
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );
	MPU6050_Init();
//	NRF24l01_SPI_Init();
//	NRF_Status = NRF_Check();
//	if( NRF_Status )
//		printf("NRF与MCU连接成功！\r\n");
//	else
//		printf("NRF与MCU连接失败，请重新检查接线。\r\n");
//	
//	LED_Init();
	Motor_Init();					//电机PWM初始化
//	PDout(13)=0;
	while(1)
	{
//		PDout(13) = ii;
//			ii = ~ii;
//		MPU6050_ReadData();//5ms计算一次姿态	


			NRF_RX_Mode();
			/*等待接收数据*/
			NRF_Status = NRF_Rx_Dat(NRF_Rxbuf);
			if(NRF_Status == RX_DR)
			{
				Tim3_temp=0;
				if( (NRF_Rxbuf[0]==0xf0)&& (NRF_Rxbuf[15]==0x0e))
				{
					
					if(NRF_Rxbuf[1]==0x01)
					{
						YM = 250;
					}
				}
			}
			else
			{
				Tim3_temp++;
			}
			
			if( Tim3_temp > 20)		//如果长时间接受不到遥控器发来的信息  则代表失联 关闭电机
			{
				YM = 0;
			}
		
		

		
	}
}

void LED_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOD , ENABLE );
	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;		  		//CE
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  		//推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_Init( GPIOD, &GPIO_InitStructure );
}

