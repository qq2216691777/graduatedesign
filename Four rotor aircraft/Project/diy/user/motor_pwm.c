#include "motor_pwm.h"

void Motor_Init( void )
{
	TIM2_PWM_Init(1000,719);	//A3
	TIM3_PWM_Init(1000,719);	//B1  10MS 触发一次中断 
	TIM4_PWM_Init(1000,719);	//B9
	Tim1_init(1000,35);			//A11
	
	
	TIM_SetCompare4( TIM2,0 );//A3
	TIM_SetCompare4( TIM3,0 );//B1
	TIM_SetCompare4( TIM4,0 );//B9
	TIM_SetCompare4( TIM1,0 );//A11
	
}

//TIM2 PWM部分初始化 
//PWM输出初始化
//arr：自动重装值
//psc：时钟预分频数
void TIM2_PWM_Init(u16 arr,u16 psc)
{  
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);	//使能定时器2时钟
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);  //使能GPIO外设和AFIO复用功能模块时钟 
 
   //设置该引脚为复用输出功能,输出TIM5 CH2的PWM脉冲波形	GPIOB.5
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 |GPIO_Pin_3; //TIM2_CH2(A1)  TIM2_CH3(A2)  TIM2_CH4(A3)
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;  //复用推挽 GPIO_Pin_2 | GPIO_Pin_3|输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOA, &GPIO_InitStructure );//初始化GPIO A
 
   //初始化TIM
	TIM_TimeBaseStructure.TIM_Period = arr-1; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值
	TIM_TimeBaseStructure.TIM_Prescaler =psc; //设置用来作为TIMx时钟频率除数的预分频值 
	TIM_TimeBaseStructure.TIM_ClockDivision = 0; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;  //TIM向下计数模式
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位
 
 	//初始化TIM Channel1 PWM模式	 
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2; //选择定时器模式:TIM脉冲宽度调制模式2
 	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low; //输出极性:TIM输出比较极性高 LOW即SetComparex中参数为高电平时间
	TIM_OC4Init(TIM2, &TIM_OCInitStructure);  //根据T指定的参数初始化外设TIM3 OC2
	TIM_OC4PreloadConfig(TIM2, TIM_OCPreload_Enable);  //使能TIM3在CCR2上的预装载寄存器
	
	TIM_Cmd(TIM2, ENABLE);  //使能TIM2

	TIM_SetCompare4( TIM2,0 );			//A3
}

void TIM3_PWM_Init(u16 arr,u16 psc)
{  
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);	//使能定时器2时钟
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);  //使能GPIO外设和AFIO复用功能模块时钟 
 
   //设置该引脚为复用输出功能,输出TIM5 CH2的PWM脉冲波形	GPIOB.5
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1; //TIM2_CH2(A1)  TIM2_CH3(A2)  TIM2_CH4(A3)
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;  //复用推挽 GPIO_Pin_2 | GPIO_Pin_3|输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOB, &GPIO_InitStructure );//初始化GPIO A
 
   //初始化TIM
	TIM_TimeBaseStructure.TIM_Period = arr-1; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值
	TIM_TimeBaseStructure.TIM_Prescaler =psc; //设置用来作为TIMx时钟频率除数的预分频值 
	TIM_TimeBaseStructure.TIM_ClockDivision = 0; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;  //TIM向下计数模式
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure); //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位
 
 	//初始化TIM Channel1 PWM模式	 
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2; //选择定时器模式:TIM脉冲宽度调制模式2
 	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low; //输出极性:TIM输出比较极性高 LOW即SetComparex中参数为高电平时间
	TIM_OC4Init(TIM3, &TIM_OCInitStructure);  //根据T指定的参数初始化外设TIM3 OC2
	TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Enable);  //使能TIM3在CCR2上的预装载寄存器
	
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE ); //使能指定的TIM3中断,允许更新中断

	//中断优先级NVIC设置
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;  //TIM3中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;  //先占优先级0级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;  //从优先级3级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);  //初始化NVIC寄存器
	
	TIM_Cmd(TIM3, ENABLE);  //使能TIM2

	TIM_SetCompare4( TIM3,0 );			//B9
}

void TIM4_PWM_Init(u16 arr,u16 psc)
{  
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);	//使能定时器2时钟
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);  //使能GPIO外设和AFIO复用功能模块时钟 
 
   //设置该引脚为复用输出功能,输出TIM5 CH2的PWM脉冲波形	GPIOB.5
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //TIM2_CH2(A1)  TIM2_CH3(A2)  TIM2_CH4(A3)
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;  //复用推挽 GPIO_Pin_2 | GPIO_Pin_3|输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOB, &GPIO_InitStructure );//初始化GPIO A
 
   //初始化TIM
	TIM_TimeBaseStructure.TIM_Period = arr-1; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值
	TIM_TimeBaseStructure.TIM_Prescaler =psc; //设置用来作为TIMx时钟频率除数的预分频值 
	TIM_TimeBaseStructure.TIM_ClockDivision = 0; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;  //TIM向下计数模式
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure); //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位
 
 	//初始化TIM Channel1 PWM模式	 
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2; //选择定时器模式:TIM脉冲宽度调制模式2
 	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low; //输出极性:TIM输出比较极性高 LOW即SetComparex中参数为高电平时间
	TIM_OC4Init(TIM4, &TIM_OCInitStructure);  //根据T指定的参数初始化外设TIM3 OC2
	TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);  //使能TIM3在CCR2上的预装载寄存器
	
	TIM_Cmd(TIM4, ENABLE);  //使能TIM2

	TIM_SetCompare4( TIM4,0 );			//B9
}

void Tim1_init(u16 arr,u16 psc)
{
	RCC->APB2ENR|=1<<11;       //TIM1????   


//////////////////////////////////////// 
 GPIOA->CRH&=0XFFFF0FFF;//PA8 11 ??
 GPIOA->CRH|=0X0000B000;//??????    
 
////////////////////////////////////////
 TIM1->BDTR |=0xC0;   //ARPE?? 
 TIM1->ARR=arr; //TIM_Period 
 TIM1->PSC=psc; //psc
 
 TIM1->CCMR1|=6<<4;  //CH2 PWM2?? CH1/2  
 TIM1->CCMR1|=1<<3; //CH2?????

 TIM1->CCMR2|=6<<12;  //CH2 PWM2?? CH3/4   
 TIM1->CCMR2|=1<<11; //CH4?????

 

 TIM1->CCER|=1<<0;   //OC2 ????
 TIM1->CCER|=1<<12;   //OC2 ????


 TIM1->CR1 |=0x80;   //ARPE?? 
 TIM1->CR1|=0x01;    //?????1

 TIM1->BDTR|=0x8000;  //?????1?? 

 TIM_SetCompare4( TIM1,0 );	
 
}

void Motor( int M1, int M2, int M3, int M4)
{
	if( M1> 900) M1=900;
	if( M2> 900) M2=900;
	if( M3> 900) M3=900;
	if( M4> 900) M4=900;
	if( M1<1) M1=0;
	if( M2<1) M2=0;
	if( M3<1) M3=0;
	if( M4<1) M4=0;
	
	TIM_SetCompare4( TIM2,M1 );//A3   1号电机
	TIM_SetCompare4( TIM3,M2 );//B1   2号电机
	TIM_SetCompare4( TIM1,M3 );//A11  3号电机	
	TIM_SetCompare4( TIM4,M4 );//B9   4号电机

	
}



