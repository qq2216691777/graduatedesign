#ifndef _MOTOR_PWM_H__
#define _MOTOR_PWM_H__

#include "stm32f10x.h"

void TIM2_PWM_Init(u16 arr,u16 psc);
void TIM4_PWM_Init(u16 arr,u16 psc);
void TIM3_PWM_Init(u16 arr,u16 psc);
void Tim1_init(u16 arr,u16 psc);
void Motor_Init( void );
void Motor( int M1, int M2, int M3, int M4);

#endif

