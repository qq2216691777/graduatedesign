#include "pid.h"
extern u8 YM;
float Last_gx;
float Last_gy;

struct Angle Err_Out_T={0,0,0}; //误差积分项
struct Angle Err_In_T={0,0,0}; //误差积分项


void PID_Control( float Err_Roll, float Err_Pitch, float Err_Yaw, float gx, float gy, float gz )
{
	struct Angle Output;
	struct Angle Out_Pid;
	struct Angle In_Pid;
	u8 Motor1;
	u8 Motor2;
	u8 Motor3;
	u8 Motor4;
	
	//如果倾角大于50°  放弃控制  停机
	if(fabs(Err_Pitch)>= 50 || fabs(Err_Roll )>= 50)
	{
//		return ;
	}
	
	
	//当倾角小于2° 认为平衡
	if( fabs(Err_Pitch)<= 2 || fabs(Err_Roll )<= 2 )
	{
	//	return;
	}

/* 由于系统期望值的Pitch Yaw Roll  均为0   所以输入的值就是 系统误差*/
	if(YM>20)			//当油门大于一个值时 外环积分
	{
		Err_Out_T.Roll += Err_Roll;
		Err_Out_T.Pitch += Err_Pitch;
	}
	else
	{
		Err_Out_T.Roll = 0;
		Err_Out_T.Pitch = 0;
	}
	
	
	
	if( Err_Out_T.Pitch > PITCH_I_MAX )
		Err_Out_T.Pitch = PITCH_I_MAX;
	else if( Err_Out_T.Pitch < -PITCH_I_MAX )
		Err_Out_T.Pitch = -PITCH_I_MAX;
	
	if( Err_Out_T.Roll > ROLL_I_MAX )
		Err_Out_T.Roll = ROLL_I_MAX;
	else if( Err_Out_T.Roll < -ROLL_I_MAX )
		Err_Out_T.Roll = -ROLL_I_MAX;
	
	Out_Pid.Pitch = Err_Pitch*OUT_Pitch_KP + Err_Out_T.Pitch*OUT_Pitch_KI;  //外环PID
	Out_Pid.Roll = Err_Roll*OUT_Roll_KP + Err_Out_T.Roll*OUT_Roll_KI;  //外环PID
	
	/*********************内环PID积分****************************/
	if(YM>20)
	{
		In_Pid.Pitch +=( Out_Pid.Pitch - gy );
		In_Pid.Roll +=( Out_Pid.Roll - gx );
	}
	else
	{
		In_Pid.Pitch  = 0;
		In_Pid.Roll  = 0;
	}
	

	
//	if( Err_In_T.Pitch > PITCH_I_MAX )
//		Err_In_T.Pitch = PITCH_I_MAX;
//	else if( Err_In_T.Pitch < -PITCH_I_MAX )
//		Err_In_T.Pitch = -PITCH_I_MAX;
//	
//	if( Err_In_T.Pitch > PITCH_I_MAX )
//		Err_In_T.Pitch = PITCH_I_MAX;
//	else if( Err_In_T.Pitch < -PITCH_I_MAX )
//		Err_In_T.Pitch = -PITCH_I_MAX;
	
	Output.Pitch = ( Out_Pid.Pitch - gy )*In_Pitch_KP + Err_In_T.Pitch*In_Pitch_KI + (gy - Last_gy)*In_Pitch_KD;
	Output.Roll = ( Out_Pid.Roll - gx )*In_Roll_KP + Err_In_T.Roll*In_Roll_KI + (gx - Last_gx)*In_Roll_KD;
	Last_gy = gy;
	Last_gx = gx;
	Output.Roll= 0;
	if(YM>20) //如果油门的数值足够大的话 开启电机 
	{
		Motor3 = YM - Output.Pitch - Output.Roll + 0;	//Yaw
		Motor4 = YM + Output.Pitch - Output.Roll - 0;	//Yaw
		Motor1 = YM + Output.Pitch + Output.Roll + 0;	//Yaw
		Motor2 = YM - Output.Pitch + Output.Roll + 0;	//Yaw
		
		if(1)			//检查飞行开关是否被打开
		{
			Motor( Motor1,Motor2,Motor3,Motor4 );
		}
		
	}
	else		//如果油门没有开  则关闭电机
	{
		Motor(0,0,0,0);
	}
	


	

		
	
	
}


