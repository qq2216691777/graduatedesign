#include "imu.h"

static float q0=1;
static float q1=0;
static float q2=0;
static float q3=0;

static float exInt = 0;
static float eyInt = 0;
static float ezInt = 0;

#define Ki 0.001f				//越大数据越不稳定
#define Kp 1.6
#define halfT 0.005f		//调节恢复速率


void BS004_IMU_Update(float ax,float ay,float az,float gx,float gy, float gz) 
{
  float norm;
  float vx, vy, vz;// wx, wy, wz;
  float ex, ey, ez;
	
  float q0q0 = q0*q0;
  float q0q1 = q0*q1;
  float q0q2 = q0*q2;
  float q0q3 = q0*q3;//
  float q1q1 = q1*q1;
  float q1q2 = q1*q2;//
  float q1q3 = q1*q3;
  float q2q2 = q2*q2;
  float q2q3 = q2*q3;
  float q3q3 = q3*q3;
	
  if(ax*ay*az==0)
	  return;
		
  norm = sqrt(ax*ax + ay*ay + az*az);       //acc?????
  ax = ax / norm;
  ay = ay / norm;
  az = az / norm;

  // estimated direction of gravity and flux (v and w)              ?????????/??
  vx = 2*(q1q3 - q0q2);												//????xyz???
  vy = 2*(q0q1 + q2q3);
  vz = q0q0 - q1q1 - q2q2 + q3q3 ;

  // error is sum of cross product between reference direction of fields and direction measured by sensors
  ex = (ay*vz - az*vy) ;                           					 //???????????????
  ey = (az*vx - ax*vz) ;
  ez = (ax*vy - ay*vx) ;

  exInt = exInt + ex * Ki;								  //???????
  eyInt = eyInt + ey * Ki;
  ezInt = ezInt + ez * Ki;
// adjusted gyroscope measurements
//	if(fabs(ay) < 0.8 && fabs(az-1) < 0.8)
//       gx = gx + Kp*ex + exInt;					   							//???PI???????,???????
//	else
//       gx = gx; 
//	if(fabs(ax) < 0.8 && fabs(az-1) < 0.8)
//       gy = gy + Kp*ey + eyInt;				   							
//	else
//       gy = gy; 
//	if(fabs(ax)<0.8 && fabs(ay) < 0.8)
//       gz = gz + Kp*ez + ezInt;					   					//???gz????????????????,??????????????		
//	else
//       gz = gz;   

gx = gx + Kp*ex + exInt;		
gy = gy + Kp*ey + eyInt;		
 gz = gz + Kp*ez + ezInt;					
	
  // integrate quaternion rate and normalise						   //????????
  q0 = q0 + (-q1*gx - q2*gy - q3*gz)*halfT;
  q1 = q1 + (q0*gx + q2*gz - q3*gy)*halfT;
  q2 = q2 + (q0*gy - q1*gz + q3*gx)*halfT;
  q3 = q3 + (q0*gz + q1*gy - q2*gx)*halfT;

  // normalise quaternion
  norm = sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3);
  q0 = q0 / norm;
  q1 = q1 / norm;
  q2 = q2 / norm;
  q3 = q3 / norm;
//	Roll=atan2(2*q2*q3+2*q0*q1, -2*q1*q1-2*q2*q2+1) * 57.2957795f;
//  Roll = atan2(2.0*q2*q3 + 2.0*q0*q1, -2.0*q1*q1 - 2.0*q2*q2 + 1) * 57.2957795f; // roll
//  Pitch = asin(-2.0*q1*q3 + 2.0*q0*q2) * 57.2957795f; // pitch
	Yaw =atan2(2.0 * q1 * q2 + 2.0 * q0 * q3, -2 * q2*q2 - 2 * q3 * q3 + 1)* 57.2957795f; //yaw	
		Roll = asin(2*(q0*q1+q2*q3 ))* 57.2957795f;
		Pitch = asin(2*(q0*q2-q1*q3 ))* 57.2957795f;;
}
