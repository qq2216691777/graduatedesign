#ifndef _IMU__H_
#define _IMU__H_
#include <math.h>
struct Angle
{
	float Roll;
	float Pitch;
	float Yaw;
};

extern float Roll;
extern float Pitch;
extern float Yaw;

void BS004_IMU_Update(float ax,float ay,float az,float gx,float gy,float gz) ;

#endif
