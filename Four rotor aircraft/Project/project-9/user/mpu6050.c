#include "mpu6050.h"
#include "usart.h"
#include "tim_pwm.h"
#include "imu.h"
#include "pid.h"
#include "math.h"

#define KALMAN_Q        0.02
#define KALMAN_R        0.8000

void MPU6050_Init( void )
{
	u8 i;
	IIC_SIM_Init();
	i = MPU6050_ReadOneReg( WHO_AM_I );
	
	while( i != 0x68 )
	{
		printf("MPU6055 error !\r\n");
		delay_ms(800);
		i = MPU6050_ReadOneReg( WHO_AM_I );
	}
	
	
	MPU6050_WriteOneData( PWR_MGMT_1, 0x00);		//解除休眠状态
	
	MPU6050_WriteOneData( SMPLRT_DIV, 0x07);
	MPU6050_WriteOneData( CONFIG, 0x06);
	MPU6050_WriteOneData( GYRO_CONFIG, 0x18 );		//陀螺仪量程 +- 2000
	MPU6050_WriteOneData( ACCEL_CONFIG, 0x01 );		//加速度量程 +- 2g
//	MPU6050_WriteOneData( 0x37, 0x02 );
	printf("MPU6055 SUCESS! !\r\n");
}




u8 MPU6050_ReadOneReg( u8 RegAddr )
{
	u8 Reg_Data;
	IIC_Sim_Start();					//起始信号
	IIC_Send_Byte(SlaveAddress);	    //发送设备地址+写信号

	IIC_Send_Byte(RegAddr);	    		//发送存储单元地址，从0开始
	IIC_Sim_Start();
	IIC_Send_Byte(SlaveAddress+1);	    //发送设备地址+读信号
	Reg_Data = IIC_Read_Byte();			//读出寄存器数据
	IIC_Send_ACK(1);					//发送应答信号
	IIC_Stop();							//发送停止信号
	return Reg_Data;
}

short Get_MPU6050_Data( u8 RegAddr )
{
	u8 H,L;
	H = MPU6050_ReadOneReg( RegAddr );
	L = MPU6050_ReadOneReg( RegAddr+1 );
	return (H<<8)+L;	 //合成数据
}

void Get_MPU6050_All_Data( u8 *p )
{
	u8 i;
	IIC_Sim_Start();					//起始信号
	IIC_Send_Byte(SlaveAddress);	    //发送设备地址+写信号

	IIC_Send_Byte(ACCEL_XOUT_H);	    //发送存储单元地址，从0开始
	
	IIC_Sim_Start();
	IIC_Send_Byte(SlaveAddress+1);	    //发送设备地址+读信号
	
	for(i=0;i<14;i++)
	{
		p[i]=IIC_Read_Byte();			//读出寄存器数据
		
		if( i==13 ) 
			IIC_Send_ACK(1);					//发送应答信号
		else
			IIC_Send_ACK(0);
	}
	
	
	
	IIC_Stop();							//发送停止信号
}

extern float Roll,Pitch,Yaw;



void Update_MPU6050( u16 Acc )
{
	u8 Buf[33];
	u32 sum;
	u8 i;
	u8 Data_Buffer[14];
	short Acc_X;
	short Acc_Y;
	short Acc_Z;
	short Groy_X;
	short Groy_Y;
	short Groy_Z;

	float K_Acc_X;
	float K_Acc_Y;
	float K_Acc_Z;

	float K_Groy_X;
	float K_Groy_Y;
	float K_Groy_Z;
	
//	float XXX;
//	float YYY;
	
		
	Get_MPU6050_All_Data( Data_Buffer );
	
	/* 三维重力加速度 */
	Acc_X = (((short)Data_Buffer[0])<<8)|Data_Buffer[1] ;
	Acc_Y = (((short)Data_Buffer[2])<<8)|Data_Buffer[3] ;
	Acc_Z = (((short)Data_Buffer[4])<<8)|Data_Buffer[5] ;
	
	/* 三轴角速度 */
	Groy_X = (((short)Data_Buffer[8])<<8)|Data_Buffer[9];
	Groy_Y = (((short)Data_Buffer[10])<<8)|Data_Buffer[11];
	Groy_Z = (((short)Data_Buffer[12])<<8)|Data_Buffer[13];
		
	while( !(Acc_X || Acc_Y || Acc_Z) )		//6050数据出错 
	{
		TIM_Cmd(TIM2, DISABLE);  //关闭定时器中断		
		while(1);
	}
	
	
	/* 对重力加速度滤波 */
	K_Acc_X = (float)KalmanFilter_x( (Acc_X)/16384.0, KALMAN_Q, KALMAN_R );
	K_Acc_Y = (float)KalmanFilter_y( (Acc_Y)/16384.0, KALMAN_Q, KALMAN_R );
	K_Acc_Z = (float)KalmanFilter_z( (Acc_Z)/16384.0, KALMAN_Q, KALMAN_R );
	
	/*	弧度转换	*/
	K_Groy_X = (float)KalmanFilter_gx( (Groy_X+21)*Gyro_Gr, KALMAN_Q, KALMAN_R ) ;
	K_Groy_Y = (float)KalmanFilter_gy( (Groy_Y-24)*Gyro_Gr, KALMAN_Q, KALMAN_R );
	K_Groy_Z = (Groy_Z+15)*Gyro_Gr;
	
	if( Groy_Z<20 && Groy_Z>10 ) 
		K_Groy_Z=0.0;
	
	/*	姿态解算	*/	
	IMUupdate( K_Groy_X, K_Groy_Y, K_Groy_Z, K_Acc_X, K_Acc_Y, K_Acc_Z );
	
	/*	PID控制	*/
	
	PID_Control( Roll, Pitch, Yaw, K_Groy_X, K_Groy_Y, K_Groy_Z, Acc );//角速度 油门
	
//	XXX = atan(Acc_X/sqrt(Acc_Y*Acc_Y+Acc_Z*Acc_Z))*57.2957795f;
//	YYY =atan(Acc_Y/sqrt(Acc_X*Acc_X+Acc_Z*Acc_Z))*57.2957795f;
	
//	Buf[0]=0x88;
//	Buf[1]=0xaf;
//	Buf[2]=0x1c;
//	Buf[3]=(short)(Acc_X)>>8;			
//	Buf[4]=(short)(Acc_X)%256;//Serial 1原始数据
//	Buf[5]=(short)(Acc_Y)>>8;			
//	Buf[6]=(short)(Acc_Y)%256;//Serial 2
//	Buf[7]=(short)(Acc_Z)>>8;//(short)(Angle_Z_Temp)>>8;
//	Buf[8]=(short)(Acc_Z)%256;//(short)(Angle_Z_Temp)%256;//Serial 3
//	
//	Buf[9]=(short)Groy_X>>8;
//	Buf[10]=(short)Groy_X%256;
//	Buf[11]=(short)Groy_Y>>8;			//Serial 5	H		
//	Buf[12]=(short)Groy_Y%256;
//	Buf[13]=(short)Groy_Z>>8;			//Serial 6	H	
//	Buf[14]=(short)Groy_Z%256;
//	
//	Buf[15]=0;		//Serial 7	H	
//	Buf[16]=0x0;
//	Buf[17]=0x0;	//Serial 8	H	
//	Buf[18]=0x0;
//	Buf[19]=0x0;		//Serial 9	H	
//	Buf[20]=0x0;
//	Buf[21]=((short)( Roll*100 ))>>8;		//Serial 10	H		
//	Buf[22]=((short)( Roll*100 ))%256;
//	Buf[23]=((short)( Pitch*100 ))>>8;		//Serial 11	H	
//	Buf[24]=((short)( Pitch*100 ))%256;
//	Buf[25]=((short)( Yaw*10 ))>>8;
//	Buf[26]=((short)( Yaw*10 ))%256;
//	Buf[27]=0x0;
//	Buf[28]=0x0;
//	Buf[29]=0x0;
//	Buf[30]=0x0;	
//	sum = 0; 
//	for(i=0;i<31;i++)
//			sum += Buf[i];
//	Buf[31]=sum;	
//	for(i=0;i<32;i++)
//	{

//		USART_SendData(USART1,Buf[i]);//向串口1发送数据
//		 while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);
//	}
	
}

