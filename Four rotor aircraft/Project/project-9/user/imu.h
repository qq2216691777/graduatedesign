#ifndef _IMU_H__
#define _IMU_H__

float FL_ABS(float x);
float Q_rsqrt(float number);
void IMUupdate(float gx, float gy, float gz, float ax, float ay, float az);

double KalmanFilter_x( double ResrcData, double ProcessNiose_Q, double MeasureNoise_R);
double KalmanFilter_y( double ResrcData, double ProcessNiose_Q, double MeasureNoise_R);
double KalmanFilter_z( double ResrcData, double ProcessNiose_Q, double MeasureNoise_R);
double KalmanFilter_gx( double ResrcData, double ProcessNiose_Q, double MeasureNoise_R);
double KalmanFilter_gy( double ResrcData, double ProcessNiose_Q, double MeasureNoise_R);


#endif

