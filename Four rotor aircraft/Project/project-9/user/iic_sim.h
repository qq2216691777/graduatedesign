#ifndef _IIC_SIM_H__
#define _IIC_SIM_H__
#include "stm32f10x.h"
#include "sys.h"
#include "delay.h"
#include "mpu6050.h"

/*********************************/
//IO方向设置
#define SDA_IN()  {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=8<<28;}
#define SDA_OUT() {GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=3<<28;}

//IO操作函数	 
#define IIC_SCL    PBout(6) //SCL
#define IIC_SDA    PBout(7) //SDA	 
#define READ_SDA   PBin(7)  //输入SDA





void IIC_SIM_Init( void );
void IIC_Sim_Start(void);
void IIC_Send_Byte(u8 txd);
u8 IIC_Wait_Ack(void);
u8 IIC_Read_Byte(void);
void MPU6050_WriteOneData( u8 RegAddr, u8 W_Data );
void IIC_Send_ACK( u8 Ack);
void IIC_Stop(void);
void IIC_Sim_Delay( int Di );

#endif

