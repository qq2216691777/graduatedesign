/* STM32F10X_HD,USE_STDPERIPH_DRIVER */

/**
	***************************************
			@作者     叶念西风 
			@作用     微四旋翼飞行器   
			@日期     2015.9.12 -- 2015.00.00
			@备注   
	***************************************
**/

#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "stm32f10x_conf.h"
#include "mpu6050.h"
#include "tim_pwm.h"
#include "nrf24l01_spi.h"

void LED_Init( void );

u16 Accelerator;			//油门
float Roll,Pitch,Yaw;

int main( void )
{
	u8 LEDD;
	u8 dat[8];
	delay_init();
	NVIC_Configuration(); //设置中断分组
	uart_init( 115200 );

	LED_Init();
	delay_ms(50);
	
	MPU6050_Init();
	
	Tim_PWM_Init();
	Accelerator = 0;
	delay_ms(50);
	Handler_TIM2_Init( 5000,72);//定时器中断 5000,72; 5ms
	PBout(12)=1;	
	delay_ms(500);
	delay_ms(500);	
	Accelerator = 370;		//350
	
//	NRF24l01_SPI_Init();
//	if( NRF_Check() )
//		printf("NRF  OK\n");
//	else
//		PBout(12)=0;
	
//	while(1);
	
//	TIM_Cmd(TIM2, ENABLE);
	
//	delay_ms(500);
//	delay_ms(500);	
//	delay_ms(500);
//	delay_ms(500);
//	delay_ms(500);
//	delay_ms(500);	
//	delay_ms(500);
//	delay_ms(500);		
//	Accelerator = 30;
//	delay_ms(20);	
//	TIM_Cmd(TIM2, DISABLE);
//	Motor(0,0,0,0);
//	while(1);
	
//	NRF_TX_Mode();
	while(1)
	{
//		if(LEDD>100)
//			PBout(12)=1;
//		else
//			PBout(12)=0;
//		LEDD ++;
//		
//		
//		
//		dat[0]=((short)( Roll*100 ))>>8;
//		dat[1]=((short)( Roll*100 ))%256;
//		
//		dat[2]=((short)( Pitch*100 ))>>8;
//		dat[3]=((short)( Pitch*100 ))%256;
//		
//		dat[4]=((short)( Yaw*10 ))>>8;
//		dat[5]=((short)( Yaw*10 ))%256;
//		
//		switch(NRF_Tx_Dat( dat ))
//		{
//			case TX_DS:
//				printf("RX OK\r\n"); break;
//			case MAX_RT:
//				printf("max\r\n"); break;
//			
//		}
		
	}
}


void LED_Init( void )
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB, ENABLE );	//使能USART1，GPIOA时钟
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12; //PB.12
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;	//复用推挽输出
	
    GPIO_Init( GPIOB, &GPIO_InitStructure ); //初始化PA9
	
}

