#include "imu.h"
#include "math.h"

#define Kp 1.8f                        //1.5f proportional gain governs rate of convergence to accelerometer/magnetometer 
#define Ki 0.002f                          // 0.002f  integral gain governs rate of convergence of gyroscope biases  
#define halfT 0.0025f                  // half the sample period采样周期的一半

#define RtA 		57.324841	

extern float Roll;
extern float Pitch;
extern float Yaw;

float q0 = 1, q1 = 0, q2 = 0, q3 = 0;    // quaternion elements representing the estimated orientation
float exInt = 0, eyInt = 0, ezInt = 0;    // scaled integral error


float Q_rsqrt(float number)
{
	long i;
	float x2, y;
	const float threehalfs = 1.5F;
 
	x2 = number * 0.5F;
	y  = number;
	i  = * ( long * ) &y;                      
	i  = 0x5f3759df - ( i >> 1 );               
	y  = * ( float * ) &i;
	y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration （第一次牛顿迭代）
	return y;
} 
//  求float型数据绝对值
float FL_ABS(float x)
{
   if(x < 0.00001)  return -x;
	 else return x; 
}

void IMUupdate(float gx, float gy, float gz, float ax, float ay, float az)
{
	float norm;
	float vx, vy, vz;// wx, wy, wz;
	float ex, ey, ez;

	// 先把这些用得到的值算好
	float q0q0 = q0*q0;
	float q0q1 = q0*q1;
	float q0q2 = q0*q2;
	//  float q0q3 = q0*q3;//
	float q1q1 = q1*q1;
	//  float q1q2 = q1*q2;//
	float q1q3 = q1*q3;
	float q2q2 = q2*q2;
	float q2q3 = q2*q3;
	float q3q3 = q3*q3;
	
	if(ax*ay*az==0)
	  return;
		
	norm = Q_rsqrt(ax*ax + ay*ay + az*az);       //acc数据归一化
	ax = ax *norm;
	ay = ay * norm;
	az = az * norm;

	// estimated direction of gravity and flux (v and w)              估计重力方向和流量/变迁
	vx = 2*(q1q3 - q0q2);												//四元素中xyz的表示
	vy = 2*(q0q1 + q2q3);
	vz = q0q0 - q1q1 - q2q2 + q3q3 ;

	// error is sum of cross product between reference direction of fields and direction measured by sensors
	ex = (ay*vz - az*vy) ;                           					 //向量外积在相减得到差分就是误差
	ey = (az*vx - ax*vz) ;
	ez = (ax*vy - ay*vx) ;

	exInt = exInt + ex * Ki;								  //对误差进行积分
	eyInt = eyInt + ey * Ki;
	ezInt = ezInt + ez * Ki;
	// adjusted gyroscope measurements
	if(FL_ABS(ay) < 0.8 && FL_ABS(az-1) < 0.8)
	   gx = gx + Kp*ex + exInt;					   							//将误差PI后补偿到陀螺仪，即补偿零点漂移
	else
	   gx = gx; 
	if(FL_ABS(ax) < 0.8 && FL_ABS(az-1) < 0.8)
	   gy = gy + Kp*ey + eyInt;				   							
	else
	   gy = gy; 
	if(FL_ABS(ax)<0.8 && FL_ABS(ay) < 0.8)
	   gz = gz + Kp*ez + ezInt;					   					//这里的gz由于没有观测者进行矫正会产生漂移，表现出来的就是积分自增或自减		
	else
	   gz = gz;   							

	// integrate quaternion rate and normalise						   //四元素的微分方程
	q0 = q0 + (-q1*gx - q2*gy - q3*gz)*halfT;
	q1 = q1 + (q0*gx + q2*gz - q3*gy)*halfT;
	q2 = q2 + (q0*gy - q1*gz + q3*gx)*halfT;
	q3 = q3 + (q0*gz + q1*gy - q2*gx)*halfT;

	// normalise quaternion
	norm = Q_rsqrt(q0q0 + q1q1 + q2q2 + q3q3);
	q0 = q0 * norm;
	q1 = q1 * norm;
	q2 = q2 * norm;
	q3 = q3 * norm;

	Roll = atan2(2*q2q3 + 2*q0q1, -2*q1q1 - 2*q2q2 + 1) *RtA; // roll
	Pitch = asin(-2*q1q3 + 2*q0q2) * RtA; // pitch	
	Yaw = atan2(2 * q1 * q2 + 2 * q0 * q3, -2 * q2*q2 - 2 * q3 * q3 + 1)* RtA; // yaw
	//if(last_mpu_yaw-mpu_yaw>350)mpu_yaw=360+mpu_yaw;
	
}

double KalmanFilter_x( double ResrcData,double ProcessNiose_Q,double MeasureNoise_R)
{
   double R = MeasureNoise_R;
   double Q = ProcessNiose_Q;
   static double x_last;
   double x_mid = x_last;
   double x_now;
   static double p_last;
   double p_mid ;
   double p_now;
   double kg;        

   x_mid=x_last; //x_last=x(k-1|k-1),x_mid=x(k|k-1)
   p_mid=p_last+Q; //p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=??
   kg=p_mid/(p_mid+R); //kg?kalman filter,R???
   x_now=x_mid+kg*(ResrcData-x_mid);//???????
                
   p_now=(1-kg)*p_mid;//??????covariance       
   p_last = p_now; //??covariance?
   x_last = x_now; //???????
   return x_now;                
 }
double KalmanFilter_y( double ResrcData,double ProcessNiose_Q,double MeasureNoise_R)
{
   double R = MeasureNoise_R;
   double Q = ProcessNiose_Q;
   static double y_last;
   double y_mid = y_last;
   double y_now;
   static double p_last;
   double p_mid ;
   double p_now;
   double kg;        

   y_mid=y_last; //x_last=x(k-1|k-1),x_mid=x(k|k-1)
   p_mid=p_last+Q; //p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=??
   kg=p_mid/(p_mid+R); //kg?kalman filter,R???
   y_now=y_mid+kg*(ResrcData-y_mid);//???????
                
   p_now=(1-kg)*p_mid;//??????covariance       
   p_last = p_now; //??covariance?
   y_last = y_now; //???????
   return y_now;                
 }
double KalmanFilter_z( double ResrcData,double ProcessNiose_Q,double MeasureNoise_R)
{
   double R = MeasureNoise_R;
   double Q = ProcessNiose_Q;
   static double z_last;
   double z_mid = z_last;
   double z_now;
   static double p_last;
   double p_mid ;
   double p_now;
   double kg;        

   z_mid=z_last; //x_last=x(k-1|k-1),x_mid=x(k|k-1)
   p_mid=p_last+Q; //p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=??
   kg=p_mid/(p_mid+R); //kg?kalman filter,R???
   z_now=z_mid+kg*(ResrcData-z_mid);//???????
                
   p_now=(1-kg)*p_mid;//??????covariance       
   p_last = p_now; //??covariance?
   z_last = z_now; //???????
   return z_now;                
 }

 double KalmanFilter_gx( double ResrcData,double ProcessNiose_Q,double MeasureNoise_R)
{
   double R = MeasureNoise_R;
   double Q = ProcessNiose_Q;
   static double x_last;
   double x_mid = x_last;
   double x_now;
   static double p_last;
   double p_mid ;
   double p_now;
   double kg;        

   x_mid=x_last; //x_last=x(k-1|k-1),x_mid=x(k|k-1)
   p_mid=p_last+Q; //p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=??
   kg=p_mid/(p_mid+R); //kg?kalman filter,R???
   x_now=x_mid+kg*(ResrcData-x_mid);//???????
                
   p_now=(1-kg)*p_mid;//??????covariance       
   p_last = p_now; //??covariance?
   x_last = x_now; //???????
   return x_now;                
 }

 double KalmanFilter_gy( double ResrcData,double ProcessNiose_Q,double MeasureNoise_R)
{
   double R = MeasureNoise_R;
   double Q = ProcessNiose_Q;
   static double x_last;
   double x_mid = x_last;
   double x_now;
   static double p_last;
   double p_mid ;
   double p_now;
   double kg;        

   x_mid=x_last; //x_last=x(k-1|k-1),x_mid=x(k|k-1)
   p_mid=p_last+Q; //p_mid=p(k|k-1),p_last=p(k-1|k-1),Q=??
   kg=p_mid/(p_mid+R); //kg?kalman filter,R???
   x_now=x_mid+kg*(ResrcData-x_mid);//???????
                
   p_now=(1-kg)*p_mid;//??????covariance       
   p_last = p_now; //??covariance?
   x_last = x_now; //???????
   return x_now;                
 }

