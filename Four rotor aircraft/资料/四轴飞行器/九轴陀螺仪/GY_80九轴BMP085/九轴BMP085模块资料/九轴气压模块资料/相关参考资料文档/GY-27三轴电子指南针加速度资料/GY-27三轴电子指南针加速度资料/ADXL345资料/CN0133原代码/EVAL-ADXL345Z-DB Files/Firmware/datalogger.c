/*----------------------------------------------------------------------
  File Name          : 
  Author             : MPD Application Team
  Version            : V0.0.1
  Date               : 12/03/2008
  Description        : 
  File ID            : $Id: multi_demo.c,v 1.14 2009/05/07 13:31:16 ngadish Exp $

  Analog Devices ADXL 345 digital output accellerometer 
  with advanced digital features.

  (c) 2008 Analog Devices application support team.
  xxx@analog.com

  ----------------------------------------------------------------------

  The present firmware which is for guidance only aims at providing
  customers with coding information regarding their products in order
  for them to save time.  As a result, Analog Devices shall not be
  held liable for any direct, indirect or consequential damages with
  respect to any claims arising from the content of such firmware and/or
  the use made by customers of the coding information contained herein
  in connection with their products.

  ----------------------------------------------------------------------*/


/*----------------------------------------------------------------------

This example is to demonstrate multiple features of the XL345

It uses activity and inactivity (5 seconds) to turn the demo on and off

It demonstrates tap-sign with a single flash

It demonstrated double-tap with a double flash

Freefall will trigger the sounder

----------------------------------------------------------------------*/

#include <string.h>
#include "XL345.h"
#include "xl345_io.h"
#include "dev_board.h"
#include <ADuC7024.h>
#include <stdio.h>
#include <stdlib.h>			   

#include "debug_printf.h"
#include "efs.h"
#include "ls.h"

typedef int bool;
#define true 1
#define false 0

#define DEFAULT_LOG_DIRECTORY "xl345"
#define DEFAULT_LOG_FILENAME "data"

typedef struct _FilesystemState {
	EmbeddedFileSystem efs;
	EmbeddedFile logFile;
	char logFileName[100];
} FilesystemState;

//#define SENSORBUFFERSIZE 211 // 211 is the max!
#define SENSORBUFFERSIZE 150
#define SENSORDATASIZE 4
typedef struct _SensorState {

	int sensorDataIndex;
	int sensorData[SENSORBUFFERSIZE * SENSORDATASIZE];
	//int *sensorData;
} SensorState;

SensorState sensorState;

int lastTime;
  
int Initialize();
int InitFs(FilesystemState *fileSystem);
int SensorLoop(FilesystemState *fileSystem);
void redLight(bool state);
void greenLight(bool state);

int main() {
	if (Initialize()) {
		FilesystemState fs;
		
		sensorState.sensorData[SENSORBUFFERSIZE * SENSORDATASIZE -1] = 0xDEADBEEF;
		DEBUG_PRINTF("You wrote: 0x%08X\n",sensorState.sensorData[SENSORBUFFERSIZE * SENSORDATASIZE - 1]);
		DEBUG_PRINTF("Line 153: 0x%08X, 0x%08X, 0x%08X\n",sensorState.sensorData[153*SENSORDATASIZE],sensorState.sensorData[153*SENSORDATASIZE+1],sensorState.sensorData[153*SENSORDATASIZE+2]);
		DEBUG_PRINTF("Line 154: 0x%08X, 0x%08X, 0x%08X\n",sensorState.sensorData[154*SENSORDATASIZE],sensorState.sensorData[154*SENSORDATASIZE+1],sensorState.sensorData[154*SENSORDATASIZE+2]);
		DEBUG_PRINTF("Line 155: 0x%08X, 0x%08X, 0x%08X\n",sensorState.sensorData[155*SENSORDATASIZE],sensorState.sensorData[155*SENSORDATASIZE+1],sensorState.sensorData[155*SENSORDATASIZE+2]);

		DEBUG_PRINTF(">>> Initialized ok\n");
		if (InitFs(&fs)) {
			DEBUG_PRINTF(">>> Initialized Filesystem ok\n");
			SensorLoop(&fs);
		} else {
			DEBUG_PRINTF("!!! Initialize Filesystem failed!\n");
			
		}

	} else {
		DEBUG_PRINTF("!!! Initialize failed!\n");
		
	}

	greenLight(false);	
	redLight(true);
	while (true);
	return 0;
}

void myisr() {
	//DEBUG_PRINTF("MYISR [irqsig=0x%08lX] [t=%d]\n", IRQSIG, T2VAL);

	// 0x0040A000
	// 0000 0000 0100 0000 1010 0000 0000 0000
	// 13, 15, 22

	unsigned char interruptSource;
	xl345Read(1, XL345_INT_SOURCE, &interruptSource);
	if (interruptSource & XL345_OVERRUN) {
		DEBUG_PRINTF("Overrun @ %d\n", T2VAL);
	}

	if (interruptSource & XL345_WATERMARK) {

		//DEBUG_PRINTF("***\n*** About to read some fifo data\n***\n");
		int count = 0;
		unsigned char entryCount = 0;
		unsigned char databuf[8];
		int thisTime = T2VAL/2;

		xl345Read(1, XL345_FIFO_STATUS, &entryCount);

		// get the lower 6 bits
		entryCount &= 0x3f;
		for (count = 0; count < entryCount; count++) {
			short x1, y1, z1;
			//DEBUG_PRINTF("data[%d]=\n", sensorState.sensorDataIndex);
			xl345Read(6,XL345_DATAX0,databuf);
			x1 = ( (int) (databuf[0] | databuf[1]<<8));
    		y1 = ( (int) (databuf[2] | databuf[3]<<8));
    		z1 = ( (int) (databuf[4] | databuf[5]<<8));
			//*******if (sensorState.sensorDataIndex <= SENSORBUFFERSIZE) { ////
				sensorState.sensorData[(sensorState.sensorDataIndex * SENSORDATASIZE)] = lastTime + count * (thisTime - lastTime) / (entryCount-1);
				sensorState.sensorData[(sensorState.sensorDataIndex * SENSORDATASIZE)+1] = x1;
				sensorState.sensorData[(sensorState.sensorDataIndex * SENSORDATASIZE)+2] = y1;
				sensorState.sensorData[(sensorState.sensorDataIndex * SENSORDATASIZE)+3] = z1;
				/*DEBUG_PRINTF("sensorData[%d] = {t: 0x%X, x: %d, y: %d, z: %d}\n",
					sensorState.sensorDataIndex,
					sensorState.sensorData[(sensorState.sensorDataIndex * SENSORDATASIZE)],
					sensorState.sensorData[(sensorState.sensorDataIndex * SENSORDATASIZE)+1],
					sensorState.sensorData[(sensorState.sensorDataIndex * SENSORDATASIZE)+2],
					sensorState.sensorData[(sensorState.sensorDataIndex * SENSORDATASIZE)+3]); */
				sensorState.sensorDataIndex += 1;

				if (sensorState.sensorDataIndex >= SENSORBUFFERSIZE) {
					//DEBUG_PRINTF("%d >= %d, resetting to 0\n", sensorState.sensorDataIndex, SENSORBUFFERSIZE);
					sensorState.sensorDataIndex = 0;		 ////
				} 
			//} else {
			//	DEBUG_PRINTF("sensorState.sensorData = %x\n", sensorState.sensorData);
			//*******} ////
			//DEBUG_PRINTF("entry[%d] = {%d, %d, %d}\n", count, x1, y1, z1);
		}
		lastTime = thisTime;
	} else {
		DEBUG_PRINTF("Unknown interrupt source: %d\n", interruptSource);
	} 
	

}

void myswi() {
	DEBUG_PRINTF("MYSWI\n");
	greenLight(false);
	redLight(true);
	while (true);
}

void myfiq() {
	DEBUG_PRINTF("MYFIQ\n");
	greenLight(false);
	redLight(true);
	while (true);
}

void myundef() {
	DEBUG_PRINTF("MYUNDEF\n");
	greenLight(false);
	redLight(true);
	while (true);
}

void mydabort() {
	DEBUG_PRINTF("MYDABORT\n");
	greenLight(false);
	redLight(true);
	while (true);
}

void mypabort() {
	DEBUG_PRINTF("MYPABORT\n");
	greenLight(false);
	redLight(true);
	while (true);
}

void delay (int length){
  while (length >=0)
    length--;
}

int Initialize() {
	unsigned char buffer[8];

	POWKEY1 = 0x01;
	POWCON = 0;
	POWKEY2 = 0xF4;
	
	IRQ = myisr;	// Specify Interrupt Service Routine
	SWI = myswi;
	FIQ = myfiq;
	UNDEF = myundef;
	DABORT = mydabort;
	PABORT = mypabort;
	
	GP3CON = GP3CONGPIOVAL; // configure GPIO pins
	GP3PAR = ALLPUPOFF;  // turn off pull ups
	GP3DAT = GP3OFF;     // set LED pins to output and PWM to GPIOOFF
	GP4CON = GP4CONVAL;
	//  GP4DAT = GP4OUT;
	GP4DAT = 0xFF000000;
	
	i2cinit(); // set up the I2C bus

	UART_Config();

 	/* soft reset for safety */
  	buffer[0] = XL345_RESERVED1; /* register address */
  	buffer[1] = XL345_SOFT_RESET;
  	xl345Write(2,buffer);

  	delay(2000);  // allow delay after a soft reset

	/*--------------------------------------------------
    TAP Configuration
    --------------------------------------------------*/
  /* set up a buffer with all the initialization for tap */
  buffer[0] = XL345_THRESH_TAP; /* register address */
  buffer[1] = 80; /* THRESH_TAP = 5 Gee (1 lsb = 1/16 gee) */	
  xl345Write(2,buffer);

  buffer[0] = XL345_DUR; /* register address */
  buffer[1] = 13; /* DUR = 8ms 0.6125ms/lsb */
  buffer[2] = 80; /* LATENT = 100 ms 1.25ms/lsb */
  buffer[3] = 240;/* WINDOW 300ms 1.25ms/lsb */
  xl345Write(4,buffer);

  buffer[0] = XL345_TAP_AXES; /* register address */
  buffer[1] = XL345_TAP_Z_ENABLE | XL345_TAP_Y_ENABLE 
    | XL345_TAP_X_ENABLE /*|  XL345_TAP_SUPPRESS*/;
  xl345Write(2,buffer);

  /*--------------------------------------------------
    activity - inactivity 
    --------------------------------------------------*/
  /* set up a buffer with all the initialization for activity and inactivity */
  buffer[0] = XL345_THRESH_ACT; /* register address */
  buffer[1] = 80; /* THRESH_ACT = 80/16 = 5 Gee (1 lsb = 1/16 gee) */
  buffer[2] = 4; /* THRESH_INACT = 14/16 .25 Gee (1 lsb = 1/16 gee) */
  buffer[3] = 5;/* TIME_INACT - 5 seconds 2 minutes*/
  buffer[4] =     /* ACT_INACT_CTL */
    XL345_ACT_DC 
    | XL345_ACT_X_ENABLE | XL345_ACT_Y_ENABLE | XL345_ACT_Z_ENABLE
    | XL345_INACT_AC | XL345_INACT_X_ENABLE 
    | XL345_INACT_Y_ENABLE | XL345_INACT_Z_ENABLE;
  xl345Write(5,buffer);


  /*--------------------------------------------------
    Power, bandwidth-rate, interupt enabling
    --------------------------------------------------*/

  /* set up a buffer with all the initization for power*/
  buffer[0] = XL345_BW_RATE;    /* register address */
  buffer[1] =                   /* BW_RATE */
    XL345_RATE_100 | XL345_LOW_NOISE;
  buffer[2] =                   /* POWER_CTL */
    XL345_WAKEUP_8HZ | XL345_MEASURE;
  xl345Write(3,buffer);
	
	// set the FIFO control
	buffer[0] = XL345_FIFO_CTL;
	buffer[1] = XL345_FIFO_MODE_FIFO | // set FIFO mode
		0 | 		 // link to INT1
		31;		     // number of samples
	xl345Write(2, buffer);

	// turn on the watermark interrupt and set the watermark interrupt to int1
	buffer[0] = XL345_INT_ENABLE;
	buffer[1] = XL345_WATERMARK | XL345_OVERRUN; // enable D1
	buffer[2] = 0; // send all interrupts to INT1
	xl345Write(3, buffer);
						 
	GP4CLR = 0xff<<16;

	// timers
	T2LD = 0;
	T2CON = (1<<6) | (1<<7) | (1<<8) | (1<<10) | 0x4;

	return true;
}

int data_log(FilesystemState *fileState, char *line) {
	int result = 0;
	result = file_write(&(fileState->logFile), strlen(line), line);
	return true;
}


int batch_log(FilesystemState *fileState, int start, int end) {
	
	//DEBUG_PRINTF("Logging from %d to %d\n", start, end);
	char line[50] = {0};
	int i = 0;
	int result = 0;

	//DEBUG_PRINTF("L1\n");
	result = efs_init(&(fileState->efs), 0);
	//DEBUG_PRINTF("L2\n");
	if (result) {
		DEBUG_PRINTF("%s:%d batch_log(): failed on efs_init() = %d\n", __FILE__, __LINE__, result);
		return false;
	}
	//DEBUG_PRINTF("L3 %s\n", fileState->logFileName);
	result = file_fopen(&(fileState->logFile), &(fileState->efs.myFs), fileState->logFileName, 'a');
	//DEBUG_PRINTF("L4\n");
	if (result) {
		DEBUG_PRINTF("%s:%d batch_log(): failed on file_fopen(\"%s\") = %d\n", __FILE__, __LINE__, fileState->logFileName, result);
		return false;
	}
	//DEBUG_PRINTF("L5\n"); 
	 
	 
	for (i = start; i < end; i++) {
		/*DEBUG_PRINTF("sensorData[%d] = {t: %d, x: %d, y: %d, z: %d}\n",
					i,
					sensorState.sensorData[(i * SENSORDATASIZE)],
					sensorState.sensorData[(i * SENSORDATASIZE)+1],
					sensorState.sensorData[(i * SENSORDATASIZE)+2],
					sensorState.sensorData[(i * SENSORDATASIZE)+3]);*/
		int t = sensorState.sensorData[i * SENSORDATASIZE];

		//snprintf(line, 49, "%02d:%02d:%02d:%02d,%d,%d,%d\n",
		snprintf(line,49,"%d,%d,%d,%d\n",
			//(t>>24)&255,(t>>16)&255,(t>>8)&255,t&255, 			 
			t,
			sensorState.sensorData[i * SENSORDATASIZE + 1],
			sensorState.sensorData[i * SENSORDATASIZE + 2],
			sensorState.sensorData[i * SENSORDATASIZE + 3]);
		result = data_log(fileState, line);
		if (!result) {
			DEBUG_PRINTF(">>> Error in writing to data log from batch_log\n");
		}
	} 

	//DEBUG_PRINTF("L6\n");
	result = file_fclose(&(fileState->logFile));
	//DEBUG_PRINTF("L7\n");
	if (result) {
		DEBUG_PRINTF("%s:%d batch_log(): failed on file_fclose(\"%s\") = %d\n", __FILE__, __LINE__, fileState->logFileName, result);
		return false;
	}
	//DEBUG_PRINTF("L8\n");
	result = fs_umount(&(fileState->efs.myFs));
	//DEBUG_PRINTF("L9\n");
	if (result) {
		DEBUG_PRINTF("%s:%d batch_log(): failed on fs_umount() = %d\n", __FILE__, __LINE__, result);
		return false;
	}
	//DEBUG_PRINTF("L10\n");

	//DEBUG_PRINTF("LDONE\n");
	return true;
}

int InitFs(FilesystemState *fileState) {
	char logDirectory[100] = {0};
	int logCount = 0;
	int result = 0;

	// log directory
	snprintf(logDirectory, 
		99, 
		"%s", 
		DEFAULT_LOG_DIRECTORY);
	 
	// log filename
	snprintf(fileState->logFileName, 
		99, 
		"%s/%s%04d.txt", 
		DEFAULT_LOG_DIRECTORY,
		DEFAULT_LOG_FILENAME,
		logCount);

	DEBUG_PRINTF("%s:%d InitFs(): going to log to %s\n", __FILE__, __LINE__, fileState->logFileName);
	
	// initialize filesystem
	result = efs_init(&(fileState->efs), 0);
	if (result) {
		DEBUG_PRINTF("%s:%d InitFs(): failed on efs_init() = %d\n", __FILE__, __LINE__, result);
		return false;
	}
	// make the directory if needed
	result = mkdir(&(fileState->efs.myFs), logDirectory);
	DEBUG_PRINTF("%s:%d InitFs(): result of mkdir(%s) = %d\n", __FILE__, __LINE__, logDirectory, result);
	// open the log file for writing
	result = file_fopen(&(fileState->logFile), &(fileState->efs.myFs), fileState->logFileName, 'a');
	if (result) {
		DEBUG_PRINTF("%s:%d InitFs(): failed on file_fopen(\"%s\") = %d\n", __FILE__, __LINE__, fileState->logFileName, result);
		return false;
	}

	result = data_log(fileState, "t,x,y,z\n");
	if (!result) {
		DEBUG_PRINTF("%s:%d InitFs(): failed on data_log(\"%s\") = %d\n", __FILE__, __LINE__, fileState->logFileName, result);
		return false;
	}
	DEBUG_PRINTF(">>> wrote header to log file\n");
	
	result = file_fclose(&(fileState->logFile));
	if (result) {
		DEBUG_PRINTF("%s:%d InitFs(): failed on file_fclose(\"%s\") = %d\n", __FILE__, __LINE__, fileState->logFileName, result);
		return false;
	}
	result = fs_umount(&(fileState->efs.myFs));
	if (result) {
		DEBUG_PRINTF("%s:%d init_file_system(): failed on fs_umount() = %d\n", __FILE__, __LINE__, result);
		return false;
	}
	
	DEBUG_PRINTF("%s:%d InitFs() done\n", __FILE__, __LINE__);
	return true;
}

int SensorLoop(FilesystemState *fileState) {
	int half = 0;
	int midway = SENSORBUFFERSIZE / 2;
	DEBUG_PRINTF(">>> Starting Sensor loop\n");
	sensorState.sensorDataIndex = 0;
   	IRQEN = (1<<15);

	greenLight(true);
	
	lastTime = T2VAL/2;	

	while (true) {
		//DEBUG_PRINTF("--- Loop iteration %08x\n", T2VAL);
		
		if ((half == 0) && (sensorState.sensorDataIndex >= midway)) {
			//DEBUG_PRINTF("Half is %d, and index is %d which is >= than %d\n", half, sensorState.sensorDataIndex, midway);
			batch_log(fileState, 0, midway);
			half = 1;
		} else if ((half == 1) && (sensorState.sensorDataIndex < midway)) {
			//DEBUG_PRINTF("Half is %d, and index is %d which is < than %d\n", half, sensorState.sensorDataIndex, midway);
			batch_log(fileState, midway, SENSORBUFFERSIZE);
			half = 0;
		}
		/*if (sensorState.sensorDataIndex >= SENSORBUFFERSIZE) {
			IRQCLR = (1<<15);
			batch_log(fileState, 0, SENSORBUFFERSIZE);
			while (true); 
		}*/
	}
}

void redLight(bool state) {
	if (state) {
		GP4SET = 1<<21;
	} else {
		GP4CLR = 1<<21;
	}
}

void greenLight(bool state) {
	if (state) {
		GP4SET = 1<<22;
	} else {
		GP4CLR = 1<<22;
	}
}
