00001/**
00002* @作者亚伦伯克
00003*
00004*@部分许可证
00005*
00006*版权所有（c）2010 ARM有限公司
00007*
特此授予权限是00008*，免费，任何人获得副本
00009*本软件及相关文档（“软件”），以处理
00010*在软件的权利不受任何限制，包括但不限于
00011*，使用，复制，修改，合并，发布，分发，转授，和/或出售
00012*软件的副本，以及该软件的许可证的人
00013*布置这样做，符合以下条件：
00014*
00015*上述版权声明和本许可声明应包括在
00016*软件的所有副本或很大一部分。
00017*
00018*的软件提供“按原样”，无任何形式的担保，明示或
00019*暗示，包括但不限于适销的担保，
00020*特定用途及不侵权的保证。在任何情况下，
00021*作者或版权持有人的任何索赔，损害赔偿或其他负责
00022*责任，无论在合同，侵权行为的诉讼或其他方式，引起的，
00023*或在与软件或使用或其他交易相关
00024*的软件。
00025  *
@节描述
00027*
00028* IMU的方向过滤器开发由塞巴斯蒂安Madgwick。
00029*
00030*查找有关他的论文这里的更多细节：
00031  *
00032  * http://code.google.com/p/imumargalgorithm30042010sohm/ 
00033  */
00034 
00035 /**
00036  * Includes
00037  */
00038 #include "IMUfilter.h"
00039 
00040 IMUfilter::IMUfilter(double rate, double gyroscopeMeasurementError){
00041 
00042     firstUpdate = 0;
00043     
00044     //四元数方向地球相辅助帧帧。
00045     AEq_1 = 1;
00046     AEq_2 = 0;
00047     AEq_3 = 0;
00048     AEq_4 = 0;
00049     
00050     //四元数与最初预计的方向元素
条件
00051     SEq_1 = 1;
00052     SEq_2 = 0;
00053     SEq_3 = 0;
00054     SEq_4 = 0;
00055 
00056     //采样周期（典型值0.1秒）.
00057     deltat = rate;
00058     
00059     //陀螺仪测量误差（度每秒）
00060     gyroMeasError = gyroscopeMeasurementError;
00061     
00062     //计算测试版
00063     beta = sqrt(3.0 / 4.0) * (PI * (gyroMeasError / 180.0));
00064 
00065 }
00066 
00067 void IMUfilter::updateFilter(double w_x, double w_y, double w_z, double a_x, double a_y, double a_z) {
00068 
00069     //本地系统变量.
00070 
00071     //向量的范数.
00072     double norm;
00073     //从陀螺仪元素的四元率.
00074     double SEqDot_omega_1;
00075     double SEqDot_omega_2;
00076     double SEqDot_omega_3;
00077     double SEqDot_omega_4;
00078     //目标函数的元素。
00079     double f_1;
00080     double f_2;
00081     double f_3;
00082     //目标函数的雅可比元素.
00083     double J_11or24;
00084     double J_12or23;
00085     double J_13or22;
00086     double J_14or21;
00087     double J_32;
00088     double J_33;
00089     //目标函数的梯度元素.
00090     double nablaf_1;
00091     double nablaf_2;
00092     double nablaf_3;
00093     double nablaf_4;
00094 
00095     //辅助变量，以避免reapeated calcualtions.
00096     double halfSEq_1 = 0.5 * SEq_1;
00097     double halfSEq_2 = 0.5 * SEq_2;
00098     double halfSEq_3 = 0.5 * SEq_3;
00099     double halfSEq_4 = 0.5 * SEq_4;
00100     double twoSEq_1 = 2.0 * SEq_1;
00101     double twoSEq_2 = 2.0 * SEq_2;
00102     double twoSEq_3 = 2.0 * SEq_3;
00103 
00104     //陀螺仪测量计算四元数率.
00105     SEqDot_omega_1 = -halfSEq_2 * w_x - halfSEq_3 * w_y - halfSEq_4 * w_z;
00106     SEqDot_omega_2 = halfSEq_1 * w_x + halfSEq_3 * w_z - halfSEq_4 * w_y;
00107     SEqDot_omega_3 = halfSEq_1 * w_y - halfSEq_2 * w_z + halfSEq_4 * w_x;
00108     SEqDot_omega_4 = halfSEq_1 * w_z + halfSEq_2 * w_y - halfSEq_3 * w_x;
00109 
00110     //Normalise的加速度测量.
00111     norm = sqrt(a_x * a_x + a_y * a_y + a_z * a_z);
00112     a_x /= norm;
00113     a_y /= norm;
00114     a_z /= norm;
00115 
00116     //计算目标函数和雅可比.
00117     f_1 = twoSEq_2 * SEq_4 - twoSEq_1 * SEq_3 - a_x;
00118     f_2 = twoSEq_1 * SEq_2 + twoSEq_3 * SEq_4 - a_y;
00119     f_3 = 1.0 - twoSEq_2 * SEq_2 - twoSEq_3 * SEq_3 - a_z;
00120     //J_11否定矩阵乘法.
00121     J_11or24 = twoSEq_3;
00122     J_12or23 = 2 * SEq_4;
00123     //J_12否定矩阵乘法
00124     J_13or22 = twoSEq_1;
00125     J_14or21 = twoSEq_2;
00126     //否定在矩阵乘法.
00127     J_32 = 2 * J_14or21;
00128     //否定在矩阵乘法.
00129     J_33 = 2 * J_11or24;
00130 
00131     //计算梯度（矩阵乘法）.
00132     nablaf_1 = J_14or21 * f_2 - J_11or24 * f_1;
00133     nablaf_2 = J_12or23 * f_1 + J_13or22 * f_2 - J_32 * f_3;
00134     nablaf_3 = J_12or23 * f_2 - J_33 * f_3 - J_13or22 * f_1;
00135     nablaf_4 = J_14or21 * f_1 + J_11or24 * f_2;
00136 
00137     //Normalise的渐变.00138     
          norm = sqrt(nablaf_1 * nablaf_1 + nablaf_2 * nablaf_2 + nablaf_3 * nablaf_3 + nablaf_4 * nablaf_4);
00139     nablaf_1 /= norm;
00140     nablaf_2 /= norm;
00141     nablaf_3 /= norm;
00142     nablaf_4 /= norm;
00143 
00144     //然后计算集成估计四元数率.
00145     SEq_1 += (SEqDot_omega_1 - (beta * nablaf_1)) * deltat;
00146     SEq_2 += (SEqDot_omega_2 - (beta * nablaf_2)) * deltat;
00147     SEq_3 += (SEqDot_omega_3 - (beta * nablaf_3)) * deltat;
00148     SEq_4 += (SEqDot_omega_4 - (beta * nablaf_4)) * deltat;
00149 
00150     //Normalise四元
00151     norm = sqrt(SEq_1 * SEq_1 + SEq_2 * SEq_2 + SEq_3 * SEq_3 + SEq_4 * SEq_4);
00152     SEq_1 /= norm;
00153     SEq_2 /= norm;
00154     SEq_3 /= norm;
00155     SEq_4 /= norm;
00156 
00157     if (firstUpdate == 0) {
00158         //商店方向的辅助框架
00159         AEq_1 = SEq_1;
00160         AEq_2 = SEq_2;
00161         AEq_3 = SEq_3;
00162         AEq_4 = SEq_4;
00163         firstUpdate = 1;
00164     }
00165 
00166 }
00167 //计算新的欧拉角
00168 void IMUfilter::computeEuler(void){
00169 
00170     //四元数描述的方向相对于地球的传感器.
00171     double ESq_1, ESq_2, ESq_3, ESq_4;
00172     //四元数描述的方向传感器的相对辅助帧。
00173     double ASq_1, ASq_2, ASq_3, ASq_4;    
00174                               
00175     //计算四元数的共轭.
00176     ESq_1 = SEq_1;
00177     ESq_2 = -SEq_2;
00178     ESq_3 = -SEq_3;
00179     ESq_4 = -SEq_4;
00180 
00181     //计算四元产品.
00182     ASq_1 = ESq_1 * AEq_1 - ESq_2 * AEq_2 - ESq_3 * AEq_3 - ESq_4 * AEq_4;
00183     ASq_2 = ESq_1 * AEq_2 + ESq_2 * AEq_1 + ESq_3 * AEq_4 - ESq_4 * AEq_3;
00184     ASq_3 = ESq_1 * AEq_3 - ESq_2 * AEq_4 + ESq_3 * AEq_1 + ESq_4 * AEq_2;
00185     ASq_4 = ESq_1 * AEq_4 + ESq_2 * AEq_3 - ESq_3 * AEq_2 + ESq_4 * AEq_1;
00186 
00187     //从四元数计算的欧拉角.
00188     phi = atan2(2 * ASq_3 * ASq_4 - 2 * ASq_1 * ASq_2, 2 * ASq_1 * ASq_1 + 2 * ASq_4 * ASq_4 - 1);
00189     theta = asin(2 * ASq_2 * ASq_3 - 2 * ASq_1 * ASq_3);
00190     psi = atan2(2 * ASq_2 * ASq_3 - 2 * ASq_1 * ASq_4, 2 * ASq_1 * ASq_1 + 2 * ASq_2 * ASq_2 - 1);
00191 
00192 }
00193 
00194 double IMUfilter::getRoll(void){
00196     return phi;
00198 }
00199 
00200 double IMUfilter::getPitch(void){
00201 
00202     return theta;
00204 }
00205 
00206 double IMUfilter::getYaw(void){
00208     return psi;
00210 }
00211 
00212 void IMUfilter::reset(void) {
00214     firstUpdate = 0;
00215 
00216     //地球辅助帧帧相对方向四元.
00217     AEq_1 = 1;
00218     AEq_2 = 0;
00219     AEq_3 = 0;
00220     AEq_4 = 0;
00221 
00222     //预计的方向与初始条件的四元要素.
00223     SEq_1 = 1;
00224     SEq_2 = 0;
00225     SEq_3 = 0;
00226     SEq_4 = 0;
00227 
00228 }
