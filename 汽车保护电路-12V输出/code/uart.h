#ifndef _UART_H__
#define _UART_H__
#include "main.h"

#define BYTE  unsigned char 
#define WORD  unsigned int 


void UartInit();
void SendData( BYTE dat );
void SendString( char *s );
void SendNum( BYTE dat );



#endif
