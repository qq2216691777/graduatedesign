#include "uart.h"

#define BYTE  unsigned char 
#define WORD  unsigned int 

void UartInit()
{
	TMOD &= 0X0F;
	TMOD |= 0X20;  //8位自动重装
	TH1 = 0xfd;
	TL1	= 0xfd;
	TR1 = 1;

	SM0 = 0;
	SM1 = 1;
	REN = 1;
	ES = 1;
	EA = 1;




}	


void  Uaty_Isr() interrupt 4 using 1
{
	if(RI)
	{
	 	RI = 0;
	}


}


void SendData( BYTE dat )
{
 	SBUF = dat;
	while(!TI);
	TI = 0;

}


void SendString( char *s )
{
 	while(*s)
	{
		SendData(*s++);
	}
}


void SendNum( BYTE dat )
{
 	SendData(dat/100+0x30);
	SendData(dat/10%10+0x30);
	SendData(dat%10+0x30);
	SendString("\r\n");
}
